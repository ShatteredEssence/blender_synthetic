import bpy
import json
from math import pi
from utils import blender_utils
from collections import OrderedDict


class BlenderCamera(object):
    def __init__(self, existing_camera=None, config=None):
        self.config = config

        if existing_camera:
            self.camera_object = existing_camera
        else:
            bpy.ops.object.camera_add(location=(0, 0, 0), rotation=(0, 0, 0))
            self.camera_object = bpy.data.objects['Camera']

        # set it as active camera in the scene
        bpy.data.scenes['Scene'].camera = self.camera_object

        if config:
            self.from_config(self.config)

    def from_config(self, camera_config):
        self.location = (camera_config['location_x'], camera_config['location_y'], camera_config['location_z'])
        self.rotation_quaternion = (camera_config['quaternion_w'], camera_config['quaternion_x'],
                                    camera_config['quaternion_y'], camera_config['quaternion_z'])

    @property
    def location(self):
        return self.camera_object.location

    @location.setter
    def location(self, values):
        self.camera_object.location = values

    @property
    def rotation_quaternion(self):
        bpy.context.object.rotation_mode = 'QUATERNION'
        return self.camera_object.rotation_quaternion

    @rotation_quaternion.setter
    def rotation_quaternion(self, values):
        bpy.context.object.rotation_mode = 'QUATERNION'
        self.camera_object.rotation_quaternion = values

    @property
    def rotation_euler(self):
        return self.camera_object.rotation_euler

    @rotation_euler.setter
    def rotation_euler(self, rotation_mode, values):
        bpy.context.object.rotation_mode = rotation_mode
        self.camera_object.rotation_euler = values

    def dump_meta(self):
        cam_loc, cam_rot, cam_scale = self.camera_object.matrix_world.decompose()
        fov = bpy.data.cameras[self.camera_object.name].angle * 180 / pi
        pitch = self.camera_object.rotation_quaternion[0]
        roll = self.camera_object.rotation_quaternion[1]
        yaw = self.camera_object.rotation_quaternion[2]
        return cam_loc, cam_rot, fov, pitch, roll, yaw

    def dump_properties(self, path, counter=0):
        blender_utils.makedirs(path / 'camera_meta')
        proporties_path = path / 'camera_meta' / 'frame_{:07}.json'.format(counter)
        cam_loc, cam_rot, fov, pitch, roll, yaw = self.dump_meta()
        data = OrderedDict()
        data['fov'] = fov
        data['location_x'] = cam_loc.x
        data['location_y'] = cam_loc.y
        data['location_z'] = cam_loc.z
        data['quaternion_w'] = cam_rot.w
        data['quaternion_x'] = cam_rot.x
        data['quaternion_y'] = cam_rot.y
        data['quaternion_z'] = cam_rot.z
        # data['gimbal_axis'] = dict(pitch=pitch-math.pi/2, roll=roll, yaw=yaw)
            # fov=fov,
            # location=dict(x=cam_loc.x, y=cam_loc.y, z=cam_loc.z),
            # quaternion=dict(x=cam_rot.x, y=cam_rot.y, z=cam_rot.z, w=cam_rot.w),
            # # gimbal_axis=dict(pitch=pitch-math.pi/2, roll=roll, yaw=yaw)
        # )
        with open(proporties_path, 'w') as file:
            json.dump(data, file, indent=4)
