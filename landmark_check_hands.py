import imageio
import json
import cv2
import matplotlib.pyplot as plt
import argparse
from pathlib import Path


RED = [255, 0, 0]
GREEN = [0, 255, 0]
BLUE = [0, 0, 255]

def random_color():
    levels = range(32, 256, 32)
    return tuple(random.choice(levels) for _ in range(3))

def main(frame_path):
    frame_data_raw = imageio.imread(frame_path)
    skeleton_2d_path = frame_path.replace('frame', 'skeleton_2d', 1).replace('.png', '.json')
    landmarks_path = frame_path.replace('frame', 'landmarks_hand', 1).replace('.png', '.csv')
    occluded = frame_path.replace('frame', 'occluded', 1).replace('.png', '.json')

    with open(skeleton_2d_path, 'r') as f:
        skeleton_2d_data = json.load(f)

    # with open(occluded, 'r') as o:
    #     occluded_data = json.load(o)['occluded']

    frame_data_show = frame_data_raw.copy()
    size_y, size_x = frame_data_show.shape[:2]
    blue = 100

    thumb = ['thumb01_R', 'thumb02_R', 'thumb03_R', (50, 100, 100)]
    index = ['index00_R', 'index01_R', 'index02_R', 'index03_R', (50, 100, 200)]
    middle = ['middle00_R', 'middle01_R', 'middle02_R', 'middle03_R', (100, 100, 50)]
    ring = ['ring00_R', 'ring01_R', 'ring02_R', 'ring03_R', (200, 100, 50)]
    pinky = ['pinky00_R', 'pinky01_R', 'pinky02_R', 'pinky03_R', (50, 100, 100)]

    color = BLUE
    for bone, co in skeleton_2d_data.items():
        for grp in [thumb, index, middle, ring, pinky]:
            if bone in grp:
                color = grp[-1]
        x, y = co['xh'], co['yh']
        x = int(x * size_x)
        y = int((1 - y) * size_y)
        cv2.circle(frame_data_show, (x, y), 4, color, -1)
        blue += 5

    # with open(landmarks_path, 'r') as f:
    #     for line in f.readlines():
    #         line = line.split('\t')
    #         i = int(line[0])
    #         x = int(float(line[1]))
    #         y = int(float(line[2]))
    #         color = RED if occluded_data[i] else GREEN
    #         cv2.circle(frame_data_show, (x, y), 1, color, -1)
    plt.imshow(frame_data_show)
    plt.show()


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--frame", required=True, help="Path to the frame")
    args = vars(ap.parse_args())
    frame = args['frame']

    main(frame)
