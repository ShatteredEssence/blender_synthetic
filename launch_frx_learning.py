import os
import shutil
from execution.tasks import selfie_runner_frx_learner
from utils.path_utils import build_seq_path
from pathlib import Path
from random import shuffle


### Dima
SLIDER_FOLDER = 'd://slava//dataset//to_go'
DATA_FOLDER = 'd://3d-generated-output//selfie_runner//2019.04.05//'

### Sergey
# SLIDER_FOLDER = 'D:/!work/synthetic_generators/blender_external_data/sliders_set4'
# DATA_FOLDER = 'D:/!work/lemezator/autorunner/selfie_runner'


def submit_tasks(leftovers=None):
    all_sliders = Path(SLIDER_FOLDER).iterdir()
    all_sliders = leftovers if leftovers else list(all_sliders)
    shuffle(all_sliders)
    i = 0
    for c in all_sliders:
        latent = c.parts[-1].split('.')[0]
        race = c.parts[-1].split('.')[1]
        sequence_path = build_seq_path('selfie_runner', c.parts[-1][:-5])
        # if not sequence_path in [str(i) for i in Path(DATA_FOLDER).iterdir()]:
        #     i += 1
        if latent in ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09']:
            i += 1
            # print(c, latent, race, sequence_path)
            selfie_runner_frx_learner.apply_async((), dict(
                sequence_path=str(sequence_path),
                blender_char_props=str(c),
                race=str(race)
            ))
    print('submitted tasks', i)


def delete_empty_dirs(DATA_FOLDER):
    for i, f in enumerate(Path(DATA_FOLDER).iterdir()):
        if f.stat().st_size == 0:
            shutil.rmtree(f)
            print('not finished', f)
            continue
        if not os.listdir(str(f)):
            os.rmdir(str(f))
            print('empty', f)


def diff():
    sequences = [i.name for i in Path(DATA_FOLDER).iterdir()]
    configs = [i.stem for i in Path(SLIDER_FOLDER).iterdir()]
    leftovers = set(configs) - set(sequences)
    leftovers = [Path(SLIDER_FOLDER, '{}.json'.format(i)) for i in leftovers]
    return leftovers


def prepare():
    for race in ['f_ca01', 'f_as01', 'f_af01', 'm_ca01', 'm_as01', 'm_af01']:
        all_sliders = Path(SLIDER_FOLDER).iterdir()
        counter = 1
        for sl in all_sliders:
            current_race = sl.parts[-1].split('.')[1]
            if current_race == race:
                old = sl
                new = Path(str(sl).replace('sliders', 'ss', 1))
                print(counter, old, new)
                # shutil.move(old, new)
                counter += 1
            if counter > 3000:
                return


if __name__ == "__main__":
    # delete_empty_dirs(DATA_FOLDER)
    # leftovers = diff()
    submit_tasks()
    # prepare()
