import bpy
import time
import datetime
import random
import logging
import root_logger
from math import pi
from utils import materials
from utils import enviroment
from utils import randomizer
from utils import blender_utils as utils
from config import cfg
from pathlib import Path


module_logger = logging.getLogger('selfie_runner')

MODULE_NAME = 'light'
# SEED = int(cfg['Python']['random_seed'])
hdri_path = Path(cfg['Paths']['hdri_path'])
data_path = Path(cfg['Paths']['data_path'])
output_path = Path(cfg['Paths']['output_path']) / MODULE_NAME

def add_particle_system(character, preset_name, name, vertex_group, mask, use_hair_dynamics):
    particle_systems_count = len(bpy.context.object.particle_systems)
    character.modifiers.new(name, type='PARTICLE_SYSTEM')
    bpy.context.object.particle_systems[particle_systems_count].name = name
    preset = bpy.data.particles[preset_name] 
    character.particle_systems[name].settings = preset
    bpy.context.object.particle_systems[name].vertex_group_density = vertex_group
    bpy.context.object.particle_systems[name].use_hair_dynamics = use_hair_dynamics
    preset.texture_slots[0].use_map_time = False
    preset.texture_slots[0].use_map_length = True
    preset.texture_slots[0].length_factor = 1.0
    preset.texture_slots[0].texture.image = bpy.data.images[mask]

def add_brows(character):
    add_particle_system(character, 'brows_preset', 'brows', 'brows', 'brows.png', False)

def get_light_positions():
    light_positions = bpy.context.scene.collection.children['light_positions']
    return [light.location for light in light_positions.objects]

def init_camera(armature, distance, vertical_offset):
    track_target = bpy.data.objects['track_target']
    rotation_target = bpy.data.objects['rotation_target']
    camera = bpy.data.objects["Camera"]
    armature.data.layers[0] = True
    armature.data.layers[1] = True
    utils.select(track_target)
    utils.select(armature)
    utils.set_active(armature)
    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='DESELECT')
    arm = bpy.data.armatures["MBLab_skeleton_base_ik"]
    arm.bones.active = arm.bones["head"]
    bpy.context.object.data.bones["head"].select = True
    head_loc = armature.location + bpy.context.active_pose_bone.head

    track_target.location = (head_loc[0], head_loc[1], head_loc[2] + vertical_offset)
    rotation_target.location = track_target.location
    camera.location = [0, -distance, 0]

    bpy.ops.pose.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')

def render(path):
    bpy.context.scene.render.image_settings.color_mode = 'RGB'
    utils.render_scene(path)

def get_random_light_color():
    h = random.uniform(0, 1)
    s = random.uniform(0, 0.4)
    v = random.uniform(0.8, 1)
    return h, s, v

def render_light(path, position):
    sun = bpy.data.objects['sun']
    sun_lamp = bpy.data.lamps['sun']

    sun.location = position
    delta_range = 0.2
    sun.location[0] += random.uniform(-delta_range, delta_range)
    sun.location[1] += random.uniform(-delta_range, delta_range)
    sun.location[2] += random.uniform(-delta_range, delta_range)

    sun_lamp.energy = random.uniform(1.0, 1.5)
    sun_lamp.color.hsv = get_random_light_color()

    render(path)

def render_pack(iteration, pack_path, gender, body_maps, vertex_groups_indices, light_positions):
    utils.gc()

    character, armature = utils.init_human(gender)
    randomizer.randomise_attr(character, 'character_age', -0.1, 0.25)
    randomizer.randomise_attr(character, 'character_tone', 0, 0.25)
    randomizer.randomise_initial_character(character)

    utils.create_vertex_groups(character, vertex_groups_indices)

    body_texture = random.choice(body_maps)
    materials.set_texture(str(body_texture), 'skin', 'SkinTexture')
    materials.set_texture(str(body_texture), 'inner_eyes', 'InnerEyeTexture')
    materials.set_character_materials(character)
    materials.add_material(character, bpy.data.materials.get('hair'))

    add_brows(character)
    bpy.data.particles['brows_preset'].material_slot = 'hair'
    
    init_camera(armature, 0.4, 0.04)

    enviroment.randomise_enviroment(hdri_path)

    bpy.data.lamps['sun'].energy = 0
    frame_out_path = pack_path / '{}.png'.format(iteration)
    render(frame_out_path)

    for index, position in enumerate(light_positions):
        frame_out_path = pack_path / '{}_{}.png'.format(iteration, index)
        render_light(frame_out_path, position)

    utils.deselect_all()
    utils.delete_object(armature)
    utils.delete_object(character)

def main():
    now = datetime.datetime.now()
    date = now.strftime("%Y.%m.%d")
    body_maps = materials.load_body_maps(data_path)
    vertex_groups_indices = [utils.load_vertex_group_indices(data_path, 'woman'), utils.load_vertex_group_indices(data_path, 'man')]
    light_positions = get_light_positions()
    
    for iteration in range(0, 10000):
        pack_path = output_path / date / '{:07}'.format(iteration)
        gender = random.choice([0, 1])
        render_pack(iteration, pack_path, gender, body_maps, vertex_groups_indices[gender], light_positions)

if __name__ == "__main__":
    main()