import bpy
from math import degrees
import time

scene = bpy.data.scenes["Scene"]

def dump_camera_meta(renderpath, dump_folder, counter):
    # save meta
    #select obj to generate meta
#    bpy.data.objects['Face_Object'].select_set(action='SELECT')
    outputCamera_meta = renderpath + dump_folder + 'frame' + '_{:07}'.format(counter) + '_camera_meta' + '.csv'
    			
    #Get camera object meta
    cam_loc, cam_rot, cam_scale = bpy.data.objects['Camera'].matrix_world.decompose()
    cam_x_rot = ("%.2f" %(bpy.data.objects['Camera'].rotation_euler[0]*180/3.14159265359))
    cam_y_rot = ("%.2f" %(bpy.data.objects['Camera'].rotation_euler[1]*180/3.14159265359))
    cam_z_rot = ("%.2f" %(bpy.data.objects['Camera'].rotation_euler[2]*180/3.14159265359))
    		
    		
    f = open(outputCamera_meta, 'w')

    f.write('Cam location: ' + str(cam_loc) + '\n')
    f.write('Cam quaternion: ' + str(cam_rot) + '\n')
    f.write('Cam radian : ' + str(bpy.data.objects['Camera'].rotation_euler) + '\n')
    f.write('Cam angles XYZ : ' + str(cam_x_rot) + ' ' + str(cam_y_rot)+ ' ' + str(cam_z_rot) + ' ' + '\n')
    f.write('Camera Fov: ' + (str("%.3f" % (((bpy.data.cameras["Camera"].angle) * 180) / 3.14159265359))))
    f.close()
    

def dump_head_meta(gender, renderpath, dump_folder, counter):
    bpy.ops.object.select_all(action='DESELECT')
    if gender == 0:
        armature = bpy.data.objects["f_ca01_skeleton"]
    if gender == 1:
        armature = bpy.data.objects["m_ca01_skeleton"]
        armature_name = 'm_ca01_skeleton'
        
    armature.data.layers[0] = True
    armature.data.layers[1] = True
    bpy.data.objects['head_meta_helper'].select = True
    armature.select = True
    bpy.context.scene.objects.active = armature
    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='DESELECT')
    arm = bpy.data.armatures["MBLab_skeleton_base_ik"]
    arm.bones.active = arm.bones["head"]
    bpy.context.object.data.bones["head"].select = True
    head_loc = bpy.data.objects[armature_name].location + bpy.context.active_pose_bone.head
    bpy.data.objects['head_meta_helper'].location = (head_loc[0], head_loc[1], head_loc[2])
    bpy.ops.pose.select_all(action='DESELECT')
    
#    bpy.data.objects['head_meta_helper'].select = True
#    armature.select = True
#    bpy.ops.object.parent_set(type='BONE')
    #    bpy.context.object.constraints["Track To"].target = bpy.data.objects[armature_name]
#    bpy.context.object.constraints["Track To"].subtarget = "IK_control_hd"
    
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    

    bpy.data.objects['head_meta_helper'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['head_meta_helper']
    
#    bpy.ops.object.constraint_add(type='TRACK_TO')
    bpy.data.objects['head_meta_helper'].constraints["Track To"].target = bpy.data.objects[armature_name]
    bpy.data.objects['head_meta_helper'].constraints["Track To"].subtarget = "IK_control_hd"
    
    scene.update()
    
    head_loc, head_rot, head_scale = bpy.data.objects['head_meta_helper'].matrix_world.decompose()
#    print(bpy.data.objects['head_meta_helper'].matrix_world.decompose())

#    bpy.data.objects['head_meta_helper'].constraints["Track To"].target = None
#    bpy.data.objects['head_meta_helper'].constraints["Track To"].subtarget = ''    

    outputHead_meta = renderpath + dump_folder + 'frame' + '_{:07}'.format(counter) + '_head_meta' + '.csv'

    f = open(outputHead_meta, 'w')

    f.write('Head location: ' + str(head_loc) + '\n')
    f.write('Head quaternion: ' + str(head_rot) + '\n')
    f.close()
    
#    head_loc, head_rot, head_scale = bpy.data.objects['head_meta_helper'].matrix_world.decompose()
#    print(bpy.data.objects['head_meta_helper'].matrix_world.decompose())    
         
#renderpath = 'D:/\!work/lemezator/autorunner_classed/seqence_000/'

#dump_head_meta(1, renderpath, 'head_meta/', 0)


