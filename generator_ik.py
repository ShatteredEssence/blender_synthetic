import bpy
import random
import math
import numpy as np
from bpy_extras.object_utils import world_to_camera_view
import datetime
import os

scene = bpy.data.scenes["Scene"]
pi = 3.14159265
r = pi / 180
ARMATURE = "human_skeleton"
prewarm_offset = 30
keyframecount = 5


def makedirs(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)

def makedirs_batch(render_path, dirs):
    for dir in dirs:
        newdir = render_path + dir
        if not os.path.exists(newdir):
            os.makedirs(newdir)

def init_human(gender):
    bpy.context.scene.mblab_use_lamps = False
    if gender == 0:
        bpy.context.scene.mblab_character_name = 'f_ca01'
    if gender == 1:
        bpy.context.scene.mblab_character_name = 'm_ca01'
    bpy.ops.mbast.init_character()


def gc():
    for datablock in bpy.data.cameras:
        if datablock.users == 0:
            bpy.data.cameras.remove(datablock)

    for datablock in bpy.data.scenes:
        if datablock.users == 0:
            bpy.data.scenes.remove(datablock)

    for datablock in bpy.data.node_groups:
        if datablock.users == 0:
            bpy.data.node_groups.remove(datablock)

    for datablock in bpy.data.lamps:
        if datablock.users == 0:
            bpy.data.lamps.remove(datablock)

    for datablock in bpy.data.window_managers:
        if datablock.users == 0:
            bpy.data.window_managers.remove(datablock)

    for datablock in bpy.data.textures:
        if datablock.users == 0:
            bpy.data.textures.remove(datablock)

    for datablock in bpy.data.worlds:
        if datablock.users == 0:
            bpy.data.worlds.remove(datablock)

    for datablock in bpy.data.particles:
        if datablock.users == 0:
            bpy.data.particles.remove(datablock)

    for datablock in bpy.data.cache_files:
        if datablock.users == 0:
            bpy.data.cache_files.remove(datablock)

    for datablock in bpy.data.objects:
        if datablock.users == 0:
            bpy.data.objects.remove(datablock)

    for datablock in bpy.data.meshes:
        if datablock.users == 0:
            bpy.data.meshes.remove(datablock)

    for datablock in bpy.data.materials:
        if datablock.users == 0:
            bpy.data.materials.remove(datablock)

    for datablock in bpy.data.images:
        if datablock.users == 0:
            bpy.data.images.remove(datablock)

    for datablock in bpy.data.armatures:
        if datablock.users == 0:
            bpy.data.armatures.remove(datablock)

    for datablock in bpy.data.actions:
        if datablock.users == 0:
            bpy.data.actions.remove(datablock)


def randomise_character(character, factor, renderpath, iteration):
    outputSliders_meta = renderpath + '/face_parameters' + '_{:07}'.format(iteration) + '.csv'
    sliders = open(outputSliders_meta, 'w')
    
    min = 0.4
    max = 0.6
    min = min / factor
    max = max * factor
    
    if min <= 0:
        min = 0
    if max >= 1:
        max = 1
    
    def randomise_attr(attr):
        if not hasattr(character, attr):
            raise Exception("Object doesnt have attribute '{}'".format(attr))
        setattr(character, attr, random.uniform(min, max))
        sliders.write('{}: {}'.format(attr, getattr(character, attr)) + '\n')

    randomise_attr('character_age')
    randomise_attr('character_mass')
    randomise_attr('character_tone')
    randomise_attr('Face_Ellipsoid')
    randomise_attr('Face_Parallelepiped')
    randomise_attr('Face_Triangle')
    randomise_attr('Eyes_BagProminence')
    randomise_attr('Eyes_BagSize')
    randomise_attr('Eyes_InnerPosX')
    randomise_attr('Eyes_InnerPosZ')
    randomise_attr('Eyes_IrisSize')
    randomise_attr('Eyes_OuterPosX')
    randomise_attr('Eyes_InnerPosZ')
    randomise_attr('Eyes_PosX')
    randomise_attr('Eyes_PosZ')
    randomise_attr('Eyes_Size')
    randomise_attr('Eyes_SizeZ')
    randomise_attr('Eyes_TypeAlmond')
    randomise_attr('Eyes_TypeHooded')
    randomise_attr('Eyes_innerSinus')
    randomise_attr('Cheeks_CreaseExt')
    randomise_attr('Cheeks_InfraVolume')
    randomise_attr('Cheeks_Mass')
    randomise_attr('Cheeks_SideCrease')
    randomise_attr('Cheeks_Tone')
    randomise_attr('Cheeks_Zygom')
    randomise_attr('Cheeks_ZygomPosZ')
    randomise_attr('Chin_Cleft')
    randomise_attr('Chin_Prominence')
    randomise_attr('Chin_SizeX')
    randomise_attr('Chin_SizeZ')
    randomise_attr('Chin_Tone')
    randomise_attr('Ears_Lobe')
    randomise_attr('Ears_LocY')
    randomise_attr('Ears_LocZ')
    randomise_attr('Ears_RotX')
    randomise_attr('Ears_Round')
    randomise_attr('Ears_SizeX')
    randomise_attr('Ears_SizeY')
    randomise_attr('Ears_SizeZ')
    randomise_attr('Eyebrows_Angle')
    randomise_attr('Eyebrows_Droop')
    randomise_attr('Eyebrows_PosZ')
    randomise_attr('Eyebrows_Ridge')
    randomise_attr('Eyebrows_SizeY')
    randomise_attr('Eyebrows_Tone')
    randomise_attr('Eyelids_Angle')
    randomise_attr('Eyelids_Crease')
    randomise_attr('Eyelids_InnerPosZ')
    randomise_attr('Eyelids_LowerCurve')
    randomise_attr('Eyelids_MiddlePosZ')
    randomise_attr('Eyelids_OuterPosZ')
    randomise_attr('Eyelids_SizeZ')
    randomise_attr('Forehead_Angle')
    randomise_attr('Forehead_Curve')
    randomise_attr('Forehead_SizeX')
    randomise_attr('Forehead_SizeZ')
    randomise_attr('Forehead_Temple')
    randomise_attr('Jaw_Angle')
    randomise_attr('Jaw_Angle2')
    randomise_attr('Jaw_LocY')
    randomise_attr('Jaw_Prominence')
    randomise_attr('Jaw_ScaleX')
    randomise_attr('Nose_BallSizeX')
    randomise_attr('Nose_BasePosZ')
    randomise_attr('Nose_BaseShape')
    randomise_attr('Nose_BaseSizeX')
    randomise_attr('Nose_BaseSizeZ')
    randomise_attr('Nose_BridgeSizeX')
    randomise_attr('Nose_Curve')
    randomise_attr('Nose_GlabellaPosZ')
    randomise_attr('Nose_GlabellaSizeX')
    randomise_attr('Nose_GlabellaSizeY')
    randomise_attr('Nose_NostrilCrease')
    randomise_attr('Nose_NostrilDiam')
    randomise_attr('Nose_NostrilPosZ')
    randomise_attr('Nose_NostrilSizeX')
    randomise_attr('Nose_NostrilSizeY')
    randomise_attr('Nose_PosY')
    randomise_attr('Nose_SeptumFlat')
    randomise_attr('Nose_SeptumRolled')
    randomise_attr('Nose_SizeY')
    randomise_attr('Nose_TipAngle')
    randomise_attr('Nose_TipPosZ')
    randomise_attr('Nose_TipSize')
    randomise_attr('Nose_WingAngle')
    randomise_attr('Nose_WingBackFlat')
    randomise_attr('Nose_WingBump')
    randomise_attr('Mouth_CornersPosZ')
    randomise_attr('Mouth_LowerlipExt')
    randomise_attr('Mouth_LowerlipSizeZ')
    randomise_attr('Mouth_LowerlipVolume')
    randomise_attr('Mouth_PhiltrumProminence')
    randomise_attr('Mouth_PhiltrumSizeX')
    randomise_attr('Mouth_PhiltrumSizeY')
    randomise_attr('Mouth_PosY')
    randomise_attr('Mouth_PosZ')
    randomise_attr('Mouth_Protusion')
    randomise_attr('Mouth_SideCrease')
    randomise_attr('Mouth_SizeX')
    randomise_attr('Mouth_UpperlipExt')
    randomise_attr('Mouth_UpperlipSizeZ')
    randomise_attr('Mouth_UpperlipVolume')
    bpy.context.scene.mblab_show_measures = False

def randomise_face_expression(character, factor, renderpath, iteration):
    outputSliders_meta = renderpath + '/expression_parameters' + '_{:07}'.format(iteration) + '.csv'
    sliders = open(outputSliders_meta, 'w')
    
    min = 0.4
    max = 0.6
    min = min / factor
    max = max * factor
    
    if min <= 0:
        min = 0
    if max >= 1:
        max = 1
    
    def randomise_attr(attr):
        if not hasattr(character, attr):
            raise Exception("Object doesnt have attribute '{}'".format(attr))
        setattr(character, attr, random.uniform(min, max))
        sliders.write('{}: {}'.format(attr, getattr(character, attr)) + '\n')

    randomise_attr('Expressions_browOutVertL')
    randomise_attr('Expressions_browOutVertR')
    randomise_attr('Expressions_browSqueezeL')
    randomise_attr('Expressions_browSqueezeR')
    randomise_attr('Expressions_browsMidVert')
    randomise_attr('Expressions_cheekSneerL')
    randomise_attr('Expressions_cheekSneerR')
    randomise_attr('Expressions_chestExpansion')
    randomise_attr('Expressions_deglutition')
    randomise_attr('Expressions_eyeClosedL')
    randomise_attr('Expressions_eyeClosedPressureL')
    randomise_attr('Expressions_eyeClosedPressureR')
    randomise_attr('Expressions_eyeClosedR')
    randomise_attr('Expressions_eyeSquintL')
    randomise_attr('Expressions_eyeSquintR')
    randomise_attr('Expressions_eyesHoriz')
    randomise_attr('Expressions_eyesVert')
    randomise_attr('Expressions_jawHoriz')
    randomise_attr('Expressions_jawOut')
    randomise_attr('Expressions_mouthBite')
    randomise_attr('Expressions_mouthChew')
    randomise_attr('Expressions_mouthClosed')
    randomise_attr('Expressions_mouthHoriz')
    randomise_attr('Expressions_mouthInflated')
    randomise_attr('Expressions_mouthLowerOut')
    randomise_attr('Expressions_mouthOpen')
    randomise_attr('Expressions_mouthOpenAggr')
    randomise_attr('Expressions_mouthOpenHalf')
    randomise_attr('Expressions_mouthOpenLarge')
    randomise_attr('Expressions_mouthOpenO')
    randomise_attr('Expressions_mouthOpenTeethClosed')
    randomise_attr('Expressions_mouthSmile')
    randomise_attr('Expressions_mouthSmileL')
    randomise_attr('Expressions_mouthSmileOpen')
    randomise_attr('Expressions_mouthSmileOpen2')
    randomise_attr('Expressions_mouthSmileR')
    randomise_attr('Expressions_nostrilsExpansion')
    randomise_attr('Expressions_pupilsDilatation')

    bpy.context.scene.mblab_show_measures = False

def deep_randomise_character(character, factor):
    min = 0.4
    max = 0.6
    min = min / factor
    max = max * factor
    #   v = (max - min) / 2
    #   m = min + v
    #   min = m - v * factor
    #   max = m + v * factor

    if min <= 0:
        min = 0
    if max >= 1:
        max = 1
    
    #general
    character_age = random.uniform(-0.5, 0.5)
    character.character_age = character_age
    
    character_mass = random.uniform(-0.2, 0.2)
    character.character_mass = character_mass
    
    character_tone = random.uniform(-0.2, 0.2)
    character.character_tone = character_tone
    
    character_mass = random.uniform(min, max)
    character.character_mass = character_mass
    
    character_tone = random.uniform(min, max)
    character.character_tone = character_tone
    # face form
    Face_Ellipsoid = random.uniform(min, max)
    character.Face_Ellipsoid = Face_Ellipsoid
    
    Face_Parallelepiped = random.uniform(max, min)
    character.Face_Parallelepiped = Face_Parallelepiped
    
    Face_Triangle = random.uniform(min, max)
    character.Face_Triangle = Face_Triangle
    # eyes form
    Eyes_BagProminence = random.uniform(min, max)
    character.Eyes_BagProminence = Eyes_BagProminence
    
    Eyes_BagSize = random.uniform(min, max)
    character.Eyes_BagSize = Eyes_BagSize
    
    Eyes_InnerPosX = random.uniform(min, max)
    character.Eyes_InnerPosX = Eyes_InnerPosX
    
    Eyes_InnerPosZ = random.uniform(min, max)
    character.Eyes_InnerPosZ = Eyes_InnerPosZ
    
    Eyes_IrisSize = random.uniform(min, max)
    character.Eyes_IrisSize = Eyes_IrisSize
    
    Eyes_OuterPosX = random.uniform(min, max)
    character.Eyes_OuterPosX = Eyes_OuterPosX
    
    Eyes_InnerPosZ = random.uniform(min, max)
    character.Eyes_InnerPosZ = Eyes_InnerPosZ
    
    Eyes_PosX = random.uniform(min, max)
    character.Eyes_PosX = Eyes_PosX
    
    Eyes_PosZ = random.uniform(min, max)
    character.Eyes_PosZ = Eyes_PosZ
    
    Eyes_Size = random.uniform(min, max)
    character.Eyes_Size = Eyes_Size
    
    Eyes_SizeZ = random.uniform(min, max)
    character.Eyes_SizeZ = Eyes_SizeZ
    
    Eyes_TypeAlmond = random.uniform(min, max)
    character.Eyes_TypeAlmond = Eyes_TypeAlmond
    
    Eyes_TypeHooded = random.uniform(min, max)
    character.Eyes_TypeHooded = Eyes_TypeHooded
    
    Eyes_innerSinus = random.uniform(min, max)
    character.Eyes_innerSinus = Eyes_innerSinus
    # cheeks form
    Cheeks_CreaseExt = random.uniform(min, max)
    character.Cheeks_CreaseExt = Cheeks_CreaseExt
    
    Cheeks_InfraVolume = random.uniform(min, max)
    character.Cheeks_InfraVolume = Cheeks_InfraVolume
    
    Cheeks_Mass = random.uniform(min, max)
    character.Cheeks_Mass = Cheeks_Mass
    
    Cheeks_SideCrease = random.uniform(min, max)
    character.Cheeks_SideCrease = Cheeks_SideCrease
    
    Cheeks_Tone = random.uniform(min, max)
    character.Cheeks_Tone = Cheeks_Tone
    
    Cheeks_Zygom = random.uniform(min, max)
    character.Cheeks_Zygom = Cheeks_Zygom
    
    Cheeks_ZygomPosZ = random.uniform(min, max)
    character.Cheeks_ZygomPosZ = Cheeks_ZygomPosZ
    # chin
    Chin_Cleft = random.uniform(min, max)
    character.Chin_Cleft = Chin_Cleft
    
    Chin_Prominence = random.uniform(min, max)
    character.Chin_Prominence = Chin_Prominence
    
    Chin_SizeX = random.uniform(min, max)
    character.Chin_SizeX = Chin_SizeX
    
    Chin_SizeZ = random.uniform(min, max)
    character.Chin_SizeZ = Chin_SizeZ
    
    Chin_Tone = random.uniform(min, max)
    character.Chin_Tone = Chin_Tone
    # ears
    Ears_Lobe = random.uniform(min, max)
    character.Ears_Lobe = Ears_Lobe
    
    Ears_LocY = random.uniform(min, max)
    character.Ears_LocY = Ears_LocY
    
    Ears_LocZ = random.uniform(min, max)
    character.Ears_LocZ = Ears_LocZ
    
    Ears_RotX = random.uniform(min, max)
    character.Ears_RotX = Ears_RotX
    
    Ears_Round = random.uniform(min, max)
    character.Ears_Round = Ears_Round
    
    Ears_SizeX = random.uniform(min, max)
    character.Ears_SizeX = Ears_SizeX
    
    Ears_SizeY = random.uniform(min, max)
    character.Ears_SizeY = Ears_SizeY
    
    Ears_SizeZ = random.uniform(min, max)
    character.Ears_SizeZ = Ears_SizeZ
    
#    Fantasy_EarsPointRotation = random.uniform(min, max)
#    character.Fantasy_EarsPointRotation = random.uniform(min, max)

    # eyebrows
    Eyebrows_Angle = random.uniform(min, max)
    character.Eyebrows_Angle = Eyebrows_Angle
    
    Eyebrows_Droop = random.uniform(min, max)
    character.Eyebrows_Droop = Eyebrows_Droop
    
    Eyebrows_PosZ = random.uniform(min, max)
    character.Eyebrows_PosZ = Eyebrows_PosZ
    
    Eyebrows_Ridge = random.uniform(min, max)
    character.Eyebrows_Ridge = Eyebrows_Ridge
    
    Eyebrows_SizeY = random.uniform(min, max)
    character.Eyebrows_SizeY = Eyebrows_SizeY
    
    Eyebrows_Tone = random.uniform(min, max)
    character.Eyebrows_Tone = Eyebrows_Tone
    # eyelids
    Eyelids_Angle = random.uniform(min, max)
    character.Eyelids_Angle = Eyelids_Angle
    
    Eyelids_Crease = random.uniform(min, max)
    character.Eyelids_Crease = Eyelids_Crease
    
    Eyelids_InnerPosZ = random.uniform(min, max)
    character.Eyelids_InnerPosZ = Eyelids_InnerPosZ
    
    Eyelids_LowerCurve = random.uniform(min, max)
    character.Eyelids_LowerCurve = Eyelids_LowerCurve
    
    Eyelids_MiddlePosZ = random.uniform(min, max)
    character.Eyelids_MiddlePosZ = Eyelids_MiddlePosZ
    
    Eyelids_OuterPosZ = random.uniform(min, max)
    character.Eyelids_OuterPosZ = Eyelids_OuterPosZ
    
    Eyelids_SizeZ = random.uniform(min, max)
    character.Eyelids_SizeZ = Eyelids_SizeZ
    # forehead
    Forehead_Angle = random.uniform(min, max)
    character.Forehead_Angle = Forehead_Angle
    
    Forehead_Curve = random.uniform(min, max)
    character.Forehead_Curve = Forehead_Curve
    
    Forehead_SizeX = random.uniform(min, max)
    character.Forehead_SizeX = Forehead_SizeX
    
    Forehead_SizeZ = random.uniform(min, max)
    character.Forehead_SizeZ = Forehead_SizeZ
    
    Forehead_Temple = random.uniform(min, max)
    character.Forehead_Temple = Forehead_Temple
    # jaw
    Jaw_Angle = random.uniform(min, max)
    character.Jaw_Angle = Jaw_Angle
    
    Jaw_Angle2 = random.uniform(min, max)
    character.Jaw_Angle2 = Jaw_Angle2
    
    Jaw_LocY = random.uniform(min, max)
    character.Jaw_LocY = Jaw_LocY
    
    Jaw_Prominence = random.uniform(min, max)
    character.Jaw_Prominence = Jaw_Prominence
    
    Jaw_ScaleX = random.uniform(min, max)
    character.Jaw_ScaleX = Jaw_ScaleX
    # nose
    Nose_BallSizeX = random.uniform(min, max)
    character.Nose_BallSizeX = Nose_BallSizeX
    
    Nose_BasePosZ = random.uniform(min, max)
    character.Nose_BasePosZ = Nose_BasePosZ
    
    Nose_BaseShape = random.uniform(min, max)
    character.Nose_BaseShape = Nose_BaseShape
    
    Nose_BaseSizeX = random.uniform(min, max)
    character.Nose_BaseSizeX = Nose_BaseSizeX
    
    Nose_BaseSizeZ = random.uniform(min, max)
    character.Nose_BaseSizeZ = Nose_BaseSizeZ
    
    Nose_BridgeSizeX = random.uniform(min, max)
    character.Nose_BridgeSizeX = Nose_BridgeSizeX
    
    Nose_Curve = random.uniform(min, max)
    character.Nose_Curve = Nose_Curve
    
    Nose_GlabellaPosZ = random.uniform(min, max)
    character.Nose_GlabellaPosZ = Nose_GlabellaPosZ
    
    Nose_GlabellaSizeX = random.uniform(min, max)
    character.Nose_GlabellaSizeX = Nose_GlabellaSizeX
    
    Nose_GlabellaSizeY = random.uniform(min, max)
    character.Nose_GlabellaSizeY = Nose_GlabellaSizeY
    
    Nose_NostrilCrease = random.uniform(min, max)
    character.Nose_NostrilCrease = Nose_NostrilCrease
    
    Nose_NostrilDiam = random.uniform(min, max)
    character.Nose_NostrilDiam = Nose_NostrilDiam
    
    Nose_NostrilPosZ = random.uniform(min, max)
    character.Nose_NostrilPosZ = Nose_NostrilPosZ
    
    Nose_NostrilSizeX = random.uniform(min, max)
    character.Nose_NostrilSizeX = Nose_NostrilSizeX
    
    Nose_NostrilSizeY = random.uniform(min, max)
    character.Nose_NostrilSizeY = Nose_NostrilSizeY
    
    Nose_PosY = random.uniform(min, max)
    character.Nose_PosY = Nose_PosY
    
    Nose_SeptumFlat = random.uniform(min, max)
    character.Nose_SeptumFlat = Nose_SeptumFlat
    
    Nose_SeptumRolled = random.uniform(min, max)
    character.Nose_SeptumRolled = Nose_SeptumRolled
    
    Nose_SizeY = random.uniform(min, max)
    character.Nose_SizeY = Nose_SizeY
    
    Nose_TipAngle = random.uniform(min, max)
    character.Nose_TipAngle = Nose_TipAngle
    
    Nose_TipPosY = random.uniform(min, max)
    character.Nose_TipPosY = Nose_TipPosY
    
    Nose_TipPosZ = random.uniform(min, max)
    character.Nose_TipPosZ = Nose_TipPosZ
    
    Nose_TipSize = random.uniform(min, max)
    character.Nose_TipSize = Nose_TipSize
    
    Nose_WingAngle = random.uniform(min, max)
    character.Nose_WingAngle = Nose_WingAngle
    
    Nose_WingBackFlat = random.uniform(min, max)
    character.Nose_WingBackFlat = Nose_WingBackFlat
    
    Nose_WingBump = random.uniform(min, max)
    character.Nose_WingBump = Nose_WingBump
    
    # mouth
    Mouth_CornersPosZ = random.uniform(min, max)
    character.Mouth_CornersPosZ = Mouth_CornersPosZ
    
    Mouth_LowerlipExt = random.uniform(min, max)
    character.Mouth_LowerlipExt = Mouth_LowerlipExt
    
    Mouth_LowerlipSizeZ = random.uniform(min, max)
    character.Mouth_LowerlipSizeZ = Mouth_LowerlipSizeZ
    
    Mouth_LowerlipVolume = random.uniform(min, max)
    character.Mouth_LowerlipVolume = Mouth_LowerlipVolume
    
    Mouth_PhiltrumProminence = random.uniform(min, max)
    character.Mouth_PhiltrumProminence = Mouth_PhiltrumProminence
    
    Mouth_PhiltrumSizeX = random.uniform(min, max)
    character.Mouth_PhiltrumSizeX = Mouth_PhiltrumSizeX
    
    Mouth_PhiltrumSizeY = random.uniform(min, max)
    character.Mouth_PhiltrumSizeY = Mouth_PhiltrumSizeY
    
    Mouth_PosY = random.uniform(min, max)
    character.Mouth_PosY = Mouth_PosY
    
    Mouth_PosZ = random.uniform(min, max)
    character.Mouth_PosZ = Mouth_PosZ
    
    Mouth_Protusion = random.uniform(min, max)
    character.Mouth_Protusion = Mouth_Protusion
    
    Mouth_SideCrease = random.uniform(min, max)
    character.Mouth_SideCrease = Mouth_SideCrease
    
    Mouth_SizeX = random.uniform(min, max)
    character.Mouth_SizeX = Mouth_SizeX
    
    Mouth_UpperlipExt = random.uniform(min, max)
    character.Mouth_UpperlipExt = Mouth_UpperlipExt
    
    Mouth_UpperlipSizeZ = random.uniform(min, max)
    character.Mouth_UpperlipSizeZ = Mouth_UpperlipSizeZ
    
    Mouth_UpperlipVolume = random.uniform(min, max)
    character.Mouth_UpperlipVolume = Mouth_UpperlipVolume
    
    # DONT FORGET TO UPDATE MORPHS!!
    bpy.ops.object.select_all(action='DESELECT')
    character.select = True
    bpy.context.scene.mblab_show_measures = False
    bpy.ops.object.select_all(action='DESELECT')
    # DONT FORGET TO UPDATE MORPHS!!

def randomise_eyes(character):
    character.eyes_hue = random.uniform(0, 1)
    character.eyes_saturation = random.uniform(0, 0.7)
    character.eyes_value = random.uniform(0.4, 0.7)
    bpy.ops.object.select_all(action='DESELECT')
    

def randomise_skin(character):
    character.skin_hue = random.uniform(0.49, 0.51)
    character.skin_saturation = random.uniform(0.85, 1)
    character.skin_value = random.uniform(0.1, 1)
    bpy.ops.object.select_all(action='DESELECT')


def set_mask_material(character, material):
    mat = material  
    bpy.ops.object.select_all(action='DESELECT')
    character.select = True
    bpy.context.scene.objects.active = character
    ob = bpy.context.active_object
    bpy.ops.object.material_slot_remove()
    bpy.ops.object.material_slot_remove()
    bpy.ops.object.material_slot_remove()
    bpy.ops.object.material_slot_remove()
    bpy.ops.object.material_slot_remove()
    bpy.ops.object.material_slot_remove()
    bpy.ops.object.material_slot_remove()
    bpy.ops.object.material_slot_remove()
    if ob.data.materials:
        # assign to 1st material slot
        1/0
        ob.data.materials[0] = mat
        bpy.ops.object.select_all(action='DESELECT')
    else:
        # no slots
        print('Setting material: ', material, 'to object: ', character)
        ob.data.materials.append(mat)
        bpy.ops.object.select_all(action='DESELECT')
        

#REMOVE material = bpy.data.materials.get("male_eyes_class")
#REMOVE character = bpy.context.scene.objects["m_ca01"]
#REMOVE set_mask_material(character, material)


def set_dress_rgb(gender, dress):
    print('CHARACTER!!!', gender)
    bpy.ops.object.select_all(action='DESELECT')
    if gender == 0:
        mat = bpy.data.materials.get("Cloth_F")
    if gender == 1:
        mat = bpy.data.materials.get("Cloth_M")

    dress.select = True
    bpy.context.scene.objects.active = dress
    ob = bpy.context.active_object
    bpy.ops.object.material_slot_remove()
    bpy.ops.object.material_slot_remove()
    print('removing mat')
    if ob.data.materials:
        # assign to 1st material slot
        print('setting mat')
        ob.data.materials[0] = mat
        bpy.ops.object.select_all(action='DESELECT')
    else:
        # no slots
        print('setting mat')
        ob.data.materials.append(mat)
        bpy.ops.object.select_all(action='DESELECT')



def hair_class_set(gender, has_hair, has_beard, has_mustache):
    bpy.ops.object.select_all(action='DESELECT')

    if gender == 0:
        human = bpy.data.objects["f_ca01"]
        bpy.context.scene.objects.active = human
        human.select = True
    if gender == 1:
        human = bpy.data.objects["m_ca01"]
        bpy.context.scene.objects.active = human
        human.select = True

    mat = bpy.data.materials.get("hair_class_mat")
    ob = bpy.context.active_object
    ob.data.materials.append(mat)
    if has_hair == 1:        
        bpy.data.particles["Hair_01_Preset"].material_slot = 'hair_class_mat'
    if has_beard == 1:
        bpy.data.particles["beard_preset"].material_slot = 'hair_class_mat'
    if has_mustache == 1:
        bpy.data.particles["mustache_preset"].material_slot = 'hair_class_mat'



def setdressclass(dress):
    print('DRESS', dress)
    dressclass = bpy.data.materials.get("dressclass")
    bpy.ops.object.select_all(action='DESELECT')
    dress.select = True
    bpy.context.scene.objects.active = dress
    ob = bpy.context.active_object
    bpy.ops.object.material_slot_remove()

    if ob.data.materials:
        # assign to 1st material slot
        print('setting mat')
        ob.data.materials[0] = dressclass
        bpy.ops.object.select_all(action='DESELECT')
    else:
        # no slots
        print('setting mat')
        ob.data.materials.append(dressclass)
        bpy.ops.object.select_all(action='DESELECT')


def generate_trajectory(gender, bone, angle_x1, angle_x2, angle_y1, angle_y2, angle_z1, angle_z2):
    if gender == 0:
        #       action = bpy.data.actions["f_ca01_skeletonAction"]
        skeleton = bpy.data.objects["f_ca01_skeleton"]
    if gender == 1:
        #       action = bpy.data.actions["m_ca01_skeletonAction"]
        skeleton = bpy.data.objects["m_ca01_skeleton"]

    bpy.ops.object.select_all(action='DESELECT')

    skeleton.select = True
    bpy.context.scene.objects.active = skeleton

    #   action=bpy.data.actions["m_ca01_skeletonAction"]
    # KEYFRAME ON BONE
    bpy.ops.object.mode_set(mode='POSE')
    thebone = bpy.context.object.pose.bones[bone]
    thebone.rotation_mode = 'XYZ'

    print(thebone)

    thebone.rotation_euler = (0.0, 0.0, 0.0)
    thebone.rotation_euler = (random.uniform(angle_x1 * r, angle_x2 * r), random.uniform(angle_y1 * r, angle_y2 * r),
                              random.uniform(angle_z1 * r, angle_z2 * r))
    thebone.keyframe_insert(data_path='rotation_euler', frame=0 + prewarm_offset)
    thebone.rotation_euler = (random.uniform(angle_x1 * r, angle_x2 * r), random.uniform(angle_y1 * r, angle_y2 * r),
                              random.uniform(angle_z1 * r, angle_z2 * r))
    thebone.keyframe_insert(data_path='rotation_euler', frame=random.randint(20, 40) + prewarm_offset)
    thebone.rotation_euler = (random.uniform(angle_x1 * r, angle_x2 * r), random.uniform(angle_y1 * r, angle_y2 * r),
                              random.uniform(angle_z1 * r, angle_z2 * r))
    thebone.keyframe_insert(data_path='rotation_euler', frame=random.randint(70, 100))
    thebone.rotation_euler = (random.uniform(angle_x1 * r, angle_x2 * r), random.uniform(angle_y1 * r, angle_y2 * r),
                              random.uniform(angle_z1 * r, angle_z2 * r))
    thebone.keyframe_insert(data_path='rotation_euler', frame=130 + prewarm_offset)
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')


def generate_trajectory_fk_f_b(gender):
    if gender == 0:
        print('no woman yet')
    if gender == 1:
        armature_name = 'm_ca01_skeleton'
        armature = bpy.data.objects["m_ca01_skeleton"]
        
        #move
    in_or_out = 0# random.randint(0, 1)

    if in_or_out == 0:
        print('zoom out animation')
        armature.select = True
        bpy.context.scene.objects.active = armature
        bpy.context.object.data.layers[0] = True
        bpy.context.object.data.layers[1] = True
        bpy.ops.object.mode_set(mode='POSE')
        bpy.ops.pose.select_all(action='DESELECT')
        thebone = bpy.data.objects['m_ca01_skeleton'].pose.bones["IK_control_a_R"]
        thebone.rotation_mode = 'XYZ'
        thebone.keyframe_insert(data_path='location', frame=30)
        
        bpy.data.objects[armature_name].data.bones.active = bpy.data.objects[armature_name].pose.bones['head'].bone
        head_tail_loc = bpy.data.objects[armature_name].location + bpy.context.active_pose_bone.tail
        
        bpy.data.objects[armature_name].data.bones.active = bpy.data.objects[armature_name].pose.bones['IK_control_a_R'].bone
        hand_tail_loc = bpy.data.objects[armature_name].location + bpy.context.active_pose_bone.tail
        
        
        distance = np.sqrt(np.sum((np.array(hand_tail_loc) - np.array(head_tail_loc)) ** 2))
        print(hand_tail_loc)
        print(head_tail_loc)
        print(distance)
        
        bpy.ops.pose.select_all(action='DESELECT')
        
        correction = random.uniform(0.14, 0.2)
        
        bpy.data.objects[armature_name].data.bones.active = bpy.data.objects[armature_name].pose.bones['IK_control_a_R'].bone
        bpy.ops.transform.translate(value=(-distance/10, distance - correction, 0), constraint_axis=(True, True, False), constraint_orientation='LOCAL')
        thebone.keyframe_insert(data_path='location', frame=60)
        
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')



        
        

        print('done')

    
#    max_distance
#    min_distance
    
#    bpy.ops.object.mode_set(mode='POSE')
#    bpy.data.objects[armature_name].data.bones.active = bpy.data.objects[armature_name].pose.bones['head'].bone
#    head_tail_loc = bpy.data.objects[armature_name].location + bpy.context.active_pose_bone.tail
#
#    bpy.ops.pose.select_all(action='DESELECT')
#    
#    bpy.data.objects[armature_name].data.bones.active = bpy.data.objects[armature_name].pose.bones['IK_control_a_R'].bone
#    hand_tail_loc = bpy.data.objects[armature_name].location + bpy.context.active_pose_bone.tail
#    bpy.data.objects[armature_name].data.bones.active = bpy.data.objects[armature_name].pose.bones['IK_control_a_R'].bone
    

        
    

    
    
    

     
    
    


def init_cam(gender):
    bpy.ops.object.select_all(action='DESELECT')
    camera = bpy.data.objects["Camera"]

    if gender == 0:
#       camera.select = True
        camera.location = (random.uniform(-0.659517, -0.613215), random.uniform(-0.179576, -0.179747), random.uniform(1.19085, 1.12073))
        camera.rotation_euler = (118 * pi / 180, 0.001 * pi / 180, -88 * pi / 180)
        skeleton = bpy.data.objects["f_ca01_skeleton"]
        camera.select = False
    if gender == 1:
#       camera.select = True
#       bpy.context.scene.objects.active = camera
#       bpy.context.space_data.transform_orientation = 'LOCAL'
#       camera.location = (-0.572789, -0.189096, 1.22638)
        skeleton = bpy.data.objects["m_ca01_skeleton"]
        armature_name = 'm_ca01_skeleton'
#       camera.select = False

    skeleton.select = True
    bpy.context.scene.objects.active = skeleton
    
    
    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='DESELECT')
    thebone = bpy.data.objects['m_ca01_skeleton'].pose.bones["IK_control_a_R"]
    thebone.rotation_mode = 'XYZ'
    
    bpy.data.objects[armature_name].data.bones.active = bpy.data.objects[armature_name].pose.bones['IK_control_a_R'].bone
    hand_head_loc = bpy.data.objects[armature_name].location + bpy.context.active_pose_bone.head
    
    camera_new_loc = bpy.data.objects[armature_name].location + camera.location
    camera.location = hand_head_loc
#    
    
    bpy.ops.pose.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    

    print(hand_head_loc)


    camera.select = True
    skeleton.select = True
    bpy.context.scene.objects.active = skeleton
    bpy.ops.object.mode_set(mode='POSE')
    #   bpy.ops.object.select_all(action='DESELECT')
    skel = bpy.data.armatures['MBLab_skeleton_base_ik']
    skel.bones.active = skel.bones["IK_control_a_R"]
    bpy.context.object.data.bones["IK_control_a_R"].select = True
    bpy.ops.object.parent_set(type='BONE')
    camera.location = (-0.865081, -0.106066 ,1.43509)
    bpy.ops.object.mode_set(mode='OBJECT')
#   camera.rotation_euler = (129 * pi / 180, 0.002 * pi / 180, -80 * pi / 180)
    bpy.ops.object.select_all(action='DESELECT')
    





def set_cam(gender):
    
    if gender == 0:
        print('no woman yet')
        pass
    if gender == 1:
        skeleton = bpy.data.objects["m_ca01_skeleton"]
    
    skeleton.select = True
    bpy.context.scene.objects.active = skeleton
    bpy.ops.object.mode_set(mode='POSE')
    thebone = bpy.context.object.pose.bones['IK_control_a_R']
    the_hand = bpy.context.object.pose.bones['IK_control_h_R'] 
    thebone.rotation_mode = 'XYZ'
    the_hand.rotation_mode = 'XYZ'

    the_hand.rotation_euler = (90 * r, -60 * r, 0)

    bpy.ops.pose.select_all(action='DESELECT')

    bpy.data.objects['m_ca01_skeleton'].data.bones.active = bpy.data.objects['m_ca01_skeleton'].pose.bones['IK_control_a_R'].bone
    bpy.ops.pose.constraint_add(type='TRACK_TO')
    bpy.context.object.pose.bones["IK_control_a_R"].constraints["Track To"].target = bpy.data.objects["track_target"]
    bpy.ops.pose.select_all(action='DESELECT')

    bpy.data.objects['m_ca01_skeleton'].pose.bones['IK_control_a_R'].location = (random.uniform(0.575, 0.63), random.uniform(0.42, 0.46), random.uniform(0.385824, 0.428699))
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    


def init_mouthcam(gender):
    bpy.ops.object.select_all(action='DESELECT')
    camera = bpy.data.objects["Camera"]

    if gender == 0:
        camera.location = (0.0, -0.3, 1.66437)
        camera.rotation_euler = (90 * pi / 180, 0 * pi / 180, 180 * pi / 180)
    if gender == 1:
        camera.location = (0.0, -0.3, 1.66437)
        camera.rotation_euler = (90 * pi / 180, 0 * pi / 180, 180 * pi / 180)


def track_to_head(gender):
    bpy.ops.object.select_all(action='DESELECT')
    if gender == 0:
        armature = bpy.data.objects["f_ca01_skeleton"]
    if gender == 1:
        armature = bpy.data.objects["m_ca01_skeleton"]
        armature_name = 'm_ca01_skeleton'
        
    armature.data.layers[0] = True
    armature.data.layers[1] = True
    bpy.data.objects['track_target'].select = True
    armature.select = True
    bpy.context.scene.objects.active = armature
    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='DESELECT')
    arm = bpy.data.armatures["MBLab_skeleton_base_ik"]
    arm.bones.active = arm.bones["head"]
    bpy.context.object.data.bones["head"].select = True
    head_loc = bpy.data.objects[armature_name].location + bpy.context.active_pose_bone.head
    bpy.data.objects['track_target'].location = (head_loc[0], head_loc[1], head_loc[2] + 0.04)
    bpy.ops.pose.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    

def init_track_tomouth(gender):
    bpy.ops.object.select_all(action='DESELECT')
    track = bpy.data.objects['track_target']

    if gender == 0:
        track.location = (0.0, -0.150923, 1.66437)
    if gender == 1:
        track.location = (0.0, -0.150923, 1.66437)


        # randomise dress color


def randomise_dresscolor(gender, dress):
    if gender == 0:
        mat = bpy.data.materials.get("Cloth_F")
    if gender == 1:
        mat = bpy.data.materials.get("Cloth_M")

    dress.select = True
    bpy.context.scene.objects.active = dress
    nodes = mat.node_tree.nodes
    cloth_variance = nodes.get("Hue Saturation Value")
    # COLOR
    cloth_variance.inputs[0].default_value = random.uniform(0.2, 0.8)
    cloth_variance.inputs[1].default_value = random.uniform(1, 2)
    cloth_variance.inputs[2].default_value = random.uniform(0.6, 1.5)
    # Texture offset
    mapping = nodes.get("Mapping")
    mapping.translation[0] = random.uniform(0, 1)
    mapping.translation[1] = random.uniform(0, 1)
    mapping.translation[2] = random.uniform(0, 1)
    mapping.rotation[0] = random.uniform(-45 * r, 45 * r)
    mapping.rotation[1] = random.uniform(-45 * r, 45 * r)
    mapping.rotation[2] = random.uniform(-45 * r, 45 * r)
    mapping.scale[0] = random.uniform(2, 6)
    mapping.scale[1] = random.uniform(2, 6)
    mapping.scale[2] = random.uniform(2, 6)
    bpy.ops.object.select_all(action='DESELECT')


def blinker(character):
    #   character.select = True
    # decide blink length and space between
    blink_between_a = round(random.uniform(1.5, 3) * 30)
    blink_between_b = round(random.uniform(1.5, 3) * 30)
    blink_length_a = round(random.uniform(80, 150) / 20)
    blink_length_b = round(random.uniform(80, 150) / 20)

    print(blink_between_a)
    print(blink_between_b)

    # set keyframes DONT forget to update mesh in frame lister
    character.Expressions_eyeClosedR = 0.5
    character.Expressions_eyeClosedL = 0.5
    character.keyframe_insert(data_path='Expressions_eyeClosedR', frame=blink_between_a - 30 + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyeClosedL', frame=blink_between_a - 30 + prewarm_offset)
    character.Expressions_eyeClosedR = 1
    character.Expressions_eyeClosedL = 1
    character.keyframe_insert(data_path='Expressions_eyeClosedR',
                              frame=blink_between_a + blink_length_a - 30 + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyeClosedL',
                              frame=blink_between_a + blink_length_a - 30 + prewarm_offset)
    character.Expressions_eyeClosedR = 0.5
    character.Expressions_eyeClosedL = 0.5
    character.keyframe_insert(data_path='Expressions_eyeClosedR',
                              frame=blink_between_a + blink_length_a * 2 - 30 + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyeClosedL',
                              frame=blink_between_a + blink_length_a * 2 - 30 + prewarm_offset)

    character.Expressions_eyeClosedR = 0.5
    character.Expressions_eyeClosedL = 0.5
    character.keyframe_insert(data_path='Expressions_eyeClosedR',
                              frame=blink_between_a + blink_between_b - 30 + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyeClosedL',
                              frame=blink_between_a + blink_between_b - 30 + prewarm_offset)
    character.Expressions_eyeClosedR = 1
    character.Expressions_eyeClosedL = 1
    character.keyframe_insert(data_path='Expressions_eyeClosedR',
                              frame=blink_between_a + blink_between_b + blink_length_b - 30 + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyeClosedL',
                              frame=blink_between_a + blink_between_b + blink_length_b - 30 + prewarm_offset)
    character.Expressions_eyeClosedR = 0.5
    character.Expressions_eyeClosedL = 0.5
    character.keyframe_insert(data_path='Expressions_eyeClosedR',
                              frame=blink_between_a + blink_between_b + blink_length_b * 2 - 30 + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyeClosedL',
                              frame=blink_between_a + blink_between_b + blink_length_b * 2 - 30 + prewarm_offset)
    bpy.ops.object.select_all(action='DESELECT')


def eye_animator(character):
    character.Expressions_eyesHoriz = random.uniform(0, 1)
    character.Expressions_eyesVert = random.uniform(0, 1)

    character.keyframe_insert(data_path='Expressions_eyesVert', frame=0 + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyesHoriz', frame=0 + prewarm_offset)

    character.Expressions_eyesHoriz = random.uniform(0, 1)
    character.Expressions_eyesVert = random.uniform(0, 1)
    eye_time = random.randint(10, 20)
    eye_delay = random.randint(20, 50)
    print(eye_time, eye_delay)
    character.keyframe_insert(data_path='Expressions_eyesVert', frame=eye_time + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyesHoriz', frame=eye_time + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyesVert', frame=eye_time + eye_delay + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyesHoriz', frame=eye_time + eye_delay + prewarm_offset)

    character.Expressions_eyesHoriz = random.uniform(0, 1)
    character.Expressions_eyesVert = random.uniform(0, 1)

    eye_time = eye_time + eye_delay + random.randint(10, 20)
    eye_delay = random.randint(20, 50)

    character.keyframe_insert(data_path='Expressions_eyesVert', frame=eye_time + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyesHoriz', frame=eye_time + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyesVert', frame=eye_time + eye_delay + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyesHoriz', frame=eye_time + eye_delay + prewarm_offset)

    character.Expressions_eyesHoriz = random.uniform(0, 1)
    character.Expressions_eyesVert = random.uniform(0, 1)

    eye_time = eye_time + eye_delay + random.randint(10, 20)
    eye_delay = random.randint(20, 50)
    character.keyframe_insert(data_path='Expressions_eyesVert', frame=eye_time + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyesHoriz', frame=eye_time + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyesVert', frame=eye_time + eye_delay + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyesHoriz', frame=eye_time + eye_delay + prewarm_offset)

    character.Expressions_eyesHoriz = random.uniform(0, 1)
    character.Expressions_eyesVert = random.uniform(0, 1)

    eye_time = eye_time + eye_delay + random.randint(10, 20)
    eye_delay = random.randint(20, 50)
    character.keyframe_insert(data_path='Expressions_eyesVert', frame=eye_time + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyesHoriz', frame=eye_time + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyesVert', frame=eye_time + eye_delay + prewarm_offset)
    character.keyframe_insert(data_path='Expressions_eyesHoriz', frame=eye_time + eye_delay + prewarm_offset)
    bpy.ops.object.select_all(action='DESELECT')


def mouth_animator(character, factor):
    min = 0.4
    max = 0.6
    min = min / factor
    max = max * factor
    if min <= 0:
        min = 0
    if max >= 1:
        max = 1
    # decide open or not
    for keyframenumber in range(0, keyframecount):
        open = random.randint(0, 1)
        print(open)
        character.Expressions_mouthBite = random.uniform(min, max)
        #    character.Expressions_mouthChew = random.uniform(0.2, 0.8) DUE TO UNFIXED TEETH
        character.Expressions_mouthClosed = random.uniform(min, max)
        character.Expressions_mouthHoriz = random.uniform(min, max)
        character.Expressions_mouthInflated = random.uniform(min, max)
        character.Expressions_mouthLowerOut = random.uniform(min, max)
        if open == 1:
            character.Expressions_mouthOpen = random.uniform(min, max)
            character.Expressions_mouthOpenAggr = random.uniform(min, max)
            character.Expressions_mouthOpenHalf = random.uniform(min, max)
            character.Expressions_mouthOpenLarge = random.uniform(min, max)
            character.Expressions_mouthOpenO = random.uniform(min, max)
            character.Expressions_mouthOpenTeethClosed = random.uniform(min, max)
        if open == 0:
            character.Expressions_mouthOpen = 0.5
            character.Expressions_mouthOpenAggr = 0.5
            character.Expressions_mouthOpenHalf = 0.5
            character.Expressions_mouthOpenLarge = 0.5
            character.Expressions_mouthOpenO = 0.5
            character.Expressions_mouthOpenTeethClosed = 0.5

        character.Expressions_mouthSmile = random.uniform(min, max)
        character.Expressions_mouthSmileL = random.uniform(min, max)
        character.Expressions_mouthSmileOpen = random.uniform(min, max)
        character.Expressions_mouthSmileOpen2 = random.uniform(min, max)
        character.Expressions_mouthSmileR = random.uniform(min, max)
        bpy.context.scene.mblab_show_measures = False
        
        if keyframenumber == 0:
            time_min = 0
            time_max = 1
        if keyframenumber == 1:
            time_min = 10
            time_max = 30
        if keyframenumber == 2:
            time_min = 50
            time_max = 70
        if keyframenumber == 3:
            time_min = 80
            time_max = 100
            time_min = 110
            time_max = 130
        character.keyframe_insert(data_path='Expressions_mouthBite', frame=random.randint(time_min, time_max))
        character.keyframe_insert(data_path='Expressions_mouthBite', frame=random.randint(time_min, time_max))
        character.keyframe_insert(data_path='Expressions_mouthClosed', frame=random.randint(time_min, time_max))
        character.keyframe_insert(data_path='Expressions_mouthHoriz', frame=random.randint(time_min, time_max))
        character.keyframe_insert(data_path='Expressions_mouthInflated', frame=random.randint(time_min, time_max))
        character.keyframe_insert(data_path='Expressions_mouthLowerOut', frame=random.randint(time_min, time_max))
        character.keyframe_insert(data_path='Expressions_mouthOpen', frame=random.randint(time_min, time_max))
        character.keyframe_insert(data_path='Expressions_mouthOpenAggr', frame=random.randint(time_min, time_max))
        character.keyframe_insert(data_path='Expressions_mouthOpenHalf', frame=random.randint(time_min, time_max))
        character.keyframe_insert(data_path='Expressions_mouthOpenLarge', frame=random.randint(time_min, time_max))
        character.keyframe_insert(data_path='Expressions_mouthOpenO', frame=random.randint(time_min, time_max))
        character.keyframe_insert(data_path='Expressions_mouthOpenTeethClosed', frame=random.randint(time_min, time_max))
        character.keyframe_insert(data_path='Expressions_mouthSmile', frame=random.randint(time_min, time_max))
        character.keyframe_insert(data_path='Expressions_mouthSmileL', frame=random.randint(time_min, time_max))
        character.keyframe_insert(data_path='Expressions_mouthSmileOpen', frame=random.randint(time_min, time_max))
        character.keyframe_insert(data_path='Expressions_mouthSmileOpen2', frame=random.randint(time_min, time_max))
        character.keyframe_insert(data_path='Expressions_mouthSmileR', frame=random.randint(time_min, time_max))
        bpy.ops.object.select_all(action='DESELECT')

        
#init_cam(1)
#track_to_head(1)
#set_cam(1)