import os
import glob
import json

import numpy as np

import imageio
import matplotlib.pyplot as plt

import pickle

import MBLab as mbl
import transformations as tr

# pip install eos-py
import eos

# pip install rmsd
import rmsd


# ======================================================
# ======================================================
# ========== Blender Hand Model ========================
# ======================================================
# ======================================================

# RIGHT/LEFT HAND (add suffix)

_MBL_CHAINS = [
          ['thumb01', 'thumb02', 'thumb03'],
          ['index01', 'index02', 'index03'],
          ['middle01', 'middle02', 'middle03'],
          ['ring01', 'ring02', 'ring03'],
          ['pinky01', 'pinky02', 'pinky03']
]

_MBL_JOINT_MAP = {}
idx = 0
_MBL_JOINT_MAP['root'] = idx
for ch in _MBL_CHAINS:
    idx += 1
    for bonestr in ch:
        _MBL_JOINT_MAP[bonestr] = [idx,idx+1]
        idx+=1

_MBL_JOINT_NUM = idx+1
        
def _calculate_mean_hand(bone3d_path = 'models/frame_0000000.json',lr='R'):
    with open(bone3d_path, 'r') as f:
        b3d = json.load(f)

    mean_hand = np.zeros((len(_MBL_JOINT_MAP.keys())+1,3))
    
    lr=lr.capitalize()
    
    mean_hand[_MBL_JOINT_MAP['root'],0] = b3d['hand_'+lr]['xh']
    mean_hand[_MBL_JOINT_MAP['root'],1] = b3d['hand_'+lr]['yh']
    mean_hand[_MBL_JOINT_MAP['root'],2] = b3d['hand_'+lr]['zh']
    
    for b in _MBL_JOINT_MAP.keys():
        if b!='root':
            mean_hand[_MBL_JOINT_MAP[b+'_'+lr][0],0] = b3d[b+'_'+lr]['xh']
            mean_hand[_MBL_JOINT_MAP[b+'_'+lr][0],1] = b3d[b+'_'+lr]['yh']
            mean_hand[_MBL_JOINT_MAP[b+'_'+lr][0],2] = b3d[b+'_'+lr]['zh']

            mean_hand[_MBL_JOINT_MAP[b+'_'+lr][1],0] = b3d[b+'_'+lr]['xt']
            mean_hand[_MBL_JOINT_MAP[b+'_'+lr][1],1] = b3d[b+'_'+lr]['yt']
            mean_hand[_MBL_JOINT_MAP[b+'_'+lr][1],2] = b3d[b+'_'+lr]['zt']
    
    return mean_hand
    

_MBL_MEAN_HAND_R = np.array([
       [-0.59773439, -0.03369223,  1.09813178],
       [-0.60631996, -0.05174106,  1.08864319],
       [-0.61373043, -0.07830875,  1.06001484],
       [-0.62523669, -0.10571334,  1.0451268 ],
       [-0.63457668, -0.12053   ,  1.02659357],
       [-0.66391802, -0.06161001,  1.06263816],
       [-0.69111669, -0.06214667,  1.04116678],
       [-0.70958006, -0.06104667,  1.02704346],
       [-0.72352999, -0.05967668,  1.01019013],
       [-0.66563797, -0.044436  ,  1.06668222],
       [-0.69590998, -0.04421   ,  1.03867352],
       [-0.71582669, -0.04402333,  1.02250683],
       [-0.73109001, -0.04405   ,  1.00432682],
       [-0.66225207, -0.02796   ,  1.06828415],
       [-0.69226998, -0.02759   ,  1.04019344],
       [-0.71054333, -0.02728334,  1.0258702 ],
       [-0.72659671, -0.02816667,  1.00972342],
       [-0.65555602, -0.01134   ,  1.06568003],
       [-0.68149674, -0.0116    ,  1.04436684],
       [-0.69505668, -0.01276333,  1.03373349],
       [-0.70541   , -0.01394   ,  1.02187347]
])

_MBL_MEAN_HAND_L = np.array([
       [ 0.59773445, -0.03369222,  1.09813178],
       [ 0.60632002, -0.05174105,  1.08864319],
       [ 0.61373049, -0.07830875,  1.06001484],
       [ 0.62523675, -0.10571334,  1.0451268 ],
       [ 0.63457674, -0.12053   ,  1.02659357],
       [ 0.66391808, -0.06160999,  1.06263816],
       [ 0.69111675, -0.06214666,  1.04116678],
       [ 0.70958012, -0.06104666,  1.02704346],
       [ 0.72353005, -0.05967667,  1.01019013],
       [ 0.66563803, -0.04443599,  1.06668222],
       [ 0.69591004, -0.04420999,  1.03867352],
       [ 0.71582675, -0.04402333,  1.02250683],
       [ 0.73109007, -0.04404999,  1.00432682],
       [ 0.66225213, -0.02796   ,  1.06828415],
       [ 0.69227004, -0.02759   ,  1.04019344],
       [ 0.71054339, -0.02728333,  1.0258702 ],
       [ 0.72659677, -0.02816666,  1.00972342],
       [ 0.65555602, -0.01133999,  1.06568003],
       [ 0.68149674, -0.01159999,  1.04436684],
       [ 0.69505668, -0.01276333,  1.03373349],
       [ 0.70541   , -0.01393999,  1.02187347]
])
    
class MBLHand:
    def __init__(self):
        return

    def bones2pts2d(self,bones2d,lr='R'):
        lr = '_'+lr.capitalize()
        
        pts2d = np.zeros((_MBL_JOINT_NUM,2))
        for jk in _MBL_JOINT_MAP.keys():
            if jk=='root':
                pts2d[_MBL_JOINT_MAP[jk],0] = bones2d['hand'+lr]['xh']
                pts2d[_MBL_JOINT_MAP[jk],1] = bones2d['hand'+lr]['yh']
            else:
                pts2d[_MBL_JOINT_MAP[jk][0],0] = bones2d[jk+lr]['xh']
                pts2d[_MBL_JOINT_MAP[jk][0],1] = bones2d[jk+lr]['yh']
                
                pts2d[_MBL_JOINT_MAP[jk][1],0] = bones2d[jk+lr]['xt']
                pts2d[_MBL_JOINT_MAP[jk][1],1] = bones2d[jk+lr]['yt']
              
        # lifehack: map to -1:1 space
        pts2d[:,0] = 2.*(pts2d[:,0]-0.5)
        pts2d[:,1] = 2.*(pts2d[:,1]-0.5)
            
        return pts2d

    def bones2pts3d(self,bones3d,lr='R'):
        lr = '_'+lr.capitalize()
        
        pts3d = np.zeros((_MBL_JOINT_NUM,3))
        for jk in _MBL_JOINT_MAP.keys():
            if jk=='root':
                pts3d[_MBL_JOINT_MAP[jk],0] = bones3d['hand'+lr]['xh']
                pts3d[_MBL_JOINT_MAP[jk],1] = bones3d['hand'+lr]['yh']
                pts3d[_MBL_JOINT_MAP[jk],2] = bones3d['hand'+lr]['zh']
            else:
                pts3d[_MBL_JOINT_MAP[jk][0],0] = bones3d[jk+lr]['xh']
                pts3d[_MBL_JOINT_MAP[jk][0],1] = bones3d[jk+lr]['yh']
                pts3d[_MBL_JOINT_MAP[jk][0],2] = bones3d[jk+lr]['zh']
                
                pts3d[_MBL_JOINT_MAP[jk][1],0] = bones3d[jk+lr]['xt']
                pts3d[_MBL_JOINT_MAP[jk][1],1] = bones3d[jk+lr]['yt']
                pts3d[_MBL_JOINT_MAP[jk][1],2] = bones3d[jk+lr]['zt']
            
        return pts3d

    def project_pts(self,pts3d,cam_meta,sz):
        p3d = pts3d.copy()
        cm = cam_meta.copy()
        _blender_unify_coords(p3d,cm)

        MVP = mbl.getBlenderMVP(cam_meta, imHeight=sz[0], imWidth=sz[1])
        
        pts2d = mbl.project(pts3d, MVP, xlim=[0,sz[0]], ylim=[sz[0],0])
        
        return pts2d

## ======================== (Blender Local) ===========================

# BLENDER: coordinate system change for Blender model
def _blender_unify_coords(pts3d,cam_meta):
    pts3d[:,0] -= cam_meta['location_x']
    pts3d[:,1] -= cam_meta['location_y']
    pts3d[:,2] -= cam_meta['location_z']
        
    cam_meta['location_x'] = 0.0
    cam_meta['location_y'] = 0.0
    cam_meta['location_z'] = 0.0
    return



    
# ======================================================
# ======================================================
# ========== MultiCam Hand Model =======================
# ======================================================
# ======================================================

# For MultiCam txt file reading
_MC_joint_list = [
            "F4_KNU1_A","F4_KNU1_B","F4_KNU2_A","F4_KNU3_A",
            "F3_KNU1_A","F3_KNU1_B","F3_KNU2_A","F3_KNU3_A",
            "F1_KNU1_A","F1_KNU1_B","F1_KNU2_A","F1_KNU3_A",
            "F2_KNU1_A","F2_KNU1_B","F2_KNU2_A","F2_KNU3_A",
            "TH_KNU1_A","TH_KNU1_B","TH_KNU2_A","TH_KNU3_A",
            "PALM_POSITION", "PALM_NORMAL"]

_MC_joint_dict = {}
for ii in range(len(_MC_joint_list)):
    _MC_joint_dict[_MC_joint_list[ii]] = ii

_MC_palm_bones = {
    'TH_0' : ['PALM_POSITION','TH_KNU1_B'],
    'TH_1' : ['TH_KNU1_B','TH_KNU1_A'],
    'TH_2' : ['TH_KNU1_A','TH_KNU2_A'],
    'TH_3' : ['TH_KNU2_A','TH_KNU3_A'],
    
    'F1_0' : ['PALM_POSITION','F1_KNU1_B'],
    'F1_1' : ['F1_KNU1_B','F1_KNU1_A'],
    'F1_2' : ['F1_KNU1_A','F1_KNU2_A'],
    'F1_3' : ['F1_KNU2_A','F1_KNU3_A'],
    
    'F2_0' : ['PALM_POSITION','F2_KNU1_B'],
    'F2_1' : ['F2_KNU1_B','F2_KNU1_A'],
    'F2_2' : ['F2_KNU1_A','F2_KNU2_A'],
    'F2_3' : ['F2_KNU2_A','F2_KNU3_A'],
    
    'F3_0' : ['PALM_POSITION','F3_KNU1_B'],
    'F3_1' : ['F3_KNU1_B','F3_KNU1_A'],
    'F3_2' : ['F3_KNU1_A','F3_KNU2_A'],
    'F3_3' : ['F3_KNU2_A','F3_KNU3_A'],
    
    'F4_0' : ['PALM_POSITION','F4_KNU1_B'],
    'F4_1' : ['F4_KNU1_B','F4_KNU1_A'],
    'F4_2' : ['F4_KNU1_A','F4_KNU2_A'],
    'F4_3' : ['F4_KNU2_A','F4_KNU3_A']
}

# pack joints in np.array
_MC_CHAINS = [
          ['TH_1', 'TH_2', 'TH_3'],
          ['F4_1', 'F4_2', 'F4_3'],
          ['F3_1', 'F3_2', 'F3_3'],
          ['F2_1', 'F2_2', 'F2_3'],
          ['F1_1', 'F1_2', 'F1_3']
]

_MC_JOINT_MAP = {}
idx = 0
_MC_JOINT_MAP['root'] = idx
for ch in _MC_CHAINS:
    idx += 1
    for bonestr in ch:
        _MC_JOINT_MAP[bonestr] = [idx,idx+1]
        idx+=1

class MultiCamHand:
    def __init__(self):
        return
    
#     def joints2pts2d(self,bones2d,lr='R'):
#         lr = '_'+lr.capitalize()
        
#         pts2d = np.zeros((_MBL_JOINT_NUM,2))
#         for jk in _MBL_JOINT_MAP.keys():
#             if jk=='root':
#                 pts2d[_MBL_JOINT_MAP[jk],0] = bones2d['hand'+lr]['xh']
#                 pts2d[_MBL_JOINT_MAP[jk],1] = bones2d['hand'+lr]['yh']
#             else:
#                 pts2d[_MBL_JOINT_MAP[jk][0],0] = bones2d[jk+lr]['xh']
#                 pts2d[_MBL_JOINT_MAP[jk][0],1] = bones2d[jk+lr]['yh']
                
#                 pts2d[_MBL_JOINT_MAP[jk][1],0] = bones2d[jk+lr]['xt']
#                 pts2d[_MBL_JOINT_MAP[jk][1],1] = bones2d[jk+lr]['yt']
              
#         # lifehack: map to -1:1 space
#         pts2d[:,0] = 2.*(pts2d[:,0]-0.5)
#         pts2d[:,1] = 2.*(pts2d[:,1]-0.5)
            
#         return pts2d

#     def joints2pts3d(self,bones3d,lr='R'):
#         lr = '_'+lr.capitalize()
        
#         pts3d = np.zeros((_MBL_JOINT_NUM,3))
#         for jk in _MBL_JOINT_MAP.keys():
#             if jk=='root':
#                 pts3d[_MBL_JOINT_MAP[jk],0] = bones3d['hand'+lr]['xh']
#                 pts3d[_MBL_JOINT_MAP[jk],1] = bones3d['hand'+lr]['yh']
#                 pts3d[_MBL_JOINT_MAP[jk],2] = bones3d['hand'+lr]['zh']
#             else:
#                 pts3d[_MBL_JOINT_MAP[jk][0],0] = bones3d[jk+lr]['xh']
#                 pts3d[_MBL_JOINT_MAP[jk][0],1] = bones3d[jk+lr]['yh']
#                 pts3d[_MBL_JOINT_MAP[jk][0],2] = bones3d[jk+lr]['zh']
                
#                 pts3d[_MBL_JOINT_MAP[jk][1],0] = bones3d[jk+lr]['xt']
#                 pts3d[_MBL_JOINT_MAP[jk][1],1] = bones3d[jk+lr]['yt']
#                 pts3d[_MBL_JOINT_MAP[jk][1],2] = bones3d[jk+lr]['zt']
            
#         return pts3d

#     def project_pts(self,pts3d,webcam_id):
#         p3d = pts3d.copy()
#         cm = cam_meta.copy()
#         _blender_unify_coords(p3d,cm)

#         MVP = mbl.getBlenderMVP(cam_meta, imHeight=sz[0], imWidth=sz[1])
        
#         pts2d = mbl.project(pts3d, MVP, xlim=[0,sz[0]], ylim=[sz[0],0])
        
#         return pts2d
    
    
## ======================== (MultiCam Local) ===========================
    
    
def _readAnnotation3D(file):
    f = open(file, "r")
    an = []
    for l in f:
        l = l.split()
        an.append((float(l[1]),float(l[2]), float(l[3])))

    return np.array(an, dtype=float)

def _getCameraMatrix():
    Fx = 614.878
    Fy = 615.479
    Cx = 313.219
    Cy = 231.288
    cameraMatrix = np.array([[Fx, 0, Cx],
                    [0, Fy, Cy],
                    [0, 0, 1]])
    return cameraMatrix

def _getDistCoeffs():
    return np.array([0.092701, -0.175877, -0.0035687, -0.00302299, 0])
#     return np.array([0., 0., 0., 0., 0.])

def _Rodrigues(v):
    R = np.zeros((3,3))
    tet = np.sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2])
    v = v / tet

    R = np.eye(3) * np.cos(tet) + (1-np.cos(tet))*np.dot(v,v.T) + \
                np.sin(tet) * np.array([[0,-v[2],v[1]],[v[2],0,-v[0]],[-v[1],v[0],0]])
    return R

def _projectPoints(pts3d, rvec, tvec, cameraMatrix, distCoeffs):
    pts2d = np.zeros((pts3d.shape[0],2))
    fx = cameraMatrix[0,0]
    fy = cameraMatrix[1,1]
    cx = cameraMatrix[0,2]
    cy = cameraMatrix[1,2]
#     R = np.zeros((3,3))
#     cv2.Rodrigues(rvec,R)

    R = _Rodrigues(rvec)

    k = distCoeffs

    rxyz = np.dot(R,pts3d.T)
    for ii in range(pts3d.shape[0]):
        xyz = rxyz[:,ii] + tvec.T
        xyz = xyz[0]
        x1 = xyz[0]/xyz[2]
        y1 = xyz[1]/xyz[2]

        r2 = x1*x1 + y1*y1

        # simplified formula for len(distCoeffs) = 5
        x11 = x1*(1+k[0]*r2+k[1]*r2*r2+k[4]*r2*r2*r2) + 2*k[2]*x1*y1 + k[3]*(r2+2*x1*x1)
        y11 = y1*(1+k[0]*r2+k[1]*r2*r2+k[4]*r2*r2*r2) + k[2]*(r2+2*y1*y1) + 2*k[3]*x1*y1

        u = fx * x11 + cx
        v = fy * y11 + cy
        pts2d[ii,0] = u
        pts2d[ii,1] = v
    return pts2d

# ======== Boxing Gloves Model =======================
# PATH_TO_BOXING_GLOVE = "boxing_gloves.obj"
class BoxingGloves:
    def __init__(self,PATH_TO_BOXING_GLOVE = "models/boxing_gloves.obj"):
        bx_gloves = mbl.ObjLoader(PATH_TO_BOXING_GLOVE)
        vx = np.array(bx_gloves.vertices)
        self.glove_R = vx[vx[:,2]>0,:]
        self.glove_L = vx[vx[:,2]<0,:]
                
        self.keypoints_R = np.array([
            [-25.,40.,120.],     # index
            [40.,40.,120.],      # pinky
            [10.,-40.,100.],      # root
            [-25.,25.,90]        # thumb
        ])
        self.keypoints_L = np.array([
            [-25.,40.,-120.],     # index
            [40.,40.,-120.],      # pinky
            [10.,-40.,-100.],     # root
            [-25.,25.,-90]        # thumb
        ])
        
        grc = np.mean(self.keypoints_R,axis=0)
        glc = np.mean(self.keypoints_L,axis=0)

        for ii in range(3):
            self.glove_R[:,ii] -= grc[ii]
            self.glove_L[:,ii] -= glc[ii]
            
            self.keypoints_R[:,ii] -= grc[ii]
            self.keypoints_L[:,ii] -= glc[ii]

        return
        
    def get_verts(self,lr='R'):
        if lr=='R':
            return self.glove_R
        else:
            return self.glove_L

    def get_keypoints(self,lr='R'):
        if lr=='R':
            return self.keypoints_R
        else:
            return self.keypoints_L

        
# ================== Palm General ===================================

_PLOT_MAP = {
    'r' : [0,1,2,3,4],             # Thumb
    'm' : [0,5,6,7,8],             # Index
    'c' : [0,9,10,11,12],           # Middle
    'b' : [0,13,14,15,16],         # Ring
    'g' : [0,17,18,19,20]          # Pinky
}

_KEYPOINTS_MAP = [ 5, 17, 0, 2 ]

# # map hand coordinates in array
# _HCS_MAP = {
#         'Qw' : 0, 'Qx' : 1, 'Qy' : 2, 'Qz' : 3,
#         'tx' : 4, 'ty' : 5, 's'  : 6
#     }

class BaseHand:
    def __init__(self):
        return
    
    def get_keypoints(self,pts):
        kpts = pts[_KEYPOINTS_MAP,:]
        return kpts
    
    # pts3d - np.array (Nx3) N - number of points
    # seek transformpation pts3d2 to pts3d1
    def fit_pose_3d(self, pts3d1, pts3d2):
        c1 = rmsd.centroid(pts3d1)
        c2 = rmsd.centroid(pts3d2)
        t = c1-c2
        
        A = pts3d1.copy()
        B = pts3d2.copy()

        A -= rmsd.centroid(A)
        B -= rmsd.centroid(B)

        An = np.linalg.norm(A,axis=1)
        Bn = np.linalg.norm(B,axis=1)
        s = np.mean(An/Bn)
        B *= s
        
        U = rmsd.kabsch(B,A) # rotation from B to A

        return (U,s,t)

    # fit 3d points to 2d using EOS
    # pts2d - points projection to image plane
    # pts3d - 3d model points
    # imSz - image size
    def fit_pose_2d(self, pts2d, pts3d):
        p2d = pts2d.copy()

        p3d = np.ones((pts3d.shape[0],4))
        p3d[:,:3] = pts3d

        pose = eos.fitting.estimate_orthographic_projection_linear(p2d,p3d)

        return pose

    # project pts3d model points to image plane
    # using pre-fitted pose
    def project_pts(self, p3d, imSz = (1024,1024)):
        pts2d = np.zeros((p3d.shape[0],2))
        pts2d[:,0] = imSz[0]*(0.5*p3d[:,0]+0.5)
        pts2d[:,1] = imSz[1]*(-0.5*p3d[:,1]+0.5)

        return pts2d



    # transform pts3d model points
    # using pre-fitted pose
    def transform_pts(self, pts3d, pose):

        U = pose.R
   
        p3d = pts3d.copy()
        p3d = np.dot(p3d,U)
        p3d[:,0] += pose.tx
        p3d[:,1] += pose.ty
        p3d *= pose.s

        return p3d
    

    # draw nice hand plot
    def plot_hand(self, dat2d):
        for bk in _PLOT_MAP.keys():
            x = dat2d[_PLOT_MAP[bk],0]
            y = dat2d[_PLOT_MAP[bk],1]
            plt.plot(x,y,bk+'.-')

        x = dat2d[:,0]
        y = dat2d[:,1]
        plt.plot(x,y,'y.')
        
        x = dat2d[_KEYPOINTS_MAP,0]
        y = dat2d[_KEYPOINTS_MAP,1]
        plt.plot(x,y,'ro')

        return

## =========================== T R A S H ===========================
