from execution.tasks import _parse_data


res = _parse_data(
    data_to_parse=['d://selfie_runner_test_o', 'd://selfie_runner_test'],
    problem_name='lip_mask',
    formula='*/frame/*.png',
    max_count=-1,
    out_data_path='d://')