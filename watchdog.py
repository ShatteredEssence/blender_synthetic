import subprocess
import time
import psutil

args = ['D:\\!work\\blenders\\blender_2.8_custom_07.07.2018\\Release\\blender.exe', 'D:\\!work\\blender_debug\\blend\\selfie_runner.blend', '--python', 'D:\\!work\\blender_debug\\prelaunch.py']
delay = 3

while True:
    print('Starting blender: ' + str(args))
    proc = subprocess.Popen(args)

    print('Waiting for process (pid={})'.format(proc.pid))
    proc.wait()

    print('Blender died. Waiting for {} seconds to restart'.format(delay))