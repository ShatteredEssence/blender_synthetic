import bpy
import sys
import time
import random
import json
import logging
import math
from math import pi
from config import cfg
from pathlib import Path
from utils import landmark_utils as landmarks_helper
from utils import blender_utils as utils
from utils import poly_hair_utils as poly_hair
from utils import materials, enviroment, path_utils
from mathutils import Vector, Euler
from characters import BLENDER_CHARACTER_FULL_CONFIG, HUMAN_INIT_CONFIG
from characters.blender_character import Human
from characters.utils import randomizer, facebox, export
from characters.utils import animation as blender_character_anumation
from accessories.glasses import Glasses
from accessories.shirts import Shirt
from environment import ENVIRONMENT_CONFIG
from environment.blender_environment import BlenderEnvironment
from cameras.blender_camera import BlenderCamera

MODULE_NAME = 'selfie_runner'

module_logger = logging.getLogger(MODULE_NAME)
module_logger.setLevel(logging.DEBUG)

data_path = Path(cfg['Paths']['data_path'])

resolution_x = 1024
resolution_y = 1024
prewarm_frame_count = 30
render_frame_count = 5

frame_start = prewarm_frame_count
frame_stop = prewarm_frame_count + render_frame_count

GLASSES = False
SHIRT = True
OCCLUSION = True

def init_camera(armature, distance, vertical_offset):
    track_target = bpy.data.objects['track_target']
    rotation_target = bpy.data.objects['rotation_target']
    camera = bpy.data.objects["Camera"]
    armature.data.layers[0] = True
    armature.data.layers[1] = True
    utils.select(track_target)
    utils.select(armature)
    utils.set_active(armature)
    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='DESELECT')
    arm = bpy.data.armatures.get("MBLab_skeleton_base_ik") or bpy.data.armatures.get("MBLab_skeleton_base_fk")
    arm.bones.active = arm.bones["head"]
    bpy.context.object.data.bones["head"].select = True
    head_loc = armature.location + bpy.context.active_pose_bone.head

    track_target.location = (head_loc[0], head_loc[1], head_loc[2] + vertical_offset)
    rotation_target.location = track_target.location
    camera.location = [0, -distance, 0]

    bpy.ops.pose.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    return camera

def init_face_camera(human, camera, distance):
    human.armature.data.layers[0] = True
    human.armature.data.layers[1] = True
    utils.select(human.armature)
    utils.set_active(human.armature)
    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='DESELECT')

    arm = bpy.data.armatures.get("MBLab_skeleton_base_ik") or bpy.data.armatures.get("MBLab_skeleton_base_fk")
    arm.bones.active = arm.bones["head"]
    bpy.context.object.data.bones["head"].select = True

    head_loc = human.armature.location + bpy.context.active_pose_bone.head
    camera.location = [0, distance, head_loc[2]]
    bpy.ops.pose.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    return camera

def get_head_coords(human):
    human.armature.data.layers[0] = True
    human.armature.data.layers[1] = True
    utils.select(human.armature)
    utils.set_active(human.armature)
    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='DESELECT')

    arm = bpy.data.armatures.get("MBLab_skeleton_base_ik") or bpy.data.armatures.get("MBLab_skeleton_base_fk")
    arm.bones.active = arm.bones["head"]
    bpy.context.object.data.bones["head"].select = True

    head_loc = human.armature.location + bpy.context.active_pose_bone.head
    bpy.ops.pose.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    return head_loc

def create_camera_animation(max_horizontal_rotation, max_vertical_rotation, start_frame, end_frame):
    rotation_target = bpy.data.objects['rotation_target']

    horizontal_rotation = random.uniform(-max_horizontal_rotation, max_horizontal_rotation)
    vertical_rotation = random.uniform(-max_vertical_rotation, max_vertical_rotation) - pi / 8
    rotation_target.rotation_euler = [vertical_rotation, 0, horizontal_rotation]
    rotation_target.keyframe_insert(data_path='rotation_euler', frame=start_frame)

    horizontal_rotation = random.uniform(-max_horizontal_rotation, max_horizontal_rotation)
    vertical_rotation = random.uniform(-max_vertical_rotation, max_vertical_rotation) - pi / 8
    rotation_target.rotation_euler = [vertical_rotation, 0, horizontal_rotation]
    rotation_target.keyframe_insert(data_path='rotation_euler', frame=end_frame)

def render_sequences_data(camera, environment, human, path, landmarks, landmark_classes, facebox_indices,
                          frame_start, frame_stop, frames_meta, mocap_file):
    for frame in range(frame_start, frame_stop):
        bpy.context.scene.frame_set(frame)
        utils.update_morphs()

        frame_meta = frames_meta[frame-prewarm_frame_count]
        frame_meta['tongue'] = 1 if human.is_tongue_out() else 0
        landmarks_helper.write_landmarks(camera, frame_meta, path, human.character, landmarks, landmark_classes,
                                         frame - prewarm_frame_count, human.vertex_groups_indices['outer_eyes'])
        landmarks_helper.write_barimetric_landmarks(camera, path, human, frame - prewarm_frame_count)
        landmarks_helper.write_landmarks_eyes(camera, path, human, frame - prewarm_frame_count)
        facebox.write_facebox_indices(camera, path, human.character, facebox_indices,
                                             frame - prewarm_frame_count, resolution_x, resolution_y)
        human.dump_properties(path, frame - prewarm_frame_count, mocap_file=str(mocap_file))
        human.dump_pose_data(path, frame - prewarm_frame_count)
        environment.dump_properties(path, frame-prewarm_frame_count)
        camera.dump_properties(path, frame-prewarm_frame_count)
        # export.dump_2d_and_3d_coords(path, human, camera, frame-prewarm_frame_count)

def render_mask_sequence(character, path, mask_name, material_name, material_slot):
    start_time = time.time()
    module_logger.info('START %s. Measuring time', path)
    #outer_eye_material = bpy.data.materials.get('outer_eyes')
    #transparent_material = bpy.data.materials.get('transparent')
    #materials.change_character_material(character, material_name)
    character.material_slots[material_slot].material = bpy.data.materials.get(material_name)
    #materials.set_vertex_group_material(character, 'outer_eyes', outer_eye_material)
    #materials.set_vertex_group_material(character, 'eyelashes', transparent_material)
    #materials.add_hair_material(character)
    utils.render_sequence(path, mask_name, 'BW', frame_start, frame_stop, -prewarm_frame_count)

    stop_time = time.time()
    time_spent = stop_time - start_time
    module_logger.info('STOP. Time spent %s', time_spent)

def init_glasses(human):
    module_logger.info('Init glasses')
    glasses = Glasses()
    mesh = human.character.to_mesh(apply_modifiers=True, depsgraph=bpy.context.depsgraph)

    if human.gender:
        ear_l_vert = mesh.vertices[3007]
        eye_l_vert = mesh.vertices[2687]
        ear_r_vert = mesh.vertices[17434]
        eye_r_vert = mesh.vertices[17114]
        nose_vert = mesh.vertices[7752]
    else:
        ear_l_vert = mesh.vertices[2541]
        eye_l_vert = mesh.vertices[2216]
        ear_r_vert = mesh.vertices[17530]
        eye_r_vert = mesh.vertices[17204]
        nose_vert = mesh.vertices[7527]
    nose_to_ear_l_vec = ear_l_vert.co - nose_vert.co
    nose_to_ear_r_vec = ear_r_vert.co - nose_vert.co
    eye_to_eye_vec = eye_l_vert.co - eye_r_vert.co
    factor = eye_to_eye_vec.length / glasses.size

    # put glasses on nose
    glasses.position = nose_vert.co

    # scaling glasses regarding character size
    glasses.scale = factor

    # putting glass ear track targets on ears
    ear_l_track, ear_r_track = glasses.ear_tracks
    ear_l_track.location = nose_to_ear_l_vec / factor
    ear_r_track.location = nose_to_ear_r_vec / factor

    bpy.data.meshes.remove(mesh)
    return glasses

def parent_glass(armature, glasses):
    module_logger.info('Parenting glasses')
    utils.select(glasses.root)
    utils.select(armature)
    utils.set_active(armature)
    bpy.context.object.data.layers[1] = True
    utils.set_pose_mode()
    skel = bpy.data.armatures.get("MBLab_skeleton_base_ik") or bpy.data.armatures.get("MBLab_skeleton_base_fk")
    skel.bones.active = skel.bones['head']
    bpy.context.object.data.bones['head'].select = True
    bpy.ops.object.parent_set(type='BONE', keep_transform=False)
    bpy.ops.object.parent_set(type='BONE', keep_transform=False)
    utils.set_object_mode()
    utils.deselect_all()

def dump_additional_meta(path, frames_meta):
    utils.makedirs(path / 'additional_meta')
    for frame in range(0, render_frame_count):
        with open((path / 'additional_meta' / 'frame_{:07}.json'.format(frame)), 'w') as meta_file:
            json.dump(frames_meta[frame], meta_file, indent=4)

def init_frames_meta():
    frames_meta = []
    for frame in range(0, render_frame_count):
        frame_meta = dict()
        frame_meta['tongue'] = dict()
        if OCCLUSION:
            frame_meta['occlusion'] = dict()
        frames_meta.append(frame_meta)
    return frames_meta

def constrain_hands(human):
    # creating copy location constrains
    left = human.armature.pose.bones['IK_control_a_L']
    left.constraints.new('COPY_LOCATION')
    left.constraints["Copy Location"].target = bpy.data.objects["right_hand_helper"]
    # right = human.armature.pose.bones['IK_control_a_R']
    # right.constraints.new('COPY_LOCATION')
    # right.constraints["Copy Location"].target = bpy.data.objects["left_hand_helper"]
    head = human.armature.pose.bones['IK_control_hd']
    head.constraints.new('COPY_LOCATION')
    head.constraints["Copy Location"].target = bpy.data.objects["head_helper"]

def run(gender, race, character_config, path, landmarks, landmark_classes, facebox_indices):
    bpy.context.scene.render.resolution_x = resolution_x
    bpy.context.scene.render.resolution_y = resolution_y
    bpy.context.scene.frame_end = prewarm_frame_count + render_frame_count

    utils.makedirs(path)
    utils.gc()

    human_config = {
        'gender': gender,
        'race': race,
        'has_hair': False,
        'has_wrinkles': False,
        'eye_iris_morph_random_factor': 1.3,
    }

    human = Human(human_config=human_config)
    character, armature = human.character, human.armature

    human.remove_modifier('mbastlab_corrective_modifier')
    human.remove_modifier('mbastlab_subdvision')
    human.remove_modifier('mbastlab_displacement')

    human.set_texture(str(human.hand_texture[0]), 'skin', 'hands_diff')
    human.set_texture(str(human.hand_texture[1]), 'skin', 'hands_spec')
    human.set_texture(str(human.hand_texture[2]), 'skin', 'hands_bump')
    human.set_texture(str(human.hand_to_body_mix_mask), 'skin', 'hands_body_mix_mask')
    human.randomise_initial_character()
    # human.randomize_initial_lips()

    environment = BlenderEnvironment()

    camera_helper = bpy.data.objects['cam_helper']
    camera = bpy.data.objects['cam_linked']
    camera = BlenderCamera(existing_camera=camera)

    # utils.init_hand_camera(armature)
    head_coords = get_head_coords(human)
    camera_helper.location = head_coords + Vector((0, 0, 0.04))
    camera.location = Vector((0, -0.3, 0))
    # human.open_mouth_random()
    human.randomize_eyes(0.6, 0.8)
    human.randomize_attributes(randomizer.expression_morphs, 1.5)
    human.randomise_attr('Expressions_eyesHoriz', 0, 1.0)
    human.randomise_attr('Expressions_eyesVert', 0, 1.0)
    human.close_eyes()
    utils.update_morphs()
    #character.modifiers["mbastlab_armature"].show_viewport = False

    camera_helper.rotation_euler = Euler((math.radians(random.uniform(-15, 15)), 0, math.radians(random.uniform(-15, 15))))
    camera_helper.keyframe_insert(data_path='rotation_euler', frame=frame_start)
    camera_helper.rotation_euler = Euler((math.radians(random.uniform(-15, 15)), 0, math.radians(random.uniform(-15, 15))))
    camera_helper.keyframe_insert(data_path='rotation_euler', frame=frame_stop)

    if SHIRT:
        shirt = Shirt(gender)
        shirt.position = (0, 0, -0.0066) if gender else (0, -0.002, 0)

        shirt.color = 'rgb'
        utils.deselect_all()
        utils.select(character)
        utils.select(shirt.root)
        bpy.ops.mbast.proxy_fit()
        shirt.randomize(character)
        utils.deselect_all()

    if GLASSES:
        glasses = init_glasses(human)
        parent_glass(armature, glasses)
        glasses.randomize()

    # randomizer.create_expressions_animation(character, prewarm_frame_count, prewarm_frame_count + render_frame_count)

    #new hair
    # poly_hair.create_hair(human)

    # human.create_mouth_animation(prewarm_frame_count, prewarm_frame_count + render_frame_count)

    # constrain_hands(human)

    origin = bpy.data.objects['track_target']

    utils.select(human.armature)
    utils.set_active(human.armature)
    # utils.set_pose_mode()

    ### setting hand pose and finger gesture mechanism
    mocap_file = None
    # human.remove_bone_constrains(human.arm_r_bones)
    # human.remove_bone_constrains(human.finger_r_bones)
    #
    # pose_folder = data_path / 'animations/poses/right'
    # mocap_file = random.choice((list(pose_folder.glob('*.json'))))
    # module_logger.info('loading json pose {}'.format(mocap_file))
    # blender_character_anumation.set_pose(mocap_file)

    # case = random.choice([
    #     'create_hands_animation_front_case_00',
    #     'create_hands_animation_front_case_01',
    #     'create_hands_animation_front_case_02',
    #     'create_hands_animation_front_case_03'
    #     ])
    # case = 'create_hands_animation_hand_gestures_00'
    case = 'skip_all_cases'

    if case == 'create_hands_animation_front_case_00':
        human.create_hands_animation_front_case_00(prewarm_frame_count, prewarm_frame_count + render_frame_count, 15, 15, 0.2, 0.5, origin, gender)
    if case == 'create_hands_animation_front_case_01':
        human.create_hands_animation_front_case_01(prewarm_frame_count, prewarm_frame_count + render_frame_count, 25, 15, 0.3, 0.6)
    if case == 'create_hands_animation_front_case_02':
        human.create_hands_animation_front_case_02(prewarm_frame_count, prewarm_frame_count + render_frame_count, 25, 15, 0.3, 0.6)
    if case == 'create_hands_animation_front_case_03':
        human.create_hands_animation_front_case_03(prewarm_frame_count, prewarm_frame_count + render_frame_count, 25, 15, 0.3, 0.6)
    if case == 'create_hands_animation_hand_gestures_00':
        human.create_hands_animation_hand_gestures_00(prewarm_frame_count, prewarm_frame_count + render_frame_count, 5, 5, 0.3, 0.5, origin, gender)

    # utils.deselect_pose_all()
    utils.deselect_all()

    # Vertices (their coordinates) to be checked
    res = human.get_vert_coords([1394, 3800, 7346, 8448, 7796])
    # Objects location to compare with
    obj_location = origin.matrix_world.decompose()[0]
    # True -> one of the point is clother to the object then 0.3
    # False -> All points are farther
    close_to = utils.is_point_close_to(res, obj_location, 0.06)

    frames_meta = init_frames_meta()
    white_sphere = bpy.data.objects['white_sphere']

    human.hair.randomise_hair_color()
    #human.hair.bake_hair_cache()

    human.hair.set_normal_colored_hair()
    utils.render_sequence(path, 'frame', 'RGB', frame_start, frame_stop, -prewarm_frame_count)
    render_sequences_data(camera, environment, human, path, landmarks, landmark_classes, facebox_indices,
                          frame_start, frame_stop, frames_meta, mocap_file)
    enviroment.set_background_strength(0)
    enviroment.disable_sun(bpy.data.objects['Sun'])
    bpy.context.scene.eevee.use_bloom = False

    character.material_slots[0].material = bpy.data.materials.get('black')
    character.material_slots[2].material = bpy.data.materials.get('black')

    poly_hair.set_black_hair()
    human.hair.set_black_hair('beard')
    human.hair.set_black_hair('mustache')

    if GLASSES:
        if SHIRT: shirt.color = 'black'
        glasses.glass_mode = 'emit'
        glasses.plastic_mode = 'emit'
        glasses.plastic_color = (0, 0, 0, 1)
        glasses.glass_color = (glasses.glass_opacity, glasses.glass_opacity, glasses.glass_opacity, 1)
        glasses.glass_opacity = 1
        utils.render_sequence(path, 'glasses_glass_mask', 'BW', frame_start, frame_stop, -prewarm_frame_count)

        glasses.glass_color = (0, 0, 0, 1)
        glasses.plastic_color = (1, 1, 1, 1)
        utils.render_sequence(path, 'glasses_frame_mask', 'BW', frame_start, frame_stop, -prewarm_frame_count)

        glasses.color = 'black'
        glasses.position = (200000, 0, 0)

    if SHIRT:
        if GLASSES: glasses.color = 'black'
        shirt.color = 'white'
        render_mask_sequence(character, path, 'clothes_mask', 'black', 0)
        shirt.color = 'black'

    poly_hair.set_white_hair()
    human.hair.set_black_hair('beard')
    human.hair.set_black_hair('mustache')
    render_mask_sequence(character, path, 'hair_mask', 'black', 0)

    poly_hair.set_black_hair()
    human.hair.set_black_hair('beard')
    human.hair.set_white_hair('mustache')
    render_mask_sequence(character, path, 'mustache_mask', 'black', 0)

    poly_hair.set_black_hair()
    human.hair.set_white_hair('beard')
    human.hair.set_black_hair('mustache')
    render_mask_sequence(character, path, 'beard_mask', 'black', 0)

    poly_hair.set_black_hair()
    human.hair.set_black_hair('beard')
    human.hair.set_black_hair('mustache')
    utils.unhide(white_sphere)
    render_mask_sequence(character, path, 'background_mask', 'black', 0)
    utils.hide(white_sphere)
    render_mask_sequence(character, path, 'arms_mask', 'arms_mask', 0)
    render_mask_sequence(character, path, 'wrist_mask', 'hand_mask_male' if gender else 'hand_mask_female', 0)
    #crunch
    character.material_slots[0].material = bpy.data.materials.get('black')
    render_mask_sequence(character, path, 'eyelashes_mask', 'eyelashes_mask', 3)
    character.material_slots[3].material = bpy.data.materials.get('eyelashes')
    render_mask_sequence(character, path, 'face_mask', 'face_mask', 0)
    render_mask_sequence(character, path, 'nose_mask', 'nose_mask', 0)
    render_mask_sequence(character, path, 'ears_mask', 'ears_mask', 0)
    render_mask_sequence(character, path, 'lip_mask', 'lip_mask', 0)
    render_mask_sequence(character, path, 'brows_mask', 'brows_mask', 0)
    render_mask_sequence(character, path, 'wrinkles_mask', 'wrinkles_mask', 0)
    render_mask_sequence(character, path, 'eyebags_mask', 'eyebags_mask', 0)
    render_mask_sequence(character, path, 'neck_mask', 'neck_mask', 0)
    render_mask_sequence(character, path, 'teeth_mask', 'teeth_mask', 0)
    render_mask_sequence(character, path, 'gum_mask', 'gum_mask', 0)
    render_mask_sequence(character, path, 'tongue_mask', 'tongue_mask', 0)
    render_mask_sequence(character, path, 'scalp_mask', 'scalp_mask', 0)
    render_mask_sequence(character, path, 'lid_mask', 'lid_mask', 0)
    render_mask_sequence(character, path, 'skin_mask', 'skin_mask', 0)

    character.material_slots[0].material = bpy.data.materials.get('black')
    character.material_slots[2].material = bpy.data.materials.get('transparent')
    render_mask_sequence(character, path, 'sclera_mask', 'sclera_mask', 1)
    render_mask_sequence(character, path, 'iris_mask', 'iris_mask', 1)
    render_mask_sequence(character, path, 'pupil_mask', 'pupil_mask', 1)

    head_loc, head_rot, head_scale = human.dump_head_meta()
    with open((path / 'head_meta.json'), 'w') as h:
        head_meta = dict()
        head_meta['location'] = {'x': head_loc.x, 'y': head_loc.y, 'z': head_loc.z}
        head_meta['quaternion'] = {'w': head_rot.w, 'x': head_rot.x, 'y': head_rot.y, 'z': head_rot.z}
        head_meta['scale'] = {'x': head_scale.x, 'y': head_scale.y, 'z': head_scale.z}
        json.dump(head_meta, h, indent=4)

    dump_additional_meta(path, frames_meta)
    landmarks_helper.write_device_info(path)

    utils.deselect_all()
    utils.delete_object(armature)
    utils.delete_object(character)

    if GLASSES:
        glasses.destroy()

    if SHIRT:
        shirt.position = (200000, 0, 0)

    utils.gc()

def job(race=None, sequence_path=None, character_config=None):
    """
    Actual quantum of work. When calling from Blender
    UI run_from_blender will iterate over this function.
    """
    landmarks_list = [landmarks_helper.get_landmarks_86('woman'), landmarks_helper.get_landmarks_86('man')]
    landmark_classes_list = [landmarks_helper.get_landmark_classes('woman'), landmarks_helper.get_landmark_classes('man')]
    facebox_list = [facebox.get_facebox_indices('woman'), facebox.get_facebox_indices('man')]
    sequence_path = sequence_path or path_utils.build_seq_path(MODULE_NAME)
    race = race or random.choice(('f_ca01', 'm_ca01'))
    gender = 0 if race.startswith('f') else 1

    start_time = time.time()
    module_logger.info('[START] Iteration %s. Measuring time', sequence_path)
    run(gender, race, character_config, Path(sequence_path), landmarks_list[gender], landmark_classes_list[gender], facebox_list[gender])
    stop_time = time.time()
    time_spent = stop_time - start_time
    module_logger.info('[STOP] Iteration %s. Time spent %s', sequence_path, time_spent)

def run_from_blender(iterations):
    """
    Function for calling this module from Blender UI
    and iterating over it till the end of time.
    """
    for i in range(iterations):
        job()


if __name__ == "__main__":
    if '--sequence_path' in sys.argv:
        index = sys.argv.index('--sequence_path') + 1
        sequence_path = sys.argv[index]
    else:
        sequence_path = None

    if '--blender_char_props' in sys.argv:
        index = sys.argv.index('--blender_char_props') + 1
        with open(sys.argv[index]) as json_data:
            blender_char_props = json.load(json_data)
    else:
        blender_char_props = None

    if '--race' in sys.argv:
        index = sys.argv.index('--race') + 1
        race = sys.argv[index]
    else:
        race = None

    try:
        job(race, sequence_path, blender_char_props)
        sys.exit(0)
    except Exception:
        sys.exit(1)
