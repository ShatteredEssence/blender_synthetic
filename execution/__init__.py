import os
import sys
from pathlib import Path


try:
    from config import cfg
except ModuleNotFoundError:
    ROOT = os.getenv('BLENDER_RND_SCRIPTS')
    sys.path.append(os.fspath(ROOT))
    from config import cfg
    sys.path.remove(os.fspath(ROOT))
finally:
    PACKAGES_PATH = os.fspath(Path(cfg['Paths']['packages_path']))
    MODULE_PATH = os.fspath(Path(cfg['Paths']['module_path']))
    ACTIVE_LEARNING_CLIENTS = os.fspath(Path(cfg['ActiveLearning']['clients_repo']))
    ROOT = os.getenv('BLENDER_RND_SCRIPTS')
    if PACKAGES_PATH not in sys.path: sys.path.append(PACKAGES_PATH)
    # if MODULE_PATH not in sys.path: sys.path.append(MODULE_PATH)
    # if ROOT not in sys.path: sys.path.append(ROOT)
os.environ['PYTHONPATH'] = os.pathsep.join([os.fspath(MODULE_PATH), os.fspath(ROOT)])
sys.path.insert(0, ACTIVE_LEARNING_CLIENTS)

# os.environ['PYTHONPATH'] = os.pathsep.join([os.fspath(MODULE_PATH), os.fspath(ROOT)])
# sys.path.append(ACTIVE_LEARNING_CLIENTS)
# BLENDER_BIN = os.fspath(Path(cfg['Paths']['blender28']))
# print('> BLENDER_BIN', BLENDER_BIN)
# print('> BLENDER_RND_SCRIPTS', os.environ.get('BLENDER_RND_SCRIPTS'))
# print('> CWD          ', os.getcwd())
# print('> ROOT_PATH    ', ROOT)
# print('> PYTHONPATH   ', os.environ.get('PYTHONPATH'))
# print('> PACKAGES_PATH', PACKAGES_PATH)
# print('> MODULE_PATH  ', MODULE_PATH)
# print('> SYS.PATH     ', sys.path)
