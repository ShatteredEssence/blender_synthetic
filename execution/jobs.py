import os
import shutil
import subprocess
from pathlib import Path
from config import cfg
from itertools import zip_longest


def run_blender28(**kwargs):
    command_line = [
        os.fspath(Path(cfg['Paths']['blender28'])),
        kwargs.pop('blend_scene'),
        '--python', kwargs.pop('python')
    ]
    for argument, value in kwargs.items():
        command_line.append('--' + argument)
        command_line.append(value)

    # command_line = [
    #     os.fspath(Path(cfg['Paths']['blender28'])), blend_scene,
    #     '--python', py_script,
    #     '--sequence_path', str(sequence_path),
    #     '--blender_char_props', blender_char_props
    # ]
    rc = subprocess.Popen(command_line).wait()
    return rc

def run_unity_app(**kwargs):
    command_line = [os.fspath(Path(cfg['Paths']['unity_app']))]
    for argument, value in kwargs.items():
        command_line.append('--' + argument)
        command_line.append(value)

    rc = subprocess.Popen(command_line).wait()
    return rc

def remove_broken_file_group(folder):
    folder = Path(folder)
    frames = folder.glob('frames/frame_*')
    cam_meta = folder.glob('meta/cam_meta_*')
    char_props = folder.glob('meta/character_proporties_*')
    landmarks = folder.glob('meta/landmarks_*')
    for fr, cam, ch, lnd in zip_longest(frames, cam_meta, char_props, landmarks):
        if None in (fr, cam, ch, lnd):
            result = tuple((fr, cam, ch, lnd))
            for i in result:
                if i:
                    print('Removing {}'.format(i))
                    os.remove(i)
            return str(fr), str(cam), str(ch), str(lnd)
    return None

def remove_broken_sequence(folder):
    folder = Path(folder)
    if not list(folder.glob('deviceinfo.json')):
        print('Removing {}'.format(folder))
        shutil.rmtree(folder)
        return None
    else:
        return str(folder)

def copy_on_server_job(src_folder, dst_folder):
    ## Put actual copy code here
    return dst_folder + '////' + src_folder
