from .tasks import selfie_runner, selfie_runner_unity, train_network, move_frame, move_sequence
from .tasks import _parse_data, _train_network_no_prior
from celery import chain, group
from utils.path_utils import build_seq_path


def active_learning_blender_chain(approved_path, out_data_path, problem_name, x_validation, y_validation, iteration):
    res = chain(
        group(selfie_runner.s() for _ in range(20)),
        train_network.s(
            approved_path=approved_path,
            out_data_path=out_data_path,
            problem_name=problem_name,
            x_validation=x_validation,
            y_validation=y_validation,
            iteration=iteration
        ),
        move_sequence.s(approved_path)
    )()
    return res

def active_learning_unity_chain(experiment_name, approved_path, out_data_path, problem_name, x_validation, y_validation):
    all_sequences = ','.join([build_seq_path('selfie_runner') for _ in range(100)])
    res = chain(
        selfie_runner_unity.s(sequence_path=all_sequences),
        train_network.s(
            approved_path=approved_path,
            out_data_path=out_data_path,
            problem_name=problem_name,
            x_validation=x_validation,
            y_validation=y_validation,
            experiment_name=experiment_name
        ),
        move_frame.s(approved_path)
    )()
    return res
