import os
import shutil
import glob
from .celery import app
from .jobs import run_blender28, run_unity_app, remove_broken_file_group, remove_broken_sequence, copy_on_server_job
from pathlib import Path
from utils import path_utils
# from training_pipeline.network_training import main
# from clients.synthetic_parser.synthetic_parser import parse_data
# from syntetic_pipeline.network_trainer import do_train, predict, predict_without_prior, load_train_data

ROOT = os.getenv('BLENDER_RND_SCRIPTS')

# @app.task(queue='cleaning')
# def copy_on_server(src_folder_list, dst_folder):
#     result = [copy_on_server_job(src, dst_folder) for src in src_folder_list]
#     return result

@app.task(queue='cleaning')
def eye_generator_cleaner(folder_path):
    result = remove_broken_file_group(folder_path)
    return result

@app.task(bind=True, queue='cleaning')
def selfie_runner_cleaner(self, folder_path):
    if not Path(folder_path).is_dir():
        return None
    result = remove_broken_sequence(folder_path)
    return result

@app.task(bind=True)
def eye_generator(self, sequence_path='', char_proporties=''):
    """
    ### sequence_path = ''
    Path will be generated automatically in Blender into your output_path directory.
    But will not be returned, so it will not be possible to use it in workflow
    in cleaning task for example.
    ### sequence_path set manually
    Generated files will be saved into this folder
    """
    rc = run_blender28(
        blend_scene=str(Path(ROOT, 'blend', 'selfie_runner.blend')),
        py_script=str(Path(ROOT, 'eye_generator', 'eye_generator.py')),
        sequence_path=sequence_path,
        blender_char_props=char_proporties
    )
    return rc, sequence_path

@app.task(bind=True)
def tongue_generator(self, sequence_path=''):
    """
    ### sequence_path = ''
    Path will be generated automatically in Blender into your output_path directory.
    But will not be returned, so it will not be possible to use it in workflow
    in cleaning task for example.
    ### sequence_path set manually
    Generated files will be saved into this folder
    """
    rc = run_blender28(
        blend_scene=str(Path(ROOT, 'blend', 'selfie_runner.blend')),
        py_script=str(Path(ROOT, 'selfie_runner', 'selfie_runner_tongue.py')),
        sequence_path=sequence_path
    )
    return rc, sequence_path

"""
@app.task(bind=True)
def _parse_data(self, data_to_parse, problem_name, formula, max_count, out_data_path):
    X, Y, frames_list = parse_data(data_to_parse, problem_name, formula, max_count, out_data_path)
    return X, Y, frames_list

@app.task(bind=True)
def _train_network_no_prior(pipe_result, x_validation, y_validation, out_data_path, experiment_name):
    X, Y = pipe_result[0], pipe_result[1]
    model = do_train(X, Y, x_validation, y_validation, out_data_path, experiment_name)
    return model

@app.task(bind=True)
def train_network(self, in_data, approved_path, out_data_path, problem_name, x_validation, y_validation, experiment_name):
    X, Y, frames_list = parse_data(data_to_parse=approved_path, problem_name=problem_name, formula="*/frame/*.png",
                                   max_count=8*1024, out_data_path=out_data_path)
    model = do_train(X, Y, x_validation, y_validation, out_data_path, experiment_name)
    _X, _Y, _frames_list = parse_data(data_to_parse=in_data[0].parent, problem_name=problem_name,
                                      formula="*/frame/*.png", max_count=-1, out_data_path=out_data_path)
    acc_list = predict_without_prior(model, _X, _Y)
    os.remove(X)
    os.remove(Y)
    os.remove(_X)
    os.remove(_Y)
    return acc_list, _frames_list

@app.task(bind=True)
def train_network_no_prior_pipeline(in_data, data_to_parse, problem_name, formula, max_count,
                                    out_data_path, x_validation, y_validation, experiment_name):
    X, Y, frames_list = parse_data(data_to_parse, problem_name, formula, max_count, out_data_path)
    model = do_train(X, Y, x_validation, y_validation, out_data_path, experiment_name)
    _X, _Y, _frames_list = parse_data(in_data[0].parent, problem_name, formula, max_count, out_data_path)
    acc_list = predict_without_prior(model, _X, _Y)
    os.remove(X)
    os.remove(Y)
    os.remove(_X)
    os.remove(_Y)
    return acc_list, _frames_list

@app.task(bind=True)
def move_sequence(self, result, approved_data_path, val=0.96):
    moved = 0
    if not Path(approved_data_path).exists():
        os.mkdir(approved_data_path)
    for acc, f in zip(result[0], result[1]):
        if acc < val:
            moved += 1
            shutil.move(str(Path(f).parent.parent), approved_data_path)
        else:
            shutil.rmtree(str(Path(f).parent.parent))
    return moved, len(result[0])

@app.task(bind=True)
def move_frame(self, result, approved_data_path, val=0.955):
    moved = 0
    if not Path(approved_data_path).exists():
        os.mkdir(approved_data_path)
    for acc, f in zip(result[0], result[1]):
        if acc < val:
            moved += 1

            seq_path = Path(f).parent.parent
            seq_id = Path(f).parent.parent.name
            dest_seq_folder = Path(approved_data_path, seq_id)
            csv_path = str(f).replace('frame', 'landmarks', 1).replace('.png', '.csv')
            masks = glob.glob(str(f).replace('frame', '*_mask', 1))

            if not dest_seq_folder.exists():
                os.mkdir(str(dest_seq_folder))
                for i in [x for x in os.listdir(str(seq_path))]:
                    os.mkdir(str(Path(dest_seq_folder, i)))

            shutil.copy(Path(f), Path(dest_seq_folder, 'frame'))
            shutil.copy(Path(csv_path), Path(dest_seq_folder, 'landmarks'))
            for mask in masks:
                shutil.copy(Path(mask), Path(dest_seq_folder, Path(mask).parent.name))

    shutil.rmtree(str(Path(result[1][0]).parent.parent.parent))
    return moved, len(result[0])
"""

@app.task(bind=True)
def card_generator(self, *agrs, **kwargs):
    cmd = {
        'blend_scene': str(Path(ROOT, 'blend', 'card_generator.blend')),
        'python': str(Path(ROOT, 'card_generator', 'card_generator.py'))
    }
    cmd.update(kwargs)
    rc = run_blender28(**cmd)
    return rc, kwargs.get('sequence_path')

@app.task(bind=True)
def selfie_runner(self, *agrs, **kwargs):
    cmd = {
        'blend_scene': str(Path(ROOT, 'blend', 'selfie_runner.blend')),
        'python': str(Path(ROOT, 'selfie_runner', 'selfie_runner.py'))
    }
    sequence_path = kwargs.get('sequence_path') or path_utils.build_seq_path('selfie_runner')
    cmd.update(kwargs)
    cmd['sequence_path'] = sequence_path
    run_blender28(**cmd)
    return Path(sequence_path)

@app.task(bind=True, queue='blender')
def selfie_runner_unity(self, *agrs, **kwargs):
    cmd = dict()
    sequence_path = kwargs.get('sequence_path') or path_utils.build_seq_path('selfie_runner')
    cmd.update(kwargs)
    cmd['sequence_path'] = sequence_path
    run_unity_app(**cmd)
    return list(map(Path, sequence_path.split(',')))

@app.task(bind=True)
def selfie_runner_frx_learner(self, *agrs, **kwargs):
    cmd = {
        'blend_scene': str(Path(ROOT, 'blend', 'selfie_runner.blend')),
        'python': str(Path(ROOT, 'selfie_runner', 'selfie_runner_frx_learner.py'))
    }
    cmd.update(kwargs)
    rc = run_blender28(**cmd)
    return rc, kwargs.get('sequence_path')

@app.task(bind=True)
def selfie_runner_gesture(self, *agrs, **kwargs):
    cmd = {
        'blend_scene': str(Path(ROOT, 'blend', 'selfie_runner.blend')),
        'python': str(Path(ROOT, 'selfie_runner', 'selfie_runner_gesture.py'))
    }
    cmd.update(kwargs)
    rc = run_blender28(**cmd)
    return rc, kwargs.get('sequence_path')

@app.task(bind=True)
def selfie_runner_ssd_train(self, *agrs, **kwargs):
    cmd = {
        'blend_scene': str(Path(ROOT, 'blend', 'selfie_runner.blend')),
        'python': str(Path(ROOT, 'selfie_runner', 'selfie_runner_ssd_train.py'))
    }
    cmd.update(kwargs)
    rc = run_blender28(**cmd)
    return rc, kwargs.get('sequence_path')

@app.task(bind=True)
def selfie_runner_faceonly(self, *agrs, **kwargs):
    cmd = {
        'blend_scene': str(Path(ROOT, 'blend', 'selfie_runner.blend')),
        'python': str(Path(ROOT, 'selfie_runner', 'selfie_runner_faceOnly.py'))
    }
    cmd.update(kwargs)
    rc = run_blender28(**cmd)
    return rc, kwargs.get('sequence_path')

@app.task(bind=True)
def hand_generator(self, *agrs, **kwargs):
    cmd = {
        'blend_scene': str(Path(ROOT, 'blend', 'hand_generator.blend')),
        'python': str(Path(ROOT, 'hand_runner', 'hands.py'))
    }
    cmd.update(kwargs)
    rc = run_blender28(**cmd)
    return rc, kwargs.get('sequence_path')

@app.task(bind=True)
def pose_setter(self, *agrs, **kwargs):
    cmd = {
        'blend_scene': str(Path(ROOT, 'blend', 'selfie_runner.blend')),
        'python': str(Path(ROOT, 'pose_setter', 'pose_setter.py'))
    }
    cmd.update(kwargs)
    rc = run_blender28(**cmd)
    return rc, kwargs.get('sequence_path')
