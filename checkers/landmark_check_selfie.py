import sys
import imageio
import cv2
import matplotlib.pyplot as plt
import argparse
from pathlib import Path


a = [[0,255,0], 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
b = [[0,0,255], 17,18,19,20,21]
c = [[255,255,0], 22,23,24,25,26]
d = [[0,255,255], 27,28,29,30,31,32,33,34,35,82,83,84,85]
e = [[255,0,255], 36,37,38,39,40,41]
f = [[0,128,255], 42,43,44,45,46,47]
g = [[128,0,255], 48,49,50,51,52,53,54,55,56,57,58,59]
h = [[128,128,0], 60,61,62,63,64,65,66,67]
i = [[0,128,128], 68,69,70,71]
k = [[128,0,128], 72,73,74,75]
l = [[250,120,160], 76,77,78,79,80,81]

def main(frame_path, radius, save_on_disk):
    frame_data_raw = imageio.imread(frame_path)
    frame_data_show = frame_data_raw.copy()
    frame_data_show_b = frame_data_raw.copy()
    skeleton_2d_path = frame_path.replace('frame', 'landmarks', 1).replace('.png', '.csv')
    skeleton_2d_path_bari = frame_path.replace('frame', 'landmarks_barimetric', 1).replace('.png', '.csv')

    with open(skeleton_2d_path, 'r') as f, open(skeleton_2d_path_bari, 'r') as ff:
        for line in f.readlines():
            color = [0,0,0]
            index = float(line.split('\t')[0])
            for points in (a,b,c,d,e,f,g,h,i,k,l):
                if index in points:
                    color = points[0]
                x = float(line.split('\t')[1])
                y = float(line.split('\t')[2])
                cv2.circle(frame_data_show, (int(x), int(y)), int(radius), color, -1)
        for line in ff.readlines():
            color = [0, 0, 0]
            index = float(line.split('\t')[0])
            for points in (a, b, c, d, e, f, g, h, i, k, l):
                if index in points:
                    color = points[0]
                x = float(line.split('\t')[1])
                y = float(line.split('\t')[2])
                cv2.circle(frame_data_show_b, (int(x), int(y)), int(radius), color, -1)

    plt.imshow(frame_data_show)
    plt.show()

    if save_on_disk:
        frame = Path(frame_path)
        output = frame.parent / '{0}.checked{1}'.format(frame.stem, frame.suffix)
        imageio.imwrite(output, frame_data_show)
        output = frame.parent / '{0}.checked_bari{1}'.format(frame.stem, frame.suffix)
        imageio.imwrite(output, frame_data_show_b)


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--frame", required=True, help="Path to the frame")
    ap.add_argument("-r", "--radius", type=int, help="Radius of a point")
    ap.add_argument("-s", "--save", type=bool, default=False, help="Save file on disk")
    args = vars(ap.parse_args())

    frame_path = args['frame']
    radius = args['radius'] if args['radius'] else 3
    save_on_disk = args.get('save')

    main(frame_path, radius, save_on_disk)
