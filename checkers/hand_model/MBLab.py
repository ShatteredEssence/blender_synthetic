#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon May 21 11:18:26 2018

@author: dk
"""

import json
import numpy as np

import itertools

import transformations as tf


class mblData:
    
    def __init__(self,mbl_base_dir):
        file_verts = mbl_base_dir+'data/vertices/f_ca01_verts.json'
        file_morphs = mbl_base_dir+'data/morphs/f_ca01_morphs.json'
        file_expressions = mbl_base_dir+'data/expressions_morphs/f_ca01_exprs.json'
        
        file_morphs1 = mbl_base_dir+'data/morphs/human_female_morphs.json'
        file_morphs2 = mbl_base_dir+'data/morphs/human_female_morphs_extra.json'
        
        file_transf = mbl_base_dir+'data/transformations/human_female_base_transf.json'
        
        file_bbox = mbl_base_dir+'data/bboxes/human_female_bbox.json'
        file_measures = mbl_base_dir+'data/measures/human_female_measures.json'
        
        file_joints = mbl_base_dir+'data/joints/human_female_joints.json'
        file_joints_offset = mbl_base_dir+'data/joints/human_female_joints_offset.json'
        file_vgroups_base = mbl_base_dir+'data/vgroups/human_female_vgroups_base.json'
        file_vgroups_muscles = mbl_base_dir+'data/vgroups/human_female_vgroups_muscles.json'
        
        file_retarget_knowledge = mbl_base_dir+'data/retarget_knowledge.json'
        
        with open(file_verts, 'r') as f:
            self.verts = np.array(json.load(f))
        with open(file_morphs, 'r') as f:
            self.morphs = json.load(f)
        with open(file_expressions, 'r') as f:
            self.expressions = json.load(f)
        
        with open(file_morphs1, 'r') as f:
            self.morphs1 = json.load(f)
        with open(file_morphs2, 'r') as f:
            self.morphs2 = json.load(f)
        
        with open(file_transf, 'r') as f:
            self.transf = json.load(f)
        
        with open(file_bbox, 'r') as f:
            self.bbox = json.load(f)
        with open(file_measures, 'r') as f:
            self.measures = json.load(f)
            
        with open(file_joints, 'r') as f:
            self.joints = json.load(f)
        with open(file_joints_offset, 'r') as f:
            self.joints_offset = json.load(f)
        with open(file_vgroups_base, 'r') as f:
            self.vgroups = json.load(f)
        with open(file_vgroups_muscles, 'r') as f:
            self.vgroups_muscles = json.load(f)
            
        with open(file_retarget_knowledge, 'r') as f:
            self.retarget_knowledge = json.load(f)
            
        return

#%%  REPROJECTOR

def getBlenderMVP(cam_meta, imHeight=1.0, imWidth=1.0):

    MV = np.eye(4)
        
    fov = cam_meta['fov']
        
    qW = cam_meta['quaternion_w']
    qX = cam_meta['quaternion_x']
    qY = cam_meta['quaternion_y']
    qZ = cam_meta['quaternion_z']
    
    cam_posX = cam_meta['location_x']
    cam_posY = cam_meta['location_y']
    cam_posZ = cam_meta['location_z']
    
    halfFovMaxItcs = np.tan(np.deg2rad(fov * 0.5))

    imgDimMax = np.max([imHeight,imWidth])
    aspect = np.array([imWidth/imgDimMax, imHeight/imgDimMax])

    halfFovItcs = aspect * halfFovMaxItcs

    rot = tf.quaternion_inverse(np.array([qW,qX,qY,qZ]))
    
#    rot = tf.quaternion_from_euler(-rX, -rY, -rZ, axes='sxyz')
    
    rotM = tf.quaternion_matrix(rot)[:3,:3]

    affineTrans = np.array([-cam_posX, -cam_posY, -cam_posZ])

    MV1 = np.eye(4)
    MV1[:3,:3] = rotM
#    MV1 = tf.quaternion_matrix(rot)

    MV2 = np.eye(4)
    MV2[:3,3] = affineTrans
    
    MV = np.dot(MV1,MV2)

#    P = np.eye(4)
    P = np.zeros((4,4))
    P[0,0] = 1 / halfFovItcs[0]
    P[1,1] = 1 / halfFovItcs[1]
    P[2,3] = 1
    P[3,2] = -1
    
    return np.dot(P,MV)


def project(modelPoints,MVP, xlim=[-1.0,1.0], ylim=[-1.0,1.0]):
    halfx = 0.5*(xlim[1]-xlim[0])
    halfy = 0.5*(ylim[1]-ylim[0])
    xmn = np.mean(xlim)
    ymn = np.mean(ylim)
    sz = modelPoints.shape
    projectedShape = np.zeros((sz[0],2))
    for ii in range(sz[0]):
        mp = np.ones(4)
        mp[:3] = modelPoints[ii,:]
        tmp = np.dot(MVP, mp)
        projectedShape[ii,0] = xmn + halfx*(tmp[0] / tmp[3])
        projectedShape[ii,1] = ymn + halfy*(tmp[1] / tmp[3])
    return projectedShape


#%% Morphs & Expressions

def function_modifier_a(val_x):
    val_y = 0.0
    if val_x > 0.5:
        val_y = 2*val_x-1
    return val_y

def function_modifier_b(val_x):
    val_y = 0.0
    if val_x < 0.5:
        val_y = 1-2*val_x
    return val_y
    
def linear_factor_to_morph(linear_factor):
    morph_value = 0.5 + linear_factor
    return morph_value

def calculate_transformation(tr_data, tr_id, tr_factor):

    if tr_factor >= 0:
        transformation_2 = tr_factor
        transformation_1 = 0
    else:
        transformation_2 = 0
        transformation_1 = -tr_factor

    tr_morphs = {}
    if tr_id in tr_data:
        for prop in tr_data[tr_id]:
            linear_factor = prop[1]*transformation_1 + prop[2]*transformation_2
            tr_morphs[prop[0]] = linear_factor_to_morph(linear_factor)
    
    return tr_morphs

def smart_combo(prefix, morph_values):

    tags = []
    names = []
    weights = []
    max_morph_values = []

    #Compute the combinations and get the max values
    for v_data in morph_values:
        tags.append(["max", "min"])
        max_morph_values.append(max(v_data))
    for n_data in itertools.product(*tags):
        names.append(prefix+"_"+'-'.join(n_data))

    #Compute the weight of each combination
    for n_data in itertools.product(*morph_values):
        weights.append(sum(n_data))

    factor = max(max_morph_values)
    best_val = max(weights)
    toll = 1.5

    #Filter on bestval and calculate the normalize factor
    summ = 0.0
    for i in range(len(weights)):
        weights[i] = max(0, weights[i]-best_val/toll)
        summ += weights[i]

    #Normalize using summ
    if summ != 0:
        for i in range(len(weights)):
            weights[i] = factor*(weights[i]/summ)

    return (names, weights)

def applyMorphs(verts,morphs,face_params):
    mrphs = face_params.keys()
    for mx in mrphs:
        prefix = mx
        morph_values = []
        val = face_params[mx]
        morph_values.append([function_modifier_a(val),function_modifier_b(val)])
        
        mxs = smart_combo(prefix,morph_values)
        
        for m_idx,m_name in enumerate(mxs[0]):
            if m_name in morphs:
                dat = morphs[m_name]
                for dt in dat:
                    verts[dt[0],0] += dt[1] * mxs[1][m_idx]
                    verts[dt[0],1] += dt[2] * mxs[1][m_idx]
                    verts[dt[0],2] += dt[3] * mxs[1][m_idx]
#            else:
#                print('WARNING! Morph '+m_name+' does not exist')
            
    # Smart Combo Stub for v1.6.1
    if ('Cheeks_Mass' in face_params) and ('Cheeks_Tone' in face_params):
        prefix = 'Cheeks_Mass-Tone'
        morph_values = []
        val1 = face_params['Cheeks_Mass']
        val2 = face_params['Cheeks_Tone']
        morph_values.append([function_modifier_a(val1),function_modifier_b(val1)])
        morph_values.append([function_modifier_a(val2),function_modifier_b(val2)])
        mxs = smart_combo(prefix,morph_values)
        
        for m_idx,m_name in enumerate(mxs[0]):
            if m_name in morphs:
                dat = morphs[m_name]
                for dt in dat:
                    verts[dt[0],0] += dt[1] * mxs[1][m_idx]
                    verts[dt[0],1] += dt[2] * mxs[1][m_idx]
                    verts[dt[0],2] += dt[3] * mxs[1][m_idx]
    return

def applyExpressions(verts,expressions,expr_params):
    exprs = expr_params.keys()
    for ex in exprs:
        prefix = ex
        exp_values = []
        val = expr_params[ex]
        exp_values.append([function_modifier_a(val),function_modifier_b(val)])
        
        exs = smart_combo(prefix,exp_values)
        
        for e_idx,e_name in enumerate(exs[0]):
            if e_name in expressions:
                dat = expressions[e_name]
                for dt in dat:
                    verts[dt[0],0] += dt[1] * exs[1][e_idx]
                    verts[dt[0],1] += dt[2] * exs[1][e_idx]
                    verts[dt[0],2] += dt[3] * exs[1][e_idx]
        
    return

def applyTransformation(verts, morphs, tr_data, tr_id, tr_factor):
    mrf = calculate_transformation(tr_data, tr_id, tr_factor)
#    print(mrf)
    applyMorphs(verts,morphs,mrf)
    return


#%%
class ObjLoader(object):
    def __init__(self, fileName):
        self.vertices = []
        self.faces = []
        ##
        try:
            f = open(fileName)
            for line in f:
                if line[:2] == "v ":
                    index1 = line.find(" ") + 1
                    index2 = line.find(" ", index1 + 1)
                    index3 = line.find(" ", index2 + 1)

                    vertex = (float(line[index1:index2]), float(line[index2:index3]), float(line[index3:-1]))
                    vertex = (round(vertex[0], 2), round(vertex[1], 2), round(vertex[2], 2))
                    self.vertices.append(vertex)

                elif line[0] == "f":
                    string = line.replace("//", "/")
                    ##
                    i = string.find(" ") + 1
                    face = []
                    for item in range(string.count(" ")):
                        if string.find(" ", i) == -1:
                            face.append(string[i:-1])
                            break
                        face.append(string[i:string.find(" ", i)])
                        i = string.find(" ", i) + 1
                    ##
                    self.faces.append(tuple(face))

            f.close()
        except IOError:
            print(".obj file not found.")


