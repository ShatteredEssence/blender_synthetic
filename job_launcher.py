import sys
from execution.tasks import *
from utils.path_utils import build_seq_path


def launch_selfie_runner(iteration_count):
    for i in range(iteration_count):
        p = build_seq_path('selfie_runner')
        ch = chain(selfie_runner.si(p), selfie_runner_cleaner.si(p))
        ch.apply_async()
    print('SUBMITTED [selfie_runner -> selfie_runner_cleaner] x {}'.format(iteration_count))


if __name__ == "__main__":
    iteration_count = int(sys.argv[1])
    launch_selfie_runner(iteration_count)
