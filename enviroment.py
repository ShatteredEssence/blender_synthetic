import bpy
import os
import glob
import math
import random
from random import choice


def hdr_lister(hdr_path):
    hdr_list = []
    os.chdir(hdr_path)
    hdr_list = glob.glob('*.hdr')
    return (hdr_list)


def randomise_enviroment(hdr_path):
#get hdr list
    hdr_list = hdr_lister(hdr_path)
    
# load enviroment
    enviroment_node = bpy.data.worlds['World'].node_tree
    background = enviroment_node.nodes['Environment Texture']
    
# swap bg image
    file_enviroment_image = hdr_path + choice(hdr_list)
    bg_image = bpy.data.images.load(file_enviroment_image)
    background.image = bg_image
    
# reset map rotation
    enviroment_node.nodes['Mapping.001'].rotation[2] = 0
    
# randomise	
    env_rotation_offset = random.uniform(0, 365)
    enviroment_node.nodes['Mapping.001'].rotation[2] = env_rotation_offset
    env_brightness_offset = random.uniform(2, 3)
    enviroment_node.nodes["Background"].inputs[1].default_value = env_brightness_offset
    enviroment_node.nodes["Hue Saturation Value"].inputs[0].default_value = random.uniform(0.4, 0.6)
    enviroment_node.nodes["Hue Saturation Value"].inputs[1].default_value = random.uniform(0.5, 1.0)
    enviroment_node.nodes["Hue Saturation Value"].inputs[2].default_value = random.uniform(0.8, 1.0)

'''
def randomise_sun(sun, energy_factor=1):
    min = 0.1
    max = 0.5
    rand_energy = energy_factor * random.uniform(min, max)
    sun.data.energy = rand_energy

    rx = math.radians(random.uniform(0, 360))
    ry = math.radians(random.uniform(0, 360))
    rz = math.radians(random.uniform(0, 360))
    sun.rotation_euler = (rx, ry, rz)

    cr = random.uniform(0.8, 1)
    cg = random.uniform(0.8, 1)
    cb = random.uniform(0.8, 1)
    sun.data.color = (cr, cg, cb)
'''

def randomise_sun(armature, sun, energy_factor=1):
    min = 0.3
    max = 0.6
    rand_energy = energy_factor * random.uniform(min, max)
    sun.data.energy = rand_energy

    rotation_target = bpy.data.objects['sun_light_rotation']
    armature.data.layers[0] = True
    armature.data.layers[1] = True
    rotation_target.select_set(action='SELECT')
    armature.select_set(action='SELECT')
    bpy.context.view_layer.objects.active = armature
    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='DESELECT')
    arm = bpy.data.armatures["MBLab_skeleton_base_ik"]
    arm.bones.active = arm.bones["head"]
    bpy.context.object.data.bones["head"].select = True
    head_loc = armature.location + bpy.context.active_pose_bone.head

    rotation_target.location = (head_loc[0], head_loc[1], head_loc[2])

    bpy.ops.pose.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')

    rx = math.radians(random.uniform(0, 360))
    ry = math.radians(random.uniform(0, 360))
    rz = math.radians(random.uniform(0, 360))
    rotation_target.rotation_euler = (rx, ry, rz)

    cr = random.uniform(0.6, 1)
    cg = random.uniform(0.6, 1)
    cb = random.uniform(0.6, 1)
    sun.data.color = (cr, cg, cb)
    sun.data.size = random.uniform(0.1, 0.5)
    sun.data.size_y = random.uniform(0.1, 0.5)