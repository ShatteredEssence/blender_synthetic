import os
import shutil
import sys
from pathlib import Path

#Edit path to a folder containing date. Not the root /card_generator/
#path_to_clean = 'D:/!work/lemezator/autorunner/card_generator/2019.02.24/'


def remove_broken_sequence(folder):
    folder = Path(folder)
    if not list(folder.glob('frame.png')):
        print('Removing {}'.format(folder))
        shutil.rmtree(folder)
        return None
    else:
        return str(folder)

def credit_card_cleaner(data_dir_for_inspect):
    path = Path(data_dir_for_inspect)
    d, r = 0, 0
    for i in path.iterdir():
        d += 1
        res = remove_broken_sequence(i)
        if not res:
            r += 1
    l = 100/d*(d-r)
    print('{0} inspected | {1} removred | {2:.4}% left'.format(d, r, l))

credit_card_cleaner(path_to_clean)