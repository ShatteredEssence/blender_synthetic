import bpy
import sys
from utils import path_utils
from config import cfg
from pathlib import Path
import random
from string import ascii_lowercase
from string import ascii_uppercase
import glob
from pathlib import Path
from math import pi
from bpy_extras.object_utils import world_to_camera_view
import datetime
import uuid
import json
import os
from math import pi
from random import choice
import math

data_path = 'D:/!work/synthetic_generators/blender_synthetic/data/credit_card/'
output_path = 'D:/!work/lemezator/autorunner/card_generator/'
hdr_path = 'D:/!work/HDRIs/'

card_number = bpy.data.objects['card_number']
card_holder_name = bpy.data.objects['card_holder_name']
expiry_date = bpy.data.objects['expiry_date']
sun = bpy.data.objects['Sun']

def randomise_sun(sun, energy_factor=1):
    min = 0.1
    max = 1.0
    rand_energy = energy_factor * random.uniform(min, max)
    sun.data.energy = rand_energy

    rx = math.radians(random.uniform(0, 360))
    ry = math.radians(random.uniform(0, 360))
    rz = math.radians(random.uniform(0, 360))
    sun.rotation_euler = (rx, ry, rz)

    cr = random.uniform(0.8, 1)
    cg = random.uniform(0.8, 1)
    cb = random.uniform(0.8, 1)
    sun.data.color = (cr, cg, cb)

def hdr_lister(hdr_path):
    hdr_list = []
    os.chdir(hdr_path)
    hdr_list = glob.glob('*.hdr')
    return (hdr_list)

def randomise_enviroment(hdr_path):
#get hdr list
    hdr_list = hdr_lister(hdr_path)
    
# load enviroment
    enviroment_node = bpy.data.worlds['World'].node_tree
    background = enviroment_node.nodes['Environment Texture']
    
# swap bg image
    file_enviroment_image = hdr_path + choice(hdr_list)
    bg_image = bpy.data.images.load(file_enviroment_image)
    background.image = bg_image
    
# reset map rotation
    enviroment_node.nodes['Mapping'].rotation[2] = 0
    
# randomise	
    env_rotation_offset = random.uniform(0, 365)
    enviroment_node.nodes['Mapping'].rotation[2] = env_rotation_offset
    env_brightness_offset = random.uniform(2, 3)
    enviroment_node.nodes["Background"].inputs[1].default_value = env_brightness_offset
    enviroment_node.nodes["Hue Saturation Value"].inputs[0].default_value = random.uniform(0.4, 0.6)
    enviroment_node.nodes["Hue Saturation Value"].inputs[1].default_value = random.uniform(0.5, 1.0)
    enviroment_node.nodes["Hue Saturation Value"].inputs[2].default_value = random.uniform(0.8, 1.0)


def randomise_card():
    #name and digits
    digit_block_00 = ''.join([str(random.randint(0, 9)) for i in range(4)])
    digit_block_01 = ''.join([str(random.randint(0, 9)) for i in range(4)])
    digit_block_02 = ''.join([str(random.randint(0, 9)) for i in range(4)])
    digit_block_03 = ''.join([str(random.randint(0, 9)) for i in range(4)])
    
    card_number.data.body = str(digit_block_00) + ' ' + str(digit_block_01) + ' ' + str(digit_block_02) + ' ' + str(digit_block_03)
    
    name_length = random.randint(4, 9)
    name = "".join(random.choice(ascii_uppercase) for i in range(name_length))
    
    sname_length = random.randint(4, 9)
    sname = "".join(random.choice(ascii_uppercase) for i in range(name_length))
    
    card_holder_name.data.body = name + ' ' + sname

    expiry_date_month = random.randint(0, 9)
    exexpiry_date_year = random.randint(10, 30)
    expiry_date.data.body = (str("{:02d}".format(expiry_date_month))) + '/' + str(exexpiry_date_year)
    
    #materials
    #BG
    credit_background = bpy.data.materials['credit_background']
    nodes = credit_background.node_tree.nodes
    
    textures_path = Path(data_path) / 'backbround/'
    card_background_textures = list(textures_path.glob('*.png'))
    card_background_texture = random.choice(card_background_textures)
    
    hue_node = nodes.get('Hue Saturation Value')
    hue_node.inputs[0].default_value = random.uniform(0, 1)
    hue_node.inputs[1].default_value = random.uniform(0.8, 1)
    hue_node.inputs[2].default_value = random.uniform(0.5, 1)
    
    mapping_node = nodes.get('Mapping')
    mapping_node.scale[0] = random.choice([-1, 1])
    mapping_node.scale[1] = random.choice([-1, 1])


    texture_node = nodes.get('card_bg')
    texture_node.image = bpy.data.images.load(str(card_background_texture))
    
    #Logo
    credit_card_type_logo = bpy.data.materials['credit_card_type_logo']
    nodes = credit_card_type_logo.node_tree.nodes
    
    textures_path = Path(data_path) / 'card_type_logos/'
    card_background_textures = list(textures_path.glob('*.png'))
    card_background_texture = random.choice(card_background_textures)
    
    texture_node = nodes.get('logo')
    texture_node.image = bpy.data.images.load(str(card_background_texture))
    
    #Enviroment
    randomise_enviroment(hdr_path)
    randomise_sun(sun, 1)
    
    #cam random
    camera_holder = bpy.data.objects['camera_holder']
    camera = bpy.data.objects['Camera']
    camera_holder.rotation_euler = random.uniform(-15, 15)*pi/180, random.uniform(-15, 15)*pi/180, random.uniform(-45, 45)*pi/180
    
    return { 
        'number': card_number.data.body, 
        'name': name,
        'sname': sname, 
        'expiry_date_month': expiry_date_month,
        'expiry_date_year': exexpiry_date_year
    }
    
def get_camera_meta(camera):
    cam_loc, cam_rot, cam_scale = camera.matrix_world.decompose()
    data = {}
    data['fov'] = bpy.data.cameras[camera.name].angle * 180 / pi
    data['location_x'] = cam_loc.x
    data['location_y'] = cam_loc.y
    data['location_z'] = cam_loc.z
    data['quaternion_w'] = cam_rot.w
    data['quaternion_x'] = cam_rot.x
    data['quaternion_y'] = cam_rot.y
    data['quaternion_z'] = cam_rot.z
    return data

def get_coord_dict(co):
    return { 'x': co.x, 'y': co.y, 'z': co.z }

def get_points(camera, object):
    coords = [v.co for v in object.to_mesh(apply_modifiers=True, depsgraph=bpy.context.depsgraph).vertices]
    points3d = {}
    points2d = {}
    for index, co in enumerate(coords):
        co3d = object.matrix_world * co
        co2d = world_to_camera_view(bpy.context.scene, camera, co3d)
        points3d[index] = get_coord_dict(co3d)
        points2d[index] = get_coord_dict(co2d)
    return points3d, points2d

def makedirs(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)

def build_seq_path():
    now = datetime.datetime.now()
    date = now.strftime("%Y.%m.%d")
    seq_id = str(uuid.uuid1())
    sequence_path = Path(output_path) / date / seq_id
    return str(sequence_path)

def render(path):
    bpy.context.scene.render.filepath = str(path)
    bpy.ops.render.opengl(write_still=True, view_context=False)

def dump(path, data):
    with open(path, 'w') as f:
        json.dump(data, f, indent=4)

def run():
    seq_path = build_seq_path()
    makedirs(seq_path)
    
    card_info = randomise_card()
    camera = bpy.data.objects['Camera']
    card_points3d, card_points2d = get_points(camera, bpy.data.objects['card_bound'])
    logo_points3d, logo_points2d = get_points(camera, bpy.data.objects['logo_mesh'])
    camera_meta = get_camera_meta(camera)
    
    card_meta = {}
    card_meta['card_info'] = card_info
    card_meta['card_points3d'] = card_points3d
    card_meta['card_points2d'] = card_points2d
    card_meta['logo_points3d'] = logo_points3d
    card_meta['logo_points2d'] = logo_points2d
    
    dump(seq_path + '/camera_meta.json', camera_meta)
    dump(seq_path + '/card_meta.json', card_meta)
    render(seq_path + '/frame.png')

def job(sequence_path=None):
    #sequence_path = sequence_path or path_utils.build_seq_path(MODULE_NAME)
    run()

def run_from_blender(iterations):
    for i in range(iterations):
        job()


if __name__ == "__main__":
    if '--sequence_path' in sys.argv:
        index = sys.argv.index('--sequence_path') + 1
        sequence_path = sys.argv[index]
    else:
        sequence_path = None

    try:
        for i in range(20):
            print('iter', i)
            job(sequence_path)
        sys.exit(0)
    except Exception:
        sys.exit(1)
