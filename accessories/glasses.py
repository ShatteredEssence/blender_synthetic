import bpy
import bmesh
import mathutils
import random
from utils.blender_processor import delete_object, append_objects_from_file
from pathlib import Path
from .accessory import Accessory


GLASS_BLEND_SCENE = str(Path(__file__).parent / 'blend/glasses_set.blend')
GLASS_META = {
    'glass01': {
        'size': 22.04,
        'geo': ['glass01_main', 'glass01_ear_l', 'glass01_ear_r', 'glass01_track_l', 'glass01_track_r',
                'glass01_track_l_dummy', 'glass01_track_r_dummy']
    },
    'glass02': {
        'size': 21.76,
        'geo': ['glass02_main', 'glass02_ear_l', 'glass02_ear_r', 'glass02_track_l', 'glass02_track_r']
    },
    'glass03': {
        'size': 23.61,
        'geo': ['glass03_main', 'glass03_ear_l', 'glass03_ear_r', 'glass03_track_l', 'glass03_track_r',
                'glass03_track_l_dummy', 'glass03_track_r_dummy']
    }
}

class Glasses(Accessory):
    def __init__(self):
        super().__init__()
        self.root = self.__create()

    def __create(self):
        self.glasses_to_create = random.choice(list(GLASS_META.values()))
        self.appended_parts = append_objects_from_file(filepath=GLASS_BLEND_SCENE, object_names=self.glasses_to_create['geo'])
        root_obj_list = [x for x in self.appended_parts if x.name.split('.')[0].endswith('_main')]
        if len(root_obj_list) != 1:
            self.destroy()
            raise Exception('Has to be exactly 1 _main part')
        return root_obj_list[0]

    @property
    def ear_tracks(self):
        track_l_name, track_r_name = self.glasses_to_create['geo'][3], self.glasses_to_create['geo'][4]
        track_l_object, track_r_object = bpy.data.objects[track_l_name], bpy.data.objects[track_r_name]
        return track_l_object, track_r_object

    @property
    def size(self):
        return self.glasses_to_create['size']

    def transform_apply(self, location, rotation, scale):
        bpy.ops.object.transform_apply(location=location, rotation=rotation, scale=scale)

    @property
    def plastic_color(self):
        plastic_mtl = bpy.data.materials['glasses_plastic_mtl']
        plastic_node = plastic_mtl.node_tree.nodes.get('plastic_bsdf')
        return plastic_node.inputs[0].default_value

    @plastic_color.setter
    def plastic_color(self, color):
        plastic_mtl = bpy.data.materials['glasses_plastic_mtl']
        plastic_node = plastic_mtl.node_tree.nodes.get('plastic_bsdf')
        plastic_node.inputs[0].default_value = color
        emission_node = plastic_mtl.node_tree.nodes.get('mask_emission')
        emission_node.inputs[0].default_value = color

    @property
    def plastic_mode(self):
        plastic_mtl = bpy.data.materials['glasses_plastic_mtl']
        plastic_mix_node = plastic_mtl.node_tree.nodes.get('rgb_to_mask_mix')
        return plastic_mix_node.inputs[0].default_value

    @plastic_mode.setter
    def plastic_mode(self, mode):
        plastic_mtl = bpy.data.materials['glasses_plastic_mtl']
        plastic_mix_node = plastic_mtl.node_tree.nodes.get('rgb_to_mask_mix')
        plastic_mix_node.inputs[0].default_value = 0 if mode == 'rgb' else 1

    @property
    def glass_mode(self):
        glass_mtl = bpy.data.materials['glasses_glass_mtl']
        glass_mix_node = glass_mtl.node_tree.nodes.get('rgb_to_mask_mix')
        return glass_mix_node.outputs[0].default_value

    @glass_mode.setter
    def glass_mode(self, mode):
        glass_mtl = bpy.data.materials['glasses_glass_mtl']
        glass_mix_node = glass_mtl.node_tree.nodes.get('rgb_to_mask_mix')
        glass_mix_node.outputs[0].default_value = 0 if mode == 'rgb' else 1

    @property
    def glass_color(self):
        glass_mtl = bpy.data.materials['glasses_glass_mtl']
        glass_node = glass_mtl.node_tree.nodes.get('glass_bsdf')
        return glass_node.inputs[0].default_value

    @glass_color.setter
    def glass_color(self, color):
        glass_mtl = bpy.data.materials['glasses_glass_mtl']
        glass_node = glass_mtl.node_tree.nodes.get('glass_bsdf')
        glass_node.inputs[0].default_value = color
        emission_node = glass_mtl.node_tree.nodes.get('mask_emission')
        emission_node.inputs[0].default_value = color

    @property
    def glass_opacity(self):
        glass_mtl = bpy.data.materials['glasses_glass_mtl']
        opacity_node = glass_mtl.node_tree.nodes.get('opacity')
        return opacity_node.outputs[0].default_value

    @glass_opacity.setter
    def glass_opacity(self, opacity):
        glass_mtl = bpy.data.materials['glasses_glass_mtl']
        opacity_node = glass_mtl.node_tree.nodes.get('opacity')
        opacity_node.outputs[0].default_value = opacity

    def set_black(self):
        self.plastic_mode = 'emit'
        self.plastic_color = (0, 0, 0, 1)
        self.glass_mode = 'emit'
        self.glass_color = (0, 0, 0, 1)
        self.glass_opacity = 1
        # plastic_mtl, glass_mtl = bpy.data.materials['glasses_plastic_mtl'], bpy.data.materials['glasses_glass_mtl']
        # emission_node = plastic_mtl.node_tree.nodes.get('mask_emission')
        # emission_node.inputs[0].default_value = (0, 0, 0, 1)
        # plastic_mix_node = plastic_mtl.node_tree.nodes.get('rgb_to_mask_mix')
        # plastic_mix_node.inputs[0].default_value = 1
        # glass_mix_node = glass_mtl.node_tree.nodes.get('rgb_to_mask_mix')
        # glass_mix_node.outputs[0].default_value = 1
        # opacity_node = glass_mtl.node_tree.nodes.get('opacity')
        # opacity_node.outputs[0].default_value = 1
        # emission_node = glass_mtl.node_tree.nodes.get('mask_emission')
        # emission_node.inputs[0].default_value = (0, 0, 0, 1)

    def set_white(self):
        self.plastic_mode = 'emit'
        self.plastic_color = (1, 1, 1, 1)
        self.glass_mode = 'emit'
        self.glass_color = (1, 1, 1, 1)
        self.glass_opacity = 1
        # plastic_mtl, glass_mtl = bpy.data.materials['glasses_plastic_mtl'], bpy.data.materials['glasses_glass_mtl']
        # emission_node = plastic_mtl.node_tree.nodes.get('mask_emission')
        # emission_node.inputs[0].default_value = (1, 1, 1, 1)
        # plastic_mix_node = plastic_mtl.node_tree.nodes.get('rgb_to_mask_mix')
        # plastic_mix_node.inputs[0].default_value = 1
        # glass_mix_node = glass_mtl.node_tree.nodes.get('rgb_to_mask_mix')
        # glass_mix_node.outputs[0].default_value = 1
        # opacity_node = glass_mtl.node_tree.nodes.get('opacity')
        # opacity_node.outputs[0].default_value = 1
        # emission_node = glass_mtl.node_tree.nodes.get('mask_emission')
        # emission_node.inputs[0].default_value = (1, 1, 1, 1)

    def randomize(self):
        self.plastic_mode = 'rgb'
        self.plastic_color = (random.uniform(0, 1), random.uniform(0, 1), random.uniform(0, 1), 1)
        self.glass_mode = 'rgb'
        self.glass_color = (random.uniform(0, 1), random.uniform(0, 1), random.uniform(0, 1), 1)
        self.glass_opacity = random.uniform(0.1, 0)
        # plastic_mtl, glass_mtl = bpy.data.materials['glasses_plastic_mtl'], bpy.data.materials['glasses_glass_mtl']
        # glass_node = glass_mtl.node_tree.nodes.get('glass_bsdf')
        # glass_node.inputs[0].default_value[0] = random.uniform(0, 1)
        # glass_node.inputs[0].default_value[1] = random.uniform(0, 1)
        # glass_node.inputs[0].default_value[2] = random.uniform(0, 1)
        # glass_mix_node = glass_mtl.node_tree.nodes.get('rgb_to_mask_mix')
        # glass_mix_node.outputs[0].default_value = 0
        # opacity_node = glass_mtl.node_tree.nodes.get('opacity')
        # opacity_node.outputs[0].default_value = random.uniform(0.05, 0)
        # plastic_node = plastic_mtl.node_tree.nodes.get('plastic_bsdf')
        # plastic_mix_node = plastic_mtl.node_tree.nodes.get('rgb_to_mask_mix')
        # plastic_mix_node.inputs[0].default_value = 0
        # plastic_node.inputs[0].default_value[0] = random.uniform(0, 1)
        # plastic_node.inputs[0].default_value[1] = random.uniform(0, 1)
        # plastic_node.inputs[0].default_value[2] = random.uniform(0, 1)

    def destroy(self):
        map(delete_object, self.appended_parts)
