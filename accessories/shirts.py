import bpy
import random
import mathutils
from utils import materials
from config import cfg
from pathlib import Path
from utils import blender_utils
from .accessory import Accessory


class Shirt(Accessory):

    def __init__(self, gender=None):
        super().__init__()
        self.root = self.__create(gender)

    def __create(self, gender):
        male_shirts = ['male_shirt_01', 'male_shirt_02', 'male_shirt_03']
        female_shirts = ['female_shirt_01', 'female_shirt_02', 'female_shirt_03']
        male_shirt_to_create = bpy.data.objects[random.choice(list(male_shirts))]
        female_shirt_to_create = bpy.data.objects[random.choice(list(female_shirts))]
        return male_shirt_to_create if gender else female_shirt_to_create

    def randomize(self, character):
        data_path = Path(cfg['Paths']['data_path'])
        cloth_textures = materials.load_cloth_maps()
        texture = random.choice(cloth_textures)

        blender_utils.deselect_all()
        material = bpy.data.materials.get('ClothMaterial')
        blender_utils.select(self.root)
        blender_utils.set_active(self.root)
        for i in range(0, len(bpy.context.active_object.material_slots)):
            bpy.ops.object.material_slot_remove()

        self.root.data.materials.append(material)
        blender_utils.deselect_all()

        nodes = material.node_tree.nodes
        texture_node = nodes.get('ClothTexture')
        texture_node.image = bpy.data.images.load(str(texture))
        mapping_node = nodes.get('Mapping')
        mapping_node.scale[0] = random.uniform(3, 5)
        mapping_node.scale[1] = random.uniform(3, 5)
        mapping_node.scale[2] = random.uniform(3, 5)
        mapping_node.rotation[0] = random.uniform(0, 360)
        hue_node = nodes.get('Hue Saturation Value')
        hue_node.inputs[0].default_value = random.uniform(0, 1)
        hue_node.inputs[1].default_value = random.uniform(0, 1)
        hue_node.inputs[2].default_value = random.uniform(0.5, 2)

        self.root.modifiers.new("cloth_shrimkwrap", 'SHRINKWRAP')
        self.root.modifiers['cloth_shrimkwrap'].target = character
        self.root.modifiers['cloth_shrimkwrap'].offset = random.uniform(0.01, 0.02)

    def set_rgb(self):
        material = bpy.data.materials['ClothMaterial']
        nodes = material.node_tree.nodes
        switch_node = nodes.get('MaterialSwitcher')
        switch_node.inputs[0].default_value = 1.0

    def set_black(self):
        material = bpy.data.materials['ClothMaterial']
        nodes = material.node_tree.nodes
        switch_node = nodes.get('MaterialSwitcher')
        color_node = nodes.get('ClothColor')
        switch_node.inputs[0].default_value = 0.0
        color_node.inputs[1].default_value = 0.0

    def set_white(self):
        material = bpy.data.materials['ClothMaterial']
        nodes = material.node_tree.nodes
        switch_node = nodes.get('MaterialSwitcher')
        color_node = nodes.get('ClothColor')
        switch_node.inputs[0].default_value = 0.0
        color_node.inputs[1].default_value = 1.0
