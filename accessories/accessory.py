import bpy
import bmesh
import mathutils


class Accessory(object):
    def __init__(self):
        self.__color = None
        self.root = None

    def __create(self):
        raise NotImplemented

    @property
    def position(self):
        return self.root.location

    @position.setter
    def position(self, position):
        if isinstance(position, mathutils.Vector):
            self.root.location = position
            return self.root.location
        vector = mathutils.Vector(position)
        self.root.location = vector
        return vector

    @property
    def scale(self):
        return self.root.scale

    @scale.setter
    def scale(self, value):
        if isinstance(value, float) or isinstance(value, int):
            vector = mathutils.Vector((value, value, value))
            self.root.scale = vector
            return vector
        elif isinstance(value, mathutils.Vector):
            self.root.scale = value
            return self.root.scale
        else:
            vector = mathutils.Vector(value)
            self.root.scale = vector
            return vector

    def set_rgb(self):
        raise NotImplemented

    def set_black(self):
        raise NotImplemented

    def set_white(self):
        raise NotImplemented

    def randomize(self):
        raise NotImplemented

    def destroy(self):
        raise NotImplemented

    @property
    def color(self):
        return self.__color

    @color.setter
    def color(self, color):
        if color == 'black':
            self.__color = 'black'
            self.set_black()
        if color == 'white':
            self.__color = 'white'
            self.set_white()
        if color == 'rgb':
            self.__color = 'rgb'
            self.set_rgb()
