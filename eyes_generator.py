import bpy
import bmesh
import generator_ik as g
import path_config
import random
import enviroment as e
import datetime
import numpy as np
from bpy_extras.object_utils import world_to_camera_view
import math
import os
import json
import shutil
from pathlib import Path
from config import cfg


gender_iterations = 4000
model_iterations = 4
face_iterations = 2
frame_count = 8
resolution_x = 1024
resolution_y = 1024
# parent_path, hdr_path, datapath = path_config.get_eyes_config()

MODULE_NAME = 'prop_generator'
hdr_path = str(Path(cfg['Paths']['hdri_path']))
datapath = str(Path(cfg['Paths']['data_path']))
parent_path = str(Path(cfg['Paths']['output_path']) / MODULE_NAME)

landmarks_out_folder = 'landmarks'
landmarks_json_out_folder = 'landmarks_json'

landmark_info = {
    "nose_class": 0,
    "left_eyes_class": 1,
    "right_eyes_class": 2,
    "left_eye_brows_class": 3,
    "right_eye_brows_class": 4,
    "mouth_class": 5,
    "nasolabial_folds_class": 6,
    "contour_class": 7,
}

expression_morphs = [
    'Expressions_browOutVertL',
    'Expressions_browOutVertR',
    'Expressions_browSqueezeL',
    'Expressions_browSqueezeR',
    'Expressions_browsMidVert',
    'Expressions_cheekSneerL',
    'Expressions_cheekSneerR',
    'Expressions_eyeSquintL',
    'Expressions_eyeSquintR',
    'Expressions_eyesHoriz',
    'Expressions_eyesVert'
]

face_morphs = [
    'Face_Ellipsoid',
    'Face_Parallelepiped',
    'Face_Triangle',
    'Eyes_BagProminence',
    'Eyes_BagSize',
    'Eyes_InnerPosX',
    'Eyes_InnerPosZ',
    'Eyes_IrisSize',
    'Eyes_OuterPosX',
    'Eyes_InnerPosZ',
    'Eyes_PosX',
    'Eyes_PosZ',
    'Eyes_Size',
    'Eyes_SizeZ',
    'Eyes_TypeAlmond',
    'Eyes_TypeHooded',
    'Eyes_innerSinus',
    'Eyebrows_Angle',
    'Eyebrows_Droop',
    'Eyebrows_PosZ',
    'Eyebrows_Ridge',
    'Eyebrows_SizeY',
    'Eyebrows_Tone',
    'Eyelids_Angle',
    'Eyelids_Crease',
    'Eyelids_InnerPosZ',
    'Eyelids_LowerCurve',
    'Eyelids_MiddlePosZ',
    'Eyelids_OuterPosZ',
    'Eyelids_SizeZ',
    'Nose_BridgeSizeX',
    'Nose_Curve'
]

mouth_expression_morphs = [
    'Expressions_mouthOpen',
    'Expressions_mouthSmileOpen',
    'Expressions_mouthOpenO',
    'Expressions_mouthOpenLarge',
    'Expressions_mouthSmile',
    'Expressions_mouthChew',
    'Expressions_jawOut',
    'Expressions_jawHoriz',
    'Expressions_mouthBite',
    'Expressions_tongueOut',
    'Expressions_tongueOutPressure',
    'Expressions_tongueHoriz',
    'Expressions_tongueVert',
    'Expressions_tongueTipUp'
]

def read_json(filepath):
    file = open(filepath, 'r')
    data = eval(file.read())
    file.close()
    return data

def load_json(filepath):
    with open(filepath, 'r') as f:
        data = json.load(f)
    return data

def create_vertex_group(object, indices, name):
    vertex_group = object.vertex_groups.new(name=name)
    vertex_group.add(indices, 1.0, 'ADD')
    return vertex_group

def get_texture_pack(texture_name):
    postfix = texture_name[-7:]
    return { 
        'face': datapath + 'textures/classes/face' + postfix, 
        'lip': datapath + 'textures/classes/lip' + postfix, 
        'diffuse': datapath + 'textures/diffuse/' + texture_name 
    }

def load_diffuse_textures():
    diffuse_folder = datapath + 'textures/diffuse/'
    return [get_texture_pack(name) for name in os.listdir(diffuse_folder)]

def get_eyes_indices(prefix):
    inner = read_json('{}json/{}_inner_eyes_indices.json'.format(datapath, prefix))
    outer = read_json('{}json/{}_outer_eyes_indices.json'.format(datapath, prefix))
    eyelashes = read_json('{}json/{}_eyelashes_indices.json'.format(datapath, prefix))
    return { 'inner': inner, 'outer': outer, 'eyelashes': eyelashes }

def get_landmarks(prefix):
    return read_json('{}json/{}_landmarks.json'.format(datapath, prefix))

def get_landmark_classes(prefix):
    return load_json('{}json/{}_landmark_classes.json'.format(datapath, prefix))

def delete_object(object):
    select(object)
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.delete(use_global=True)
    bpy.ops.object.select_all(action='DESELECT')

def select(obj):
    obj.select_set(action='SELECT')

def deselect(obj):
    obj.select_set(action='DESELECT')

def set_active(obj):
    bpy.context.view_layer.objects.active = obj

def hide(obj):
    obj.location[0] += 200000

def unhide(obj):
    obj.location[0] -= 200000

def init_camera(armature, distance, vertical_offset):
    track_target = bpy.data.objects['track_target']
    rotation_target = bpy.data.objects['rotation_target']
    camera = bpy.data.objects["Camera"]
    armature.data.layers[0] = True
    armature.data.layers[1] = True
    select(track_target)
    select(armature)
    set_active(armature)
    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='DESELECT')
    arm = bpy.data.armatures["MBLab_skeleton_base_ik"]
    arm.bones.active = arm.bones["head"]
    bpy.context.object.data.bones["head"].select = True
    head_loc = armature.location + bpy.context.active_pose_bone.head

    track_target.location = (head_loc[0], head_loc[1], head_loc[2] + vertical_offset)
    rotation_target.location = track_target.location
    camera.location = [0, -distance, 0]

    bpy.ops.pose.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')

def create_camera_animation(max_horizontal_rotation, max_vertical_rotation):
    rotation_target = bpy.data.objects['rotation_target']

    horizontal_rotation = random.uniform(-max_horizontal_rotation, max_horizontal_rotation)
    vertical_rotation = random.uniform(-max_vertical_rotation, max_vertical_rotation)
    rotation_target.rotation_euler = [vertical_rotation, 0, horizontal_rotation]
    rotation_target.keyframe_insert(data_path='rotation_euler', frame=0)

    horizontal_rotation = random.uniform(-max_horizontal_rotation, max_horizontal_rotation)
    vertical_rotation = random.uniform(-max_vertical_rotation, max_vertical_rotation)
    rotation_target.rotation_euler = [vertical_rotation, 0, horizontal_rotation]
    rotation_target.keyframe_insert(data_path='rotation_euler', frame=frame_count)

def create_expressions_animation(character):
    randomise_character(character, expression_morphs, 1.6, False)
    randomise_eyes_position(character)
    for morph in expression_morphs:
        character.keyframe_insert(data_path=morph, frame = 0)

    randomise_character(character, expression_morphs, 1.6, False)
    randomise_eyes_position(character)
    for morph in expression_morphs:
        character.keyframe_insert(data_path=morph, frame = frame_count)

def create_mouth_animation(character):
    keyframes = [0, frame_count / 2, frame_count]
    for keyframe in keyframes:

        open_type = random.randint(0, 3)
        if open_type == 1:
            randomise_attr(character, 'Expressions_mouthOpen', 0.6, 0.8)
            randomise_attr(character, 'Expressions_mouthSmileOpen', 0.4, 0.6)
        elif open_type == 2:
            randomise_attr(character, 'Expressions_mouthOpenO', 0.6, 0.8)
            randomise_attr(character, 'Expressions_mouthSmileOpen', 0.4, 0.6)
        elif open_type == 3:
            randomise_attr(character, 'Expressions_mouthOpenLarge', 0.6, 0.8)
            randomise_attr(character, 'Expressions_mouthSmileOpen', 0.4, 0.6)
        else:
            randomise_attr(character, 'Expressions_mouthSmile', 0.3, 0.7)
        
        tongue_out = random.randint(0, 1) and open_type > 0
        if tongue_out:
            randomise_attr(character, 'Expressions_tongueOut', 0.75, 0.9)
            randomise_attr(character, 'Expressions_tongueVert', 0.6, 0.8)
            randomise_attr(character, 'Expressions_tongueTipUp', 0.2, 0.8)

        for morph in mouth_expression_morphs:
            character.keyframe_insert(data_path=morph, frame = keyframe)

def get_character(gender):
    if gender == 0:
        character = bpy.context.scene.objects["f_ca01"]
        armature = bpy.data.objects["f_ca01_skeleton"]

    if gender == 1:
        character = bpy.context.scene.objects["m_ca01"]
        armature = bpy.data.objects["m_ca01_skeleton"]

    return character, armature

def randomise_attr(character, attr, min, max):
    if not hasattr(character, attr):
        raise Exception("Object doesnt have attribute '{}'".format(attr))
    setattr(character, attr, random.uniform(min, max))

def randomise_character(character, morphs, factor, update=True):
    min = 0.4
    max = 0.6
    min = min / factor
    max = max * factor
    
    if min <= 0:
        min = 0
    if max >= 1:
        max = 1

    for morph in morphs:
        randomise_attr(character, morph, min, max)

    if update:
        bpy.context.scene.mblab_show_measures = False

def get_render_path(folderpath, subfolderpath, iteration):
    return '{}/{}/frame_{:07}.png'.format(folderpath, subfolderpath, iteration)

def render_scene(folderpath, subfolderpath, iteration, format):
    bpy.context.scene.render.image_settings.color_mode = format
    bpy.context.scene.render.filepath = get_render_path(folderpath, subfolderpath, iteration)
    bpy.ops.render.opengl(write_still=True, view_context = False)

def set_vertex_group_material(object, vertex_group_name, material):
    bpy.ops.object.vertex_group_set_active(group=vertex_group_name)
    bpy.ops.object.material_slot_add()
    object.material_slots[object.material_slots.__len__() - 1].material = material
    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.select_all(action='DESELECT')
    bpy.ops.object.vertex_group_select()
    bpy.ops.object.material_slot_assign()
    bpy.ops.object.editmode_toggle()

def set_character_material(character, material_name):
    material = bpy.data.materials.get(material_name)

    select(character)
    set_active(character)
    for i in range(0, len(bpy.context.active_object.material_slots)):
        bpy.ops.object.material_slot_remove()

    character.data.materials.append(material)

def create_vertex_groups(character, eyes_indices):
    create_vertex_group(character, eyes_indices['inner'], 'Inner Eyes')
    create_vertex_group(character, eyes_indices['outer'], 'Outer Eyes')
    create_vertex_group(character, eyes_indices['eyelashes'], 'Eyelashes')

def set_character_materials(character):
    set_character_material(character, 'human_eevee_skin_shader')

    inner_eye_material = bpy.data.materials.get('inner_eye_shader')
    outer_eye_material = bpy.data.materials.get('outer_eye_shader')
    eyelashes_material = bpy.data.materials.get('human_eyelid')

    set_vertex_group_material(character, 'Inner Eyes', inner_eye_material)
    set_vertex_group_material(character, 'Outer Eyes', outer_eye_material)
    set_vertex_group_material(character, 'Eyelashes', eyelashes_material)

def close_eye(character, side):
    setattr(character, 'Expressions_eyeClosed' + side, 0.9)
    setattr(character, 'Expressions_eyeClosedPressure' + side, 0.5)
    bpy.context.scene.mblab_show_measures = False

def open_eye(character, side, pressure):
    setattr(character, 'Expressions_eyeClosed' + side, 0.5)
    setattr(character, 'Expressions_eyeClosedPressure' + side, pressure)
    bpy.context.scene.mblab_show_measures = False

def randomise_eyes_position(character):
    setattr(character, 'Expressions_eyesHoriz', random.uniform(0.1, 0.9))
    setattr(character, 'Expressions_eyesVert', random.uniform(0.1, 0.9))

def close_eyes(character):
    pressure = random.uniform(0.3, 0.55)
    choice = np.random.choice([0, 1, 2, 3], p=[0.7, 0.1, 0.1, 0.1])
    if choice == 1:
        close_eye(character, 'L')
    elif choice == 2:
        close_eye(character, 'R')
    elif choice == 3:
        close_eye(character, 'L')
        close_eye(character, 'R')
    else:
        open_eye(character, 'L', pressure)
        open_eye(character, 'R', pressure)

def set_background_strength(value):
    enviroment_node = bpy.data.worlds['World'].node_tree
    enviroment_node.nodes["Background"].inputs[1].default_value = value

def get_background_strength():
    enviroment_node = bpy.data.worlds['World'].node_tree
    return enviroment_node.nodes["Background"].inputs[1].default_value

def set_white_background():
    unhide(bpy.data.objects['WhiteSphere'])

def unset_white_background():
    hide(bpy.data.objects['WhiteSphere'])

def write_device_info(folderpath):
    now = datetime.datetime.now()
    date_now = str(now.day) + '.' + '{:02}'.format(now.month) + '.' + str(now.year)
    data = {
        "date": date_now,
        "version": "1.1",
        "source": "blender",
        "OS": "blender",
    }
    data.update(landmark_info)
    with open((folderpath + 'deviceinfo.json'), 'w') as file:
        json.dump(data, file)

def write_landmarks(folderpath, character, landmarks, landmark_classes, iteration):
    render = bpy.context.scene.render
    vertices = [vertex.co for vertex in character.data.vertices]
    camera = bpy.data.objects["Camera"]
    coords_2d = np.array([world_to_camera_view(bpy.context.scene, camera, coordinate) for coordinate in vertices])

    with open('{}/{}/frame_{:07}.csv'.format(folderpath, landmarks_out_folder, iteration), 'w') as landmarks_file:
        counter = 0
        landmark_data = dict()
        for landmark in landmarks:
            landmark_class_num = -1
            for landmark_class, verts_list in landmark_classes.items():
                if landmark in verts_list:
                    landmark_class_num = landmark_info[landmark_class]
                    break

            x, y = coords_2d[landmark][0], coords_2d[landmark][1]
            line = str(counter) + '\t' + str(render.resolution_x * x) + \
                   '\t' + str(resolution_y - render.resolution_y * y) + '\t' + str(landmark_class_num) + '\n'
            landmarks_file.write(line)

            vert_data = dict(
                resolution_x=render.resolution_x * x,
                resolution_y=resolution_y - render.resolution_y * y,
                landmark_class=landmark_class_num
            )
            landmark_data[counter] = vert_data

            counter += 1

        with open('{}/{}/frame_landmarks_{:07}.json'.format(folderpath, landmarks_json_out_folder, iteration), 'w') as landmarks_json_file:
            json.dump(landmark_data, landmarks_json_file, indent=4)

def set_texture(texture, material, node_name):
    texture = bpy.data.images.load(texture)
    nodes = material.node_tree.nodes
    node = nodes.get(node_name)
    node.image = texture

def set_random_skin_hsv(material):
    nodes = material.node_tree.nodes
    node = nodes.get('SkinHSV')
    node.inputs[0].default_value = random.uniform(0.45, 0.55)
    node.inputs[1].default_value = random.uniform(0.8, 1.0)
    node.inputs[2].default_value = random.uniform(0.8, 1.0)

def set_random_eye_hsv(material):
    nodes = material.node_tree.nodes
    node = nodes.get('InnerEyeHSV')
    node.inputs[0].default_value = random.uniform(0.2, 0.8)
    node.inputs[1].default_value = random.uniform(0.8, 1.0)
    node.inputs[2].default_value = random.uniform(0.8, 1.0)

def save_dummy_image(folderpath, subfolder, iteration):
    src_path = datapath + 'textures/util/dummy.png'
    dst_path = '{}/{}/frame_{}.png'.format(folderpath, subfolder, iteration)
    shutil.copy(src_path, dst_path)

def render_character_frames(folderpath, iteration, gender, eyes_indices, landmarks, landmark_classes, textures):
    g.gc()
    g.init_human(gender)
    character, armature = get_character(gender)

    e.randomise_enviroment(hdr_path)
    e.randomise_sun(armature, sun=bpy.data.objects['Sun'], energy_factor=1)

    outer_eye_material = bpy.data.materials.get('outer_eye_shader')
    skin_material = bpy.data.materials.get('human_eevee_skin_shader')
    inner_eye_material = bpy.data.materials.get('inner_eye_shader')
    transparent_material = bpy.data.materials.get('transparent')
    face_material = bpy.data.materials.get('face_mask')
    lip_material = bpy.data.materials.get('lip_mask')

    create_vertex_groups(character, eyes_indices)

    for mi in range(0, model_iterations):
        randomise_attr(character, 'character_age', 0.2, 0.8)
        randomise_attr(character, 'character_mass', -0.5, 0.5)
        randomise_attr(character, 'character_tone', 0.2, 0.5)
        randomise_character(character, face_morphs, 1.1)

        for fi in range(0, face_iterations):
            close_eyes(character)

            texture_pack = random.choice(textures)
            set_texture(texture_pack['diffuse'], skin_material, 'SkinTexture')
            set_texture(texture_pack['diffuse'], inner_eye_material, 'InnerEyeTexture')
            set_texture(texture_pack['face'], face_material, 'FaceMaskTexture')
            set_texture(texture_pack['lip'], lip_material, 'LipMaskTexture')

            set_random_skin_hsv(skin_material)
            set_random_eye_hsv(inner_eye_material)
            e.randomise_enviroment(hdr_path)

            initial_strength = get_background_strength()
            init_camera(armature, 0.3, 0.005)
            create_camera_animation(math.pi / 5, math.pi / 10)
            create_expressions_animation(character)
            create_mouth_animation(character)

            sequence_path = '{}sequence_{:07}/'.format(folderpath, iteration)

            g.makedirs(sequence_path + landmarks_json_out_folder + '/')
            g.makedirs(sequence_path + landmarks_out_folder + '/')
            g.makedirs(sequence_path + 'glasses_frame_class/')
            g.makedirs(sequence_path + 'glasses_glass_class/')

            for frame in range(0, frame_count):
                bpy.context.scene.frame_set(frame)
                select(character)
                bpy.context.scene.mblab_show_measures = False

                set_background_strength(initial_strength)
                set_character_materials(character)
                render_scene(sequence_path, 'frame', frame, 'RGB')

                set_background_strength(0)

                set_character_material(character, 'face_mask')
                set_vertex_group_material(character, 'Outer Eyes', outer_eye_material)
                set_vertex_group_material(character, 'Eyelashes', transparent_material)
                render_scene(sequence_path, 'face_class', frame, 'BW')

                set_character_material(character, 'sclera_mask')
                set_vertex_group_material(character, 'Outer Eyes', outer_eye_material)
                set_vertex_group_material(character, 'Eyelashes', transparent_material)
                render_scene(sequence_path, 'sclera_class', frame, 'BW')

                set_character_material(character, 'iris_mask')
                set_vertex_group_material(character, 'Outer Eyes', outer_eye_material)
                set_vertex_group_material(character, 'Eyelashes', transparent_material)
                render_scene(sequence_path, 'iris_class', frame, 'BW')

                set_character_material(character, 'pupil_mask')
                set_vertex_group_material(character, 'Outer Eyes', outer_eye_material)
                set_vertex_group_material(character, 'Eyelashes', transparent_material)
                render_scene(sequence_path, 'pupil_class', frame, 'BW')

                set_character_material(character, 'lid_mask')
                set_vertex_group_material(character, 'Outer Eyes', outer_eye_material)
                set_vertex_group_material(character, 'Eyelashes', transparent_material)
                render_scene(sequence_path, 'eyelid_class', frame, 'BW')

                set_character_material(character, 'lip_mask')
                set_vertex_group_material(character, 'Outer Eyes', outer_eye_material)
                set_vertex_group_material(character, 'Eyelashes', transparent_material)
                render_scene(sequence_path, 'lip_class', frame, 'BW')

                set_character_material(character, 'black')
                set_white_background()
                render_scene(sequence_path, 'background_class', frame, 'BW')
                unset_white_background()

                write_landmarks(sequence_path, character, landmarks, landmark_classes, frame)
                save_dummy_image(sequence_path, 'glasses_frame_class', frame)
                save_dummy_image(sequence_path, 'glasses_glass_class', frame)
                write_device_info(sequence_path)

            iteration = iteration + 1

    delete_object(character)
    return iteration

def main():
    g.gc()
    bpy.context.scene.render.resolution_x = resolution_x
    bpy.context.scene.render.resolution_y = resolution_y
    bpy.context.scene.frame_end = frame_count
    now = datetime.datetime.now()
    date = str(now.day) + '.' + '{:02}'.format(now.month) + '.' + str(now.year)
    folderpath = parent_path + date + '/'
    eyes_indices = [get_eyes_indices('woman'), get_eyes_indices('man')]
    landmarks = [get_landmarks('woman'), get_landmarks('man')]
    landmark_classes = [get_landmark_classes('woman'), get_landmark_classes('man')]
    textures = load_diffuse_textures()
    gender = random.randint(0, 1)
    iteration = 0
    for ci in range(0, gender_iterations):
        iteration = render_character_frames(folderpath, iteration, gender, eyes_indices[gender], landmarks[gender], landmark_classes[gender], textures)
        gender = int(not gender)


if __name__ == "__main__":
    main()