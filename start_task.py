from execution.tasks import *
from utils.menu_utils import ask_user_for_param, read_manual_input

TASKS_NAME_TO_TASK = {
    'selfie_runner': selfie_runner,
    'selfie_runner_unity': selfie_runner_unity,
    'selfie_runner_frx_learner': selfie_runner_frx_learner,
    'selfie_runner_gesture': selfie_runner_gesture,
    'selfie_runner_ssd_train': selfie_runner_ssd_train,
    'selfie_runner_faceonly': selfie_runner_faceonly,
}

def ask_user_for_task_count():
    return read_manual_input('Input count of tasks:', 1000)

def ask_user_for_celery_task(tasks):
    task_name = ask_user_for_param('Select celery task:', tasks)
    return TASKS_NAME_TO_TASK[task_name]

def get_existing_celery_tasks():
    return list(TASKS_NAME_TO_TASK.keys())


if __name__ == '__main__':
    chosen_task = ask_user_for_celery_task(get_existing_celery_tasks())
    chosen_count = ask_user_for_task_count()
    for _ in range(chosen_count):
        chosen_task.apply_async()

    print(f"DONE {chosen_task.name} x {chosen_count}")