"""
Script for debugging Blender module
Load this script to "Text Editor" of Blender and use it to run your module
"""
import os
import sys
import importlib
from pathlib import Path


## README
# 1)
# Before using in please add BLENDER_RND_SCRIPTS environment variable
# pointing to your repository root directory.
# 2)
# Your personal settings should be stored in <config\personal.ini>,
# just copy <config\personal.ini.example> --> <config\personal.ini>
# and polulate with your own values. They are under .gitinore, so no more overrides.
# 3)
# Enjoy it.
if not os.getenv('BLENDER_RND_SCRIPTS'):
    print('BLENDER_RND_SCRIPTS env variable not set')
    sys.exit(1)

try:
    from config import cfg
except ModuleNotFoundError:
    ROOT = os.getenv('BLENDER_RND_SCRIPTS')
    sys.path.append(os.fspath(ROOT))
    from config import cfg
    sys.path.remove(os.fspath(ROOT))
finally:
    PACKAGES_PATH = os.fspath(Path(cfg['Paths']['packages_path']))
    MODULE_PATH = os.fspath(Path(cfg['Paths']['module_path']))
    ROOT = os.getenv('BLENDER_RND_SCRIPTS')
    if PACKAGES_PATH not in sys.path: sys.path.append(PACKAGES_PATH)
    if MODULE_PATH not in sys.path: sys.path.append(MODULE_PATH)
    if ROOT not in sys.path: sys.path.append(ROOT)

## just printing values for visual control
print('> BLENDER_RND_SCRIPTS', os.environ.get('BLENDER_RND_SCRIPTS'))
print('> CWD          ', os.getcwd())
print('> ROOT_PATH    ', ROOT)
print('> PYTHONPATH   ', os.environ.get('PYTHONPATH'))
print('> PACKAGES_PATH', PACKAGES_PATH)
print('> MODULE_PATH  ', MODULE_PATH)
print('> SYS.PATH     ', sys.path)

ENABLE_DEBUG = False

## actual imports
import selfie_runner as module
importlib.reload(module)

def dbg_wait_debugger(timeout=30, secret="my_secret", address=("127.0.0.1", 3000)):
    """
    Start debugger server and wait for debugger to attach
    """
    if not ENABLE_DEBUG:
        return

    import ptvsd

    try:
        ptvsd.enable_attach(secret, address=address)
    except ptvsd.AttachAlreadyEnabledError:
        print("DEBUG: attach is already enabled")

    print("DEBUG: waiting for debugger ({}s)".format(timeout))
    ptvsd.wait_for_attach(timeout)

    if ptvsd.is_attached():
        print("DEBUG: debugger is attached")
    else:
        print("DEBUG: no debugger")

dbg_wait_debugger()
module.run_from_blender(200)
