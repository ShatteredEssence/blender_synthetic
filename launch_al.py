import time
import logging
from table_logger import TableLogger
from celery import chain, group
from execution.tasks import *
from execution.workflows import active_learning_blender_chain, active_learning_unity_chain
from utils.path_utils import build_seq_path
from pathlib import Path
from config import cfg


VALIDATION_DIR = Path(cfg['Paths']['data_path'], 'validation_data')
X_VALIDATION = VALIDATION_DIR / 'x_lip_mask_yuv_372_256_256_False.npy'
Y_VALIDATION = VALIDATION_DIR / 'y_lip_mask_yuv_372_256_256_False.npy'
PROBLEM_NAME = 'lip_mask'
TMP_DATA_PATH = 'd://3d-generated-output//selfie_runner//2019.06.04_tmp'
APPROVED_DATA_PATH = 'd://3d-generated-output//selfie_runner//2019.06.04_approved'

args = {
    'approved_path': APPROVED_DATA_PATH,
    'out_data_path': TMP_DATA_PATH,
    'problem_name': PROBLEM_NAME,
    'x_validation': X_VALIDATION,
    'y_validation': Y_VALIDATION,
}

# logging.basicConfig(level=logging.DEBUG,
#                     format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
#                     datefmt='%m-%d %H:%M',
#                     filename=Path(TMP_DATA_PATH, "log.txt"),
#                     filemode='w')

logging.basicConfig(level=logging.DEBUG,
                    format='%(message)s',
                    filename=Path(TMP_DATA_PATH, "log.csv"),
                    filemode='w')

logger = logging.getLogger('active_learning')

# render_syntetic_group = group(selfie_runner.s(sequence_path=f) for f in ff)
# res = chain(render_syntetic_group, train_network.si(s=res.get())).apply_async()

# res2 = train_network.apply_async((), dict(
#     problem_name='face_mask',
#     in_data_path=res.get(),
#     out_data_path=OUT_DATA_PATH,
#     x_validation=Path(VALIDATION_DIR, 'x_skin_mask_yuv_4081_144_256_False.npy'),
#     y_validation=Path(VALIDATION_DIR, 'y_skin_mask_yuv_4081_144_256_False.npy')
# ))

# chain(
#     train_network.s(
#         in_data_path=res.get(),
#         out_data_path=OUT_DATA_PATH,
#         problem_name='face_mask',
#         x_validation=Path(VALIDATION_DIR, 'x_skin_mask_yuv_4081_144_256_False.npy'),
#         y_validation=Path(VALIDATION_DIR, 'y_skin_mask_yuv_4081_144_256_False.npy')
#     ),
#     move_sequence.s(APPROVED_DATA_PATH)
# )()


# render_syntetic_group = group(selfie_runner.s() for s in range(5))


if __name__ == '__main__':
    logger.info(f'iteration,approved,total')
    approved_total = 0
    i = 1
    factor = 1.0
    # while factor > 0.02 and approved_total < 20000:
    while approved_total < 20000:
        experiment_name = 'lips_proir_{}'.format(i)
        result = active_learning_unity_chain(experiment_name, **args)
        while result.status != 'SUCCESS':
            time.sleep(1)
        # acc_list, _frames_list = result.get()
        # approved, total = result.children[0].get()
        approved, total = result.get()
        acc_list, _frames_list = result.parent.get()
        factor = 1.0 * approved / total
        approved_total += approved
        logger.info(f'{i},{approved},{total}')
        i += 1


