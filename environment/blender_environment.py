import bpy
import random
import logging
import json
from utils import materials
from collections import OrderedDict
from utils import blender_utils


module_logger = logging.getLogger(__name__)

class BlenderEnvironment(object):
    def __init__(self, energy_factor=1, config=None):
        self.config = config or dict()
        self.__get_random_textures()
        self.default_values = {
            'env_image': self.env_image,
            'env_rotation_offset': random.uniform(0, 365),
            'env_brightness_offset': random.uniform(0.2, 2),
            'sun_hue_saturation_value_r': random.uniform(0.4, 0.6),
            'sun_hue_saturation_value_g': random.uniform(0.5, 1.0),
            'sun_hue_saturation_value_b': random.uniform(0.8, 1.0),
            'sun_energy': energy_factor * random.uniform(0.2, 1),
            'sun_rotation_target_x': random.uniform(0, 360),
            'sun_rotation_target_y': random.uniform(0, 360),
            'sun_rotation_target_z': random.uniform(0, 360),
            'sun_color_r': random.uniform(0.6, 1),
            'sun_color_g': random.uniform(0.6, 1),
            'sun_color_b': random.uniform(0.6, 1),
            'sun_size': random.uniform(0.1, 0.5),
            'sun_size_y': random.uniform(0.1, 0.5)
        }

        for prop_name, default_value in self.default_values.items():
            config_value = self.config.get(prop_name, None)
            setattr(self, prop_name, config_value or default_value)

        self.sun = bpy.data.objects['Sun']
        self.randomise_enviroment()
        self.randomise_sun()

    def __get_random_textures(self):
        self.env_images = materials.load_hdri_maps()
        self.env_image = str(random.choice(self.env_images))

    def randomise_enviroment(self):
        module_logger.info('')
        # load enviroment
        enviroment_node = bpy.data.worlds['World'].node_tree
        background = enviroment_node.nodes['Environment Texture']
        background.image = bpy.data.images.load(self.env_image)     # swap bg image
        enviroment_node.nodes['Mapping'].rotation[2] = 0            # reset map rotation
        enviroment_node.nodes['Mapping'].rotation[2] = self.env_rotation_offset
        enviroment_node.nodes["Background"].inputs[1].default_value = self.env_brightness_offset
        enviroment_node.nodes["Hue Saturation Value"].inputs[0].default_value = self.sun_hue_saturation_value_r
        enviroment_node.nodes["Hue Saturation Value"].inputs[1].default_value = self.sun_hue_saturation_value_g
        enviroment_node.nodes["Hue Saturation Value"].inputs[2].default_value = self.sun_hue_saturation_value_b
        enviroment_node.nodes["Background"].inputs[1].default_value = random.uniform(0.05, 0.75)

    def randomise_sun(self):
        module_logger.info('')
        rotation_target = bpy.data.objects['sun_light_rotation']
        rotation_target.rotation_euler = (self.sun_rotation_target_x, self.sun_rotation_target_y, self.sun_rotation_target_z)
        self.sun.data.energy = self.sun_energy
        self.sun.data.color = (self.sun_color_r, self.sun_color_g, self.sun_color_b)
        self.sun.data.size = self.sun_size
        self.sun.data.size_y = self.sun_size_y

    def set_background_strength(self, value):
        enviroment_node = bpy.data.worlds['World'].node_tree
        enviroment_node.nodes["Background"].inputs[1].default_value = value

    def get_background_strength(self):
        enviroment_node = bpy.data.worlds['World'].node_tree
        return enviroment_node.nodes["Background"].inputs[1].default_value

    def random_room(self):
        room = bpy.data.objects['Room']
        room.location = (0, 0, 1.75)
        room.rotation_euler = (0, 0, random.uniform(-0.5, 0.5))
        room.data.materials[0].node_tree.nodes['Principled_BSDF'].inputs[0].default_value = (
            random.uniform(0.8, 1), random.uniform(0.8, 1), random.uniform(0.8, 1), 1
        )
        return room

    def disable_sun(self):
        self.sun.data.energy = 0

    def create_point_lights(self):
        bpy.ops.object.lamp_add(type='POINT')
        bpy.ops.object.lamp_add(type='POINT')
        p1 = bpy.data.objects['Point']
        p2 = bpy.data.objects['Point.001']
        p1.data.energy = random.uniform(0, 0.5)
        p2.data.energy = random.uniform(0, 0.5)
        p1.data.color = (random.uniform(0.8, 1), random.uniform(0.8, 1), random.uniform(0.8, 1))
        p2.data.color = (random.uniform(0.8, 1), random.uniform(0.8, 1), random.uniform(0.8, 1))
        p1.location = (random.uniform(-1.0, 1.0), random.uniform(0.0, 1.0), random.uniform(0.7, 2.7))
        p2.location = (random.uniform(-1.0, 1.0), random.uniform(0.0, 1.0), random.uniform(0.7, 2.7))

    def dump_properties(self, path, counter=0):
        blender_utils.makedirs(path / 'environment_meta')
        proporties_path = path / 'environment_meta' / 'frame_{:07}.json'.format(counter)
        data = OrderedDict()
        with open(proporties_path, 'w') as file:
            for prop in self.default_values:
                value = getattr(self, prop)
                data[prop] = value
            json.dump(data, file, indent=4)
