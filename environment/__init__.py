ENVIRONMENT_CONFIG = {
    "env_image": "d:\\__HDRiS\\hamburg_hbf_4k.hdr",
    "env_rotation_offset": 349.3184522415022,
    "env_brightness_offset": 1.929326372942438,
    "sun_hue_saturation_value_r": 0.43517653997265215,
    "sun_hue_saturation_value_g": 0.5968820408375564,
    "sun_hue_saturation_value_b": 0.8131290879331399,
    "sun_energy": 0.9154535226629779,
    "sun_rotation_target_x": 274.7089812513073,
    "sun_rotation_target_y": 109.51413089058643,
    "sun_rotation_target_z": 240.57407667876564,
    "sun_color_r": 0.6401709323330207,
    "sun_color_g": 0.8909445561005789,
    "sun_color_b": 0.962586009181675,
    "sun_size": 0.3683930520491121,
    "sun_size_y": 0.31145671360560223
}
