import bpy
import logging
import root_logger
import random
import json
import sys
import math
import time
from config import cfg
from pathlib import Path
from mathutils import Vector, Euler
from bpy_extras.object_utils import world_to_camera_view
from utils import blender_utils, path_utils, camera_utils, landmark_utils
from characters import BLENDER_CHARACTER_FULL_CONFIG, HUMAN_INIT_CONFIG, HUMAN_POSE
from characters.blender_character import Human
from cameras import CAMERA_CONFIG
from cameras.blender_camera import BlenderCamera
from environment import ENVIRONMENT_CONFIG
from environment.blender_environment import BlenderEnvironment


MODULE_NAME = 'eye_generator'
module_logger = logging.getLogger(MODULE_NAME)

data_path = Path(cfg['Paths']['data_path'])

prewarm_frame_count = 30
render_frame_count = 10
frame_start = prewarm_frame_count
frame_stop = prewarm_frame_count + render_frame_count

def run(gender, path, landmarks_list, landmark_classes_list):
    bpy.context.scene.render.resolution_x = 1024
    bpy.context.scene.render.resolution_y = 1024
    bpy.context.scene.render.resolution_percentage = 100
    blender_utils.makedirs(path)
    blender_utils.makedirs(path / 'meta')
    blender_utils.gc()

    bpy.context.scene.mblab_use_ik = False
    human_config = {'gender': gender, 'has_hair': False}
    human = Human(human_config)
    # human = Human(human_config=HUMAN_INIT_CONFIG)
    # human = Human(human_config=HUMAN_INIT_CONFIG, blender_character_config=BLENDER_CHARACTER_FULL_CONFIG)
    human.remove_modifier('mbastlab_corrective_modifier')
    human.remove_modifier('mbastlab_subdvision')
    human.remove_modifier('mbastlab_armature')

    human.hair.randomise_hair_color()
    human.hair.bake_hair_cache()
    human.hair.set_normal_colored_hair()

    # camera = BlenderCamera()
    # camera = BlenderCamera(config=CAMERA_CONFIG)
    camera_helper = bpy.data.objects['cam_helper']
    camera = bpy.data.objects['cam_linked']
    # bpy.data.scenes['Scene'].camera = camera
    camera = BlenderCamera(existing_camera=camera)

    blender_utils.select(human.armature)
    blender_utils.set_active(human.armature)
    blender_utils.set_pose_mode()

    # human.set_pose(HUMAN_POSE)

    for frame in range(frame_start, frame_stop):
        environment = BlenderEnvironment()
        # environment = BlenderEnvironment(config=ENVIRONMENT_CONFIG)
        environment.dump_properties(path, frame-prewarm_frame_count)

        human.randomise_attr('character_age', 0.2, 0.8)
        human.randomise_attr('character_mass', -0.5, 0.5)
        human.randomise_attr('character_tone', 0.2, 0.5)
        human.randomise_attr('Eyes_IrisSize', 0.45, 0.6)
        human.randomise_attr('Expressions_eyesHoriz', 0.1, 0.9)
        human.randomise_attr('Expressions_eyesVert', 0.1, 0.9)
        human.randomize_head(0.25, 0.75)
        human.randomize_eyes(0.25, 0.75)
        human.randomize_eyebrows(0.1, 0.9)
        human.close_eyes_randomly()

        bpy.context.scene.transform_orientation = 'LOCAL'
        camera_helper.location = bpy.context.object.pose.bones['head'].head + Vector((0, 0, 0.04))
        # camera_helper.rotation_mode = 'YZX'
        camera_helper.rotation_mode = 'XYZ'
        camera_helper.rotation_euler = Euler((math.radians(random.uniform(-30, 30)), 0, math.radians(random.uniform(-30, 30))))
        # camera_helper.rotation_euler = Euler((math.radians(random.uniform(-90, 75)), 0, math.radians(random.uniform(-90, 90))))
        camera.location = Vector((0, random.uniform(-0.45, -0.6), 0))
        bpy.context.scene.mblab_show_measures = False

        camera.dump_properties(path, frame-prewarm_frame_count)

        # human.randomize_attributes(randomizer.expression_morphs, 1.6, False)
        # human.randomize_attributes(randomizer.mouth_expression_morphs, 1.1, False)

        blender_utils.render_sequence_frame(path, 'frame', 'RGB', frame, -prewarm_frame_count)

        frame_meta = dict()
        landmark_utils.write_landmarks(
            camera, frame_meta, path, human.character, landmarks_list, landmark_classes_list,
            frame - prewarm_frame_count, human.vertex_groups_indices['outer_eyes'])

        write_landmarks(path, frame-prewarm_frame_count, human.character, camera, human.vertex_groups_indices['outer_eyes'])
        human.dump_properties(path, frame-prewarm_frame_count)

    landmark_utils.write_device_info(path)

    blender_utils.set_object_mode()
    blender_utils.deselect_all()
    blender_utils.delete_object(human.armature)
    blender_utils.delete_object(human.character)

def ignore_face(indices, ignored_indices):
    for index in indices:
        if index not in ignored_indices:
            return False
    return True

def is_point_occluded(cam_loc, location, ignored_indices):
    treshold = 0.0005
    direction = location - cam_loc
    distance = direction.length
    direction.normalize()
    hit = bpy.context.scene.ray_cast(bpy.context.view_layer, cam_loc, direction)
    if not hit[0]:
        return True
    hit_point = hit[1]
    face_index = hit[3]
    obj = hit[4]
    face = obj.data.polygons[face_index]
    if ignore_face(face.vertices, ignored_indices):
        return False

    return (hit_point - cam_loc).length < distance - treshold

def write_landmarks(path, counter, character, camera, outer_eyes_indices):
    right_eye_vert = [17067, 17072, 17070, 17071, 17076, 17078, 17066, 17059, 17060, 17065,
                      17064, 17063, 17062, 17061, 17083, 17073, 17081, 17080, 17079, 17077,
                      17075, 17074, 17082, 17069, 17068]
    right_eye_outer_vert = [16301, 16299, 16297, 16327, 16295, 16293, 16291, 16263, 16262, 16266,
                            16268, 16270, 16272, 16274, 16860, 16275, 16277, 16279, 16282, 16284,
                            16286, 16288, 16861, 16290, 16303]
    right_iris_vert = [6352, 6353, 6378, 6388, 6402, 6416, 6430, 6444, 6455, 6483,
                       6498, 6513, 6527, 6541, 6555, 6569]
    left_eye_vert = [2079, 2084, 2082, 2083, 2088, 2090, 2078, 2071, 2072, 2077,
                     2076, 2075, 2074, 2073, 2095, 2085, 2093, 2092, 2091, 2089,
                     2087, 2086, 2094, 2081, 2080]
    left_eye_outer_vert = [1215, 1213, 1211, 1241, 1209, 1207, 1205, 1177, 1176, 1180,
                           1182, 1184, 1186, 1188, 1872, 1189, 1191, 1193, 1196, 1198,
                           1200, 1202, 1873, 1204, 1217]
    left_iris_vert = [4, 5, 30, 40, 54, 68, 82, 96, 107, 135,
                      150, 165, 179, 193, 207, 221]

    render = bpy.context.scene.render
    scene = bpy.context.scene

    mesh = character.to_mesh(apply_modifiers=True, depsgraph=bpy.context.depsgraph)
    vertices = [Vector((vert.co.x, vert.co.y, vert.co.z)) for vert in mesh.vertices]
    bpy.data.meshes.remove(mesh)

    cam_loc, cam_rot, fov, pitch, roll, yaw = camera.dump_meta()

    verts = right_eye_vert + right_iris_vert + left_eye_vert + left_iris_vert + right_eye_outer_vert + left_eye_outer_vert
    line = str()
    for vert in verts:
        coordinate = vertices[vert]
        occlusion = is_point_occluded(cam_loc, coordinate, outer_eyes_indices)
        coords_2d = world_to_camera_view(scene, camera.camera_object, coordinate)
        x, y = coords_2d[0], coords_2d[1]
        line += str(render.resolution_x * x) + '\t' + str(render.resolution_y - render.resolution_y * y) + '\t' + str(occlusion) + '\n'

    landmarks_file =     / 'meta' / 'landmarks_{:07}.csv'.format(counter)
    with landmarks_file.open('w') as c:
        c.write(line)

def job(sequence_path=None):
    landmarks_list = [landmark_utils.get_landmarks_86(data_path, 'woman'), landmark_utils.get_landmarks_86(data_path, 'man')]
    landmark_classes_list = [landmark_utils.get_landmark_classes(data_path, 'woman'), landmark_utils.get_landmark_classes(data_path, 'man')]
    gender = 0
    sequence_path = sequence_path or path_utils.build_seq_path(MODULE_NAME)

    start_time = time.time()
    module_logger.info('[START] results in %s. Measuring time', sequence_path)
    run(gender, Path(sequence_path), landmarks_list[gender], landmark_classes_list[gender])
    stop_time = time.time()
    time_spent = stop_time - start_time
    module_logger.info('[STOP] Iteration %s. Time spent %s', sequence_path, time_spent)

def run_from_blender(iterations):
    for i in range(iterations):
        job()


if __name__ == "__main__":
    if '--sequence_path' in sys.argv:
        index = sys.argv.index('--sequence_path') + 1
        sequence_path = sys.argv[index]
    else:
        sequence_path = None

    try:
        job(sequence_path)
        sys.exit(0)
    except Exception:
        sys.exit(1)
