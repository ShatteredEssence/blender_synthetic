import os
import shutil
import sys
from execution.tasks import remove_broken_sequence
from pathlib import Path

# DATAPATH = 'D:/selfie_runner/'
# CHECK_FILE = 'deviceinfo.json'
#
# counter = 0
# for dir in os.listdir(DATAPATH):
#     dir = os.path.join(DATAPATH, dir)
#     if os.path.isdir(dir):
#         for sequence_dir in os.listdir(dir):
#             sequence_dir = os.path.join(dir, sequence_dir)
#             if os.path.isdir(sequence_dir):
#                 if not os.path.exists(os.path.join(sequence_dir, CHECK_FILE)):
#                     print('Deleting ' + sequence_dir)
#                     counter += 1
#                     shutil.rmtree(sequence_dir)
#
# print('Deleted {} direcoties.'.format(counter))


def selfie_runner_cleaner(data_dir_for_inspect):
    path = Path(data_dir_for_inspect)
    d, r = 0, 0
    for i in path.iterdir():
        d += 1
        res = remove_broken_sequence(i)
        if not res:
            r += 1
    l = 100/d*(d-r)
    print('{0} inspected | {1} removred | {2:.4}% left'.format(d, r, l))


if __name__ == "__main__":
    data_dir_for_inspect = sys.argv[1]
    selfie_runner_cleaner(data_dir_for_inspect)
