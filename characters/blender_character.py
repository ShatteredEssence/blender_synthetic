import bpy
import json
import random
import logging
import mathutils
import math
from math import pi
from .utils import randomizer, hair_utils
from utils import materials, blender_utils
from config import cfg
from pathlib import Path
from collections import OrderedDict
from mathutils import Vector


module_logger = logging.getLogger(__name__)
data_path = Path(cfg['Paths']['data_path'])

f_character_proporties = [
    'character_age', 'character_mass', 'character_tone', 'upperleg_length', 'buttock_depth_Y', 'buttock_width_X', 'wrist_girth', 'upperarm_axillary_girth', 'neck_girth',
    'lowerleg_length', 'lowerleg_bottom_girth', 'head_height_Z', 'feet_length', 'lowerleg_calf_girth', 'feet_heel_width',
    'torso_height_Z', 'upperleg_top_girth', 'shoulders_width', 'feet_height_Z', 'waist_girth', 'elbow_girth', 'head_width_X',
    'chest_depth_Y', 'upperleg_bottom_girth', 'forearm_length', 'buttock_height_Z', 'hands_width', 'hands_length', 'chest_girth',
    'upperarm_length', 'feet_width', 'body_height_Z', 'neck_height_Z', 'head_length', 'buttock_girth', 'chest_width_X',
    'Feet_SizeZ', 'Neck_Angle', 'Shoulders_SizeX2', 'Chest_Girth', 'Torso_Mass', 'Torso_Tone', 'Feet_Size', 'Legs_CalfGirth',
    'Neck_Mass', 'Neck_Tone', 'Legs_AnkleSize', 'Legs_Bow', 'Head_SizeZ', 'Pelvis_Length', 'Pelvis_CrotchDist', 'Torso_BreastPosZ',
    'Torso_ToracicCurve', 'Neck_SideCurve', 'Arms_UpperarmGirth', 'Armpit_PosZ', 'Hands_Lenght', 'Torso_BreastMass', 'Torso_BreastTone',
    'Legs_LowerlegLength', 'Stomach_Volume', 'Torso_SizeX', 'Elbows_Size', 'Hands_FingersInterDist', 'Arms_ForearmMass',
    'Arms_ForearmTone', 'Pelvis_CrotchVolume', 'Head_SizeX', 'Hands_PalmLength', 'Neck_TrapeziousSize', 'Torso_BreastPosX',
    'Chest_SizeZ', 'Waist_Size', 'Hands_FingersLength', 'Hands_Size', 'Shoulders_Mass', 'Shoulders_Tone', 'Legs_KneeProminence',
    'Head_SizeY', 'Neck_Size', 'Pelvis_SizeX', 'Legs_UpperlegInCurve', 'Feet_SizeY', 'Torso_SizeY', 'Legs_UpperThighGirth',
    'Torso_BreastScaleY', 'Torso_BreastNipple', 'Arms_ForearmSize', 'Shoulders_Size', 'Arms_UpperarmSize', 'Torso_Vshape',
    'Feet_Mass', 'Legs_LowerlegsMass', 'Legs_LowerlegsTone', 'Feet_SizeX', 'Torso_Dorsi', 'Abdomen_Mass', 'Abdomen_Tone',
    'Stomach_LocalFat', 'Legs_KneeSize', 'Neck_Back', 'Neck_Length', 'Arms_ForearmLength', 'Hands_FingersDiam', 'Pelvis_Angle',
    'Shoulders_Length', 'Arms_UpperarmLength', 'Hands_FingersTipSize', 'Legs_LowerlegSize', 'Chest_SizeX',
    'Legs_UpperlegSize', 'Torso_BellyPosZ', 'Torso_Length', 'Neck_Collarbone', 'Arms_UpperarmMass', 'Arms_UpperarmTone',
    'Head_Size', 'Legs_UpperlegsMass', 'Legs_UpperlegsTone', 'Hands_Mass', 'Hands_Tone', 'Torso_AureolaSize', 'Shoulders_PosZ',
    'Feet_HeelWidth', 'Body_Size', 'Eyes_IrisSize', 'Legs_UpperlegLength', 'Chest_SizeY', 'Pelvis_Girth', 'Shoulders_SizeX',
    'Pelvis_SizeY', 'Legs_LowerThighGirth', 'Pelvis_GluteusMass', 'Pelvis_GluteusTone', 'Pelvis_GluteusSize', 'Nose_PosY',
    'Forehead_Curve', 'Cheeks_InfraVolume', 'Eyes_Size', 'Nose_BaseSizeZ', 'Eyebrows_SizeY', 'Eyes_TypeAlmond', 'Face_Ellipsoid',
    'Eyes_OuterPosX', 'Nose_NostrilSizeZ', 'Mouth_UpperlipExt', 'Eyes_innerSinus', 'Eyes_Crosscalibration', 'Mouth_SizeX',
    'Eyelids_InnerPosZ', 'Mouth_LowerlipVolume', 'Eyebrows_Droop', 'Nose_TipPosY', 'Ears_LocY', 'Mouth_CornersPosZ', 'Nose_TipAngle',
    'Mouth_SideCrease', 'Cheeks_ZygomPosZ', 'Ears_LocZ', 'Face_Parallelepiped', 'Mouth_LowerlipExt', 'Mouth_LowerlipSizeZ',
    'Nose_BaseShape', 'Nose_SeptumFlat', 'Eyes_OuterPosZ', 'Forehead_Temple', 'Head_CraniumPentagonoides', 'Nose_BaseSizeX',
    'Ears_SizeY', 'Eyes_BagSize', 'Nose_NostrilSizeY', 'Eyelids_MiddlePosZ', 'Chin_Cleft', 'Head_Flat', 'Face_Triangle',
    'Jaw_Prominence', 'Eyelids_Angle', 'Nose_GlabellaSizeX', 'Ears_Lobe', 'Forehead_SizeZ', 'Mouth_UpperlipVolume', 'Chin_SizeX',
    'Nose_Curve', 'Eyelids_OuterPosZ', 'Mouth_UpperlipSizeZ', 'Nose_BridgeSizeX', 'Eyes_PosZ', 'Head_CraniumPlatycephalus',
    'Ears_SizeZ', 'Eyebrows_Tone', 'Eyelids_Crease', 'Nose_NostrilPosZ', 'Mouth_PosY', 'Nose_TipPosZ', 'Cheeks_SideCrease',
    'Chin_Tone', 'Chin_Prominence', 'Cheeks_Mass', 'Cheeks_Tone', 'Jaw_ScaleX', 'Nose_GlabellaPosZ', 'Eyelids_LowerCurve',
    'Eyes_InnerPosZ', 'Head_CraniumDolichocephalic', 'Jaw_Angle2', 'Nose_BallSizeX', 'Nose_SizeY', 'Nose_WingBackFlat',
    'Cheeks_CreaseExt', 'Eyes_InnerPosX', 'Forehead_SizeX', 'Nose_NostrilCrease', 'Nose_TipSize', 'Mouth_PhiltrumProminence',
    'Nose_SeptumRolled', 'Eyes_SizeZ', 'Mouth_PhiltrumSizeY', 'Nose_GlabellaSizeY', 'Head_Nucha', 'Jaw_LocY', 'Ears_Round',
    'Eyebrows_Angle', 'Ears_RotX', 'Eyelids_SizeZ', 'Eyes_PosX', 'Mouth_Protusion', 'Jaw_Angle', 'Nose_NostrilSizeX',
    'Nose_BasePosZ', 'Mouth_PhiltrumSizeX', 'Eyes_TypeHooded', 'Cheeks_Zygom', 'Eyes_BagProminence', 'Ears_SizeX', 'Eyebrows_Ridge',
    'Eyebrows_PosZ', 'Nose_WingAngle', 'Forehead_Angle', 'Mouth_PosZ', 'Nose_NostrilDiam', 'Nose_WingBump', 'Chin_SizeZ',
    'Fantasy_EarsPointRotation', 'Fantasy_EarsPointed', 'Fantasy_EarsPointedDown', 'Fantasy_TeethOrc', 'Fantasy_TeethVampire',
    'Fantasy_EarsNone', 'Fantasy_PupilCat', 'Fantasy_EarsPointedUp', 'Expressions_mouthInflated', 'Expressions_browSqueezeL',
    'Expressions_mouthOpenTeethClosed', 'Expressions_mouthOpenAggr', 'Expressions_jawOut', 'Expressions_eyeClosedPressureR',
    'Expressions_eyeClosedL', 'Expressions_mouthOpenLarge', 'Expressions_eyeClosedPressureL', 'Expressions_mouthClosed',
    'Expressions_browOutVertR', 'Expressions_eyeSquintR', 'Expressions_tongueHoriz', 'Expressions_eyesVert', 'Expressions_cheekSneerL',
    'Expressions_nostrilsExpansion', 'Expressions_mouthSmileOpen2', 'Expressions_jawHoriz', 'Expressions_mouthHoriz',
    'Expressions_mouthOpen', 'Expressions_mouthSmileL', 'Expressions_mouthChew', 'Expressions_tongueTipUp', 'Expressions_IDHumans',
    'Expressions_mouthLowerOut', 'Expressions_chestExpansion', 'Expressions_mouthSmileR', 'Expressions_eyeClosedR', 'Expressions_mouthOpenO',
    'Expressions_mouthSmileOpen', 'Expressions_deglutition', 'Expressions_abdomExpansion', 'Expressions_tongueOutPressure',
    'Expressions_browSqueezeR', 'Expressions_tongueOut', 'Expressions_cheekSneerR', 'Expressions_mouthBite', 'Expressions_browOutVertL',
    'Expressions_browsMidVert', 'Expressions_mouthOpenHalf', 'Expressions_pupilsDilatation', 'Expressions_eyeSquintL',
    'Expressions_mouthSmile', 'Expressions_tongueVert', 'Expressions_eyesHoriz', 'eyes_value', 'eyes_saturation', 'eyes_hue',
    'skin_sss', 'skin_bump', 'skin_age', 'skin_oil', 'skin_fresnel', 'skin_reflection', 'skin_complexion', 'skin_hue', 'skin_value',
    'skin_saturation', 'Wrists_Size', 'Hands_FingersDiam','Hands_FingersInterDist','Hands_FingersLength','Hands_FingersTipSize','Hands_Lenght',
    'Hands_Mass','Hands_NailsLength','Hands_PalmLength','Hands_Size','Hands_Tone'
]
m_character_proporties = [
    'character_age', 'character_mass', 'character_tone', 'upperleg_length', 'buttock_depth_Y', 'chest_girth', 'wrist_girth',
    'upperarm_axillary_girth', 'neck_girth', 'lowerleg_length', 'lowerleg_bottom_girth', 'head_height_Z', 'feet_length', 'lowerleg_calf_girth',
    'feet_heel_width', 'torso_height_Z', 'upperleg_top_girth', 'shoulders_width', 'feet_height_Z', 'buttock_width_X', 'waist_girth', 'elbow_girth',
    'head_width_X', 'chest_depth_Y', 'upperleg_bottom_girth', 'forearm_length', 'buttock_height_Z', 'hands_width', 'hands_length', 'upperarm_length',
    'feet_width', 'body_height_Z', 'neck_height_Z', 'head_length', 'buttock_girth', 'chest_width_X', 'Wrists_Size', 'Torso_BreastMass',
    'Torso_Mass', 'Torso_Tone', 'Torso_BellyPosZ', 'Legs_KneeProminence', 'Waist_Size', 'Legs_UpperlegLength', 'Pelvis_GluteusSize',
    'Torso_Dorsi', 'Torso_ToracicCurve', 'Chest_SizeZ', 'Hands_Lenght', 'Torso_Length', 'Neck_Mass', 'Neck_Tone', 'Feet_SizeY',
    'Chest_Girth', 'Arms_ForearmSize', 'Torso_SizeY', 'Head_Size', 'Abdomen_Mass', 'Abdomen_Tone', 'Head_SizeX', 'Pelvis_SizeY',
    'Hands_FingersDiam', 'Body_Size', 'Neck_Size', 'Hands_PalmLength', 'Shoulders_SizeX2', 'Pelvis_Girth', 'Legs_UpperlegInCurve',
    'Pelvis_CrotchVolume', 'Arms_ForearmLength', 'Arms_UpperarmLength', 'Legs_AnkleSize', 'Arms_UpperarmSize', 'Arms_UpperarmGirth',
    'Shoulders_SizeX', 'Shoulders_Length', 'Neck_Back', 'Pelvis_GluteusMass', 'Pelvis_GluteusTone', 'Elbows_Size', 'Hands_Size',
    'Arms_ForearmMass', 'Arms_ForearmTone', 'Hands_Mass', 'Hands_Tone', 'Chest_SizeY', 'Pelvis_CrotchDist', 'Hands_FingersInterDist',
    'Legs_LowerlegSize', 'Legs_LowerThighGirth', 'Armpit_PosZ', 'Arms_UpperarmMass', 'Arms_UpperarmTone', 'Shoulders_Mass',
    'Shoulders_Tone', 'Neck_Angle', 'Chest_SizeX', 'Pelvis_SizeX', 'Head_SizeZ', 'Hands_FingersTipSize', 'Legs_KneeSize',
    'Neck_TrapeziousSize', 'Shoulders_PosZ', 'Stomach_LocalFat', 'Legs_LowerlegsMass', 'Legs_LowerlegsTone', 'Feet_Size',
    'Torso_SizeX', 'Legs_LowerlegLength', 'Shoulders_Size', 'Feet_SizeX', 'Neck_Length', 'Legs_CalfGirth', 'Feet_Mass',
    'Torso_Vshape', 'Eyes_IrisSize', 'Feet_SizeZ', 'Stomach_Volume', 'Legs_Bow', 'Legs_UpperThighGirth', 'Neck_SideCurve',
    'Neck_Collarbone', 'Hands_FingersLength', 'Feet_HeelWidth', 'Legs_UpperlegsMass', 'Legs_UpperlegsTone', 'Legs_UpperlegSize',
    'Pelvis_Length', 'Ears_SizeX', 'Eyelids_Angle', 'Head_CraniumPlatycephalus', 'Mouth_UpperlipSizeZ', 'Eyebrows_PosZ',
    'Chin_Tone', 'Nose_NostrilDiam', 'Eyebrows_SizeY', 'Head_Nucha', 'Nose_GlabellaSizeY', 'Mouth_Protusion', 'Jaw_LocY',
    'Eyes_Crosscalibration', 'Cheeks_ZygomPosZ', 'Nose_TipPosZ', 'Eyes_TypeHooded', 'Eyelids_SizeZ', 'Nose_WingBump',
    'Head_CraniumDolichocephalic', 'Cheeks_Mass', 'Cheeks_Tone', 'Eyes_SizeZ', 'Nose_Curve', 'Mouth_SideCrease', 'Nose_TipSize',
    'Ears_LocZ', 'Cheeks_CreaseExt', 'Eyes_OuterPosX', 'Nose_BallSizeX', 'Eyes_PosX', 'Eyes_BagProminence', 'Forehead_SizeZ',
    'Nose_GlabellaSizeX', 'Eyes_PosZ', 'Forehead_Angle', 'Nose_TipAngle', 'Mouth_PosY', 'Eyes_innerSinus', 'Nose_NostrilCrease',
    'Eyes_BagSize', 'Mouth_PhiltrumSizeX', 'Nose_PosY', 'Jaw_Angle', 'Nose_NostrilSizeY', 'Face_Parallelepiped', 'Mouth_LowerlipSizeZ',
    'Mouth_UpperlipExt', 'Eyelids_Crease', 'Nose_GlabellaPosZ', 'Cheeks_Zygom', 'Eyelids_MiddlePosZ', 'Chin_SizeZ', 'Ears_SizeY',
    'Forehead_SizeX', 'Mouth_LowerlipVolume', 'Nose_BaseSizeX', 'Eyelids_LowerCurve', 'Ears_RotX', 'Eyebrows_Angle', 'Eyes_Size',
    'Chin_Cleft', 'Mouth_PhiltrumProminence', 'Eyelids_InnerPosZ', 'Eyebrows_Droop', 'Forehead_Temple', 'Mouth_CornersPosZ',
    'Ears_SizeZ', 'Eyebrows_Tone', 'Ears_Round', 'Cheeks_InfraVolume', 'Face_Triangle', 'Forehead_Curve', 'Mouth_SizeX',
    'Ears_Lobe', 'Nose_BasePosZ', 'Mouth_PhiltrumSizeY', 'Nose_SeptumFlat', 'Nose_BaseShape', 'Eyelids_OuterPosZ', 'Jaw_ScaleX',
    'Mouth_LowerlipExt', 'Cheeks_SideCrease', 'Nose_NostrilSizeX', 'Nose_TipPosY', 'Eyes_InnerPosX', 'Jaw_Angle2', 'Nose_NostrilPosZ',
    'Head_Flat', 'Face_Ellipsoid', 'Chin_Prominence', 'Chin_SizeX', 'Ears_LocY', 'Nose_BridgeSizeX', 'Eyes_OuterPosZ', 'Nose_WingAngle',
    'Eyes_InnerPosZ', 'Mouth_UpperlipVolume', 'Nose_NostrilSizeZ', 'Eyes_TypeAlmond', 'Nose_SeptumRolled', 'Nose_SizeY', 'Nose_BaseSizeZ',
    'Nose_WingBackFlat', 'Head_CraniumPentagonoides', 'Jaw_Prominence', 'Eyebrows_Ridge', 'Mouth_PosZ', 'Fantasy_EarsPointRotation',
    'Fantasy_TeethOrc', 'Fantasy_EarsPointed', 'Fantasy_TeethVampire', 'Fantasy_EarsPointedDown', 'Fantasy_EarsNone', 'Fantasy_PupilCat',
    'Fantasy_EarsPointedUp', 'Expressions_browSqueezeL', 'Expressions_browsMidVert', 'Expressions_eyeClosedR', 'Expressions_abdomExpansion',
    'Expressions_mouthOpenTeethClosed', 'Expressions_mouthInflated', 'Expressions_mouthOpenO', 'Expressions_mouthOpen',
    'Expressions_mouthOpenLarge', 'Expressions_tongueHoriz', 'Expressions_mouthOpenAggr', 'Expressions_cheekSneerL', 'Expressions_browOutVertL',
    'Expressions_tongueOut', 'Expressions_mouthSmile', 'Expressions_browOutVertR', 'Expressions_mouthClosed', 'Expressions_jawOut',
    'Expressions_eyeSquintL', 'Expressions_eyeClosedPressureR', 'Expressions_pupilsDilatation', 'Expressions_chestExpansion',
    'Expressions_mouthSmileL', 'Expressions_mouthLowerOut', 'Expressions_eyeSquintR', 'Expressions_eyeClosedL', 'Expressions_mouthSmileOpen2',
    'Expressions_tongueVert', 'Expressions_mouthOpenHalf', 'Expressions_deglutition', 'Expressions_mouthHoriz', 'Expressions_eyesHoriz',
    'Expressions_browSqueezeR', 'Expressions_eyesVert', 'Expressions_tongueTipUp', 'Expressions_eyeClosedPressureL', 'Expressions_mouthSmileOpen',
    'Expressions_nostrilsExpansion', 'Expressions_mouthSmileR', 'Expressions_mouthBite', 'Expressions_mouthChew', 'Expressions_cheekSneerR',
    'Expressions_tongueOutPressure', 'Expressions_IDHumans', 'Expressions_jawHoriz', 'eyes_value', 'eyes_saturation', 'eyes_hue', 'skin_sss',
    'skin_bump', 'skin_age', 'skin_oil', 'skin_fresnel', 'skin_reflection', 'skin_complexion', 'skin_hue', 'skin_value', 'skin_saturation',
    'Wrists_Size', 'Hands_FingersDiam','Hands_FingersInterDist','Hands_FingersLength','Hands_FingersTipSize','Hands_Lenght',
    'Hands_Mass','Hands_PalmLength','Hands_Size','Hands_Tone'
]
human_object_proporties = [
    'race',
    'gender',
    'has_hair',
    'has_wrinkles',
    'has_eyebags',
    'randomize_lips',
    'randomize_brows',
    'global_morphs_random_factor',
    'eye_iris_morph_random_factor',
    'eyes_morphs_random_factor',
    'eyebrows_morphs_random_factor',
    'head_morphs_random_factor',
    'hand_morphs_random_factor',
    'lip_expression_morphs_random_factor',
    'lip_expression_morphs_open_random_factor',
    'expression_morphs_random_factor',
    'tongue_expression_morphs',
    'lip_morps_random_factor',
    'hand_texture',
    'body_texture',
    'mouth_texture',
    'lips_texture',
    'brows_texture',
    'wrinkles_texture',
    'face_hair_textures',
    'eyebags_texture'
]

BLENDER_CHARACTER_HAND_BONES = [
    'clavicle_R',
    # 'upperarm_twist_R',
    # 'lowerarm_twist_R',
    'upperarm_R',
    'lowerarm_R',
    'hand_R',
    'thumb01_R',
    'thumb02_R',
    'thumb03_R',
    'index00_R',
    'index01_R',
    'index02_R',
    'index03_R',
    'middle00_R',
    'middle01_R',
    'middle02_R',
    'middle03_R',
    'ring00_R',
    'ring01_R',
    'ring02_R',
    'ring03_R',
    'pinky00_R',
    'pinky01_R',
    'pinky02_R',
    'pinky03_R'
]

BLENDER_CHARACTER_ALL_BONES = [
    "root",
    "pelvis",
    "thigh_R",
    "calf_R",
    "foot_R",
    "toes_R",
    "calf_twist_R",
    "thigh_twist_R",
    "thigh_L",
    "calf_L",
    "foot_L",
    "toes_L",
    "calf_twist_L",
    "thigh_twist_L",
    "spine01",
    "spine02",
    "spine03",
    "clavicle_L",
    "upperarm_L",
    "lowerarm_L",
    "hand_L",
    "thumb01_L",
    "thumb02_L",
    "thumb03_L",
    "index00_L",
    "index01_L",
    "index02_L",
    "index03_L",
    "middle00_L",
    "middle01_L",
    "middle02_L",
    "middle03_L",
    "ring00_L",
    "ring01_L",
    "ring02_L",
    "ring03_L",
    "pinky00_L",
    "pinky01_L",
    "pinky02_L",
    "pinky03_L",
    "lowerarm_twist_L",
    "upperarm_twist_L",
    "clavicle_R",
    "upperarm_R",
    "lowerarm_R",
    "hand_R",
    "thumb01_R",
    "thumb02_R",
    "thumb03_R",
    "index00_R",
    "index01_R",
    "index02_R",
    "index03_R",
    "middle00_R",
    "middle01_R",
    "middle02_R",
    "middle03_R",
    "ring00_R",
    "ring01_R",
    "ring02_R",
    "ring03_R",
    "pinky00_R",
    "pinky01_R",
    "pinky02_R",
    "pinky03_R",
    "lowerarm_twist_R",
    "upperarm_twist_R",
    "neck",
    "head",
    "breast_L",
    "breast_R"
]

BLENDER_CHARACTER_ARM_BONES = [
    'clavicle_L',
    'upperarm_twist_L',
    'lowerarm_twist_L',
    'upperarm_L',
    'lowerarm_L',
    'hand_L',
    'clavicle_R',
    'upperarm_twist_R',
    'lowerarm_twist_R',
    'upperarm_R',
    'lowerarm_R',
    'hand_R'
]

BLENDER_CHARACTER_ARM_L_BONES = [
    'clavicle_L',
    'upperarm_twist_L',
    'lowerarm_twist_L',
    'upperarm_L',
    'lowerarm_L',
    'hand_L'
]

BLENDER_CHARACTER_ARM_R_BONES = [
    'clavicle_R',
    'upperarm_twist_R',
    'lowerarm_twist_R',
    'upperarm_R',
    'lowerarm_R',
    'hand_R'
]

BLENDER_CHARACTER_FINGER_BONES = [
    'thumb01_L',
    'thumb02_L',
    'thumb03_L',
    'index00_L',
    'index01_L',
    'index02_L',
    'index03_L',
    'middle00_L',
    'middle01_L',
    'middle02_L',
    'middle03_L',
    'ring00_L',
    'ring01_L',
    'ring02_L',
    'ring03_L',
    'pinky00_L',
    'pinky01_L',
    'pinky02_L',
    'pinky03_L',
    'thumb01_R',
    'thumb02_R',
    'thumb03_R',
    'index00_R',
    'index01_R',
    'index02_R',
    'index03_R',
    'middle00_R',
    'middle01_R',
    'middle02_R',
    'middle03_R',
    'ring00_R',
    'ring01_R',
    'ring02_R',
    'ring03_R',
    'pinky00_R',
    'pinky01_R',
    'pinky02_R',
    'pinky03_R'
]

BLENDER_CHARACTER_FINGER_L_BONES = [
    'thumb01_L',
    'thumb02_L',
    'thumb03_L',
    'index00_L',
    'index01_L',
    'index02_L',
    'index03_L',
    'middle00_L',
    'middle01_L',
    'middle02_L',
    'middle03_L',
    'ring00_L',
    'ring01_L',
    'ring02_L',
    'ring03_L',
    'pinky00_L',
    'pinky01_L',
    'pinky02_L',
    'pinky03_L'
]

BLENDER_CHARACTER_FINGER_R_BONES = [
    'thumb01_R',
    'thumb02_R',
    'thumb03_R',
    'index00_R',
    'index01_R',
    'index02_R',
    'index03_R',
    'middle00_R',
    'middle01_R',
    'middle02_R',
    'middle03_R',
    'ring00_R',
    'ring01_R',
    'ring02_R',
    'ring03_R',
    'pinky00_R',
    'pinky01_R',
    'pinky02_R',
    'pinky03_R'
]

male_face_indices = [2455, 2464, 2642, 7758, 7761, 7763, 16222, 16882, 16891, 17069]
female_face_indices = [1561, 2152, 7532, 7536, 7538, 16227, 16254, 16647, 17140, 17829]

class Hair(object):
    def __init__(self, human):

        #  Not used for now. Because of using hair cards hairs.
        has_particle_hair = False
        if has_particle_hair:
            hair_utils.allign_colliders(human.armature)
            hair_utils.styler_to_head(human.armature)
            hair_utils.add_hair(human.character)
            hair_utils.setup_hair_style(character=human.character, styles=['short', 'mid'])  # "long' not implemented yet
        if human.gender == 1:
            has_beard = random.randint(0, 1)
            has_mustache = random.randint(0, 1)
            hair_utils.create_facial_hairs(
                human.character, has_beard, has_mustache,
                human.face_hair_textures[0], human.face_hair_textures[1]
            )

    def __set_color(self, material_name, first, second):
        material = bpy.data.materials[material_name]
        nodes = material.node_tree.nodes
        node = nodes.get('ColorRamp')
        node.color_ramp.elements[0].color = first
        node.color_ramp.elements[1].color = second

    def __get_random_color(self):
        type = random.choice(['ginger', 'blond', 'brown'])

        if type == 'brown':
            R = random.uniform(105 / 255, 155 / 255)
            G = R / random.uniform(1.2, 2)
            B = G / random.uniform(1.2, 1.8)
            return R, G, B

        if type == 'ginger':
            R = random.uniform(140 / 255, 220 / 255)
            G = R / random.uniform(1.8, 2.2)
            B = R / random.uniform(3.8, 4.2)
            return R, G, B

        if type == 'blond':
            R = random.uniform(200 / 255, 255 / 255)
            G = R * random.uniform(0.9, 1.1)
            B = R * random.uniform(0.9, 1.1)
            return R, G, B

    def randomise_hair_color(self):
        R, G, B = self.__get_random_color()

        R1 = (R / 60) * random.uniform(1, 25)
        G1 = (G / 60) * random.uniform(1, 25)
        B1 = (B / 60) * random.uniform(1, 25)

        first = (R, G, B, 1.0)
        second = (R1, G1, B1, 1.0)
        self.__set_color('hair', first, second)
        self.__set_color('beard', first, second)
        self.__set_color('mustache', first, second)

    def bake_hair_cache(self):
        hair_utils.set_up_hair_cache(human.character)
        hair_utils.cache_hair(human.character)

    def get_switcher_node(self, material_name):
        material = bpy.data.materials[material_name]
        nodes = material.node_tree.nodes
        return nodes.get('MaterialSwitcher')

    def get_hair_color_node(self, material_name):
        material = bpy.data.materials[material_name]
        nodes = material.node_tree.nodes
        return nodes.get('HairColor')

    def set_white_hair(self, material_name):
        switcher = self.get_switcher_node(material_name)
        switcher.inputs[0].default_value = 1.0
        color = self.get_hair_color_node(material_name)
        color.inputs[1].default_value = 10.0

    def set_black_hair(self, material_name):
        switcher = self.get_switcher_node(material_name)
        switcher.inputs[0].default_value = 1.0
        color = self.get_hair_color_node(material_name)
        color.inputs[1].default_value = 0.0

    def set_normal_colored_hair(self):
        for material_name in ['hair', 'beard', 'mustache']:
            switcher = self.get_switcher_node(material_name)
            switcher.inputs[0].default_value = 0.0


class HairMock(object):
    def set_black_hair(self, *args, **kwargs):
        pass

    def set_white_hair(self, *args, **kwargs):
        pass

    def set_normal_colored_hair(self, *args, **kwargs):
        pass

    def randomise_hair_color(self, *args, **kwargs):
        pass

    def bake_hair_cache(self, *args, **kwargs):
        pass


class Human(object):
    def __init__(self, human_config=None, blender_character_config=None):
        self.human_config = human_config or dict()
        self.blender_character_config = blender_character_config or dict()
        self.gender = self.human_config.get('gender')
        self.race = self.human_config.get('race')
        self.__get_random_textures(self.race, self.gender)
        self.all_bones = BLENDER_CHARACTER_ALL_BONES
        self.hand_bones = BLENDER_CHARACTER_HAND_BONES
        self.arm_bones = BLENDER_CHARACTER_ARM_BONES
        self.arm_l_bones = BLENDER_CHARACTER_ARM_L_BONES
        self.arm_r_bones = BLENDER_CHARACTER_ARM_R_BONES
        self.finger_bones = BLENDER_CHARACTER_FINGER_BONES
        self.finger_l_bones = BLENDER_CHARACTER_FINGER_L_BONES
        self.finger_r_bones = BLENDER_CHARACTER_FINGER_R_BONES

        self.default_human_values = {
            'has_hair': True,
            'has_wrinkles': True,
            'has_eyebags': False,
            'randomize_lips': True,
            'randomize_brows': True,
            'global_morphs_random_factor': 1.1,
            'eye_iris_morph_random_factor': 1.1,
            'eyes_morphs_random_factor': 1.1,
            'eyebrows_morphs_random_factor': 1.1,
            'head_morphs_random_factor': 1.1,
            'hand_morphs_random_factor': 1.1,
            'lip_expression_morphs_random_factor': 1.1,
            'lip_expression_morphs_open_random_factor': 1.1,
            'expression_morphs_random_factor': 1.1,
            'tongue_expression_morphs': 1.1,
            'lip_morps_random_factor': 1.1,
            'hand_texture': self.hand_texture,
            'body_texture': self.body_texture,
            'mouth_texture': self.mouth_texture,
            'lips_texture': self.lips_texture,
            'brows_texture': self.brows_texture,
            'wrinkles_texture': self.wrinkles_texture,
            'face_hair_textures': self.face_hair_textures,
            'eyebags_texture': self.eyebags_texture
        }

        for prop_name, default_value in self.default_human_values.items():
            config_value = self.human_config.get(prop_name, None)
            if config_value != None:
                setattr(self, prop_name, config_value)
            else:
                setattr(self, prop_name, default_value)

        self.tongue_tip_index = 16109 if self.gender else 16132
        self.lip_tip_index = 7775 if self.gender else 7550

        self.mouth_uv_data = json.loads(open(data_path / 'uvs/male_mouth.json').read()) if self.gender \
            else json.loads(open(data_path / 'uvs/female_mouth.json').read())
        self.head_uv_data = json.loads(open(data_path / 'uvs/male_head.json').read()) if self.gender \
            else json.loads(open(data_path / 'uvs/female_head.json').read())
        self.body_uv_data = json.loads(open(data_path / 'uvs/male_body.json').read()) if self.gender \
            else json.loads(open(data_path / 'uvs/female_body.json').read())
        self.vertex_groups_indices = blender_utils.load_vertex_group_indices(data_path, 'man') if self.gender \
            else blender_utils.load_vertex_group_indices(data_path, 'woman')
        self.lip_colors = json.loads(open(data_path / 'json/lip_colors.json').read())

        bpy.context.scene.mblab_use_lamps = False
        bpy.context.scene.mblab_character_name = self.race
        bpy.ops.mbast.init_character()
        self.character = bpy.data.objects[self.race]
        self.armature = bpy.data.objects["{}_skeleton".format(self.race)]

        self.blender_character_proporties = f_character_proporties if self.gender == 0 else m_character_proporties
        self.human_object_proporties = human_object_proporties

        self.__textures_prewarm()
        self.__uvs_prewarm()
        self.__create_vertex_groups(self.vertex_groups_indices)
        self.set_character_materials()
        self.randomise_eye_iris_mtl()
        self.add_hair_material()

        self.hair = HairMock()
        if self.has_hair:
            self.init_hair()

        if self.gender == 1 and self.blender_character_config:
            self.blender_character_config.pop('Hands_NailsLength', None)
            self.blender_character_config.pop('Head_SizeY', None)
            self.blender_character_config.pop('Pelvis_Angle', None)
            self.blender_character_config.pop('Torso_AureolaSize', None)
            self.blender_character_config.pop('Torso_BreastTone', None)
            self.blender_character_config.pop('Torso_BreastPosZ', None)
            self.blender_character_config.pop('Torso_BreastPosX', None)
            self.blender_character_config.pop('Torso_BreastScaleY', None)
            self.blender_character_config.pop('Torso_BreastNipple', None)
        if self.gender == 0 and self.blender_character_config:
            self.blender_character_config.pop('Expressions_schechki', None)
            self.blender_character_config.pop('Expressions_schechki2', None)
        for attr, config_value in self.blender_character_config.items():
            setattr(self.character, attr, config_value)

    def init_hair(self):
        self.hair = Hair(self)

    def shuffle_textures(self):
        self.__get_random_textures(self.race, self.gender)

    def shuffle_materials(self):
        self.__textures_prewarm()

    def __get_random_textures(self, race, gender):
        self.hand_maps = materials.load_hand_maps(gender)
        self.body_maps = materials.load_body_maps(race)
        self.mouth_maps = materials.load_mouth_maps()
        self.lips_maps = materials.load_lips_maps()
        self.brows_maps = materials.load_brows_maps()
        self.wrinkles_maps = materials.load_wrinkles_maps()
        self.face_hair_maps = materials.load_face_hair_maps()
        self.eyebags_maps = materials.load_eyebags_maps()

        self.hand_texture = getattr(self, 'hand_texture', random.choice(self.hand_maps))
        self.body_texture = getattr(self, 'body_texture', random.choice(self.body_maps))
        self.mouth_texture = getattr(self, 'mouth_texture', random.choice(self.mouth_maps))
        self.lips_texture = getattr(self, 'lips_texture', random.choice(self.lips_maps))
        self.brows_texture = getattr(self, 'brows_texture', random.choice(self.brows_maps))
        self.wrinkles_texture = getattr(self, 'wrinkles_texture', random.choice(self.wrinkles_maps))
        self.face_hair_textures = getattr(self, 'face_hair_textures', random.choice(self.face_hair_maps))
        self.eyebags_texture = getattr(self, 'eyebags_texture', random.choice(self.eyebags_maps))
        self.hand_to_body_mix_mask = materials.load_hand_to_body_mix_mask(gender)

    def __uvs_prewarm(self):
        self.create_uvmap('UVMap.001')
        self.create_uvmap('UVMap.002')
        self.create_uvmap('UVMap.003')
        self.set_uvs(self.mouth_uv_data, uvset_name='UVMap.001')
        self.set_uvs(self.head_uv_data, uvset_name='UVMap.002')
        self.set_uvs(self.body_uv_data, uvset_name='UVMap.003')

    def __textures_prewarm(self):
        materials.skin_mtl_teeth_tweek(str(self.mouth_texture))
        materials.set_uv_mask('tongue_mask', 'MaskUV', 'UVMap.001')
        materials.set_uv_mask('gum_mask', 'MaskUV', 'UVMap.001')
        materials.set_uv_mask('teeth_mask', 'MaskUV', 'UVMap.001')
        materials.set_texture(str(self.body_texture), 'skin', 'SkinTexture')
        materials.set_texture(str(self.body_texture), 'inner_eyes', 'InnerEyeTexture')

        if self.randomize_lips:
            lip_color = random.choice(self.lip_colors)
            materials.skin_mtl_lips_tweek(str(self.lips_texture[1]), str(self.lips_texture[0]), self.gender, lip_color)
            materials.set_uv_texture_mask('lip_mask', 'MaskTexture', str(self.lips_texture[1]), 'UVMap.001')
            materials.set_texture(str(self.lips_texture[1]), 'face_mask', 'LipsMaskTexture')

        if self.randomize_brows:
            materials.skin_mtl_brows_tweek(str(self.brows_texture[0]), str(self.brows_texture[1]))
            materials.set_uv_texture_mask('brows_mask', 'MaskTexture', str(self.brows_texture[1]), 'UVMap.002')
            materials.set_texture(str(self.brows_texture[1]), 'face_mask', 'BrowsMaskTexture')

        if self.has_wrinkles:
            materials.skin_mtl_wrinkles_tweek(str(self.wrinkles_texture[1]))
            materials.set_uv_texture_mask('wrinkles_mask', 'MaskTexture', str(self.wrinkles_texture[1]), 'UVMap.002')

        if self.has_eyebags:
            self.randomise_character_mtl_eyebag()

    def set_uvset(self, mtl_name, uvset_node, uvset):
        materials.set_uv_mask(mtl_name, uvset_node, uvset)

    def set_texture(self, texture, mtl_name, texture_node):
        materials.set_texture(texture, mtl_name, texture_node)

    def randomise_eye_iris_mtl(self):
        METALLIC = 4
        SPECULAR = 5

        material_inner = bpy.data.materials.get('inner_eyes')
        nodes = material_inner.node_tree.nodes
        InnerEyeHSV = nodes.get('InnerEyeHSV')
        InnerEyeHSV.inputs[0].default_value = random.choice((random.uniform(0, 0.12), random.uniform(0.28, 0.7)))
        InnerEyeHSV.inputs[1].default_value = random.uniform(0.7, 1.5)
        InnerEyeHSV.inputs[2].default_value = random.uniform(0.7, 1.5)
        bsdf_node = nodes.get('principled_bsdf')
        bsdf_node.inputs[METALLIC].default_value = 0
        bsdf_node.inputs[SPECULAR].default_value = 0

        material_outer = bpy.data.materials.get('outer_eyes')
        nodes = material_outer.node_tree.nodes
        bsdf_node = nodes.get('principled_bsdf')
        bsdf_node.inputs[METALLIC].default_value = 0
        bsdf_node.inputs[SPECULAR].default_value = 0

    def randomise_character_mtl_eyebag(self):
        skin_mtl = bpy.data.materials.get('skin')
        nodes = skin_mtl.node_tree.nodes
        materials.set_texture(str(self.eyebags_texture), 'skin', 'eyebag_shader_mask')
        bump_node = nodes.get('eyebag_bump')
        bump_node.inputs[0].default_value = random.uniform(0.05, 0.2)
        color_ramp_node = nodes.get('eyebag_color_ramp')
        color_ramp_node.color_ramp.elements[1].position = random.uniform(0.01, 0.05)
        hsv_node = nodes.get('eyebag_hsv')
        hsv_node.inputs[2].default_value = random.uniform(0.7, 0.9)

    def create_uvmap(self, uvset_name):
        #self.character.select_set(action='SELECT')
        blender_utils.select(self.character)
        result = bpy.ops.mesh.uv_texture_add()
        uv_layers = self.character.data.uv_layers
        new_layer = uv_layers[len(uv_layers) - 1]
        uv_layers.active_index = 0
        #self.character.select_set(action='DESELECT')
        blender_utils.deselect(self.character)
        return result

    def set_uvs(self, data, uvset_name):
        for loop in self.character.data.loops:
            self.character.data.uv_layers[uvset_name].data[loop.index].uv = data[str(loop.index)]

    def __create_vertex_group(self, indices, name):
        vertex_group = self.character.vertex_groups.new(name=name)
        vertex_group.add(indices, 1.0, 'ADD')

    def __create_vertex_groups(self, indices_data):
        for name, indices in indices_data.items():
            self.__create_vertex_group(indices, name)

    def set_character_material(self, material_name):
        material = bpy.data.materials.get(material_name)

        blender_utils.select(self.character)
        blender_utils.set_active(self.character)
        for i in range(0, len(bpy.context.active_object.material_slots)):
            bpy.ops.object.material_slot_remove()

        self.character.data.materials.append(material)
        blender_utils.deselect_all()

    def set_character_materials(self):
        self.set_character_material('skin')
        inner_eye_material = bpy.data.materials.get('inner_eyes')
        outer_eye_material = bpy.data.materials.get('outer_eyes')
        eyelashes_material = bpy.data.materials.get('eyelashes')
        materials.set_vertex_group_material(self.character, 'inner_eyes', inner_eye_material)
        materials.set_vertex_group_material(self.character, 'outer_eyes', outer_eye_material)
        materials.set_vertex_group_material(self.character, 'eyelashes', eyelashes_material)

    def add_material(self, material):
        bpy.ops.object.material_slot_add()
        self.character.material_slots[-1].material = material

    def add_hair_material(self):
        self.add_material(bpy.data.materials.get('hair'))
        self.add_material(bpy.data.materials.get('beard'))
        self.add_material(bpy.data.materials.get('mustache'))
        self.add_material(bpy.data.materials.get('brows'))
        bpy.data.particles['hair_preset'].material_slot = 'hair'
        bpy.data.particles['beard_preset'].material_slot = 'beard'
        bpy.data.particles['mustache_preset'].material_slot = 'mustache'

    def tongue_out_random(self):
        randomizer.randomise_attr(self.character, 'Expressions_tongueHoriz', 0.0, 0.75)
        randomizer.randomise_attr(self.character, 'Expressions_tongueOut', 0.6, 0.75)
        randomizer.randomise_attr(self.character, 'Expressions_tongueOutPressure', 0.5, 0.75)
        randomizer.randomise_attr(self.character, 'Expressions_tongueTipUp', 0.0, 0.75)
        randomizer.randomise_attr(self.character, 'Expressions_tongueVert', 0.4, 0.6)

    def open_mouth_random(self):
        open_type = random.randint(0, 2)
        if open_type == 0:
            randomizer.randomise_attr(self.character, 'Expressions_mouthOpen', 0.5, 0.9)
            randomizer.randomise_attr(self.character, 'Expressions_mouthSmileOpen', 0.5, 0.9)
        elif open_type == 1:
            randomizer.randomise_attr(self.character, 'Expressions_mouthOpenO', 0.5, 0.9)
            randomizer.randomise_attr(self.character, 'Expressions_mouthSmileOpen', 0.4, 0.9)
        elif open_type == 2:
            randomizer.randomise_attr(self.character, 'Expressions_mouthOpenLarge', 0.4, 0.9)
            randomizer.randomise_attr(self.character, 'Expressions_mouthSmileOpen', 0.4, 0.9)

    def randomise_attr(self, attr, min, max):
        randomizer.randomise_attr(self.character, attr, min, max)

    def randomize_attributes(self, morphs, factor, update=True):
        randomizer.randomise_character(self.character, morphs, factor, update)

    def randomise_initial_character(self):
        if self.gender and 'Head_SizeY' in randomizer.head_morphs:
            randomizer.head_morphs.remove('Head_SizeY')
        randomizer.randomize_main_morphs(self.character, self.gender)
        randomizer.randomise_character(self.character, randomizer.global_morphs, self.global_morphs_random_factor)
        randomizer.randomise_character(self.character, randomizer.eye_iris_morph, self.eye_iris_morph_random_factor)
        randomizer.randomise_character(self.character, randomizer.eyes_morphs, self.eyes_morphs_random_factor)
        randomizer.randomise_character(self.character, randomizer.eyebrows_morphs, self.eyebrows_morphs_random_factor)
        randomizer.randomise_character(self.character, randomizer.head_morphs, self.head_morphs_random_factor)

    def randomize_initial_lips(self):
        randomizer.randomise_character(self.character, randomizer.lip_morphs, self.lip_morps_random_factor)
        case = random.choice(['mouth_closed', 'mouth_opened', 'tongue_out'])
        if case == 'mouth_closed':
            randomizer.randomise_character(
                self.character, randomizer.lip_expression_morphs_closed, self.lip_expression_morphs_random_factor)
        elif case == 'mouth_opened':
            randomizer.randomise_character(
                self.character, randomizer.lip_expression_morphs_opened, self.lip_expression_morphs_random_factor)
        elif case == 'tongue_out':
            randomizer.randomise_character(
                self.character, randomizer.tongue_expression_morphs, self.lip_expression_morphs_random_factor)

    def randomize_head(self, min, max):
        if self.gender and 'Head_SizeY' in randomizer.head_morphs:
            randomizer.head_morphs.remove('Head_SizeY')
        for morph in randomizer.head_morphs:
            setattr(self.character, morph, random.uniform(min, max))

    def randomize_eyes(self, min, max):
        for morph in randomizer.eyes_morphs:
            setattr(self.character, morph, random.uniform(min, max))

    def manage_eyes_opening(self):
        randomizer.manage_eyes_opening(self.character)

    def randomize_eyebrows(self, min, max):
        for morph in randomizer.eyebrows_morphs:
            setattr(self.character, morph, random.uniform(min, max))

    def remove_modifier(self, modifier):
        modifier = self.character.modifiers[modifier]
        self.character.modifiers.remove(modifier)

    def create_hands_animation(self, start_frame, end_frame, horiz_angle, vert_angle, RMin, RMax):
        blender_utils.select(self.armature)
        blender_utils.set_active(self.armature)
        blender_utils.set_pose_mode()

        right = self.armature.pose.bones['IK_control_a_R']
        right_elbow = self.armature.pose.bones['IK_control_ebw_R']
        right_palm = self.armature.pose.bones['IK_control_h_R']
        right_palm.rotation_mode = 'XYZ'

        left = self.armature.pose.bones['IK_control_a_L']
        left_elbow = self.armature.pose.bones['IK_control_ebw_L']
        left_palm = self.armature.pose.bones['IK_control_h_L']
        left_finger_01 = self.armature.pose.bones['IK_control_fg01_L']
        left_finger_02 = self.armature.pose.bones['IK_control_fg02_L']
        left_finger_03 = self.armature.pose.bones['IK_control_fg03_L']
        left_finger_04 = self.armature.pose.bones['IK_control_fg04_L']
        left_finger_05 = self.armature.pose.bones['IK_control_fg05_L']
        left_fingers = self.armature.pose.bones['IK_control_fg06_L']
        left_palm.rotation_mode = 'XYZ'
        left_finger_01.rotation_mode = 'XYZ'
        left_finger_02.rotation_mode = 'XYZ'
        left_finger_03.rotation_mode = 'XYZ'
        left_finger_04.rotation_mode = 'XYZ'
        left_finger_05.rotation_mode = 'XYZ'
        left_fingers.rotation_mode = 'XYZ'

        spine = self.armature.pose.bones['IK_control_usp']
        spine.rotation_mode = 'XYZ'

        head = self.armature.pose.bones['IK_control_hd']

        xyz1 = blender_utils.get_radial_point(horiz_angle, vert_angle, RMin, RMax)
        right.location[0] = (xyz1[0] - right.head[0])
        right.location[1] = xyz1[2] - right.head[2] + random.uniform (-0.33, 0.33)
        right.location[2] = -(xyz1[1] - right.head[1]) + random.uniform(0, 0.05)
        right_elbow.location[0] = right_elbow.location[1] - 2

        xyz2 = blender_utils.get_radial_point(horiz_angle/1.5, vert_angle/1.5, RMin/2, RMax/2)
        left.location[0] = (xyz2[0] - left.head[0]) + 0.2
        left.location[1] = (xyz2[2] - left.head[2]) - 0.2
        left.location[2] = (-(xyz2[1] - left.head[1])) - 0.2
        left.location[2] = left.location[2] - (random.uniform(0, -0.5))
        left_elbow.location[0] = left_elbow.location[1] + 2
        left_elbow.location[2] = left_elbow.location[2] + random.uniform(0, -1)
        left_palm.rotation_euler = ((random.uniform(25, -25) * pi / 180), (random.uniform(60, -80) * pi / 180), (random.uniform(25, -25) * pi / 180))
        left_finger_01.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_02.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_03.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_04.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_05.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_fingers.rotation_euler[0] = (random.uniform(45, -45) * pi / 180)

        spine.rotation_euler = ((random.uniform(15, -15) * pi / 180), (random.uniform(25, -25) * pi / 180), (random.uniform(5, -5) * pi / 180))

        xyz3 = blender_utils.get_radial_point(15, 15, 1.3, 1.4)
        head.location[0] = (xyz3[0] - head.head[0])
        head.location[1] = (xyz3[2] - head.head[2])
        head.location[2] = (-(xyz3[1] - head.head[1]))

        right.keyframe_insert(data_path='location', frame=start_frame)
        left.keyframe_insert(data_path='location', frame=start_frame)
        left_elbow.keyframe_insert(data_path='location', frame=start_frame)
        left_palm.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_01.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_02.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_03.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_04.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_05.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_fingers.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        spine.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        head.keyframe_insert(data_path='location', frame=start_frame)

        xyz1 = blender_utils.get_radial_point(horiz_angle, vert_angle, RMin, RMax)
        right.location[0] = (xyz1[0] - right.head[0])
        right.location[1] = xyz1[2] - right.head[2]
        right.location[2] = -(xyz1[1] - right.head[1])
        right_elbow.location[0] = right_elbow.location[1] - 2

        xyz2 = blender_utils.get_radial_point(horiz_angle/1.5, vert_angle/1.5, RMin/2, RMax/2)
        left.location[0] = (xyz2[0] - left.head[0]) + 0.2
        left.location[1] = (xyz2[2] - left.head[2]) - 0.2
        left.location[2] = (-(xyz2[1] - left.head[1])) - 0.2
        left.location[2] = left.location[2] - (random.uniform(0, -0.5))
        left_elbow.location[0] = left_elbow.location[1] + 2
        left_elbow.location[2] = left_elbow.location[2] + random.uniform(0, -1)
        left_palm.rotation_euler = ((random.uniform(25, -25) * pi / 180), (random.uniform(60, -80) * pi / 180), (random.uniform(25, -25) * pi / 180))
        left_finger_01.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_02.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_03.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_04.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_05.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_fingers.rotation_euler[0] = (random.uniform(45, -45) * pi / 180)

        spine.rotation_euler = ((random.uniform(15, -15) * pi / 180), (random.uniform(25, -25) * pi / 180), (random.uniform(5, -5) * pi / 180))

        xyz3 = blender_utils.get_radial_point(15, 15, 1.3, 1.4)
        head.location[0] = (xyz3[0] - head.head[0])
        head.location[1] = (xyz3[2] - head.head[2])
        head.location[2] = (-(xyz3[1] - head.head[1]))

        right.keyframe_insert(data_path='location', frame=end_frame)
        left.keyframe_insert(data_path='location', frame=end_frame)
        left_elbow.keyframe_insert(data_path='location', frame=end_frame)
        left_palm.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_01.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_02.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_03.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_04.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_05.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_fingers.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        spine.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        head.keyframe_insert(data_path='location', frame=end_frame)

        blender_utils.deselect_pose_all()
        blender_utils.set_object_mode()
        blender_utils.deselect_all()

    def create_hands_animation_front_case_00(self, start_frame, end_frame, horiz_angle, vert_angle, RMin, RMax, origin, gender):
        vert_list = male_face_indices if gender == 1 else female_face_indices
        res = self.get_vert_coords(vert_list)
        #obj_location = origin.matrix_world.decompose()[0]

        right = bpy.data.objects["right_hand_helper"]
        right_elbow = self.armature.pose.bones['IK_control_ebw_R']
        right_palm = self.armature.pose.bones['IK_control_h_R']
        right_palm.rotation_mode = 'XYZ'

        left = bpy.data.objects["left_hand_helper"]
        left_elbow = self.armature.pose.bones['IK_control_ebw_L']
        left_palm = self.armature.pose.bones['IK_control_h_L']
        left_finger_01 = self.armature.pose.bones['IK_control_fg01_L']
        left_finger_02 = self.armature.pose.bones['IK_control_fg02_L']
        left_finger_03 = self.armature.pose.bones['IK_control_fg03_L']
        left_finger_04 = self.armature.pose.bones['IK_control_fg04_L']
        left_finger_05 = self.armature.pose.bones['IK_control_fg05_L']
        left_fingers = self.armature.pose.bones['IK_control_fg06_L']
        left_palm.rotation_mode = 'XYZ'
        left_finger_01.rotation_mode = 'XYZ'
        left_finger_02.rotation_mode = 'XYZ'
        left_finger_03.rotation_mode = 'XYZ'
        left_finger_04.rotation_mode = 'XYZ'
        left_finger_05.rotation_mode = 'XYZ'
        left_fingers.rotation_mode = 'XYZ'

        spine = self.armature.pose.bones['IK_control_usp']
        spine.rotation_mode = 'XYZ'

        head = bpy.data.objects["head_helper"]

        def create_safepoint(horiz_angle, vert_angle, RMin, RMax, origin):
            xyz = blender_utils.get_radial_point(horiz_angle, vert_angle, RMin, RMax, origin)
            point_location = Vector((xyz))
            close_to = blender_utils.is_point_close_to(res, point_location, 0.06)
            if close_to == True:
                print(close_to)
                create_safepoint(horiz_angle, vert_angle, RMin, RMax, origin)
            return xyz

        xyz1 = create_safepoint(horiz_angle, vert_angle, RMin, RMax, origin)
        right.location[0] = xyz1[0]
        right.location[1] = xyz1[1]
        right.location[2] = xyz1[2]

        right_elbow.location[2] = random.uniform(-0.1, -0.3)
        left_elbow.location[0] = random.uniform(0.5, 1)

        xyz2 = create_safepoint(horiz_angle, vert_angle, RMin/2, RMax/2, origin)
        left.location[0] = xyz2[0]
        left.location[1] = xyz2[1]
        left.location[2] = xyz2[2]

        left_palm.rotation_euler = ((random.uniform(25, -25) * pi / 180), (random.uniform(60, -80) * pi / 180), (random.uniform(25, -25) * pi / 180))
        left_finger_01.rotation_euler[0] = (random.uniform(10, -45) * pi / 180)
        left_finger_02.rotation_euler[0] = (random.uniform(10, -45) * pi / 180)
        left_finger_03.rotation_euler[0] = (random.uniform(10, -45) * pi / 180)
        left_finger_04.rotation_euler[0] = (random.uniform(10, -45) * pi / 180)
        left_finger_05.rotation_euler[0] = (random.uniform(10, -45) * pi / 180)
        left_fingers.rotation_euler[0] = (random.uniform(45, -45) * pi / 180)

        spine.rotation_euler = ((random.uniform(15, -15) * pi / 180), (random.uniform(25, -25) * pi / 180), (random.uniform(5, -5) * pi / 180))

        xyz3 = create_safepoint(15, 15, 1.3, 1.4, origin)
        head.location[0] = xyz3[0]
        head.location[1] = xyz3[1]
        head.location[2] = xyz3[2]

        right.keyframe_insert(data_path='location', frame=start_frame)
        left.keyframe_insert(data_path='location', frame=start_frame)
        left_elbow.keyframe_insert(data_path='location', frame=start_frame)
        left_palm.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_01.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_02.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_03.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_04.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_05.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_fingers.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        spine.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        head.keyframe_insert(data_path='location', frame=start_frame)

        xyz1 = create_safepoint(horiz_angle, vert_angle, RMin, RMax, origin)
        right.location[0] = xyz1[0]
        right.location[1] = xyz1[1]
        right.location[2] = xyz1[2]

        right_elbow.location[2] = random.uniform(-0.1, -0.3)
        left_elbow.location[0] = random.uniform(0.5, 1)

        xyz2 = create_safepoint(horiz_angle, vert_angle, RMin/2, RMax/2, origin)
        left.location[0] = xyz2[0]
        left.location[1] = xyz2[1]
        left.location[2] = xyz2[2]

        left_palm.rotation_euler = ((random.uniform(25, -25) * pi / 180), (random.uniform(60, -80) * pi / 180), (random.uniform(25, -25) * pi / 180))
        left_finger_01.rotation_euler[0] = (random.uniform(10, -45) * pi / 180)
        left_finger_02.rotation_euler[0] = (random.uniform(10, -45) * pi / 180)
        left_finger_03.rotation_euler[0] = (random.uniform(10, -45) * pi / 180)
        left_finger_04.rotation_euler[0] = (random.uniform(10, -45) * pi / 180)
        left_finger_05.rotation_euler[0] = (random.uniform(10, -45) * pi / 180)
        left_fingers.rotation_euler[0] = (random.uniform(45, -45) * pi / 180)

        spine.rotation_euler = ((random.uniform(15, -15) * pi / 180), (random.uniform(25, -25) * pi / 180), (random.uniform(5, -5) * pi / 180))

        xyz3 = create_safepoint(15, 15, 1.3, 1.4, origin)
        head.location[0] = xyz3[0]
        head.location[1] = xyz3[1]
        head.location[2] = xyz3[2]

        right.keyframe_insert(data_path='location', frame=end_frame)
        left.keyframe_insert(data_path='location', frame=end_frame)
        left_elbow.keyframe_insert(data_path='location', frame=end_frame)
        left_palm.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_01.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_02.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_03.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_04.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_05.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_fingers.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        spine.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        head.keyframe_insert(data_path='location', frame=end_frame)

    def create_hands_animation_front_case_01(self, start_frame, end_frame, horiz_angle, vert_angle, RMin, RMax):
        right = self.armature.pose.bones['IK_control_a_R']
        right_elbow = self.armature.pose.bones['IK_control_ebw_R']
        right_palm = self.armature.pose.bones['IK_control_h_R']
        right_palm.rotation_mode = 'XYZ'

        left = self.armature.pose.bones['IK_control_a_L']
        left_elbow = self.armature.pose.bones['IK_control_ebw_L']
        left_palm = self.armature.pose.bones['IK_control_h_L']
        left_finger_01 = self.armature.pose.bones['IK_control_fg01_L']
        left_finger_02 = self.armature.pose.bones['IK_control_fg02_L']
        left_finger_03 = self.armature.pose.bones['IK_control_fg03_L']
        left_finger_04 = self.armature.pose.bones['IK_control_fg04_L']
        left_finger_05 = self.armature.pose.bones['IK_control_fg05_L']
        left_fingers = self.armature.pose.bones['IK_control_fg06_L']
        left_palm.rotation_mode = 'XYZ'
        left_finger_01.rotation_mode = 'XYZ'
        left_finger_02.rotation_mode = 'XYZ'
        left_finger_03.rotation_mode = 'XYZ'
        left_finger_04.rotation_mode = 'XYZ'
        left_finger_05.rotation_mode = 'XYZ'
        left_fingers.rotation_mode = 'XYZ'

        spine = self.armature.pose.bones['IK_control_usp']
        spine.rotation_mode = 'XYZ'

        head = self.armature.pose.bones['IK_control_hd']

        xyz1 = blender_utils.get_radial_point(horiz_angle, vert_angle, RMin, RMax)
        # right.location[0] = (xyz1[0] - right.head[0])
        # right.location[1] = xyz1[2] - right.head[2] + random.uniform (-0.33, 0.33)
        # right.location[2] = -(xyz1[1] - right.head[1]) + random.uniform(0, 0.05)
        right.location = (0.59, 0.28, 0.62)
        right_elbow.location[0] = right_elbow.location[1] - 2

        xyz2 = blender_utils.get_radial_point(horiz_angle/1.5, vert_angle/1.5, RMin/2, RMax/2)
        # left.location[0] = (xyz2[0] - left.head[0]) + 0.2
        # left.location[1] = (xyz2[2] - left.head[2]) - 0.2
        # left.location[2] = (-(xyz2[1] - left.head[1])) - 0.2
        # left.location[2] = left.location[2] - (random.uniform(0, -0.5))
        left.location = (random.uniform(-0.405325, -0.385325), random.uniform(0.342284, 0.492284), random.uniform(-0.082263, 0.217737))
        left_elbow.location[0] = left_elbow.location[1] + 2
        left_elbow.location[2] = left_elbow.location[2] + random.uniform(-1.5, -2)
        left_palm.rotation_euler = ((random.uniform(25, -25) * pi / 180), (random.uniform(60, -80) * pi / 180), (random.uniform(25, -25) * pi / 180))
        left_finger_01.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_02.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_03.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_04.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_05.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_fingers.rotation_euler[0] = (random.uniform(45, -45) * pi / 180)

        spine.rotation_euler = ((random.uniform(15, -15) * pi / 180), (random.uniform(25, -25) * pi / 180), (random.uniform(5, -5) * pi / 180))

        xyz3 = blender_utils.get_radial_point(15, 15, 1.3, 1.4)
        head.location[0] = (xyz3[0] - head.head[0])
        head.location[1] = (xyz3[2] - head.head[2])
        head.location[2] = (-(xyz3[1] - head.head[1]))

        right.keyframe_insert(data_path='location', frame=start_frame)
        left.keyframe_insert(data_path='location', frame=start_frame)
        left_elbow.keyframe_insert(data_path='location', frame=start_frame)
        left_palm.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_01.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_02.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_03.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_04.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_05.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_fingers.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        spine.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        head.keyframe_insert(data_path='location', frame=start_frame)

        xyz1 = blender_utils.get_radial_point(horiz_angle, vert_angle, RMin, RMax)
        right.location = (0.59, 0.28, random.uniform(0.25, 0.35))
        # right.location[0] = (xyz1[0] - right.head[0])
        # right.location[1] = xyz1[2] - right.head[2]
        # right.location[2] = -(xyz1[1] - right.head[1])
        right_elbow.location[0] = right_elbow.location[1] - 2

        xyz2 = blender_utils.get_radial_point(horiz_angle/1.5, vert_angle/1.5, RMin/2, RMax/2)
        # left.location[0] = (xyz2[0] - left.head[0]) + 0.2
        # left.location[1] = (xyz2[2] - left.head[2]) - 0.2
        # left.location[2] = (-(xyz2[1] - left.head[1])) - 0.2
        # left.location[2] = left.location[2] - (random.uniform(0, -0.5))
        left.location = (random.uniform(-0.136954, -0.006954), random.uniform(0.510439, 0.200438), random.uniform(-0.064273, 0.425727))
        left_elbow.location[0] = left_elbow.location[1] + 2
        left_elbow.location[2] = left_elbow.location[2] + random.uniform(0, -1)
        left_palm.rotation_euler = ((random.uniform(25, -25) * pi / 180), (random.uniform(60, -80) * pi / 180), (random.uniform(25, -25) * pi / 180))
        left_finger_01.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_02.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_03.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_04.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_05.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_fingers.rotation_euler[0] = (random.uniform(45, -45) * pi / 180)

        spine.rotation_euler = ((random.uniform(15, -15) * pi / 180), (random.uniform(25, -25) * pi / 180), (random.uniform(5, -5) * pi / 180))

        xyz3 = blender_utils.get_radial_point(15, 15, 1.3, 1.4)
        head.location[0] = (xyz3[0] - head.head[0])
        head.location[1] = (xyz3[2] - head.head[2])
        head.location[2] = (-(xyz3[1] - head.head[1]))

        right.keyframe_insert(data_path='location', frame=end_frame)
        left.keyframe_insert(data_path='location', frame=end_frame)
        left_elbow.keyframe_insert(data_path='location', frame=end_frame)
        left_palm.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_01.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_02.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_03.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_04.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_05.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_fingers.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        spine.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        head.keyframe_insert(data_path='location', frame=end_frame)

    def create_hands_animation_front_case_02(self, start_frame, end_frame, horiz_angle, vert_angle, RMin, RMax):
        track_target = bpy.data.objects["track_target"]

        right = self.armature.pose.bones['IK_control_a_R']
        right_elbow = self.armature.pose.bones['IK_control_ebw_R']
        right_palm = self.armature.pose.bones['IK_control_h_R']
        right_palm.rotation_mode = 'XYZ'

        left = self.armature.pose.bones['IK_control_a_L']
        left_elbow = self.armature.pose.bones['IK_control_ebw_L']
        left_palm = self.armature.pose.bones['IK_control_h_L']
        left_finger_01 = self.armature.pose.bones['IK_control_fg01_L']
        left_finger_02 = self.armature.pose.bones['IK_control_fg02_L']
        left_finger_03 = self.armature.pose.bones['IK_control_fg03_L']
        left_finger_04 = self.armature.pose.bones['IK_control_fg04_L']
        left_finger_05 = self.armature.pose.bones['IK_control_fg05_L']
        left_fingers = self.armature.pose.bones['IK_control_fg06_L']
        left_palm.rotation_mode = 'XYZ'
        left_finger_01.rotation_mode = 'XYZ'
        left_finger_02.rotation_mode = 'XYZ'
        left_finger_03.rotation_mode = 'XYZ'
        left_finger_04.rotation_mode = 'XYZ'
        left_finger_05.rotation_mode = 'XYZ'
        left_fingers.rotation_mode = 'XYZ'

        spine = self.armature.pose.bones['IK_control_usp']
        spine.rotation_mode = 'XYZ'

        head = self.armature.pose.bones['IK_control_hd']

        xyz1 = blender_utils.get_radial_point(horiz_angle, vert_angle, RMin, RMax)
        # right.location[0] = (xyz1[0] - right.head[0])
        # right.location[1] = xyz1[2] - right.head[2] + random.uniform (-0.33, 0.33)
        # right.location[2] = -(xyz1[1] - right.head[1]) + random.uniform(0, 0.05)
        right.location = (0.59, 0.28, 0.62)
        right_elbow.location[0] = right_elbow.location[1] - 2

        xyz2 = blender_utils.get_radial_point(horiz_angle/1.5, vert_angle/1.5, RMin/2, RMax/2)
        # left.location[0] = (xyz2[0] - left.head[0]) + 0.2
        # left.location[1] = (xyz2[2] - left.head[2]) - 0.2
        # left.location[2] = (-(xyz2[1] - left.head[1])) - 0.2
        # left.location[2] = left.location[2] - (random.uniform(0, -0.5))
        left.location = (random.uniform(-0.405325, -0.385325), random.uniform(0.342284, 0.492284), random.uniform(-0.082263, 0.217737))
        left_elbow.location[0] = left_elbow.location[1] + 2
        left_elbow.location[2] = left_elbow.location[2] + random.uniform(-1.5, -2)
        left_palm.rotation_euler = ((random.uniform(25, -25) * pi / 180), (random.uniform(60, -80) * pi / 180), (random.uniform(25, -25) * pi / 180))
        left_finger_01.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_02.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_03.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_04.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_05.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_fingers.rotation_euler[0] = (random.uniform(45, -45) * pi / 180)
        track_target.location[0] = track_target.location[0] + random.uniform(-0.2, 0.2)
        track_target.location[2] = track_target.location[2] + random.uniform(-0.2, 0.2)

        spine.rotation_euler = ((random.uniform(15, -15) * pi / 180), (random.uniform(25, -25) * pi / 180), (random.uniform(5, -5) * pi / 180))

        xyz3 = blender_utils.get_radial_point(15, 15, 1.3, 1.4)
        head.location[0] = (xyz3[0] - head.head[0])
        head.location[1] = (xyz3[2] - head.head[2])
        head.location[2] = (-(xyz3[1] - head.head[1]))

        right.keyframe_insert(data_path='location', frame=start_frame)
        left.keyframe_insert(data_path='location', frame=start_frame)
        left_elbow.keyframe_insert(data_path='location', frame=start_frame)
        left_palm.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_01.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_02.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_03.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_04.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_05.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_fingers.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        spine.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        head.keyframe_insert(data_path='location', frame=start_frame)
        track_target.keyframe_insert(data_path='location', frame=start_frame)

        xyz1 = blender_utils.get_radial_point(horiz_angle, vert_angle, RMin, RMax)
        right.location = (0.59, 0.28, random.uniform(0.25, 0.35))
        # right.location[0] = (xyz1[0] - right.head[0])
        # right.location[1] = xyz1[2] - right.head[2]
        # right.location[2] = -(xyz1[1] - right.head[1])
        right_elbow.location[0] = right_elbow.location[1] - 2

        xyz2 = blender_utils.get_radial_point(horiz_angle/1.5, vert_angle/1.5, RMin/2, RMax/2)
        # left.location[0] = (xyz2[0] - left.head[0]) + 0.2
        # left.location[1] = (xyz2[2] - left.head[2]) - 0.2
        # left.location[2] = (-(xyz2[1] - left.head[1])) - 0.2
        # left.location[2] = left.location[2] - (random.uniform(0, -0.5))
        left.location = (random.uniform(-0.136954, -0.006954), random.uniform(0.510439, 0.200438), random.uniform(-0.064273, 0.425727))
        left_elbow.location[0] = left_elbow.location[1] + 2
        left_elbow.location[2] = left_elbow.location[2] + random.uniform(0, -1)
        left_palm.rotation_euler = ((random.uniform(25, -25) * pi / 180), (random.uniform(60, -80) * pi / 180), (random.uniform(25, -25) * pi / 180))
        left_finger_01.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_02.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_03.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_04.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_05.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_fingers.rotation_euler[0] = (random.uniform(45, -45) * pi / 180)
        track_target.location[0] = track_target.location[0] + random.uniform(-0.2, 0.2)
        track_target.location[2] = track_target.location[2] + random.uniform(-0.2, 0.2)

        spine.rotation_euler = ((random.uniform(15, -15) * pi / 180), (random.uniform(25, -25) * pi / 180), (random.uniform(5, -5) * pi / 180))

        xyz3 = blender_utils.get_radial_point(15, 15, 1.3, 1.4)
        head.location[0] = (xyz3[0] - head.head[0])
        head.location[1] = (xyz3[2] - head.head[2])
        head.location[2] = (-(xyz3[1] - head.head[1]))

        right.keyframe_insert(data_path='location', frame=end_frame)
        left.keyframe_insert(data_path='location', frame=end_frame)
        left_elbow.keyframe_insert(data_path='location', frame=end_frame)
        left_palm.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_01.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_02.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_03.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_04.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_05.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_fingers.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        spine.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        head.keyframe_insert(data_path='location', frame=end_frame)
        track_target.keyframe_insert(data_path='location', frame=end_frame)

    def create_hands_animation_front_case_03(self, start_frame, end_frame, horiz_angle, vert_angle, RMin, RMax):
        print('CASE 3!')
        track_target = bpy.data.objects["track_target"]

        right = self.armature.pose.bones['IK_control_a_R']
        right_elbow = self.armature.pose.bones['IK_control_ebw_R']
        right_palm = self.armature.pose.bones['IK_control_h_R']
        right_palm.rotation_mode = 'XYZ'

        left = self.armature.pose.bones['IK_control_a_L']
        left_elbow = self.armature.pose.bones['IK_control_ebw_L']
        left_palm = self.armature.pose.bones['IK_control_h_L']
        left_finger_01 = self.armature.pose.bones['IK_control_fg01_L']
        left_finger_02 = self.armature.pose.bones['IK_control_fg02_L']
        left_finger_03 = self.armature.pose.bones['IK_control_fg03_L']
        left_finger_04 = self.armature.pose.bones['IK_control_fg04_L']
        left_finger_05 = self.armature.pose.bones['IK_control_fg05_L']
        left_fingers = self.armature.pose.bones['IK_control_fg06_L']
        left_palm.rotation_mode = 'XYZ'
        left_finger_01.rotation_mode = 'XYZ'
        left_finger_02.rotation_mode = 'XYZ'
        left_finger_03.rotation_mode = 'XYZ'
        left_finger_04.rotation_mode = 'XYZ'
        left_finger_05.rotation_mode = 'XYZ'
        left_fingers.rotation_mode = 'XYZ'

        spine = self.armature.pose.bones['IK_control_usp']
        spine.rotation_mode = 'XYZ'

        head = self.armature.pose.bones['IK_control_hd']

        xyz1 = blender_utils.get_radial_point(horiz_angle, vert_angle, RMin, RMax)
        # right.location[0] = (xyz1[0] - right.head[0])
        # right.location[1] = xyz1[2] - right.head[2] + random.uniform (-0.33, 0.33)
        # right.location[2] = -(xyz1[1] - right.head[1]) + random.uniform(0, 0.05)
        right.location = (0.444681 + random.uniform(-0.2, 0.2), 0.836794 + random.uniform(-0.2, 0.2) , 0.219917 + random.uniform(-0.2, 0.2))
        right_elbow.location[0] = right_elbow.location[1] - 2

        xyz2 = blender_utils.get_radial_point(horiz_angle/1.5, vert_angle/1.5, RMin/2, RMax/2)
        # left.location[0] = (xyz2[0] - left.head[0]) + 0.2
        # left.location[1] = (xyz2[2] - left.head[2]) - 0.2
        # left.location[2] = (-(xyz2[1] - left.head[1])) - 0.2
        # left.location[2] = left.location[2] - (random.uniform(0, -0.5))
        left.location = (random.uniform(-0.405325, -0.385325), random.uniform(0.342284, 0.492284), random.uniform(-0.082263, 0.217737))
        # left_elbow.location[0] = left_elbow.location[1] + 2
        # left_elbow.location[2] = left_elbow.location[2] + random.uniform(-1.5, -2)
        left_elbow.location = (0.152913, -0.710604, 0.088514)
        left_palm.rotation_euler = ((random.uniform(25, -25) * pi / 180), (random.uniform(60, -80) * pi / 180), (random.uniform(25, -25) * pi / 180))
        left_finger_01.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_02.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_03.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_04.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_05.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_fingers.rotation_euler[0] = (random.uniform(45, -45) * pi / 180)
        track_target.location[0] = track_target.location[0] + random.uniform(-0.2, 0.2)
        track_target.location[2] = track_target.location[2] + random.uniform(-0.2, 0.2)

        spine.rotation_euler = ((random.uniform(15, -15) * pi / 180), (random.uniform(25, -25) * pi / 180), (random.uniform(5, -5) * pi / 180))

        xyz3 = blender_utils.get_radial_point(15, 15, 1.3, 1.4)
        # head.location[0] = (xyz3[0] - head.head[0])
        # head.location[1] = (xyz3[2] - head.head[2])
        # head.location[2] = (-(xyz3[1] - head.head[1]))
        head.location = (1.05795 + random.uniform(-0.2, 0.2), -0.09231 + random.uniform(-0.2, 0.2), -0.201006 + random.uniform(-0.2, 0.2))

        right.keyframe_insert(data_path='location', frame=start_frame)
        left.keyframe_insert(data_path='location', frame=start_frame)
        left_elbow.keyframe_insert(data_path='location', frame=start_frame)
        left_palm.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_01.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_02.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_03.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_04.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_finger_05.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        left_fingers.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        spine.keyframe_insert(data_path='rotation_euler', frame=start_frame)
        head.keyframe_insert(data_path='location', frame=start_frame)
        track_target.keyframe_insert(data_path='location', frame=start_frame)

        xyz1 = blender_utils.get_radial_point(horiz_angle, vert_angle, RMin, RMax)
        right.location = (0.444681 + random.uniform(-0.2, 0.2), 0.836794 + random.uniform(-0.2, 0.2) , 0.219917 + random.uniform(-0.2, 0.2))
        # right.location[0] = (xyz1[0] - right.head[0])
        # right.location[1] = xyz1[2] - right.head[2]
        # right.location[2] = -(xyz1[1] - right.head[1])
        right_elbow.location[0] = right_elbow.location[1] - 2

        xyz2 = blender_utils.get_radial_point(horiz_angle/1.5, vert_angle/1.5, RMin/2, RMax/2)
        # left.location[0] = (xyz2[0] - left.head[0]) + 0.2
        # left.location[1] = (xyz2[2] - left.head[2]) - 0.2
        # left.location[2] = (-(xyz2[1] - left.head[1])) - 0.2
        # left.location[2] = left.location[2] - (random.uniform(0, -0.5))
        left.location = (-0.380522 + random.uniform(-0.2, 0.2), 0.496587 + random.uniform(-0.2, 0.2), -0.007659 + random.uniform(-0.2, 0.2))
        left_elbow.location = (0.152913, -0.710604, 0.088514)
        left_palm.rotation_euler = ((random.uniform(25, -25) * pi / 180), (random.uniform(60, -80) * pi / 180), (random.uniform(25, -25) * pi / 180))
        left_finger_01.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_02.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_03.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_04.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_finger_05.rotation_euler[0] = (random.uniform(25, -80) * pi / 180)
        left_fingers.rotation_euler[0] = (random.uniform(45, -45) * pi / 180)
        track_target.location[0] = track_target.location[0] + random.uniform(-0.2, 0.2)
        track_target.location[2] = track_target.location[2] + random.uniform(-0.2, 0.2)

        spine.rotation_euler = ((random.uniform(15, -15) * pi / 180), (random.uniform(25, -25) * pi / 180), (random.uniform(5, -5) * pi / 180))

        xyz3 = blender_utils.get_radial_point(15, 15, 1.3, 1.4)
        # head.location[0] = (xyz3[0] - head.head[0])
        # head.location[1] = (xyz3[2] - head.head[2])
        # head.location[2] = (-(xyz3[1] - head.head[1]))
        head.location = (1.05795 + random.uniform(-0.2, 0.2), -0.09231 + random.uniform(-0.2, 0.2), -0.201006 + random.uniform(-0.2, 0.2))

        right.keyframe_insert(data_path='location', frame=end_frame)
        left.keyframe_insert(data_path='location', frame=end_frame)
        left_elbow.keyframe_insert(data_path='location', frame=end_frame)
        left_palm.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_01.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_02.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_03.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_04.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_finger_05.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        left_fingers.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        spine.keyframe_insert(data_path='rotation_euler', frame=end_frame)
        head.keyframe_insert(data_path='location', frame=end_frame)
        track_target.keyframe_insert(data_path='location', frame=end_frame)

    def create_hands_animation_hand_gestures_00(self, start_frame, end_frame, horiz_angle, vert_angle, RMin, RMax, origin, gender):
        vert_list = male_face_indices if gender == 1 else female_face_indices
        res = self.get_vert_coords(vert_list)

        right = bpy.data.objects["right_hand_helper"]
        right_elbow = self.armature.pose.bones['IK_control_ebw_R']
        right_palm = self.armature.pose.bones['IK_control_h_R']
        right_palm.rotation_mode = 'XYZ'

        left = bpy.data.objects["left_hand_helper"]
        left_elbow = self.armature.pose.bones['IK_control_ebw_L']
        left_palm = self.armature.pose.bones['IK_control_h_L']
        left_finger_01 = self.armature.pose.bones['IK_control_fg01_L']
        left_finger_02 = self.armature.pose.bones['IK_control_fg02_L']
        left_finger_03 = self.armature.pose.bones['IK_control_fg03_L']
        left_finger_04 = self.armature.pose.bones['IK_control_fg04_L']
        left_finger_05 = self.armature.pose.bones['IK_control_fg05_L']
        left_fingers = self.armature.pose.bones['IK_control_fg06_L']
        left_palm.rotation_mode = 'XYZ'
        left_finger_01.rotation_mode = 'XYZ'
        left_finger_02.rotation_mode = 'XYZ'
        left_finger_03.rotation_mode = 'XYZ'
        left_finger_04.rotation_mode = 'XYZ'
        left_finger_05.rotation_mode = 'XYZ'
        left_fingers.rotation_mode = 'XYZ'

        spine = self.armature.pose.bones['IK_control_usp']
        spine.rotation_mode = 'XYZ'

        head = bpy.data.objects["head_helper"]

        right.location = random.uniform(-0.09, -0.01), random.uniform(-0.5, -0.55), random.uniform(1.5, 1.55)
        right_elbow.location[0] = random.uniform(-0.5, -0.53)
        left.location = random.uniform(0.14, 0.17), random.uniform(-0.27, -0.2), random.uniform(1.5, 1.58)
        left_elbow.location = random.uniform(0.3, 0.5), random.uniform(-0.9, -1.0), random.uniform(0, -0.6)
        #head.location = (1.05795 + random.uniform(-0.05, 0.05), -0.09231 + random.uniform(-0.05, 0.05), -0.201006 + random.uniform(-0.05, 0.05))

        right.keyframe_insert(data_path='location', frame=start_frame)
        right_elbow.keyframe_insert(data_path='location', frame=start_frame)
        left.keyframe_insert(data_path='location', frame=start_frame)
        left_elbow.keyframe_insert(data_path='location', frame=start_frame)
        head.keyframe_insert(data_path='location', frame=start_frame)

        right.location = random.uniform(-0.09, -0.01), random.uniform(-0.5, -0.55), random.uniform(1.5, 1.55)
        right_elbow.location[0] = random.uniform(-0.5, -0.53)
        left.location = random.uniform(0.14, 0.17), random.uniform(-0.27, -0.2), random.uniform(1.5, 1.58)
        left_elbow.location = random.uniform(0.3, 0.5), random.uniform(-0.9, -1.0), random.uniform(0, -0.6)
        #head.location = (1.05795 + random.uniform(-0.2, 0.2), -0.09231 + random.uniform(-0.2, 0.2), -0.201006 + random.uniform(-0.2, 0.2))

        right.keyframe_insert(data_path='location', frame=end_frame)
        right_elbow.keyframe_insert(data_path='location', frame=end_frame)
        left.keyframe_insert(data_path='location', frame=end_frame)
        left_elbow.keyframe_insert(data_path='location', frame=end_frame)
        head.keyframe_insert(data_path='location', frame=start_frame)

    def remove_bone_constrains(self, bone_list):
        for bone_name in bone_list:
            bone = bpy.context.object.pose.bones.get(bone_name)
            for constrain in bone.constraints:
                bone.constraints.remove(constrain)

    def create_mouth_animation(self, start_frame, end_frame):
        keyframes = [start_frame, start_frame + (end_frame - start_frame) / 2, end_frame]
        for keyframe in keyframes:
            self.open_mouth_random()
            for morph in randomizer.mouth_expression_morphs:
                self.character.keyframe_insert(data_path=morph, frame=keyframe)

    def is_tongue_out(self):
        mesh = self.character.to_mesh(apply_modifiers=True, depsgraph=bpy.context.depsgraph)
        vertices = [vertex.co for vertex in mesh.vertices]
        tongue_vert = vertices[self.tongue_tip_index]
        lip_vert = vertices[self.lip_tip_index]
        return tongue_vert.y < lip_vert.y

    def get_vert_coords(self, verts):
        mesh = self.character.to_mesh(apply_modifiers=True, depsgraph=bpy.context.depsgraph)
        return [mesh.vertices[v].co for v in verts]

    def dump_properties(self, path, counter=0, **kwargs):
        self.dump_blender_properties(path, counter)
        self.dump_human_properties(path, counter, **kwargs)

    def dump_blender_properties(self, path, counter):
        blender_utils.makedirs(path / 'character_meta')
        proporties_path = path / 'character_meta' / 'frame_{:07}.json'.format(counter)
        data = OrderedDict()
        with open(proporties_path, 'w') as file:
            for prop in self.blender_character_proporties:
                data[prop] = getattr(self.character, prop)
            json.dump(data, file, indent=4)

    def dump_human_properties(self, path, counter, **kwargs):
        blender_utils.makedirs(path / 'human_meta')
        proporties_path = path / 'human_meta' / 'frame_{:07}.json'.format(counter)
        data = OrderedDict()
        with open(proporties_path, 'w') as file:
            for prop in self.human_object_proporties:
                value = getattr(self, prop)
                if isinstance(value, Path):
                    data[prop] = str(value)
                elif isinstance(value, tuple):
                    data[prop] = list(map(str, value))
                else:
                    data[prop] = value
            if kwargs:
                data.update(kwargs)
            json.dump(data, file, indent=4)

    def set_pose(self, pose_data):
        module_logger.info('')
        for counter, bone in enumerate(self.all_bones):
            bone_angles = pose_data[counter*3: counter*3+3]
            bone = bpy.context.object.pose.bones.get(bone)
            if bone:
                bone.rotation_mode = 'XYZ'
                bone.rotation_euler = mathutils.Euler((bone_angles[0], bone_angles[1], bone_angles[2]))

    def dump_pose_data(self, path, counter):
        module_logger.info('')
        blender_utils.makedirs(path / 'character_pose_meta')
        pose_file = path / 'character_pose_meta' / 'frame_{:07}.json'.format(counter)
        data = list()
        with open(pose_file, 'w') as f:
            for bone in self.all_bones:
                rotation_euler = bpy.context.object.pose.bones[bone].rotation_euler
                data.extend([rotation_euler.x, rotation_euler.y, rotation_euler.z])
            json.dump(data, f)

    def dump_head_meta(self):
        blender_utils.deselect_all()

        head_meta_helper = bpy.data.objects['head_meta_helper']
        arm = bpy.data.armatures["MBLab_skeleton_base_ik"]
        scene = bpy.data.scenes["Scene"]

        self.armature.data.layers[0] = True
        self.armature.data.layers[1] = True

        blender_utils.select(head_meta_helper)
        blender_utils.select(self.armature)
        blender_utils.set_active(self.armature)
        blender_utils.set_pose_mode()
        blender_utils.deselect_pose_all()

        arm.bones.active = arm.bones["head"]
        bpy.context.object.data.bones["head"].select = True

        head_loc = bpy.data.objects[self.armature.name].location + bpy.context.active_pose_bone.head
        head_meta_helper.location = (head_loc[0], head_loc[1], head_loc[2])

        blender_utils.deselect_pose_all()
        blender_utils.set_mode('OBJECT')
        blender_utils.deselect_all()
        blender_utils.select(head_meta_helper)
        blender_utils.set_active(head_meta_helper)

        bpy.ops.object.constraint_add(type='TRACK_TO')
        head_meta_helper.constraints["Track To"].target = bpy.data.objects[self.armature.name]
        head_meta_helper.constraints["Track To"].subtarget = "IK_control_hd"
        scene.update()

        head_loc, head_rot, head_scale = bpy.data.objects['head_meta_helper'].matrix_world.decompose()
        return head_loc, head_rot, head_scale
