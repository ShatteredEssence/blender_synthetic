import bpy
import random
import logging
from utils import blender_utils


module_logger = logging.getLogger(__name__)

def add_hair(character):
    module_logger.info('')
    blender_utils.select(character)
    blender_utils.set_active(character)
    add_particle_system(character, 'hair_preset', 'Hair', 'sculp', None, True)
    blender_utils.set_object_mode()
    blender_utils.deselect_all()

def allign_collider(skeleton, armature, collider, bone):
    blender_utils.deselect_all()
    blender_utils.select(collider)
    blender_utils.select(armature)
    blender_utils.set_active(armature)
    blender_utils.set_pose_mode()
    blender_utils.deselect_pose_all()
    skeleton.bones.active = skeleton.bones[bone]
    bpy.context.object.data.bones[bone].select = True
    bpy.ops.object.parent_set(type='BONE')
    blender_utils.deselect_pose_all()
    blender_utils.select(collider)
    blender_utils.set_active(collider)
    bpy.ops.object.parent_clear(type='CLEAR_INVERSE')
    bpy.ops.object.location_clear(clear_delta=False)
    bpy.ops.object.rotation_clear(clear_delta=False)
    blender_utils.set_object_mode()
    blender_utils.deselect_all()

def allign_colliders(armature):
    module_logger.info('')
    armature.data.layers[0] = True
    armature.data.layers[1] = True

    head_collider = bpy.data.objects['head_collider']
    neck_collider = bpy.data.objects['neck_collider']
    shoulder_collider_l = bpy.data.objects['shoulder_collider_l']
    shoulder_collider_r = bpy.data.objects['shoulder_collider_r']
    skeleton = bpy.data.armatures.get("MBLab_skeleton_base_ik") or bpy.data.armatures.get("MBLab_skeleton_base_fk")

    allign_collider(skeleton, armature, head_collider, "head")
    allign_collider(skeleton, armature, neck_collider, "neck")
    allign_collider(skeleton, armature, shoulder_collider_l, "clavicle_L")
    allign_collider(skeleton, armature, shoulder_collider_r, "clavicle_R")

def styler_to_head(armature):
    module_logger.info('')
    style_holder = bpy.data.objects['styler_01_holder']

    armature.data.layers[0] = True
    armature.data.layers[1] = True

    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    # Set Neck Collider

    blender_utils.select(style_holder)
    blender_utils.select(armature)
    blender_utils.set_active(armature)
    bpy.ops.object.mode_set(mode='POSE')
    blender_utils.deselect_pose_all()
    arm = bpy.data.armatures.get("MBLab_skeleton_base_ik") or bpy.data.armatures.get("MBLab_skeleton_base_fk")
    arm.bones.active = arm.bones["head"]
    bpy.context.object.data.bones["head"].select = True
    bpy.ops.object.parent_set(type='BONE')
    blender_utils.deselect_pose_all()
    blender_utils.select(style_holder)
    blender_utils.set_active(style_holder)
    bpy.ops.object.parent_clear(type='CLEAR_INVERSE')
    bpy.ops.object.location_clear(clear_delta=False)
    bpy.ops.object.rotation_clear(clear_delta=False)
    bpy.ops.object.parent_clear(type='CLEAR_KEEP_TRANSFORM')
    bpy.ops.object.rotation_clear(clear_delta=False)
    #    bpy.ops.pose.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')

def add_particle_system(character, preset_name, name, vertex_group, mask, use_hair_dynamics):
    module_logger.info('')
    particle_systems_count = len(bpy.context.object.particle_systems)
    character.modifiers.new(name, type='PARTICLE_SYSTEM')
    bpy.context.object.particle_systems[particle_systems_count].name = name
    preset = bpy.data.particles[preset_name]
    character.particle_systems[name].settings = preset
    bpy.context.object.particle_systems[name].vertex_group_density = vertex_group
    bpy.context.object.particle_systems[name].use_hair_dynamics = use_hair_dynamics
    if mask:
        preset.texture_slots[0].blend_type = 'MULTIPLY'
        preset.texture_slots[0].use_map_time = False
        preset.texture_slots[0].use_map_length = True
        preset.texture_slots[0].length_factor = 1.0
        preset.texture_slots[0].texture.image = bpy.data.images.load(str(mask))

def create_facial_hairs(character, has_beard, has_mustache, beard_map, mustache_map):
    module_logger.info('')
    blender_utils.set_object_mode()
    blender_utils.deselect_all()
    blender_utils.select(character)
    blender_utils.set_active(character)

    if has_mustache == 1:
        add_particle_system(character, 'mustache_preset', 'Mustache', 'mustache', mustache_map, False)

    if has_beard == 1:
        add_particle_system(character, 'beard_preset', 'Beard', 'beard', beard_map, False)

    blender_utils.deselect_all()

    facial_hair_length = random.uniform(0.001, 0.005)
    bpy.data.particles["beard_preset"].hair_length = facial_hair_length
    bpy.data.particles["beard_preset"].kink = 'NO'
    bpy.data.particles["mustache_preset"].hair_length = facial_hair_length
    bpy.data.particles["mustache_preset"].kink = 'NO'

    '''
    scene = bpy.context.scene
    if has_beard == 1:
        setup_bending_damping(character, 'Beard', prewarm_frame_count, frame_count)
        character.particle_systems['Beard'].point_cache.frame_start = scene.frame_start
        character.particle_systems['Beard'].point_cache.frame_start = scene.frame_end

    if has_mustache == 1:
        setup_bending_damping(character, 'Mustache', prewarm_frame_count, frame_count)
        character.particle_systems['Mustache'].point_cache.frame_start = scene.frame_start
        character.particle_systems['Mustache'].point_cache.frame_start = scene.frame_end
    '''

def setup_hair_style(character, styles):
    module_logger.info('Character %s | Style %s', character.name, styles)
    hair_ramp = bpy.data.objects['hair_ramp']
    style_vortex = bpy.data.objects['styler_vortex_01']
    styler_vortex_l = bpy.data.objects['styler_vortex_l_01']
    styler_vortex_r = bpy.data.objects['styler_vortex_r_01']
    styler_vortex_backwards = bpy.data.objects['styler_vortex_backwards_01']
    turbulence = bpy.data.objects['turbulence_01']
    wind_f = bpy.data.objects['wind_f_01']
    wind_l = bpy.data.objects['wind_l_01']
    wind_r = bpy.data.objects['wind_r_01']
    hair_curve_01 = bpy.data.objects['hair_curve_01']
    hair_curve_02 = bpy.data.objects['hair_curve_02']


    blender_utils.deselect_all()
    blender_utils.set_active(character)
    character.modifiers["Hair"].show_viewport = False

    def randomize_style(kink, kink_amplitude, cloth_bending_stiffness, kink_frequency):
        roughness_1 = random.uniform(0, 0.005)
        roughness_2 = random.uniform(0, 0.005)
        roughness_2_threshold = random.uniform(0, 1)
        bpy.data.particles["hair_preset"].kink = kink
        bpy.data.particles["hair_preset"].kink_amplitude = kink_amplitude
        bpy.data.particles["hair_preset"].kink_frequency = kink_frequency
        bpy.data.particles["hair_preset"].roughness_1 = roughness_1
        bpy.data.particles["hair_preset"].roughness_2 = roughness_2
        bpy.data.particles["hair_preset"].roughness_2_threshold = roughness_2_threshold
        bpy.context.object.particle_systems["Hair"].cloth.settings.bending_stiffness = cloth_bending_stiffness


    def randimize_field_strength(obj, value):
        blender_utils.select(obj)
        blender_utils.set_active(obj)
        bpy.context.object.field.strength = value
        blender_utils.deselect_all()

    def animate_field_strength(obj, data_path, *anim_data):
        blender_utils.select(obj)
        blender_utils.set_active(obj)
        for frame, value in anim_data:
            obj.field.strength = value
            obj.keyframe_insert(data_path=data_path, frame=frame)
        blender_utils.deselect_all()
    style = random.choice(styles)
    if style == 'short':
        bpy.data.particles["hair_preset"].hair_length = random.uniform(0.01, 0.03)
        bpy.data.particles["hair_preset"].kink = 'NO'

        randomstyle = random.randint(1, 5)
        if randomstyle == 1:
            randomize_style('CURL', random.uniform(0.005, 0.01), random.uniform(0.1, 0.25), random.uniform(0.005, 0.007))
        if randomstyle == 2:
            randomize_style('RADIAL', random.uniform(0.01, 1), random.uniform(0.1, 0.25), random.uniform(0.005, 0.007))
        if randomstyle == 3:
            randomize_style('WAVE', random.uniform(0.001, 0.03), random.uniform(0.1, 0.25), random.uniform(0.005, 0.007))
        if randomstyle == 4:
            randomize_style('BRAID', random.uniform(0.015, 0.03), random.uniform(0.1, 0.25), random.uniform(0.005, 0.007))
        if randomstyle == 5:
            # randomize_style('NO', random.uniform(0.001, 0.01), random.uniform(0.25, 0.4))
            bpy.data.particles["hair_preset"].kink = 'NO'

        # Set The random Factors!!!!
        # Set styler vortex
        randimize_field_strength(style_vortex, random.uniform(2, 4))
        randimize_field_strength(styler_vortex_backwards, random.uniform(1, 2.5))

        side = random.choice(['left', 'right'])
        if side == 'left':
            randimize_field_strength(styler_vortex_l, random.uniform(2, 4))
            randimize_field_strength(styler_vortex_r, random.uniform(0.1, 0.5))
            animate_field_strength(wind_r, 'field.strength', (0, random.uniform(0, 4)), (30, 0))
            animate_field_strength(wind_l, 'field.strength', (0, random.uniform(0, 0.5)), (30, 0))
        elif side == 'right':
            randimize_field_strength(styler_vortex_r, random.uniform(2, 4))
            randimize_field_strength(styler_vortex_l, random.uniform(0.1, 0.5))
            animate_field_strength(wind_r, 'field.strength', (0, random.uniform(0, 0.5)), (30, 0))
            animate_field_strength(wind_l, 'field.strength', (0, random.uniform(0, 4)), (30, 0))

        animate_field_strength(wind_f, 'field.strength', (0, random.uniform(2, -4)), (30, 0))       # Set backwind
        randimize_field_strength(hair_ramp, random.uniform(0, -4))                                  # Set hair ramp
        animate_field_strength(turbulence, 'field.strength', (0, random.uniform(1.5, 4)), (30, 0))  # Set Turbulence
        randimize_field_strength(hair_curve_01, random.uniform(0, -5))                              # Set Curve 01
        randimize_field_strength(hair_curve_02, random.uniform(0, -5))                              # Set Curve 02

    elif style == 'mid':
        bpy.data.particles["hair_preset"].hair_length = random.uniform(0.03, 0.05)
        bpy.data.particles["hair_preset"].kink = 'NO'

        randomstyle = random.randint(1, 5)
        if randomstyle == 1:
            randomize_style('CURL', random.uniform(0.005, 0.01), random.uniform(0.25, 0.4), random.uniform(0.005, 0.007))
        if randomstyle == 2:
            randomize_style('RADIAL', random.uniform(0.01, 1), random.uniform(0.25, 0.4), random.uniform(0.005, 0.007))
        if randomstyle == 3:
            randomize_style('WAVE', random.uniform(0.001, 0.01), random.uniform(0.25, 0.4), random.uniform(0.005, 0.007))
        if randomstyle == 4:
            randomize_style('BRAID', random.uniform(0.015, 0.01), random.uniform(0.25, 0.4), random.uniform(0.005, 0.007))
        if randomstyle == 5:
            # randomize_style('NO', random.uniform(0.001, 0.01), random.uniform(0.25, 0.4))
            bpy.data.particles["hair_preset"].kink = 'NO'

        # Set The random Factors!!!!
        # Set styler vortex
        randimize_field_strength(style_vortex, random.uniform(5, 8))
        randimize_field_strength(styler_vortex_backwards, random.uniform(1, 2.5))

        side = random.choice(['left', 'right'])
        if side == 'left':
            randimize_field_strength(styler_vortex_l, random.uniform(5, 10))
            randimize_field_strength(styler_vortex_r, random.uniform(0.1, 1))
            animate_field_strength(wind_r, 'field.strength', (0, random.uniform(0, 8)), (30, 0))
            animate_field_strength(wind_l, 'field.strength', (0, random.uniform(0, 1)), (30, 0))
        elif side == 'right':
            randimize_field_strength(styler_vortex_r, random.uniform(5, 10))
            randimize_field_strength(styler_vortex_l, random.uniform(0.1, 1))
            animate_field_strength(wind_r, 'field.strength', (0, random.uniform(0, 1)), (30, 0))
            animate_field_strength(wind_l, 'field.strength', (0, random.uniform(0, 8)), (30, 0))

        animate_field_strength(wind_f, 'field.strength', (0, random.uniform(4, -8)), (30, 0))       # Set backwind
        randimize_field_strength(hair_ramp, random.uniform(0, -5))                                  # Set hair ramp
        animate_field_strength(turbulence, 'field.strength', (0, random.uniform(3, 8)), (30, 0))  # Set Turbulence
        randimize_field_strength(hair_curve_01, random.uniform(0, -10))                            # Set Curve 01
        randimize_field_strength(hair_curve_02, random.uniform(0, -10))

    # elif style == 'long':
    #     bpy.data.particles["hair_preset"].hair_length = random.uniform(0.15, 0.3)
    #     bpy.data.particles["hair_preset"].kink = 'NO'
    #     randomstyle = random.randint(1, 5)
    #     if randomstyle == 1:
    #         randomize_style('CURL', random.uniform(0.05, 0.1), random.uniform(0.25, 0.4), random.uniform(0.1, 0.5))
    #     if randomstyle == 2:
    #         randomize_style('RADIAL', random.uniform(0.05, 0.1), random.uniform(0.25, 0.4), random.uniform(0.1, 0.5))
    #     if randomstyle == 3:
    #         randomize_style('WAVE', random.uniform(0.05, 0.1), random.uniform(0.25, 0.4), random.uniform(0.1, 0.5))
    #     if randomstyle == 4:
    #         randomize_style('BRAID', random.uniform(0.05, 0.1), random.uniform(0.25, 0.4), random.uniform(0.1, 0.5))
    #     if randomstyle == 5:
    #         # randomize_style('NO', random.uniform(0.001, 0.01), random.uniform(0.25, 0.4))
    #         bpy.data.particles["hair_preset"].kink = 'NO'
    #
    #     human.keyframe_insert(data_path = 'modifiers["Hair"].particle_system.settings.object_align_factor[1]', frame = 0)
    #     human.modifiers['Hair'].particle_system.settings.object_align_factor[1] = 0
    #
    #     bpy.data.particles["hair_preset"].object_align_factor[1] = 0.5
    #     bpy.data.particles["hair_preset"].keyframe_insert(data_path = 'object_align_factor', frame =  0)
    #     bpy.data.particles["hair_preset"].object_align_factor[1] = 0.5
    #     bpy.data.particles["hair_preset"].keyframe_insert(data_path = 'object_align_factor', frame = 10)
    #     bpy.data.particles["hair_preset"].object_align_factor[1] = 0.0
    #     bpy.data.particles["hair_preset"].keyframe_insert(data_path = 'object_align_factor', frame = 20)


    #     randomstyle = random.randint(1, 5)
    #     if randomstyle == 1:
    #         randomize_style('CURL', random.uniform(0.05, 0.1), random.uniform(0.25, 0.4), random.uniform(0.1, 0.5))
    #     if randomstyle == 2:
    #         randomize_style('RADIAL', random.uniform(0.05, 0.1), random.uniform(0.25, 0.4), random.uniform(0.1, 0.5))
    #     if randomstyle == 3:
    #         randomize_style('WAVE', random.uniform(0.05, 0.1), random.uniform(0.25, 0.4), random.uniform(0.1, 0.5))
    #     if randomstyle == 4:
    #         randomize_style('BRAID', random.uniform(0.05, 0.1), random.uniform(0.25, 0.4), random.uniform(0.1, 0.5))
    #     if randomstyle == 5:
    #         # randomize_style('NO', random.uniform(0.001, 0.01), random.uniform(0.25, 0.4))
    #         bpy.data.particles["hair_preset"].kink = 'NO'

    #     # Set The random Factors!!!!
    #     # Set styler vortex
    #     randimize_field_strength(style_vortex, random.uniform(5, 8))
    #     randimize_field_strength(styler_vortex_backwards, random.uniform(2, 3))

    #     side = random.choice(['left', 'right'])
    #     if side == 'left':
    #         randimize_field_strength(styler_vortex_l, random.uniform(5, 10))
    #         randimize_field_strength(styler_vortex_r, random.uniform(0.1, 1))
    #         animate_field_strength(wind_r, 'field.strength', (0, random.uniform(0, 8)), (30, 0))
    #         animate_field_strength(wind_l, 'field.strength', (0, random.uniform(0, 1)), (30, 0))
    #     elif side == 'right':
    #         randimize_field_strength(styler_vortex_r, random.uniform(5, 10))
    #         randimize_field_strength(styler_vortex_l, random.uniform(0.1, 1))
    #         animate_field_strength(wind_r, 'field.strength', (0, random.uniform(0, 1)), (30, 0))
    #         animate_field_strength(wind_l, 'field.strength', (0, random.uniform(0, 8)), (30, 0))

    #     animate_field_strength(wind_f, 'field.strength', (0, random.uniform(4, -8)), (30, 0))       # Set backwind
    #     randimize_field_strength(hair_ramp, random.uniform(0, -5))                                  # Set hair ramp
    #     animate_field_strength(turbulence, 'field.strength', (0, random.uniform(3, 8)), (30, 0))    # Set Turbulence
    #     randimize_field_strength(hair_curve_01, random.uniform(0, 5))                             # Set Curve 01
    #     randimize_field_strength(hair_curve_02, random.uniform(0, 5))
    # character.modifiers["Hair"].show_viewport = True

def set_up_hair_cache(character):
    module_logger.info('')
    scene = bpy.data.scenes["Scene"]
    blender_utils.select(character)
    blender_utils.set_active(character)

    hair = character.particle_systems["Hair"]

    hair.point_cache.frame_start = scene.frame_start
    hair.point_cache.frame_end = scene.frame_end
    hair.point_cache.use_disk_cache = False

    blender_utils.set_object_mode()
    blender_utils.deselect_all()

def cache_hair(character):
    module_logger.info('')
    scene = bpy.data.scenes["Scene"]
    hair = character.particle_systems["Hair"]
    override = { "blend_data": bpy.data, "scene": scene, "active_object": character.name, "point_cache": hair.point_cache }
    bpy.context.scene.frame_set(1)
    bpy.ops.ptcache.bake(override, bake=True)
    bpy.context.scene.frame_set(0)
