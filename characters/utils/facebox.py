import bpy
import os
import numpy as np
from pathlib import Path
from config import cfg
from bpy_extras.object_utils import world_to_camera_view
from utils import blender_utils as utils

data_path = Path(cfg['Paths']['data_path'])

def get_facebox_indices(prefix):
    return utils.load_json(data_path / 'json/{}_facebox_indices.json'.format(prefix))

def write_facebox_indices(camera, folderpath, character, facebox_indices, frame, resolution_x, resolution_y):
    render = bpy.context.scene.render
    vertices = [vertex.co for vertex in character.data.vertices]
    coords_2d = np.array([world_to_camera_view(bpy.context.scene, camera.camera_object, coordinate) for coordinate in vertices])

    utils.makedirs(folderpath / 'facebox')

    counter = 0
    with open('{}/{}/frame_{:07}.csv'.format(folderpath, 'facebox', frame), 'w') as facebox_file:
        for index in facebox_indices:
            x, y = coords_2d[index][0], coords_2d[index][1]
            facebox_file.write(str(counter) + '\t' + str(resolution_x * x) + '\t' + str(resolution_y - resolution_y * y) + '\n')
            counter += 1
