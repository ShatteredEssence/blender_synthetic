import bpy
import random
import numpy as np
import logging


module_logger = logging.getLogger('selfie_runner')


hands_morphs = [
    'Wrists_Size',
    'Hands_FingersDiam',
    'Hands_FingersInterDist',
    'Hands_FingersLength',
    'Hands_FingersTipSize',
    'Hands_Lenght',
    'Hands_Mass',
    # 'Hands_NailsLength',
    'Hands_PalmLength',
    'Hands_Size',
    'Hands_Tone'
]

head_morphs = [
    'Head_CraniumDolichocephalic',
    'Head_CraniumPlatycephalus',
    'Head_CraniumPentagonoides',
    'Head_Size',
    'Head_Nucha',
    'Head_Flat',
    'Head_SizeZ',
    'Head_SizeX',
    'Head_SizeY'
]

eye_iris_morph = [
    'Eyes_IrisSize'
]

eyes_morphs = [
    'Eyes_BagProminence',
    'Eyes_BagSize',
    'Eyes_Crosscalibration',
    'Eyes_InnerPosX',
    'Eyes_InnerPosZ',
    'Eyes_PupilSize',
    'Eyes_OuterPosX',
    'Eyes_InnerPosZ',
    'Eyes_PosX',
    'Eyes_PosZ',
    'Eyes_Size',
    'Eyes_SizeZ',
    'Eyes_TypeAlmond',
    'Eyes_TypeHooded',
    'Eyes_innerSinus'
]

eyebrows_morphs = [
    'Eyebrows_Angle',
    'Eyebrows_Droop',
    'Eyebrows_PosZ',
    'Eyebrows_Ridge',
    'Eyebrows_SizeY',
    'Eyebrows_Tone'
]

ears_morphs = [
    'Ears_Lobe',
    'Ears_LocY',
    'Ears_LocZ',
    'Ears_RotX',
    'Ears_Round',
    'Ears_SizeX',
    'Ears_SizeY',
    'Ears_SizeZ'
]

global_morphs = [
    'Face_Ellipsoid',
    'Face_Parallelepiped',
    'Face_Triangle',
    'Cheeks_CreaseExt',
    'Cheeks_InfraVolume',
    'Cheeks_Mass',
    'Cheeks_SideCrease',
    'Cheeks_Tone',
    'Cheeks_Zygom',
    'Cheeks_ZygomPosZ',
    'Chin_Cleft',
    'Chin_Prominence',
    'Chin_SizeX',
    'Chin_SizeZ',
    'Chin_Tone',
    'Ears_Lobe',
    'Ears_LocY',
    'Ears_LocZ',
    'Ears_RotX',
    'Ears_Round',
    'Ears_SizeX',
    'Ears_SizeY',
    'Ears_SizeZ',
    'Eyelids_Angle',
    'Eyelids_Crease',
    'Eyelids_InnerPosZ',
    'Eyelids_LowerCurve',
    'Eyelids_MiddlePosZ',
    'Eyelids_OuterPosZ',
    'Eyelids_SizeZ',
    'Forehead_Angle',
    'Forehead_Curve',
    'Forehead_SizeX',
    'Forehead_SizeZ',
    'Forehead_Temple',
    'Jaw_Angle',
    'Jaw_Angle2',
    'Jaw_LocY',
    'Jaw_Prominence',
    'Jaw_ScaleX',
    'Nose_BallSizeX',
    'Nose_BasePosZ',
    'Nose_BaseShape',
    'Nose_BaseSizeX',
    'Nose_BaseSizeZ',
    'Nose_BridgeSizeX',
    'Nose_Curve',
    'Nose_GlabellaPosZ',
    'Nose_GlabellaSizeX',
    'Nose_GlabellaSizeY',
    'Nose_NostrilCrease',
    'Nose_NostrilDiam',
    'Nose_NostrilPosZ',
    'Nose_NostrilSizeX',
    'Nose_NostrilSizeY',
    'Nose_PosY',
    'Nose_SeptumFlat',
    'Nose_SeptumRolled',
    'Nose_SizeY',
    'Nose_TipAngle',
    'Nose_TipPosZ',
    'Nose_TipSize',
    'Nose_WingAngle',
    'Nose_WingBackFlat',
    'Nose_WingBump',
    'Mouth_CornersPosZ',
    'Mouth_LowerlipExt',
    'Mouth_LowerlipSizeZ',
    'Mouth_LowerlipVolume',
    'Mouth_PhiltrumProminence',
    'Mouth_PhiltrumSizeX',
    'Mouth_PhiltrumSizeY',
    'Mouth_PosY',
    'Mouth_PosZ',
    'Mouth_Protusion',
    'Mouth_SideCrease',
    'Mouth_SizeX',
    'Mouth_UpperlipExt',
    'Mouth_UpperlipSizeZ',
    'Mouth_UpperlipVolume'
]

expression_morphs = [
    'Expressions_browOutVertL',
    'Expressions_browOutVertR',
    'Expressions_browSqueezeL',
    'Expressions_browSqueezeR',
    'Expressions_browsMidVert',
    'Expressions_cheekSneerL',
    'Expressions_cheekSneerR',
    'Expressions_chestExpansion',
    'Expressions_deglutition',
    'Expressions_eyeClosedL',
    'Expressions_eyeClosedPressureL',
    'Expressions_eyeClosedPressureR',
    'Expressions_eyeClosedR',
    'Expressions_eyeSquintL',
    'Expressions_eyeSquintR',
    'Expressions_eyesHoriz',
    'Expressions_eyesVert',
    'Expressions_jawHoriz',
    'Expressions_jawOut',
    'Expressions_nostrilsExpansion',
    'Expressions_pupilsDilatation'
]

lip_morphs = [
    'Mouth_CornersPosZ',
    'Mouth_LowerlipExt',
    'Mouth_LowerlipSizeZ',
    'Mouth_LowerlipVolume',
    'Mouth_PhiltrumProminence',
    'Mouth_PhiltrumSizeX',
    'Mouth_PhiltrumSizeY',
    'Mouth_PosY',
    'Mouth_PosZ',
    'Mouth_Protusion',
    'Mouth_Protusion',
    'Mouth_SideCrease',
    'Mouth_SizeX',
    'Mouth_UpperlipExt',
    'Mouth_UpperlipSizeZ',
    'Mouth_UpperlipVolume'
]

lip_expression_morphs_closed = [
    'Expressions_jawHoriz',
    'Expressions_jawOut',
    'Expressions_mouthBite',
    'Expressions_mouthChew',
    'Expressions_mouthClosed',
    'Expressions_mouthHoriz',
    'Expressions_mouthInflated',
    'Expressions_mouthLowerOut',
    'Expressions_mouthOpenAggr',
    'Expressions_mouthOpenTeethClosed',
    'Expressions_mouthSmile',
    'Expressions_mouthSmileL',
    'Expressions_mouthSmileOpen',
    'Expressions_mouthSmileOpen2',
    'Expressions_mouthSmileR'
]

lip_expression_morphs_opened = [
    'Expressions_mouthOpen',
    'Expressions_mouthOpenAggr'
]

tongue_expression_morphs = [
    'Expressions_tongueHoriz',
    'Expressions_tongueOut',
    'Expressions_tongueOutPressure',
    'Expressions_tongueTipUp',
    'Expressions_tongueVert'
]

mouth_expression_morphs = [
    'Expressions_mouthBite',
    'Expressions_mouthChew',
    'Expressions_mouthClosed',
    'Expressions_mouthHoriz',
    'Expressions_mouthInflated',
    'Expressions_mouthLowerOut',
    'Expressions_mouthOpen',
    'Expressions_mouthOpenAggr',
    'Expressions_mouthOpenHalf',
    'Expressions_mouthOpenLarge',
    'Expressions_mouthOpenO',
    'Expressions_mouthOpenTeethClosed',
    'Expressions_mouthSmile',
    'Expressions_mouthSmileL',
    'Expressions_mouthSmileOpen',
    'Expressions_mouthSmileOpen2',
    'Expressions_mouthSmileR'
]

all_expression_morphs = expression_morphs + mouth_expression_morphs
all_face_morphs = global_morphs + eyes_morphs + eye_iris_morph + eyebrows_morphs + ['character_age', 'character_mass', 'character_tone']

def randomise_attr(character, attr, min, max):
    if not hasattr(character, attr):
        module_logger.error("Object doesnt have attribute '%s'", attr)
        raise Exception("Object doesnt have attribute '{}'".format(attr))
    val = random.uniform(min, max)
    setattr(character, attr, val)

def randomise_character(character, morphs, factor, update=True):
    min = 0.4
    max = 0.6
    min = min / factor
    max = max * factor
    
    if min <= 0:
        min = 0
    if max >= 1:
        max = 1

    for morph in morphs:
        randomise_attr(character, morph, min, max)

    if update:
        bpy.context.scene.mblab_show_measures = False

def get_tone_range(gender):
    return (-0.5, 0.3) if gender == 0 else (-0.5, 0.6)

def get_mass_range(gender):
    return (-0.2, 0.2) if gender == 0 else (0.1, 0.2)

def randomize_main_morphs(character, gender):
    randomise_attr(character, 'character_age', 0, 1)
    
    mass_range = get_mass_range(gender)
    randomise_attr(character, 'character_mass', mass_range[0], mass_range[1])
    
    tone_range = get_tone_range(gender)
    randomise_attr(character, 'character_tone', tone_range[0], tone_range[1])

def create_expressions_animation(character, start_frame, end_frame):
    for frame in [start_frame, end_frame]:
        randomise_character(character, expression_morphs, 1.6, False)
        randomise_character(character, mouth_expression_morphs, 1.1, False)
        randomise_eyes_position(character)
        manage_eyes_opening(character)
        for morph in expression_morphs + mouth_expression_morphs:
            character.keyframe_insert(data_path=morph, frame=frame)

def randomise_eyes_position(character):
    setattr(character, 'Expressions_eyesHoriz', random.uniform(0.1, 0.9))
    setattr(character, 'Expressions_eyesVert', random.uniform(0.1, 0.9))

def close_eye_randomly(character, sides):
    weight = np.random.uniform(0.5, 0.9)
    for side in sides:
        setattr(character, 'Expressions_eyeClosed' + side, weight)
        setattr(character, 'Expressions_eyeClosedPressure' + side, 0.5)
    bpy.context.scene.mblab_show_measures = False

def close_eye_full(character, sides):
    for side in sides:
        setattr(character, 'Expressions_eyeClosed' + side, 0.95)
        setattr(character, 'Expressions_eyeClosedPressure' + side, 0.5)
    setattr(character, 'Expressions_eyesVert', 0.0)
    bpy.context.scene.mblab_show_measures = False

def open_eye_randomly(character, sides):
    weight = np.random.uniform(0.0, 0.5)
    for side in sides:
        setattr(character, 'Expressions_eyeClosed' + side, weight)
        setattr(character, 'Expressions_eyeClosedPressure' + side, weight)
    bpy.context.scene.mblab_show_measures = False

def open_eye_full(character, sides):
    for side in sides:
        setattr(character, 'Expressions_eyeClosed' + side, 0.0)
        setattr(character, 'Expressions_eyeClosedPressure' + side, 0.0)
    bpy.context.scene.mblab_show_measures = False

def manage_eyes_opening(character):
    opening_type = np.random.choice(['OPEN_FULL', 'OPEN_RAND', 'CLOSE_FULL', 'CLOSE_RAND'], p=[0.25, 0.25, 0.25, 0.25])
    which_side = np.random.choice([('L', 'R'), ('L', ), ('R', )], p=[0.5, 0.25, 0.25])
    action = {
        'OPEN_FULL': open_eye_full,
        'OPEN_RAND': open_eye_randomly,
        'CLOSE_FULL': close_eye_full,
        'CLOSE_RAND': close_eye_randomly,
    }
    action[opening_type](character, which_side)
