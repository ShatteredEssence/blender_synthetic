import bpy
import json
from utils import blender_utils
from .randomizer import all_expression_morphs, all_face_morphs
from characters.blender_character import f_character_proporties, m_character_proporties
from bpy_extras.object_utils import world_to_camera_view


def write_morphs(character, path, morphs):
    with open(path, 'w') as file:
        for morph in morphs:
            file.write('{}: {}'.format(morph, getattr(character, morph)) + '\n')

def write_face_morphs(character, folderpath, iteration):
    blender_utils.makedirs(folderpath / 'face_parameters')
    path = folderpath / 'face_parameters/frame_{:07}.csv'.format(iteration)
    write_morphs(character, path, all_face_morphs)

def write_expression_morphs(character, folderpath, iteration):
    blender_utils.makedirs(folderpath / 'expression_parameters')
    path = folderpath / 'expression_parameters/frame_{:07}.csv'.format(iteration)
    write_morphs(character, path, all_expression_morphs)

def write_character_proporties(human, path):
    character_proporties = f_character_proporties if human.gender == 0 else m_character_proporties
    data = dict()
    with open(path, 'w') as file:
        data['gender'] = human.gender
        for prop in character_proporties:
            data[prop] = getattr(human.character, prop)
        json.dump(data, file, indent=4)

def get_bone_coord_dict(bone_coords):
    return {
        'xh': bone_coords[0].x, 'yh': bone_coords[0].y, 'zh': bone_coords[0].z,
        'xt': bone_coords[1].x, 'yt': bone_coords[1].y, 'zt': bone_coords[1].z
    }

def get_quaternions(bone_list, armature):
    quaternions = dict()
    for bone_name in bone_list:
        bone = armature.pose.bones[bone_name]
        quaternion = bone.rotation_quaternion
        quaternions[bone_name] = {'w': quaternion.w, 'x': quaternion.x, 'y': quaternion.y, 'z': quaternion.z}
    return quaternions

def dump_2d_and_3d_coords(path, human, camera, itaretion):
    blender_utils.makedirs(path / 'skeleton_2d')
    blender_utils.makedirs(path / 'skeleton_3d')
    blender_utils.makedirs(path / 'quaternions')

    hand_screen_coords = dict()
    hand_scene_coords = dict()

    human_bones = human.armature.pose.bones
    bones_to_export = human.finger_r_bones + human.arm_r_bones

    coords3d = [(human_bones[bone].head, human_bones[bone].tail) for bone in bones_to_export]
    coords2d = [(world_to_camera_view(bpy.context.scene, camera.camera_object, c[0]),
                 world_to_camera_view(bpy.context.scene, camera.camera_object, c[1])) for c in coords3d]

    for i, bone in enumerate(bones_to_export):
        hand_screen_coords[bone] = get_bone_coord_dict(coords2d[i])
        hand_scene_coords[bone] = get_bone_coord_dict(coords3d[i])

    quaternions = get_quaternions(bones_to_export, human.armature)

    # landmark_utils.write_hand_landmarks(camera, path, human, frame)
    coords2d_path = path / 'skeleton_2d' / 'frame_{:07}.json'.format(itaretion)
    coords3d_path = path / 'skeleton_3d' / 'frame_{:07}.json'.format(itaretion)
    quaternions_path = path / 'quaternions' / 'frame_{:07}.json'.format(itaretion)
    with open(coords2d_path, 'w') as f:
        json.dump(hand_screen_coords, f, indent=4)
    with open(coords3d_path, 'w') as f:
        json.dump(hand_scene_coords, f, indent=4)
    with open(quaternions_path, 'w') as f:
        json.dump(quaternions, f, indent=4)
