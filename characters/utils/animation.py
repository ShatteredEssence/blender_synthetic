import bpy
import json
import mathutils
import random
import numpy as np
from math import pi, radians
from utils import blender_utils
from utils.blender_utils import euler2quaternion
from characters.blender_character import BLENDER_CHARACTER_FINGER_BONES, BLENDER_CHARACTER_ARM_BONES
from characters.blender_character import BLENDER_CHARACTER_ARM_L_BONES, BLENDER_CHARACTER_ARM_R_BONES

available_angles = {
    'far_up': {
        'clavicle_R': {'x': 17, 'y': 1, 'z': [-15, 60]},
        'upperarm_R': {'x': 62, 'y': 4, 'z': -2},
        'lowerarm_R': {'x': [0, 30], 'y': 4, 'z': 0},
        'hand_R': {'x': [-25, 25], 'y': [130, 180], 'z': [-25, 25]},
    },
    'far_down': {
        'clavicle_R': {'x': 17, 'y': 1, 'z': [-15, 60]},
        'upperarm_R': {'x': 62, 'y': 4, 'z': -2},
        'lowerarm_R': {'x': [0, 30], 'y': 4, 'z': 0},
        'hand_R': {'x': [-25, 25], 'y': [-40, 10], 'z': [-25, 25]},
    },
    'far_slava': {
        'clavicle_R': {'x': 17, 'y': 1, 'z': [-15, 60]},
        'upperarm_R': {'x': 62, 'y': 4, 'z': -2},
        'lowerarm_R': {'x': [0, 60], 'y': 4, 'z': 0},
        'hand_R': {'x': [-50, 50], 'y': [-90, 90], 'z': [-25, 25]},
    },
    'close': {
        'clavicle_R': {'x': -4, 'y': 1.5, 'z': [-15, 55]},
        'upperarm_R': {'x': 60, 'y': 4, 'z': 5},
        'lowerarm_R': {'x': [80, 93], 'y': 4, 'z': 0},
        'hand_R': {'x': [-45, 45], 'y': [-90, 90], 'z': [-30, 30]},
    },
    'selfie_gesture': {
        'lowerarm_L': {'x': [115, 125], 'y': [-5, 5], 'z': [-5, 5]},
    }
}

def set_hand_pose(dist):
    angles_map = available_angles[dist]
    for bone_name in BLENDER_CHARACTER_ARM_BONES:
        bone = bpy.context.object.pose.bones.get(bone_name)
        available_bone_angles = angles_map.get(bone_name)
        if bone and available_bone_angles:
            bone.rotation_mode = 'XYZ'
            x_angle = available_bone_angles['x']
            y_angle = available_bone_angles['y']
            z_angle = available_bone_angles['z']
            x_angle = random.randrange(x_angle[0], x_angle[1]) if isinstance(x_angle, list) else x_angle
            y_angle = random.randrange(y_angle[0], y_angle[1]) if isinstance(y_angle, list) else y_angle
            z_angle = random.randrange(z_angle[0], z_angle[1]) if isinstance(z_angle, list) else z_angle
            bone.rotation_euler = mathutils.Euler((radians(x_angle), radians(y_angle), radians(z_angle)), 'XYZ')
            bone.rotation_quaternion = bone.rotation_euler.to_quaternion()
            if bone_name == 'hand_R':
                upperarm_twist_bone = bpy.context.object.pose.bones.get('upperarm_twist_R')
                upperarm_twist_bone.rotation_mode = 'XYZ'
                upperarm_twist_bone.rotation_euler = mathutils.Euler((0, radians(y_angle / 4), 0), 'XYZ')
                upperarm_twist_bone.rotation_quaternion = upperarm_twist_bone.rotation_euler.to_quaternion()
                lowerarm_twist_bone = bpy.context.object.pose.bones.get('lowerarm_twist_R')
                lowerarm_twist_bone.rotation_mode = 'XYZ'
                lowerarm_twist_bone.rotation_euler = mathutils.Euler((0, radians(y_angle / 2), 0), 'XYZ')
                lowerarm_twist_bone.rotation_quaternion = lowerarm_twist_bone.rotation_euler.to_quaternion()
            if bone_name == 'hand_L':
                upperarm_twist_bone = bpy.context.object.pose.bones.get('upperarm_twist_L')
                upperarm_twist_bone.rotation_mode = 'XYZ'
                upperarm_twist_bone.rotation_euler = mathutils.Euler((0, radians(y_angle / 4), 0), 'XYZ')
                upperarm_twist_bone.rotation_quaternion = upperarm_twist_bone.rotation_euler.to_quaternion()
                lowerarm_twist_bone = bpy.context.object.pose.bones.get('lowerarm_twist_L')
                lowerarm_twist_bone.rotation_mode = 'XYZ'
                lowerarm_twist_bone.rotation_euler = mathutils.Euler((0, radians(y_angle / 2), 0), 'XYZ')
                lowerarm_twist_bone.rotation_quaternion = lowerarm_twist_bone.rotation_euler.to_quaternion()

def set_finger_pose(pose_data):
    for bone_name, euler in pose_data.items():
        if bone_name in BLENDER_CHARACTER_FINGER_BONES:
            bone = bpy.context.object.pose.bones.get(bone_name)
            if bone:
                bone.rotation_mode = 'XYZ'
                bone.rotation_euler = mathutils.Euler((euler['x'], euler['y'], euler['z']), 'XYZ')
                bone.rotation_quaternion = bone.rotation_euler.to_quaternion()

def set_pose(pose_file):
    with open(pose_file, 'r') as f:
        pose_data = json.loads(f.read())
        for bone_name, euler in pose_data.items():
            bone = bpy.context.object.pose.bones.get(bone_name)
            if bone:
                bone.rotation_mode = 'XYZ'
                bone.rotation_euler = mathutils.Euler((euler['x'], euler['y'], euler['z']), 'XYZ')
                bone.rotation_quaternion = bone.rotation_euler.to_quaternion()

def set_hand_p(pose_file):
    with open(pose_file, 'r') as f:
        pose_data = json.loads(f.read())
        for bone_name, euler in pose_data.items():
            if bone_name in BLENDER_CHARACTER_ARM_L_BONES+BLENDER_CHARACTER_ARM_R_BONES:
                bone = bpy.context.object.pose.bones.get(bone_name)
                if bone:
                    bone.rotation_mode = 'XYZ'
                    bone.rotation_euler = mathutils.Euler((euler['x'], euler['y'], euler['z']), 'XYZ')
                    bone.rotation_quaternion = bone.rotation_euler.to_quaternion()

def load_bvh_animation(path):
    bpy.ops.mbast.load_animation(filepath=path)

def key_pose(pose_data, frame):
    for bone_name in pose_data:
        if bone_name in BLENDER_CHARACTER_ARM_BONES + BLENDER_CHARACTER_FINGER_BONES:
            bone = bpy.context.object.pose.bones.get(bone_name)
            bone.keyframe_insert(data_path='rotation_euler', frame=frame)

def zero_finger_pose(armature):
    for bone in BLENDER_CHARACTER_FINGER_BONES:
        bone = armature.pose.bones.get(bone)
        if bone:
            bone.rotation_euler.zero()

def save_pose(pose_file, pose_name):
    with open(pose_file, 'w') as f:
        pose_data = dict(pose=pose_name)
        for bone in BLENDER_CHARACTER_ARM_BONES:
            rotation_euler = bpy.context.object.pose.bones[bone].rotation_euler
            bone_data = dict(x=rotation_euler.x, y=rotation_euler.y, z=rotation_euler.z, order=rotation_euler.order)
            pose_data[bone] = bone_data
        json.dump(pose_data, f, indent=4)

def create_hands_runner_animation_00(start_frame, end_frame, armature, horiz_angle, vert_angle, RMin, RMax, origin):
    blender_utils.select(armature)
    blender_utils.set_active(armature)
    blender_utils.set_pose_mode()

    right = bpy.data.objects["right_hand_helper"]

    right_elbow = bpy.data.objects["left_hand_helper"]
    right_palm = armature.pose.bones['IK_control_h_R']
    right_upperarm_twist = armature.pose.bones['upperarm_twist_R']
    right_upperarm_twist.rotation_mode = 'XYZ'
    right_palm.rotation_mode = 'XYZ'

    right_finger_01 = armature.pose.bones['IK_control_fg01_R']
    right_finger_02 = armature.pose.bones['IK_control_fg02_R']
    right_finger_03 = armature.pose.bones['IK_control_fg03_R']
    right_finger_04 = armature.pose.bones['IK_control_fg04_R']
    right_finger_05 = armature.pose.bones['IK_control_fg05_R']
    right_fingers = armature.pose.bones['IK_control_fg06_R']
    right_palm.rotation_mode = 'XYZ'
    right_finger_01.rotation_mode = 'XYZ'
    right_finger_02.rotation_mode = 'XYZ'
    right_finger_03.rotation_mode = 'XYZ'
    right_finger_04.rotation_mode = 'XYZ'
    right_finger_05.rotation_mode = 'XYZ'
    right_fingers.rotation_mode = 'XYZ'

    spine = armature.pose.bones['IK_control_usp']
    spine.rotation_mode = 'XYZ'

    head = armature.pose.bones['IK_control_hd']

    xyz1 = blender_utils.get_radial_point(horiz_angle, vert_angle, RMin, RMax, origin)

    hand_offset_horiz = -0.045  # this number is the x offset from palm root to hand geometry center, feel free to correct it
    # hand_offset_vert

    right.location[0] = xyz1[0] + hand_offset_horiz
    right.location[1] = xyz1[1]
    right.location[2] = xyz1[2]

    right_elbow.location[2] = random.uniform(-0.2, -0.6)

    # hand rotation for cases
    right_palm_rotation_angle = (random.uniform(-25, 10) * pi / 180), (random.uniform(-5, 25) * pi / 180), (
                random.uniform(-15, 15) * pi / 180)
    right_palm.rotation_euler = (
    right_palm_rotation_angle[0], right_palm_rotation_angle[1] / 2, right_palm_rotation_angle[2])
    right_upperarm_twist.rotation_euler[1] = right_palm_rotation_angle[1] / 2

    # dinger rotation for cases
    finger_bend_angle = (random.uniform(10, -15) * pi / 180), (random.uniform(5, -5) * pi / 180), (
                random.uniform(10, -5) * pi / 180)
    right_finger_01.rotation_euler = finger_bend_angle
    right_finger_02.rotation_euler = finger_bend_angle
    right_finger_03.rotation_euler = finger_bend_angle
    right_finger_04.rotation_euler = finger_bend_angle
    right_finger_05.rotation_euler = finger_bend_angle
    right_fingers.rotation_euler = finger_bend_angle

    right.keyframe_insert(data_path='location', frame=start_frame)
    right.keyframe_insert(data_path='location', frame=start_frame)
    right_elbow.keyframe_insert(data_path='location', frame=start_frame)
    right_palm.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_finger_01.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_finger_02.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_finger_03.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_finger_04.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_finger_05.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_fingers.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_upperarm_twist.keyframe_insert(data_path='rotation_euler', frame=start_frame)

    xyz2 = blender_utils.get_radial_point(horiz_angle, vert_angle, RMin, RMax, origin)

    right.location[0] = xyz2[0] + hand_offset_horiz
    right.location[1] = xyz2[1]
    right.location[2] = xyz2[2]

    right_elbow.location[2] = random.uniform(-0.2, -0.4)

    # hand rotation for cases
    right_palm_rotation_angle = (random.uniform(-25, 10) * pi / 180), (random.uniform(-5, 25) * pi / 180), (
                random.uniform(-15, 15) * pi / 180)
    right_palm.rotation_euler = (
    right_palm_rotation_angle[0], right_palm_rotation_angle[1] / 2, right_palm_rotation_angle[2])
    right_upperarm_twist.rotation_euler[1] = right_palm_rotation_angle[1] / 2

    # dinger rotation for cases
    finger_bend_angle = (random.uniform(10, -15) * pi / 180), (random.uniform(5, -5) * pi / 180), (
                random.uniform(10, -5) * pi / 180)
    right_finger_01.rotation_euler = finger_bend_angle
    right_finger_02.rotation_euler = finger_bend_angle
    right_finger_03.rotation_euler = finger_bend_angle
    right_finger_04.rotation_euler = finger_bend_angle
    right_finger_05.rotation_euler = finger_bend_angle
    right_fingers.rotation_euler = finger_bend_angle

    right.keyframe_insert(data_path='location', frame=end_frame)
    right.keyframe_insert(data_path='location', frame=end_frame)
    right_elbow.keyframe_insert(data_path='location', frame=end_frame)
    right_palm.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_finger_01.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_finger_02.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_finger_03.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_finger_04.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_finger_05.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_fingers.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_upperarm_twist.keyframe_insert(data_path='rotation_euler', frame=end_frame)

    blender_utils.deselect_pose_all()
    blender_utils.set_object_mode()
    blender_utils.deselect_all()

def create_hands_runner_animation_01(start_frame, end_frame, armature, horiz_angle, vert_angle, RMin, RMax, origin):
    blender_utils.select(armature)
    blender_utils.set_active(armature)
    blender_utils.set_pose_mode()

    right = bpy.data.objects["right_hand_helper"]

    right_elbow = bpy.data.objects["left_hand_helper"]
    right_palm = armature.pose.bones['IK_control_h_R']
    right_upperarm_twist = armature.pose.bones['lowerarm_R']
    right_upperarm_twist.rotation_mode = 'XYZ'
    right_palm.rotation_mode = 'XYZ'

    right_finger_01 = armature.pose.bones['IK_control_fg01_R']
    right_finger_02 = armature.pose.bones['IK_control_fg02_R']
    right_finger_03 = armature.pose.bones['IK_control_fg03_R']
    right_finger_04 = armature.pose.bones['IK_control_fg04_R']
    right_finger_05 = armature.pose.bones['IK_control_fg05_R']
    right_fingers = armature.pose.bones['IK_control_fg06_R']
    right_palm.rotation_mode = 'XYZ'
    right_finger_01.rotation_mode = 'XYZ'
    right_finger_02.rotation_mode = 'XYZ'
    right_finger_03.rotation_mode = 'XYZ'
    right_finger_04.rotation_mode = 'XYZ'
    right_finger_05.rotation_mode = 'XYZ'
    right_fingers.rotation_mode = 'XYZ'

    spine = armature.pose.bones['IK_control_usp']
    spine.rotation_mode = 'XYZ'

    head = armature.pose.bones['IK_control_hd']

    xyz1 = blender_utils.get_radial_point(horiz_angle, vert_angle, RMin, RMax, origin)

    hand_offset_horiz = -0.045  # this number is the x offset from palm root to hand geometry center, feel free to correct it
    # hand_offset_vert

    right.location[0] = xyz1[0] + hand_offset_horiz
    right.location[1] = xyz1[1]
    right.location[2] = xyz1[2]

    right_elbow.location[2] = random.uniform(-0.2, -0.6)

    # hand rotation for cases
    right_palm_rotation_angle = (random.uniform(-20, -50) * pi / 180), (random.uniform(100, 120) * pi / 180), (
                random.uniform(-15, 15) * pi / 180)
    right_palm.rotation_euler = (
    right_palm_rotation_angle[0], right_palm_rotation_angle[1] / 2, right_palm_rotation_angle[2])
    right_upperarm_twist.rotation_euler[1] = right_palm_rotation_angle[1] / 2

    # dinger rotation for cases
    finger_bend_angle = (random.uniform(10, -80) * pi / 180), (random.uniform(5, -5) * pi / 180), (
                random.uniform(10, -5) * pi / 180)
    right_finger_01.rotation_euler = finger_bend_angle
    right_finger_02.rotation_euler = finger_bend_angle
    right_finger_03.rotation_euler = finger_bend_angle
    right_finger_04.rotation_euler = finger_bend_angle
    right_finger_05.rotation_euler = finger_bend_angle
    right_fingers.rotation_euler = finger_bend_angle

    right.keyframe_insert(data_path='location', frame=start_frame)
    right.keyframe_insert(data_path='location', frame=start_frame)
    right_elbow.keyframe_insert(data_path='location', frame=start_frame)
    right_palm.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_finger_01.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_finger_02.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_finger_03.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_finger_04.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_finger_05.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_fingers.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_upperarm_twist.keyframe_insert(data_path='rotation_euler', frame=start_frame)

    xyz2 = blender_utils.get_radial_point(horiz_angle, vert_angle, RMin, RMax, origin)

    right.location[0] = xyz2[0] + hand_offset_horiz
    right.location[1] = xyz2[1]
    right.location[2] = xyz2[2]

    right_elbow.location[2] = random.uniform(-0.2, -0.4)

    # hand rotation for cases
    right_palm_rotation_angle = (random.uniform(-20, 50) * pi / 180), (random.uniform(100, 120) * pi / 180), (
                random.uniform(-15, 15) * pi / 180)
    right_palm.rotation_euler = (
    right_palm_rotation_angle[0], right_palm_rotation_angle[1] / 2, right_palm_rotation_angle[2])
    right_upperarm_twist.rotation_euler[1] = right_palm_rotation_angle[1] / 2

    # dinger rotation for cases
    finger_bend_angle = (random.uniform(10, -80) * pi / 180), (random.uniform(5, -5) * pi / 180), (
                random.uniform(10, -5) * pi / 180)
    right_finger_01.rotation_euler = finger_bend_angle
    right_finger_02.rotation_euler = finger_bend_angle
    right_finger_03.rotation_euler = finger_bend_angle
    right_finger_04.rotation_euler = finger_bend_angle
    right_finger_05.rotation_euler = finger_bend_angle
    right_fingers.rotation_euler = finger_bend_angle

    right.keyframe_insert(data_path='location', frame=end_frame)
    right.keyframe_insert(data_path='location', frame=end_frame)
    right_elbow.keyframe_insert(data_path='location', frame=end_frame)
    right_palm.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_finger_01.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_finger_02.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_finger_03.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_finger_04.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_finger_05.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_fingers.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_upperarm_twist.keyframe_insert(data_path='rotation_euler', frame=end_frame)

    blender_utils.deselect_pose_all()
    blender_utils.set_object_mode()
    blender_utils.deselect_all()

def create_hands_runner_animation_02(start_frame, end_frame, armature, horiz_angle, vert_angle, RMin, RMax, origin):
    blender_utils.select(armature)
    blender_utils.set_active(armature)
    blender_utils.set_pose_mode()

    right = bpy.data.objects["right_hand_helper"]

    right_elbow = bpy.data.objects["left_hand_helper"]
    right_palm = armature.pose.bones['IK_control_h_R']
    right_upperarm_twist = armature.pose.bones['lowerarm_R']
    right_upperarm_twist.rotation_mode = 'XYZ'
    right_palm.rotation_mode = 'XYZ'

    right_finger_01 = armature.pose.bones['IK_control_fg01_R']
    right_finger_02 = armature.pose.bones['IK_control_fg02_R']
    right_finger_03 = armature.pose.bones['IK_control_fg03_R']
    right_finger_04 = armature.pose.bones['IK_control_fg04_R']
    right_finger_05 = armature.pose.bones['IK_control_fg05_R']
    right_fingers = armature.pose.bones['IK_control_fg06_R']
    right_palm.rotation_mode = 'XYZ'
    right_finger_01.rotation_mode = 'XYZ'
    right_finger_02.rotation_mode = 'XYZ'
    right_finger_03.rotation_mode = 'XYZ'
    right_finger_04.rotation_mode = 'XYZ'
    right_finger_05.rotation_mode = 'XYZ'
    right_fingers.rotation_mode = 'XYZ'

    spine = armature.pose.bones['IK_control_usp']
    spine.rotation_mode = 'XYZ'

    head = armature.pose.bones['IK_control_hd']

    xyz1 = blender_utils.get_radial_point(horiz_angle, vert_angle, RMin, RMax, origin)

    hand_offset_horiz = -0.045  # this number is the x offset from palm root to hand geometry center, feel free to correct it
    # hand_offset_vert

    right.location[0] = xyz1[0] + hand_offset_horiz
    right.location[1] = xyz1[1]
    right.location[2] = xyz1[2]

    right_elbow.location[2] = random.uniform(-0.2, -0.6)

    # hand rotation for cases
    right_palm_rotation_angle = (random.uniform(-25, -50) * pi / 180), (random.uniform(140, 160) * pi / 180), (
                random.uniform(-15, 15) * pi / 180)
    right_palm.rotation_euler = (
    right_palm_rotation_angle[0], right_palm_rotation_angle[1] / 2, right_palm_rotation_angle[2])
    right_upperarm_twist.rotation_euler[1] = right_palm_rotation_angle[1] / 2

    # dinger rotation for cases
    finger_bend_angle = (random.uniform(10, -80) * pi / 180), (random.uniform(5, -5) * pi / 180), (
                random.uniform(10, -5) * pi / 180)
    right_finger_01.rotation_euler = finger_bend_angle
    right_finger_02.rotation_euler = finger_bend_angle
    right_finger_03.rotation_euler = finger_bend_angle
    right_finger_04.rotation_euler = finger_bend_angle
    right_finger_05.rotation_euler = finger_bend_angle
    right_fingers.rotation_euler = finger_bend_angle

    right.keyframe_insert(data_path='location', frame=start_frame)
    right.keyframe_insert(data_path='location', frame=start_frame)
    right_elbow.keyframe_insert(data_path='location', frame=start_frame)
    right_palm.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_finger_01.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_finger_02.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_finger_03.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_finger_04.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_finger_05.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_fingers.keyframe_insert(data_path='rotation_euler', frame=start_frame)
    right_upperarm_twist.keyframe_insert(data_path='rotation_euler', frame=start_frame)

    xyz2 = blender_utils.get_radial_point(horiz_angle, vert_angle, RMin, RMax, origin)

    right.location[0] = xyz2[0] + hand_offset_horiz
    right.location[1] = xyz2[1]
    right.location[2] = xyz2[2]

    right_elbow.location[2] = random.uniform(-0.2, -0.4)

    # hand rotation for cases
    right_palm_rotation_angle = (random.uniform(-25, -50) * pi / 180), (random.uniform(140, 160) * pi / 180), (
                random.uniform(-15, 15) * pi / 180)
    right_palm.rotation_euler = (
    right_palm_rotation_angle[0], right_palm_rotation_angle[1] / 2, right_palm_rotation_angle[2])
    right_upperarm_twist.rotation_euler[1] = right_palm_rotation_angle[1] / 2

    # dinger rotation for cases
    finger_bend_angle = (random.uniform(10, -80) * pi / 180), (random.uniform(5, -5) * pi / 180), (
                random.uniform(10, -5) * pi / 180)
    right_finger_01.rotation_euler = finger_bend_angle
    right_finger_02.rotation_euler = finger_bend_angle
    right_finger_03.rotation_euler = finger_bend_angle
    right_finger_04.rotation_euler = finger_bend_angle
    right_finger_05.rotation_euler = finger_bend_angle
    right_fingers.rotation_euler = finger_bend_angle

    right.keyframe_insert(data_path='location', frame=end_frame)
    right.keyframe_insert(data_path='location', frame=end_frame)
    right_elbow.keyframe_insert(data_path='location', frame=end_frame)
    right_palm.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_finger_01.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_finger_02.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_finger_03.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_finger_04.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_finger_05.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_fingers.keyframe_insert(data_path='rotation_euler', frame=end_frame)
    right_upperarm_twist.keyframe_insert(data_path='rotation_euler', frame=end_frame)

    blender_utils.deselect_pose_all()
    blender_utils.set_object_mode()
    blender_utils.deselect_all()