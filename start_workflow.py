from execution.workflows import *
from pathlib import Path
from config import cfg
from utils.menu_utils import ask_user_for_param, read_manual_input


VALIDATION_DIR = Path(cfg['Paths']['data_path'], 'validation_data')
X_VALIDATION = VALIDATION_DIR / 'x_skin_mask_yuv_4081_144_256_False.npy'
Y_VALIDATION = VALIDATION_DIR / 'y_skin_mask_yuv_4081_144_256_False.npy'
PROBLEM_NAME = 'face_mask'
TMP_DATA_PATH = 'd://3d-generated-output//selfie_runner//2019.05.14_tmp'
APPROVED_DATA_PATH = 'd://3d-generated-output//selfie_runner//2019.05.14_approved'

args = {
    'approved_path': APPROVED_DATA_PATH,
    'out_data_path': TMP_DATA_PATH,
    'problem_name': PROBLEM_NAME,
    'x_validation': X_VALIDATION,
    'y_validation': Y_VALIDATION,
}

WORKFLOW_NAME_TO_WORKFLOW = {
    'actile_learning_chain': actile_learning_chain
}

def ask_user_for_celery_task(tasks):
    task_name = ask_user_for_param('Select celery task:', tasks)
    return WORKFLOW_NAME_TO_WORKFLOW[task_name]

def ask_user_for_task_count():
    return read_manual_input('Input count of tasks:', 1000)

def get_existing_celery_tasks():
    return list(WORKFLOW_NAME_TO_WORKFLOW.keys())


if __name__ == '__main__':
    chosen_workflow = ask_user_for_celery_task(get_existing_celery_tasks())
    chosen_count = ask_user_for_task_count()
    for i in range(chosen_count):
        chosen_workflow(**args, iteration=i)

    print(f"DONE {chosen_workflow} x {chosen_count}")