import logging


FORMAT = u'%(name)s [%(filename)s %(funcName)s %(lineno)d] %(levelname)-4s [%(asctime)s] %(message)s'
DATE_FORMAT = '%d/%m %H:%M:%S'
LEVEL = logging.DEBUG

logging.basicConfig(level=LEVEL, format=FORMAT, datefmt=DATE_FORMAT)

# logging.basicConfig(
#     level=LEVEL,
#     format=FORMAT,
#     datefmt=DATE_FORMAT,
#     filename='d:\\dev\\blender-scripts\\logs\\selfie_runner.log'
# )

# create logger with 'spam_application'
# module_logger = logging.getLogger('X')
# module_logger.setLevel(logging.DEBUG)

# create file handler which logs even debug messages
# log_file = Path(__file__).parent.parent / 'logs' / 'selfie_runner.log'
# fh = logging.FileHandler(log_file)
# fh.setLevel(logging.DEBUG)

# # create formatter and add it to the handlers
# formatter = logging.Formatter(u'%(filename)s %(name)s [%(funcName)s] %(levelname)s [%(asctime)s] %(message)s')
# fh.setFormatter(formatter)
#
# # add the handlers to the logger
# module_logger.addHandler(fh)
