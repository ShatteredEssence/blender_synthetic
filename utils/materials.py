import bpy
import random
import logging
from config import cfg
from pathlib import Path
from utils import blender_utils as utils
import struct

module_logger = logging.getLogger(__name__)

hdri_path = Path(cfg['Paths']['hdri_path'])
data_path = Path(cfg['Paths']['data_path'])

def set_texture(texture, material_name, node_name):
    module_logger.info('Texture %s | Material name %s | Node name %s', texture, material_name, node_name)
    material = bpy.data.materials.get(material_name)
    texture = bpy.data.images.load(texture)
    nodes = material.node_tree.nodes
    node = nodes.get(node_name)
    node.image = texture

def load_mouth_maps():
    textures_path = Path(data_path) / 'textures/diffuse/mouth/'
    return list(textures_path.glob('*mouth.*.diff.png'))

def load_lips_maps():
    textures_path = Path(data_path) / 'textures/diffuse/mouth/'
    return list(zip(
        textures_path.glob('*lips.*.diff.png'),
        textures_path.glob('*lips.*.mask.png'),
        textures_path.glob('*lips.*.gray.png')
    ))

def load_brows_maps():
    textures_path = Path(data_path) / 'textures/diffuse/brows/'
    return list(zip(textures_path.glob('*.diff.png'), textures_path.glob('*.mask.png')))

def load_wrinkles_maps():
    textures_path = Path(data_path) / 'textures/diffuse/head/'
    return list(zip(textures_path.glob('wrinkles.*.diff.png'), textures_path.glob('wrinkles.*.mask.png')))

def load_body_maps(race):
    textures_path = Path(data_path) / 'textures/diffuse/body/{}'.format(race)
    return list(textures_path.glob('*.diff.png'.format(race)))

def load_eyebags_maps():
    textures_path = Path(data_path) / 'textures/diffuse/head/'
    return list(textures_path.glob('eyebags.*.mask.png'))

def load_cloth_maps():
    textures_path = Path(data_path) / 'textures/diffuse/cloth/'
    return list(textures_path.glob('*.png'))

def load_hand_maps(gender):
    textures_path = Path(data_path) / 'textures/diffuse/hand/'
    gender = 'male' if gender else 'female'
    return list(zip(
        textures_path.glob('{}.*.diff.png'.format(gender)),
        textures_path.glob('{}.*.spec.png'.format(gender)),
        textures_path.glob('{}.*.bump.png'.format(gender)),
    ))

def load_hand_to_body_mix_mask(gender):
    textures_path = Path(data_path) / 'textures/classes/'
    gender = 'male' if gender else 'female'
    return list(textures_path.glob('{}_arm.uv003.mask.png'.format(gender)))[0]

def load_face_hair_maps():
    textures_path = Path(data_path) / 'textures/diffuse/hair/beard_mustache'
    return list(zip(textures_path.glob('beard.*.mask.png'), textures_path.glob('mustache.*.mask.png')))

def load_hdri_maps():
    textures_path = Path(hdri_path)
    return list(textures_path.glob('*.hdr'))

def set_character_material(character, material_name):
    module_logger.info('Character %s | Material name %s', character.name, material_name)
    material = bpy.data.materials.get(material_name)

    utils.select(character)
    utils.set_active(character)
    for i in range(0, len(bpy.context.active_object.material_slots)):
        bpy.ops.object.material_slot_remove()

    character.data.materials.append(material)
    utils.deselect_all()

def add_material(object, material):
    bpy.ops.object.material_slot_add()
    object.material_slots[-1].material = material

def set_vertex_group_material(object, vertex_group_name, material):
    bpy.ops.object.vertex_group_set_active(group=vertex_group_name)
    add_material(object, material)
    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.select_all(action='DESELECT')
    bpy.ops.object.vertex_group_select()
    bpy.ops.object.material_slot_assign()
    bpy.ops.object.editmode_toggle()

def set_character_materials(character):
    module_logger.info(character.name)
    inner_eye_material = bpy.data.materials.get('inner_eyes')
    outer_eye_material = bpy.data.materials.get('outer_eyes')
    eyelashes_material = bpy.data.materials.get('eyelashes')

    set_character_material(character, 'skin')
    set_vertex_group_material(character, 'inner_eyes', inner_eye_material)
    set_vertex_group_material(character, 'outer_eyes', outer_eye_material)
    set_vertex_group_material(character, 'eyelashes', eyelashes_material)

def add_hair_material(character):
    module_logger.info(character.name)
    add_material(character, bpy.data.materials.get('hair'))
    add_material(character, bpy.data.materials.get('beard'))
    add_material(character, bpy.data.materials.get('mustache'))
    add_material(character, bpy.data.materials.get('brows'))
    bpy.data.particles['hair_preset'].material_slot = 'hair'
    bpy.data.particles['beard_preset'].material_slot = 'beard'
    bpy.data.particles['mustache_preset'].material_slot = 'mustache'
    #bpy.data.particles['brows_preset'].material_slot = 'brows'

def skin_mtl_tweek(uv_set_name, uv_set_node, diff_node, mask_node, hsv_node, diffuse=None, mask=None):
    material = bpy.data.materials.get('skin')
    nodes = material.node_tree.nodes
    if diffuse:
        diff_node = nodes.get(diff_node)
        diff_node.image = bpy.data.images.load(diffuse)
    if mask:
        mask_node = nodes.get(mask_node)
        mask_node.image = bpy.data.images.load(mask)
    hsv_node = nodes.get('SkinHSV')
    hsv_node.inputs[1].default_value = random.uniform(0.9, 1.1)
    uv_node_mouth = nodes.get(uv_set_node)
    uv_node_mouth.uv_map = uv_set_name

def skin_mtl_brows_tweek(diffuse, mask):
    skin_mtl_tweek(uv_set_name='UVMap.002', uv_set_node='head_uv', diff_node='brows_diff',
                   mask_node='brows_mask', hsv_node='brows_hsv', diffuse=diffuse, mask=mask)

def skin_mtl_wrinkles_tweek(mask):
    skin_mtl_tweek(uv_set_name='UVMap.002', uv_set_node='head_uv', diff_node='wrinkles_diff',
                   mask_node='wrinkles_mask', hsv_node='wrinkles_hsv', mask=mask)

def skin_mtl_teeth_tweek(mouth_diffuse):
    skin_mtl_tweek(uv_set_name='UVMap.001', uv_set_node='mouth_uv', diff_node='teeth_diff',
                   mask_node='lips_mask', hsv_node='lips_hsv', diffuse=mouth_diffuse)

def srgb_to_linear(rgb):
    def srgb(c):
        a = .055
        if c <= .04045:
            return c / 12.92
        else:
            return ((c+a) / (1+a)) ** 2.4
    return tuple(srgb(c) for c in rgb)

def hex_to_rgb(rgb_str):
    int_tuple = struct.unpack('BBB', bytes.fromhex(rgb_str))
    values = [val/255 for val in int_tuple]
    values = [val for val in srgb_to_linear(values)] 
    values.append(1)
    return tuple(values)

def skin_mtl_lips_tweek(lips_mask, lips_diff, gender, hex_color):
    material = bpy.data.materials.get('skin')
    nodes = material.node_tree.nodes
    lips_color_node = nodes.get('lip_rgb')
    lips_bumpness_node = nodes.get('lip_bump')
    lips_bumpness_node.inputs[0].default_value = random.uniform(0.1, 0.15)
    lips_color_node.outputs[0].default_value = hex_to_rgb(hex_color)

    skin_mtl_tweek(uv_set_name='UVMap.001', uv_set_node='mouth_uv', diff_node='lips_diff',
                   mask_node='lips_mask', hsv_node='lips_hsv', diffuse=lips_diff, mask=lips_mask)

def eye_mtl_randomise_iris():
    METALLIC = 4
    SPECULAR = 5

    material_inner = bpy.data.materials.get('inner_eyes')
    nodes = material_inner.node_tree.nodes
    InnerEyeHSV = nodes.get('InnerEyeHSV')
    InnerEyeHSV.inputs[0].default_value = random.uniform(0, 1)
    InnerEyeHSV.inputs[1].default_value = random.uniform(0.7, 1.5)
    InnerEyeHSV.inputs[2].default_value = random.uniform(0.7, 1.5)

    material_outer = bpy.data.materials.get('outer_eyes')
    nodes = material_outer.node_tree.nodes
    bsdf_node = nodes.get('principled_bsdf')
    bsdf_node.inputs[METALLIC].default_value = 0
    bsdf_node.inputs[SPECULAR].default_value = 0
    material_inner = bpy.data.materials.get('inner_eyes')
    nodes = material_inner.node_tree.nodes
    bsdf_node = nodes.get('principled_bsdf')
    bsdf_node.inputs[METALLIC].default_value = 0
    bsdf_node.inputs[SPECULAR].default_value = 0

def set_uv_mask(material_name, uv_set_node, uv_set_name):
    material = bpy.data.materials[material_name]
    nodes = material.node_tree.nodes
    uv_node = nodes.get(uv_set_node)
    uv_node.uv_map = uv_set_name

def set_uv_texture_mask(material_name, texture_node, texture, uv_set_name):
    # TODO: call set_uv_mask
    material = bpy.data.materials[material_name]
    nodes = material.node_tree.nodes
    texture_node = nodes.get(texture_node)
    texture_node.image = bpy.data.images.load(texture)
    uv_node = nodes.get('MaskUV')
    uv_node.uv_map = uv_set_name
