import datetime
import uuid
from pathlib import Path
from config import cfg


def build_seq_path(module_name, sequence_id=None):
    now = datetime.datetime.now()
    date = now.strftime("%Y.%m.%d")
    seq_id = sequence_id or str(uuid.uuid1())
    sequence_path = Path(cfg['Paths']['output_path']) / module_name / date / seq_id
    return str(sequence_path)
