import bpy
import bmesh
import math
import random
import numpy as np
import json
import os
import mathutils
import logging
import random
from pathlib import Path
from math import pi

module_logger = logging.getLogger(__name__)
blender_latest = bpy.app.version_string == '2.80 (sub 40)'

def delete_object(object):
    select(object)
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.delete(use_global=True)
    bpy.ops.object.select_all(action='DESELECT')

def set_object_mode():
     set_mode('OBJECT')

def set_pose_mode():
     set_mode('POSE')

def set_edit_mode():
    set_mode('EDIT')

def set_mode(mode):
    bpy.ops.object.mode_set(mode=mode)

def select(obj):
    if blender_latest:
        obj.select_set(True)
    else:
        obj.select_set(action='SELECT')

def deselect(obj):
    if blender_latest:
        obj.select_set(False)
    else:
        obj.select_set(action='DESELECT')

def deselect_all():
    bpy.ops.object.select_all(action='DESELECT')

def deselect_pose_all():
    bpy.ops.pose.select_all(action='DESELECT')

def set_active(obj):
    bpy.context.view_layer.objects.active = obj

def hide(obj):
    obj.location[0] += 200000

def unhide(obj):
    obj.location[0] -= 200000

def get_radial_point(horiz_angle, vert_angle, RMin, RMax, origin):
    #horiz
    AMin = -horiz_angle * 3.14 / 180
    AMax = horiz_angle * 3.14 / 180
    
    m = RMin
    RMin = -RMax
    RMax = -m

   # RMin = RMin
   # RMax = RMax

    #vert
    BMin = -vert_angle * 3.14 / 180
    BMax = vert_angle * 3.14 / 180

    #randoms
    Ra = random.uniform(0.0, 1.0)
    Rb = random.uniform(0.0, 1.0)
    Rr = random.uniform(0.0, 1.0)


    A = AMin + Ra * (AMax - AMin)
    B = BMin + Rb * (BMax - BMin)
    R = RMin + Rr * (RMax - RMin)

    cam_loc, cam_rot, cam_scale = origin.matrix_world.decompose()
    X0 = cam_loc[0]
    Y0 = cam_loc[1]
    Z0 = cam_loc[2]

    X = X0 + math.sin(A) * R
    Y = Y0 + math.cos(A) * math.cos(B) * R
    Z = Z0 + math.sin(B) * R

    point_coord = (X, Y, Z)
    return point_coord

def export_obj_active(path):
    bpy.ops.export_scene.obj(
        filepath=str(path),
        use_selection=True,
        use_animation=False,
        use_mesh_modifiers=True,
        use_mesh_modifiers_render=True,
        use_edges=False,
        use_smooth_groups=False,
        use_smooth_groups_bitflags=False,
        use_normals=False,
        use_uvs=False,
        use_materials=False,
        use_triangles=False,
        use_nurbs=False,
        use_vertex_groups=False,
        use_blen_objects=False,
        group_by_object=False,
        group_by_material=False,
        keep_vertex_order=True
    )

def is_point_close_to(points, obj_location, distance):
    for p in points:
        if distance > abs((p - obj_location).length):
            return True
    return False

def render_sequence(path, folder_name, color_mode, frame_start, frame_stop, image_counter_offset=0):
    module_logger.info('Folder: %s, color_mode: %s', folder_name, color_mode)
    for frame in range(frame_start, frame_stop):
        render_sequence_frame(path, folder_name, color_mode, frame, image_counter_offset)

def render_sequence_frame(path, folder_name, color_mode, frame, image_counter_offset=0):
    bpy.context.scene.frame_set(frame)
    #update_morphs()
    bpy.context.scene.render.image_settings.color_mode = color_mode
    frame_path = path / folder_name / 'frame_{:07}.png'.format(frame + image_counter_offset)
    render_scene(frame_path)

def render_scene(path):
    module_logger.info("Rendering to %s", str(path))
    bpy.context.scene.render.filepath = str(path)
    bpy.ops.render.opengl(write_still=True, view_context=False)

def parent_camera_to_arm(camera, armature, skeleton):
    select(camera)
    select(armature)
    set_active(armature)
    set_pose_mode()
    skeleton.bones.active = skeleton.bones["IK_control_a_R"]
    bpy.context.object.data.bones["IK_control_a_R"].select = True
    camera.location = armature.location + bpy.context.active_pose_bone.head
    bpy.ops.object.parent_set(type='BONE') 
    deselect_pose_all()
    set_object_mode()
    deselect_all()

def bring_arm_to_face(armature):
    select(armature)
    set_active(armature)
    set_pose_mode()
    arm = armature.pose.bones["IK_control_a_R"]
    hand = armature.pose.bones['IK_control_h_R']
    arm.rotation_mode = 'XYZ'
    hand.rotation_mode = 'XYZ'
    hand.rotation_euler = (math.radians(-80), math.radians(-20), math.radians(-95))
    arm.location = (0.5, 0.36, 0.24)
    deselect_pose_all()
    set_object_mode()
    deselect_all()

def position_arm(armature, track_target, arm, hand):
    select(armature)
    set_active(armature)
    set_pose_mode()
    arm.rotation_mode = 'XYZ'
    hand.rotation_mode = 'XYZ'
    hand.rotation_euler = (math.radians(90), math.radians(-60), math.radians(0))
    deselect_pose_all()
    armature.data.bones.active = arm.bone
    bpy.ops.pose.constraint_add(type='TRACK_TO')
    arm.constraints["Track To"].target = track_target
    deselect_pose_all()
    distance_factor = 1.1
    arm.location = (random.uniform(0.3, 0.9) * distance_factor, random.uniform(0.0, 0.5) * distance_factor, random.uniform(0.5, 0.7) * distance_factor)
    set_object_mode()
    deselect_all()

def cam_to_hand(armature):
    follow_obj = bpy.data.objects['camera_follow_target']
    follow_obj.constraints['Copy Location'].target = armature
    follow_obj.constraints['Copy Location'].subtarget = 'IK_control_a_L'
    follow_obj.constraints['Copy Location'].head_tail = 0.7

def position_track_target(armature, track_target):
    track_target.constraints['Copy Location'].target = armature
    track_target.constraints['Copy Location'].subtarget = 'head'
    track_target.constraints['Copy Location'].head_tail = 0.3

def init_hand_camera(armature):
    camera = bpy.data.objects['cam_hand']
    camera.rotation_euler[1] = random.uniform(-5*pi/180, -5*pi/180)
    # camera.location.x += random.uniform(-0.05, 0.05)
    # camera.location.y += random.uniform(0, 0.2)
    # camera.location.z += random.uniform(-0.05, 0.05)
    # arm = armature.pose.bones["IK_control_a_L"]
    # hand = armature.pose.bones['IK_control_h_L']
    track_target = bpy.data.objects["track_target"]
    cam_to_hand(armature)
    position_track_target(armature, track_target)

def init_free_camera(armature):
    camera = bpy.data.objects['cam_hand']
    camera.rotation_euler[1] = random.uniform(-5*pi/180, -5*pi/180)
    track_target = bpy.data.objects["track_target"]
    position_track_target(armature, track_target)

def set_camera_look_at(obj_camera, target_point):
    loc_camera = obj_camera.matrix_world.to_translation()
    direction = target_point - loc_camera
    # point the cameras '-Z' and use its 'Y' as up
    rot_quat = direction.to_track_quat('-Z', 'Y')
    # assume we're using euler rotation
    obj_camera.rotation_euler = rot_quat.to_euler()

# def move_in_local_space(object, v):
#     vec = mathutils.Vector(v)
#     inv = object.matrix_world.copy()
#     inv.invert()
#     vec_rot = vec * inv
#     object.location = object.location + vec_rot
#     return object.location

def generate_traectory(armature, start_frame, end_frame):
    select(armature)
    set_active(armature)
    set_pose_mode()
    deselect_pose_all()
    arm = armature.pose.bones["IK_control_a_R"]

    arm.keyframe_insert(data_path='location', frame=start_frame)
    
    armature.data.bones.active = armature.pose.bones['head'].bone
    head_tail_location = armature.location + bpy.context.active_pose_bone.tail
    
    armature.data.bones.active = arm.bone
    arm_tail_location = armature.location + bpy.context.active_pose_bone.tail
    
    distance = np.sqrt(np.sum((np.array(head_tail_location) - np.array(arm_tail_location)) ** 2))
    
    deselect_pose_all()
    
    correction = random.uniform(0.14, 0.2)
    
    armature.data.bones.active = arm.bone
    bpy.ops.transform.translate(
        value=(
            random.uniform(-distance/2, distance/2),
            random.uniform(-distance, distance),
            random.uniform(-distance + correction, distance - correction)
        ),
        constraint_axis=(True, True, True),
        constraint_orientation='LOCAL')
    arm.keyframe_insert(data_path='location', frame=end_frame)
    
    deselect_pose_all()
    set_object_mode()
    deselect_all()

def create_vertex_group(object, indices, name):
    vertex_group = object.vertex_groups.new(name=name)
    vertex_group.add(indices, 1.0, 'ADD')
    return vertex_group

def create_vertex_groups(character, indices_data):
    module_logger.info(character)
    for name, indices in indices_data.items():
        create_vertex_group(character, indices, name)

def load_json(filepath):
    with open(filepath, 'r') as f:
        data = json.load(f)
    return data

def makedirs(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)

def init_human(gender):
    module_logger.info('Gender %s', gender)
    bpy.context.scene.mblab_use_lamps = False
    bpy.context.scene.mblab_character_name = 'f_ca01' if gender == 0 else 'm_ca01'
    bpy.ops.mbast.init_character()

    if gender == 0:
        character = bpy.context.scene.objects["f_ca01"]
        armature = bpy.data.objects["f_ca01_skeleton"]

    if gender == 1:
        character = bpy.context.scene.objects["m_ca01"]
        armature = bpy.data.objects["m_ca01_skeleton"]

    return character, armature

def create_uvmap(object, uvset_name):
    module_logger.info('Object %s, uvset_name %s', object, uvset_name)
    object.select_set(action='SELECT')
    result = bpy.ops.mesh.uv_texture_add()
    uv_layers = object.data.uv_layers
    new_layer = uv_layers[len(uv_layers) - 1]
    #new_layer.name = uvset_name
    uv_layers.active_index = 0
    object.select_set(action='DESELECT')
    return result

def set_uvs(obj, data, uvset_name):
    module_logger.info('Object %s, uvset_name %s', obj, uvset_name)
    for loop in obj.data.loops:
        obj.data.uv_layers[uvset_name].data[loop.index].uv = data[str(loop.index)]

def gc():
    for datablock in bpy.data.cameras:
        if datablock.users == 0:
            bpy.data.cameras.remove(datablock)

    for datablock in bpy.data.scenes:
        if datablock.users == 0:
            bpy.data.scenes.remove(datablock)

    for datablock in bpy.data.node_groups:
        if datablock.users == 0:
            bpy.data.node_groups.remove(datablock)

    if blender_latest:
        for datablock in bpy.data.lights:
            if datablock.users == 0:
                bpy.data.lights.remove(datablock)
    else:
        for datablock in bpy.data.lamps:
            if datablock.users == 0:
                bpy.data.lamps.remove(datablock)

    for datablock in bpy.data.window_managers:
        if datablock.users == 0:
            bpy.data.window_managers.remove(datablock)

    for datablock in bpy.data.textures:
        if datablock.users == 0:
            bpy.data.textures.remove(datablock)

    for datablock in bpy.data.worlds:
        if datablock.users == 0:
            bpy.data.worlds.remove(datablock)

    for datablock in bpy.data.particles:
        if datablock.users == 0:
            bpy.data.particles.remove(datablock)

    for datablock in bpy.data.cache_files:
        if datablock.users == 0:
            bpy.data.cache_files.remove(datablock)

    for datablock in bpy.data.objects:
        if datablock.users == 0:
            bpy.data.objects.remove(datablock)

    for datablock in bpy.data.meshes:
        if datablock.users == 0:
            bpy.data.meshes.remove(datablock)

    for datablock in bpy.data.materials:
        if datablock.users == 0:
            bpy.data.materials.remove(datablock)

    for datablock in bpy.data.images:
        if datablock.users == 0:
            bpy.data.images.remove(datablock)

    for datablock in bpy.data.armatures:
        if datablock.users == 0:
            bpy.data.armatures.remove(datablock)

    for datablock in bpy.data.actions:
        if datablock.users == 0:
            bpy.data.actions.remove(datablock)

def load_vertex_group_indices(data_path, prefix):
    indices_data = {
        'inner_eyes': load_json(data_path / 'json/{}_inner_eyes_indices.json'.format(prefix)),
        'outer_eyes': load_json(data_path / 'json/{}_outer_eyes_indices.json'.format(prefix)),
        'eyelashes':  load_json(data_path / 'json/{}_eyelashes_indices.json'.format(prefix)),
        'sculp':      load_json(data_path / 'json/{}_sculp.json'.format(prefix)),
        'brows':      load_json(data_path / 'json/{}_brows.json'.format(prefix)),
        'beard':      load_json(data_path / 'json/{}_beard.json'.format(prefix)),
        'mustache':   load_json(data_path / 'json/{}_mustache.json'.format(prefix)),
    }
    return indices_data

def update_morphs():
    bpy.context.scene.mblab_show_measures = False

def print_vert_details(selected_verts):
    num_verts = len(selected_verts)
    print("number of verts: {}".format(num_verts))
    print("vert indices: {}".format([id.index for id in selected_verts]))

def get_vertex_data(object_reference):
    bm = bmesh.from_edit_mesh(object_reference.data)
    selected_verts = [vert for vert in bm.verts if vert.select]
    print_vert_details(selected_verts)

def euler2quaternion(vector):
    euler_x = vector[0] * np.pi / 180
    euler_y = vector[1] * np.pi / 180
    euler_z = vector[2] * np.pi / 180

    XY = np.tensordot(
        np.array([np.cos(euler_z/2), 0, 0, np.sin(euler_z/2)]).reshape(-1, 1),
        np.array([np.cos(euler_y/2), 0, np.sin(euler_y/2), 0]).reshape(-1, 1),
        axes=[1, 1]
    )

    quaternion = np.tensordot(
        XY,
        np.array([np.cos(euler_x/2), np.sin(euler_x/2), 0, 0]),
        axes=[0, 0]
    )

    return quaternion

'''
TBD

#min max radius
RMin = -4
RMax = -6

#horiz
LMin = -25*3.14/180
LMax = 25*3.14/180

#vert
BMin = -45*3.14/180
BMax = 45*3.14/180

#randoms
Rl = random.uniform(0.0, 1.0)
Rb = random.uniform(0.0, 1.0)
Rr = random.uniform(0.0, 1.0)


L = LMin + Rl * (LMax - LMin)
B = BMin + Rb * (BMax - BMin)
R = RMin + Rr * (RMax - RMin)

X0 = 0.0
Y0 = 0.0
Z0 = 0.0

Y = X0 + math.cos(L) * math.cos(B) * R
X = Y0 = math.sin(L) * R
Z = Z0 + math.sin(B) * R

tgt = bpy.data.objects['target']

tgt.location = (X, Y, Z)
'''
