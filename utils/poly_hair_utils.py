import bpy
import random
import logging
from utils import blender_utils as utils

female_hairs = ['f_hair_01', 'f_hair_02', 'f_hair_03', 'f_hair_04', 'f_hair_05', 'f_hair_06', 'f_hair_07', 'f_hair_08', 'f_hair_09', 'f_hair_10']
male_hairs = ['m_hair_01', 'm_hair_02', 'm_hair_03', 'm_hair_04', 'm_hair_05', 'm_hair_06', 'm_hair_07', 'm_hair_08', 'm_hair_09', 'm_hair_10']

def create_hair(human):
    hairs = male_hairs if human.gender else female_hairs
    hair = bpy.data.objects[random.choice(hairs)]
    hair.location = (0, 0, 0)

    material = bpy.data.materials.get('Poly_Hair')
    nodes = material.node_tree.nodes
    hue_node = nodes.get('Hue Saturation Value')
    hue_node.inputs[0].default_value = random.uniform(0.48, .52)
    hue_node.inputs[1].default_value = random.uniform(0, 2)
    hue_node.inputs[2].default_value = random.uniform(0.5, 2)

    emission_factor = nodes.get('Emission_factor')
    emission_factor.inputs[0].default_value = 0

    utils.deselect_all()

    utils.select(human.character)
    utils.select(hair)

    bpy.ops.mbast.proxy_fit()
    utils.deselect_all()

def set_white_hair():
    material = bpy.data.materials['Poly_Hair']
    nodes = material.node_tree.nodes
    emission_factor = nodes.get('Emission_factor')
    color = nodes.get('Color')
    emission_factor.inputs[0].default_value = 1
    color.inputs[0].default_value = (1, 1, 1, 1)

def set_black_hair():
    material = bpy.data.materials['Poly_Hair']
    nodes = material.node_tree.nodes
    emission_factor = nodes.get('Emission_factor')
    color = nodes.get('Color')
    emission_factor.inputs[0].default_value = 1
    color.inputs[0].default_value = (0, 0, 0, 1)