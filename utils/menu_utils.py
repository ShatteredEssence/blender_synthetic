

def read_input_choise(invitation_msg, input_range):
    """Read number of menu from input"""
    success = False
    input_choise = None
    while not success:
        try:
            input_choise = input(invitation_msg)
            input_choise = int(input_choise)
            success = True if input_choise < input_range else False
        except ValueError:
            success = False

        if not success:
            err_msg = f"\nInvalid value '{input_choise}'. " \
                      f"Must be integer number in range [0, {input_range - 1}]. Try again."
            # todo: logging
            print(err_msg)

    return input_choise

def read_manual_input(base_invitation_msg, input_range):
    success = False
    input_choise = None
    while not success:
        try:
            input_choise = input(base_invitation_msg)
            input_choise = int(input_choise)
            success = True if input_choise < input_range else False
        except ValueError:
            success = False

        if not success:
            err_msg = f"\nInvalid value '{input_choise}'. " \
                      f"Must be integer number in range [0, {input_range}]. Try again."
            print(err_msg)
    return int(input_choise)

def prepare_menu_message(base_invitation_msg, menu_items):
    invitation_msg = f"\n{base_invitation_msg}\n"
    for i in range(len(menu_items)):
        invitation_msg += f'{i}) {menu_items[i]}\n'
    invitation_msg += 'Enter number: '
    return invitation_msg

def ask_user_for_param(base_invitation_msg, values):
    invitation_msg = prepare_menu_message(base_invitation_msg, values)
    choosed_idx = read_input_choise(invitation_msg, len(values))
    return values[choosed_idx]