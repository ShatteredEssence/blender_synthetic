import bpy
import random
import math
import logging
from . import blender_utils
from random import choice
from pathlib import Path

"""DEPRECATED"""

def randomise_enviroment(hdr_path):
    module_logger.info('')
    # load enviroment
    enviroment_node = bpy.data.worlds['World'].node_tree
    background = enviroment_node.nodes['Environment Texture']
    
    # swap bg image
    file_enviroment_image = choice([str(x) for x in Path(hdr_path).rglob('*.hdr')])
    bg_image = bpy.data.images.load(file_enviroment_image)
    background.image = bg_image
    
    # reset map rotation
    enviroment_node.nodes['Mapping'].rotation[2] = 0
    
    # randomise
    env_rotation_offset = random.uniform(0, 365)
    enviroment_node.nodes['Mapping'].rotation[2] = env_rotation_offset
    env_brightness_offset = random.uniform(1, 2)
    enviroment_node.nodes["Background"].inputs[1].default_value = env_brightness_offset
    enviroment_node.nodes["Hue Saturation Value"].inputs[0].default_value = random.uniform(0.4, 0.6)
    enviroment_node.nodes["Hue Saturation Value"].inputs[1].default_value = random.uniform(0.5, 1.0)
    enviroment_node.nodes["Hue Saturation Value"].inputs[2].default_value = random.uniform(0.8, 1.0)

def set_background_strength(value):
    enviroment_node = bpy.data.worlds['World'].node_tree
    enviroment_node.nodes["Background"].inputs[1].default_value = value

def get_background_strength():
    enviroment_node = bpy.data.worlds['World'].node_tree
    return enviroment_node.nodes["Background"].inputs[1].default_value

def randomise_sun(armature, sun, energy_factor=1):
    module_logger.info('')
    min = 0.3
    max = 1
    rand_energy = energy_factor * random.uniform(min, max)
    sun.data.energy = rand_energy

    rotation_target = bpy.data.objects['sun_light_rotation']
    armature.data.layers[0] = True
    armature.data.layers[1] = True
    rotation_target.select_set(action='SELECT')
    blender_utils.select(armature)
    blender_utils.set_active(armature)
    blender_utils.set_pose_mode()
    head_loc = bpy.context.object.pose.bones['head'].head
    blender_utils.deselect_pose_all()
    blender_utils.set_object_mode()
    blender_utils.deselect_all()
    ###

    rotation_target.location = (head_loc[0], head_loc[1], head_loc[2])
    rx = math.radians(random.uniform(0, 360))
    ry = math.radians(random.uniform(0, 360))
    rz = math.radians(random.uniform(0, 360))
    rotation_target.rotation_euler = (rx, ry, rz)

    cr = random.uniform(0.6, 1)
    cg = random.uniform(0.6, 1)
    cb = random.uniform(0.6, 1)
    sun.data.color = (cr, cg, cb)
    sun.data.size = random.uniform(0.1, 0.5)
    sun.data.size_y = random.uniform(0.1, 0.5)

def disable_sun(sun):
    sun.data.energy = 0