import bpy
from math import pi


def dump_meta(camera, target):
    cam_loc, cam_rot, cam_scale = camera.matrix_world.decompose()
    fov = bpy.data.cameras[camera.name].angle * 180 / pi

    pitch = camera.rotation_euler.x
    roll = camera.rotation_euler.y
    yaw = camera.rotation_euler.z
    # dolly = camera.location.x
    # truck = camera.location.y
    # boom = camera.location.z

    # target_location = target.location
    # target_rotation_mode = target.rotation_mode
    # target_rotation_euler = target.rotation_euler

    return cam_loc, cam_rot, fov, pitch, roll, yaw

def export_camera_meta():
    pass