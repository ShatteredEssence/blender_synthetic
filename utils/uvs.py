import bpy
from pathlib import Path


def get_uvs(obj):
    uvs = dict()
    for loop in obj.data.loops:
        uv_coords = obj.data.uv_layers.active.data[loop.index].uv
        # uv_coords = obj.data.uv_layers['NAME'].active.data[loop.index].uv
        uvs[str(loop.index)] = (uv_coords.x, uv_coords.y)
    return uvs

def set_uvs(obj, data, uvset_name='UVMap.001'):
    for loop in obj.data.loops:
        obj.data.uv_layers[uvset_name].data[loop.index].uv = data[str(loop.index)]

def create_uvmap(object):
    object.select_set(action='SELECT')
    result = bpy.ops.mesh.uv_texture_add()
    object.select_set(action='DESELECT')
    return result

# def load_uvs(datapath, gender, set):
#     uvs_path = Path(datapath) / 'uvs/'
#     return uvs_path.glob('{0}_{1}.json'.format(gender, set))
