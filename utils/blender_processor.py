import random
import bpy


#  https://docs.blender.org/api/blender_python_api_2_78_0/bpy.types.BlendDataLibraries.html?highlight=libraries#bpy.types.BlendDataLibraries.load

def append_objects_from_blend_grp(filepath, grp_name=None, delete_grp=False):
    """
    Append all objects from given group and
    group itself into current blender scene.
    :param filepath: path to blender file to append objects from
    :param grp_name: group name. If NONE, will automatically choose random group
    :param delete_grp: If TRUE, will delete group itself after appending objects
    :return: List of objects [<class 'bpy_types.Object'>, ... ]
    """
    with bpy.data.libraries.load(filepath, link=False) as (data_src, data_dst):
        group_to_import = random.choice(data_src.groups) if not grp_name else grp_name
        print('[i] Group to import: ', group_to_import)
        ## all groups
        # data_to.groups = data_from.groups

        # only append a single group we already know the name of
        data_dst.groups = [group_to_import]

    # link the objects contained in the group to your current scene
    # otherwise you won't ever see them!
    for o in data_dst.groups[0].objects:
        # bpy.context.scene.objects.link(o)  # blender 2.79
        bpy.context.scene.collection.objects.link(o)  # blender 2.8
        # print('[+] linked', o)

    # for safety measures, update the scene
    # most likely only necessary from within an add-on
    bpy.context.scene.update()
    objects_in_group = bpy.data.groups[group_to_import].objects.values()
    if delete_grp:
        print(delete_group(group_to_import))
    return objects_in_group


def append_objects_from_file(filepath, object_names):
    with bpy.data.libraries.load(filepath, link=False) as (data_src, data_dst):
        data_dst.objects = [name for name in data_src.objects if name in object_names]
        print('[i] Objects to import: ', data_dst.objects)

    # link the objects contained in the group to your current scene
    # otherwise you won't ever see them!
    for o in data_dst.objects:
        # bpy.context.scene.objects.link(o)  # blender 2.79
        bpy.context.scene.collection.objects.link(o)  # blender 2.8
        print('[+] linked', o)

    # for safety measures, update the scene
    # most likely only necessary from within an add-on
    bpy.context.scene.update()
    return data_dst.objects


def delete_object(object):
    """
    Delete given object.
    :param object:
    :return: {'FINISHED'} if deleted, {'CANCELLED'} if not deleted
    """
    object.select_set(action='SELECT')
    if bpy.ops.object.mode_set.poll():
        bpy.ops.object.mode_set(mode='OBJECT')
    # object.select = True  # 2.79
    # bpy.context.scene.objects.active = object  # 2.79
    res = bpy.ops.object.delete(use_global=True)
    bpy.ops.object.select_all(action='DESELECT')
    return res


def delete_group(grp_name):
    group = bpy.data.groups.get(grp_name, None)
    if group:
        bpy.data.groups.remove(group)
        return True
    else:
        return False