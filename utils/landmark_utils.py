import bpy
import json
import datetime
import numpy as np
from pathlib import Path
from config import cfg
from utils import blender_utils
from bpy_extras.object_utils import world_to_camera_view
from mathutils import Vector


landmark_info = {
    "nose_class": 0,
    "left_eyes_class": 1,
    "right_eyes_class": 2,
    "left_eye_brows_class": 3,
    "right_eye_brows_class": 4,
    "mouth_class": 5,
    "nasolabial_folds_class": 6,
    "contour_class": 7,
    "left_eye_vert": 8,
    "left_eye_outer_vert": 9,
    "left_iris_vert": 10,
    "right_eye_vert": 11,
    "right_eye_outer_vert": 12,
    "right_iris_vert": 13,
}

data_path = Path(cfg['Paths']['data_path'])

def write_device_info(path):
    data = dict(
        date=datetime.datetime.now().strftime("%Y.%m.%d"),
        version="1.1",
        source="blender",
        os="blender",
    )
    data.update(landmark_info)
    with open((path / 'deviceinfo.json'), 'w') as file:
        json.dump(data, file, indent=4)

def get_landmarks(prefix):
    return blender_utils.load_json(data_path / 'json/{}_landmarks.json'.format(prefix))

def get_landmarks_86(prefix):
    return blender_utils.load_json(data_path / 'json/{}_landmarks_86.json'.format(prefix))

def get_landmark_classes(prefix):
    return blender_utils.load_json(data_path / 'json/{}_landmark_classes.json'.format(prefix))

def ignore_face(indices, ignored_indices):
    for index in indices:
        if index not in ignored_indices:
            return False
    return True

def is_point_occluded(cam_loc, location, ignored_indices):
    factor = 0.9
    direction = location - cam_loc
    distance = direction.length
    direction.normalize()
    hit = bpy.context.scene.ray_cast(bpy.context.view_layer, cam_loc, direction)
    if not hit[0]:
        return True
    hit_point = hit[1]
    face_index = hit[3]
    obj = hit[4]
    face = obj.data.polygons[face_index]
    if ignore_face(face.vertices, ignored_indices):
        return False

    return (hit_point - cam_loc).length < distance * factor

def write_landmarks(camera, frame_meta, folderpath, character, landmarks, landmark_classes, iteration, ignored_indices):
    blender_utils.makedirs(folderpath / 'landmarks')
    render = bpy.context.scene.render
    mesh = character.to_mesh(apply_modifiers=True, depsgraph=bpy.context.depsgraph)
    vertices = [vertex.co for vertex in mesh.vertices]
    bpy.data.meshes.remove(mesh)
    coords_2d = np.array([world_to_camera_view(bpy.context.scene, camera.camera_object, Vector(coordinate)) for coordinate in vertices])
    cam_loc, cam_rot, fov, pitch, roll, yaw = camera.dump_meta()

    with open('{}/landmarks/frame_{:07}.csv'.format(folderpath, iteration), 'w') as landmarks_file:
        counter = 0

        for landmark in landmarks:
            landmark_class_num = -1
            for landmark_class, verts_list in landmark_classes.items():
                if landmark in verts_list:
                    landmark_class_num = landmark_info[landmark_class]
                    break

            if 'occlusion' in frame_meta and cam_loc != None:
                cam_loc = Vector((cam_loc.x, cam_loc.y, cam_loc.z))
                frame_meta['occlusion'][landmark] = int(is_point_occluded(cam_loc, vertices[landmark], ignored_indices))

            x, y = coords_2d[landmark][0], coords_2d[landmark][1]
            line = str(counter) + '\t' + str(render.resolution_x * x) + '\t' + str(render.resolution_y - render.resolution_y * y) + '\n'
            landmarks_file.write(line)
            counter += 1

def write_barimetric_landmarks(camera, folderpath, human, iteration):
    blender_utils.makedirs(folderpath / 'landmarks_barimetric')
    render = bpy.context.scene.render
    mesh = human.character.to_mesh(apply_modifiers=True, depsgraph=bpy.context.depsgraph)
    vertices = [vertex.co for vertex in mesh.vertices]
    bpy.data.meshes.remove(mesh)

    race = human.race
    race_data = blender_utils.load_json(data_path / 'json/{}_barimetric_landmarks.json'.format(race))
    manual_correction = race_data['manual_correction']
    lm = race_data['lm_indeces']

    barimetric_landmarks = list()
    for num, lm_data in enumerate(lm):

        point_ids = lm_data[0]
        weights = lm_data[1]

        ### include manual corrections
        manual_points = manual_correction.get(str(num))
        if manual_points:
            point_ids = manual_points
            weights = [1.0 / len(manual_points)] * len(manual_points)

        point_positions = list()
        for p in point_ids:
            point_positions.append(vertices[p])

        coords_w = map(lambda j: j[0] * j[1], zip(point_positions, weights))
        sum_vec = np.zeros(3)
        for v in coords_w:
            sum_vec += v
        barimetric_landmarks.append(sum_vec)

    coords_2d = np.array([world_to_camera_view(bpy.context.scene, camera.camera_object, Vector(coordinate)) for coordinate in barimetric_landmarks])

    with open('{}/landmarks_barimetric/frame_{:07}.csv'.format(folderpath, iteration), 'w') as landmarks_file:
        for counter, coords in enumerate(coords_2d):
            x, y = coords[0], coords[1]
            line = str(counter) + '\t' + \
                   str(render.resolution_x * x) + '\t' + \
                   str(render.resolution_y - render.resolution_y * y) + '\t' + '\n'
            landmarks_file.write(line)

def write_landmarks_eyes(camera, folderpath, human, iteration):
    blender_utils.makedirs(folderpath / 'landmarks_eyes')
    render = bpy.context.scene.render
    mesh = human.character.to_mesh(apply_modifiers=True, depsgraph=bpy.context.depsgraph)
    vertices = [vertex.co for vertex in mesh.vertices]
    bpy.data.meshes.remove(mesh)
    coords_2d = np.array([world_to_camera_view(bpy.context.scene, camera.camera_object, Vector(coordinate)) for coordinate in vertices])

    landmarks = [7166, 1214] if human.gender else [6941, 593]
    with open('{}/landmarks_eyes/frame_{:07}.csv'.format(folderpath, iteration), 'w') as landmarks_file:
        counter = 0
        for landmark in landmarks:
            x, y = coords_2d[landmark][0], coords_2d[landmark][1]
            line = str(counter) + '\t' + str(render.resolution_x * x) + '\t' + str(render.resolution_y - render.resolution_y * y) + '\n'
            landmarks_file.write(line)
            counter += 1

def write_hand_landmarks(camera, folderpath, human, iteration=0):
    blender_utils.makedirs(folderpath / 'landmarks_hand')
    render = bpy.context.scene.render
    mesh = human.character.to_mesh(apply_modifiers=True, depsgraph=bpy.context.depsgraph)
    vertices = [vertex.co for vertex in mesh.vertices]
    bpy.data.meshes.remove(mesh)

    gender = 'man' if human.gender else 'woman'
    arm_indices = blender_utils.load_json(data_path / 'json/{}_arm_indices.json'.format(gender))

    coords_2d = np.array([world_to_camera_view(bpy.context.scene, camera.camera_object, vertices[i]) for i in arm_indices])

    with open('{}/landmarks_hand/frame_{:07}.csv'.format(folderpath, iteration), 'w') as landmarks_file:
        for index, coords in zip(arm_indices, coords_2d):
            x, y = coords[0], coords[1]
            line = str(index) + '\t' + str(render.resolution_x * x) + '\t' + str(render.resolution_y - render.resolution_y * y) + '\n'
            landmarks_file.write(line)
