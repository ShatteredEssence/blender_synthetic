import platform

def get_face_mesh_config():
    folderpath = 'C:/'
    datapath = 'C:/'

    if platform.node() == 'd497':
        folderpath = 'D:/head_mesh/'
        datapath = 'D:/Projects/blender-scripts/data/'

    elif platform.node() == 'ShatteredEssence':
        folderpath = 'D:/!work/lemezator/head_mesh/'
        datapath = 'D:/!work/blender_debug/data/'

    elif platform.node() == 'ShatteredEssence':
        folderpath = 'PATH'
        datapath = 'PATH'

    else:
        raise Exception('Machine not present in face mesh config. Fix it.')
    return folderpath, datapath

def get_selfie_config():
    hdr_path = 'C:/'
    parent_path = 'C:/'

    if platform.node() == 'd497':
        parent_path = 'D:/head_mesh/'
        hdr_path = 'D:/Projects/blender-scripts/HDRiS/'

    elif platform.node() == 'ShatteredEssence':
        parent_path = 'D:/!work/lemezator/autorunner_classed/'
        hdr_path = 'C:/\!work/HDRiS/'

    elif platform.node() == ' DESKTOP-P3TC5S2':
        parent_path = 'PATH'
        hdr_path = 'PATH'
        
    else:
        raise Exception('Machine not present in selfie config. Fix it.')
    return parent_path, hdr_path

def get_eyes_config():
    hdr_path = 'C:/'
    parent_path = 'C:/'
    datapath = 'C:/'

    if platform.node() == 'd497':
        parent_path = 'D:/eyes/'
        hdr_path = 'D:/Projects/blender-scripts/HDRiS/'
        datapath = 'D:/Projects/blender-scripts/data/'

    elif platform.node() == 'ShatteredEssence':
        parent_path = 'D:/!work/lemezator/autorunner_classed/'
        hdr_path = 'C:/\!work/HDRiS/'
        datapath = 'D:/!work/blender_debug/data/'

    elif platform.node() == 'd535':
        parent_path = 'd:/3d-generated-output/eyes/'
        hdr_path = 'd:/dev/blender-scripts/HDRiS/'
        datapath = 'd:/dev/blender-scripts/data/'

    else:
        raise Exception('Machine not present in selfie config. Fix it.')
    return parent_path, hdr_path, datapath