import bpy
import bmesh
import generator_ik as g
import datetime
import random
import path_config

male_indices_filename = 'man_face_indices.json'
female_indices_filename = 'woman_head_indices.json'

folderpath, datapath = path_config.get_face_mesh_config()

count = 35000

def get_head_indices_filepath(gender):
    filename = male_indices_filename if gender == 1 else female_indices_filename
    return datapath + '/json/' + filename

def run():
    now = datetime.datetime.now()
    date_now = str(now.day) + '.' + '{:02}'.format(now.month) + '.' + str(now.year)

    g.gc()
    gender = 1 #random.randint(0, 1)

    g.makedirs(folderpath + date_now)

    print('Human Created')
    g.init_human(gender)

    if gender == 0:
        character = bpy.context.scene.objects["f_ca01"]
        skeleton = bpy.data.objects["f_ca01_skeleton"]
    if gender == 1:
        character = bpy.context.scene.objects["m_ca01"]
        skeleton = bpy.data.objects["m_ca01_skeleton"]

    for iteration in range (0, count):
        meta_path = folderpath + date_now
        print('Randomising character')
        g.randomise_character(character, 1.3, meta_path, iteration)
        g.randomise_face_expression(character, 1.3, meta_path, iteration)
        bpy.context.scene.objects.active = character
        #create OBJ_Mesh proxy
        character.select = True
        bpy.ops.object.duplicate()
        bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)
        for obj in bpy.context.selected_objects:
            obj.name = "OBJ_Mesh"
        OBJ_Mesh = bpy.data.objects["OBJ_Mesh"]
        bpy.context.scene.objects.active = OBJ_Mesh
        bpy.ops.object.modifier_apply(modifier = 'mbastlab_armature')
        #bpy.ops.object.modifier_apply(modifier = 'mbastlab_corrective_modifier')
        OBJ_Mesh.modifiers.clear()
        bpy.ops.object.select_all(action='DESELECT')

        #Delete polygons from OBJ_Mesh
        OBJ_Mesh.select = True
        bpy.ops.object.mode_set(mode='EDIT')
        indices_filepath = get_head_indices_filepath(gender)
        indices_file = open(indices_filepath, 'r')
        indices = eval(indices_file.read())

        bpy.ops.mesh.select_mode(use_extend=False, use_expand=True, type='VERT')

        obj = bpy.context.object
        me = obj.data
        bm = bmesh.from_edit_mesh(me)

        vertices= [e for e in bm.verts]
        oa = bpy.context.active_object

        for vert in vertices:
            if vert.index in indices:
                vert.select = True
            else:
                vert.select = False

        bmesh.update_edit_mesh(me, True)
        bpy.ops.mesh.select_all(action='INVERT')
        bpy.ops.mesh.delete(type='FACE')
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.context.scene.objects.active = character

        #Remove vertex groups
        for ob in bpy.data.objects['OBJ_Mesh'].vertex_groups:
            bpy.data.objects['OBJ_Mesh'].vertex_groups.remove(ob)

        #Prepare mesh for export
        bpy.ops.object.select_all(action='DESELECT')
        OBJ_Mesh.select = True
        bpy.context.scene.objects.active = OBJ_Mesh
        bpy.ops.object.convert(target='MESH')

        bpy.ops.object.select_all(action='DESELECT')
        #skeleton.select = True #Remove Later
        #bpy.ops.object.delete(use_global=True) #Remove Later
        #bpy.ops.object.select_all(action='DESELECT') #Remove Later

        OBJ_Mesh.select = True

        target_file = folderpath + date_now +'/' + 'head' + '_{:07}'.format(iteration) + '.obj'
        g.makedirs(target_file[:-16])

        print('Exporting Face OBJ_ ({:07})'.format(iteration))
        bpy.ops.export_scene.obj(filepath=target_file, use_selection = True, use_animation = False, use_mesh_modifiers=False, use_mesh_modifiers_render=False, use_edges=False, use_smooth_groups=False, use_smooth_groups_bitflags=False, use_normals=False, use_uvs=True, use_materials=False, use_triangles=False, use_nurbs=False, use_vertex_groups=False, use_blen_objects=False, group_by_object=False, group_by_material=False, keep_vertex_order=False)

        #Clear OBJ_Mesh object, end of cicle
        OBJ_Mesh.select = True
        bpy.context.scene.objects.active = OBJ_Mesh
        bpy.ops.object.delete(use_global=True)
        bpy.ops.object.select_all(action='DESELECT')


def main():
    print('Test run')
    run()


if __name__ == "__main__":
    main()