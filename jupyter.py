import imageio
# pip install scikit-image

from skimage import img_as_float, img_as_ubyte
from skimage.color import rgb2hsv
import matplotlib.pyplot as plt

frame_path = 'd://tmp_parsing/0000000.png'
frame_rgb = img_as_float(imageio.imread(frame_path))
frame_hsv = rgb2hsv(frame_rgb)

for i in range(3):
    plt.imshow(frame_rgb[:,:,i],  cmap='gray')
    plt.show()

for i in range(3):
    plt.imshow(frame_hsv[:,:,i], cmap='gray')
    plt.show()

plt.hist(frame_hsv[:,:,0].flatten(), bins=100)

plt.imshow(frame_hsv[:,:,0] > 0.08, cmap='gray')
plt.show()