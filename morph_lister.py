import bpy
import bmesh
import generator_ik as g
import path_config

male_indices_filename = 'man_head_indices.json'
female_indices_filename = 'woman_head_indices.json'
folderpath, datapath = path_config.get_face_mesh_config()
morph_names = [
    'character_age',
    'character_mass',
    'character_tone',
    'Face_Ellipsoid',
    'Face_Parallelepiped',
    'Face_Triangle',
    'Eyes_BagProminence',
    'Eyes_BagSize',
    'Eyes_InnerPosX',
    'Eyes_InnerPosZ',
    'Eyes_IrisSize',
    'Eyes_OuterPosX',
    'Eyes_InnerPosZ',
    'Eyes_PosX',
    'Eyes_PosZ',
    'Eyes_Size',
    'Eyes_SizeZ',
    'Eyes_TypeAlmond',
    'Eyes_TypeHooded',
    'Eyes_innerSinus',
    'Cheeks_CreaseExt',
    'Cheeks_InfraVolume',
    'Cheeks_Mass',
    'Cheeks_SideCrease',
    'Cheeks_Tone',
    'Cheeks_Zygom',
    'Cheeks_ZygomPosZ',
    'Chin_Cleft',
    'Chin_Prominence',
    'Chin_SizeX',
    'Chin_SizeZ',
    'Chin_Tone',
    'Ears_Lobe',
    'Ears_LocY',
    'Ears_LocZ',
    'Ears_RotX',
    'Ears_Round',
    'Ears_SizeX',
    'Ears_SizeY',
    'Ears_SizeZ',
    'Eyebrows_Angle',
    'Eyebrows_Droop',
    'Eyebrows_PosZ',
    'Eyebrows_Ridge',
    'Eyebrows_SizeY',
    'Eyebrows_Tone',
    'Eyelids_Angle',
    'Eyelids_Crease',
    'Eyelids_InnerPosZ',
    'Eyelids_LowerCurve',
    'Eyelids_MiddlePosZ',
    'Eyelids_OuterPosZ',
    'Eyelids_SizeZ',
    'Forehead_Angle',
    'Forehead_Curve',
    'Forehead_SizeX',
    'Forehead_SizeZ',
    'Forehead_Temple',
    'Jaw_Angle',
    'Jaw_Angle2',
    'Jaw_LocY',
    'Jaw_Prominence',
    'Jaw_ScaleX',
    'Nose_BallSizeX',
    'Nose_BasePosZ',
    'Nose_BaseShape',
    'Nose_BaseSizeX',
    'Nose_BaseSizeZ',
    'Nose_BridgeSizeX',
    'Nose_Curve',
    'Nose_GlabellaPosZ',
    'Nose_GlabellaSizeX',
    'Nose_GlabellaSizeY',
    'Nose_NostrilCrease',
    'Nose_NostrilDiam',
    'Nose_NostrilPosZ',
    'Nose_NostrilSizeX',
    'Nose_NostrilSizeY',
    'Nose_PosY',
    'Nose_SeptumFlat',
    'Nose_SeptumRolled',
    'Nose_SizeY',
    'Nose_TipAngle',
    'Nose_TipPosZ',
    'Nose_TipSize',
    'Nose_WingAngle',
    'Nose_WingBackFlat',
    'Nose_WingBump',
    'Mouth_CornersPosZ',
    'Mouth_LowerlipExt',
    'Mouth_LowerlipSizeZ',
    'Mouth_LowerlipVolume',
    'Mouth_PhiltrumProminence',
    'Mouth_PhiltrumSizeX',
    'Mouth_PhiltrumSizeY',
    'Mouth_PosY',
    'Mouth_PosZ',
    'Mouth_Protusion',
    'Mouth_SideCrease',
    'Mouth_SizeX',
    'Mouth_UpperlipExt',
    'Mouth_UpperlipSizeZ',
    'Mouth_UpperlipVolume',
    'Expressions_browOutVertL',
    'Expressions_browOutVertR',
    'Expressions_browSqueezeL',
    'Expressions_browSqueezeR',
    'Expressions_browsMidVert',
    'Expressions_cheekSneerL',
    'Expressions_cheekSneerR',
    'Expressions_chestExpansion',
    'Expressions_deglutition',
    'Expressions_eyeClosedL',
    'Expressions_eyeClosedPressureL',
    'Expressions_eyeClosedPressureR',
    'Expressions_eyeClosedR',
    'Expressions_eyeSquintL',
    'Expressions_eyeSquintR',
    'Expressions_eyesHoriz',
    'Expressions_eyesVert',
    'Expressions_jawHoriz',
    'Expressions_jawOut',
    'Expressions_mouthBite',
    'Expressions_mouthChew',
    'Expressions_mouthClosed',
    'Expressions_mouthHoriz',
    'Expressions_mouthInflated',
    'Expressions_mouthLowerOut',
    'Expressions_mouthOpen',
    'Expressions_mouthOpenAggr',
    'Expressions_mouthOpenHalf',
    'Expressions_mouthOpenLarge',
    'Expressions_mouthOpenO',
    'Expressions_mouthOpenTeethClosed',
    'Expressions_mouthSmile',
    'Expressions_mouthSmileL',
    'Expressions_mouthSmileOpen',
    'Expressions_mouthSmileOpen2',
    'Expressions_mouthSmileR',
    'Expressions_nostrilsExpansion',
    'Expressions_pupilsDilatation'
]
values = [0, 0.25, 0.5, 0.75, 1]

def get_head_indices_filepath(gender):
    filename = male_indices_filename if gender == 1 else female_indices_filename
    return datapath + filename

def get_output_filename(morph_name, value):
    return '{}head_{}_{}.obj'.format(folderpath, morph_name, value)

def set_morph(character, morph_name, value):
    if not hasattr(character, morph_name):
        raise Exception("Object doesnt have attribute '{}'".format(morph_name))
    setattr(character, morph_name, value)
    bpy.context.scene.mblab_show_measures = False

def export_morph_file(character, indices, morph_name, value):
    set_morph(character, morph_name, value)
    bpy.context.scene.objects.active = character
    character.select = True
    bpy.ops.object.duplicate()
    bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)
    for obj in bpy.context.selected_objects:
        obj.name = "OBJ_Mesh"
    OBJ_Mesh = bpy.data.objects["OBJ_Mesh"]
    bpy.context.scene.objects.active = OBJ_Mesh
    bpy.ops.object.modifier_apply(modifier = 'mbastlab_armature')
    OBJ_Mesh.modifiers.clear()
    bpy.ops.object.select_all(action='DESELECT')
    OBJ_Mesh.select = True
    bpy.ops.object.mode_set(mode='EDIT')

    bpy.ops.mesh.select_mode(use_extend=False, use_expand=True, type='VERT')

    obj = bpy.context.object
    me = obj.data
    bm = bmesh.from_edit_mesh(me)

    vertices = [e for e in bm.verts]

    for vert in vertices:
        if vert.index in indices:
            vert.select = True
        else:
            vert.select = False

    bmesh.update_edit_mesh(me, True)
    bpy.ops.mesh.select_all(action='INVERT')
    bpy.ops.mesh.delete(type='FACE')
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.context.scene.objects.active = character

    for ob in bpy.data.objects['OBJ_Mesh'].vertex_groups:
        bpy.data.objects['OBJ_Mesh'].vertex_groups.remove(ob)

    bpy.ops.object.select_all(action='DESELECT')
    OBJ_Mesh.select = True
    bpy.context.scene.objects.active = OBJ_Mesh
    bpy.ops.object.convert(target='MESH')

    bpy.ops.object.select_all(action='DESELECT')

    OBJ_Mesh.select = True
    target_file = get_output_filename(morph_name, value)
    bpy.ops.export_scene.obj(filepath=target_file, use_selection = True, use_animation = False, use_mesh_modifiers=False, use_mesh_modifiers_render=False, use_edges=False, use_smooth_groups=False, use_smooth_groups_bitflags=False, use_normals=False, use_uvs=True, use_materials=False, use_triangles=False, use_nurbs=False, use_vertex_groups=False, use_blen_objects=False, group_by_object=False, group_by_material=False, keep_vertex_order=False)

    OBJ_Mesh.select = True
    bpy.context.scene.objects.active = OBJ_Mesh
    bpy.ops.object.delete(use_global=True)
    bpy.ops.object.select_all(action='DESELECT')

def run():
    g.gc()
    gender = 1
    g.makedirs(folderpath)
    g.init_human(gender)

    if gender == 0:
        character = bpy.context.scene.objects["f_ca01"]
    if gender == 1:
        character = bpy.context.scene.objects["m_ca01"]

    indices_filepath = get_head_indices_filepath(gender)
    indices_file = open(indices_filepath, 'r')
    indices = eval(indices_file.read())

    total = len(morph_names)
    number = 1
    for morph_name in morph_names:
        print("Exporting '{}' ({}/{})".format(morph_name, number, total))
        default_value = getattr(character, morph_name)
        for value in values:
            export_morph_file(character, indices, morph_name, value)
        set_morph(character, morph_name, default_value)
        number = number + 1

def main():
    run()

if __name__ == "__main__":
    main()