from pathlib import Path
import configparser

cfg = configparser.ConfigParser()
config_file = Path(__file__).parent / 'personal.ini'
cfg.read(config_file)