import bpy
import random
import math
import numpy as np
from bpy_extras.object_utils import world_to_camera_view
import datetime
scene = bpy.data.scenes["Scene"]
pi = 3.14159265
ARMATURE = 'MBLab_skeleton_base_ik'

def allign_colliders(gender):
    if gender == 0:
        armature = scene.objects["f_ca01_skeleton"]
    if gender == 1:
        armature = scene.objects["m_ca01_skeleton"]
        
    armature.data.layers[0] = True
    armature.data.layers[1] = True
    # Set Head Collider    
#    bpy.context.area.type = 'VIEW_3D' 
#    bpy.context.area.type = 'TEXT_EDITOR' 
    bpy.ops.object.select_all(action='DESELECT')
    bpy.data.objects['head_collider'].select = True
#    armature = scene.objects["MBLab_skeleton_base_fk"]
    armature.select = True
    bpy.context.scene.objects.active = armature
    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='DESELECT')
    arm = bpy.data.armatures[ARMATURE]
    arm.bones.active = arm.bones["head"] 
    bpy.context.object.data.bones["head"].select = True
    bpy.ops.object.parent_set(type='BONE')
#    bpy.ops.object.select_all(action='DESELECT')
    bpy.data.objects['head_collider'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['head_collider']
    bpy.ops.object.parent_clear(type='CLEAR_INVERSE')
    bpy.ops.object.location_clear(clear_delta=False)
    bpy.ops.object.rotation_clear(clear_delta=False)
    bpy.ops.object.select_all(action='DESELECT')
    #Set Neck Collider
    bpy.data.objects['neck_collider'].select = True
#    armature = scene.objects["MBLab_skeleton_base_fk"]
    armature.select = True
    bpy.context.scene.objects.active = armature
    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='DESELECT')
    arm = bpy.data.armatures[ARMATURE]
    arm.bones.active = arm.bones["neck"] 
    bpy.context.object.data.bones["neck"].select = True
    bpy.ops.object.parent_set(type='BONE')
#    bpy.ops.object.select_all(action='DESELECT')
    bpy.data.objects['neck_collider'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['neck_collider']
    bpy.ops.object.parent_clear(type='CLEAR_INVERSE')
    bpy.ops.object.location_clear(clear_delta=False)
    bpy.ops.object.rotation_clear(clear_delta=False)
    bpy.ops.object.select_all(action='DESELECT')
    #Set Shoulder_R Collider
    bpy.data.objects['shoulder_collider_r'].select = True
#    armature = scene.objects["MBLab_skeleton_base_fk"]
    armature.select = True
    bpy.context.scene.objects.active = armature
    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='DESELECT')
    arm = bpy.data.armatures[ARMATURE]
    arm.bones.active = arm.bones["clavicle_R"] 
    bpy.context.object.data.bones["clavicle_R"].select = True
    bpy.ops.object.parent_set(type='BONE')
#    bpy.ops.object.select_all(action='DESELECT')
    bpy.data.objects['shoulder_collider_r'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['shoulder_collider_r']
    bpy.ops.object.parent_clear(type='CLEAR_INVERSE')
    bpy.ops.object.location_clear(clear_delta=False)
    bpy.ops.object.rotation_clear(clear_delta=False)
    bpy.ops.object.select_all(action='DESELECT')
    #Set Shoulder_L Colli1der
    bpy.data.objects['shoulder_collider_l'].select = True
#    armature = scene.objects["MBLab_skeleton_base_fk"]
    armature.select = True
    bpy.context.scene.objects.active = armature
    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='DESELECT')
    arm = bpy.data.armatures[ARMATURE]
    arm.bones.active = arm.bones["clavicle_L"] 
    bpy.context.object.data.bones["clavicle_L"].select = True
    bpy.ops.object.parent_set(type='BONE')
#    bpy.ops.object.select_all(action='DESELECT')
    bpy.data.objects['shoulder_collider_l'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['shoulder_collider_l']
    bpy.ops.object.parent_clear(type='CLEAR_INVERSE')
    bpy.ops.object.location_clear(clear_delta=False)
    bpy.ops.object.rotation_clear(clear_delta=False)
    
    
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
 
    
def styler_to_head(gender):
    if gender == 0:
        armature = scene.objects["f_ca01_skeleton"]
    if gender == 1:
        armature = scene.objects["m_ca01_skeleton"]
        
    armature.data.layers[0] = True
    armature.data.layers[1] = True
    
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    #Set Neck Collider
    bpy.data.objects['styler_01_holder'].select = True
    armature.select = True
    bpy.context.scene.objects.active = armature
    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='DESELECT')
    arm = bpy.data.armatures[ARMATURE]
    arm.bones.active = arm.bones["head"] 
    bpy.context.object.data.bones["head"].select = True
    bpy.ops.object.parent_set(type='BONE')
    bpy.ops.pose.select_all(action='DESELECT')
    bpy.data.objects['styler_01_holder'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['styler_01_holder']
    bpy.ops.object.parent_clear(type='CLEAR_INVERSE')
    bpy.ops.object.location_clear(clear_delta=False)
    bpy.ops.object.rotation_clear(clear_delta=False)
    bpy.ops.object.parent_clear(type='CLEAR_KEEP_TRANSFORM')
    bpy.ops.object.rotation_clear(clear_delta=False)
#    bpy.ops.pose.select_all(action='DESELECT')
    
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')

def reset_colliders_syler():
    bpy.ops.object.select_all(action='DESELECT')
    bpy.data.objects['head_collider'].select = True
    bpy.data.objects['neck_collider'].select = True
    bpy.data.objects['shoulder_collider_r'].select = True
    bpy.data.objects['shoulder_collider_l'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['head_collider']
    bpy.ops.object.parent_clear(type='CLEAR')
    bpy.ops.object.parent_clear(type='CLEAR')
    bpy.ops.object.select_all(action='DESELECT')
    cam = bpy.data.objects["styler_01_holder"]
    cam.select = True
    bpy.ops.object.location_clear(clear_delta=False)
    bpy.ops.object.rotation_clear(clear_delta=False)
    bpy.ops.object.scale_clear(clear_delta=False)
    
    
def create_hair(gender):
    bpy.ops.object.select_all(action='DESELECT')
    if gender == 0:
        human = bpy.data.objects["f_ca01"]
        bpy.context.scene.objects.active  = human
    if gender == 1:
        human = bpy.data.objects["m_ca01"]
        bpy.context.scene.objects.active  = human
        
    obj = bpy.context.active_object
    
    if len(obj.particle_systems) == 0:
        obj.modifiers.new("Hair", type='PARTICLE_SYSTEM')
        bpy.context.object.particle_systems["ParticleSystem"].name = "Hair"
        par_set = bpy.data.particles['Hair_01_Preset'] #get particle setting you want to assign
        human.particle_systems['Hair'].settings = par_set #assign particle settings to object's particle system
        bpy.context.object.particle_systems["Hair"].vertex_group_density = "sculp"
        bpy.context.object.particle_systems["Hair"].use_hair_dynamics = True



#Styler Decay
def decay(object, decay_frame):
    #!!!set frames to decay!!!!
    object.keyframe_insert(data_path = 'field.strength', frame = 0)
    object.field.strength = 0
    object.keyframe_insert(data_path = 'field.strength', frame = decay_frame)
    
def damp_decay(gender):
    bpy.ops.object.select_all(action='DESELECT')
    if gender == 0:
        human = bpy.data.objects["f_ca01"]
    if gender == 1:
        human = bpy.data.objects["m_ca01"]
    
    human.modifiers["Hair"].particle_system.cloth.settings.bending_damping = 1
    human.keyframe_insert(data_path = 'modifiers["Hair"].particle_system.cloth.settings.bending_damping', frame = 0)
    human.keyframe_insert(data_path = 'modifiers["Hair"].particle_system.cloth.settings.bending_damping', frame = 25)
    human.modifiers["Hair"].particle_system.cloth.settings.bending_damping = 10
    human.keyframe_insert(data_path = 'modifiers["Hair"].particle_system.cloth.settings.bending_damping', frame = 30)
    

#Setting Up Short Hair
def short_hair_setup(gender):

    bpy.ops.object.select_all(action='DESELECT')

    if gender == 0:
        human = bpy.data.objects["f_ca01"]
        bpy.context.scene.objects.active = human
        human.select = True
    if gender == 1:
        human = bpy.data.objects["m_ca01"]
        bpy.context.scene.objects.active = human
        human.select = True


    obj = bpy.context.active_object
    bpy.data.particles["Hair_01_Preset"].hair_length = random.uniform(0.01, 0.03)
    bpy.data.particles["Hair_01_Preset"].kink = 'NO'
    randomstyle = random.randint(1, 5)
    if randomstyle == 1:
        bpy.data.particles["Hair_01_Preset"].kink = 'CURL'
        bpy.data.particles["Hair_01_Preset"].kink_amplitude = random.uniform(0.005, 0.01)
        bpy.context.object.particle_systems["Hair"].cloth.settings.bending_stiffness = random.uniform(0.1, 0.25)
    if randomstyle == 2:
        bpy.data.particles["Hair_01_Preset"].kink = 'RADIAL'
        bpy.data.particles["Hair_01_Preset"].kink_amplitude = random.uniform(0.01, 1)
        bpy.context.object.particle_systems["Hair"].cloth.settings.bending_stiffness = random.uniform(0.1, 0.25)
    if randomstyle == 3:
        bpy.data.particles["Hair_01_Preset"].kink = 'WAVE'
        bpy.data.particles["Hair_01_Preset"].kink_amplitude = random.uniform(0.001, 0.03)
        bpy.context.object.particle_systems["Hair"].cloth.settings.bending_stiffness = random.uniform(0.1, 0.25)
    if randomstyle == 4:
        bpy.data.particles["Hair_01_Preset"].kink = 'BRAID'
        bpy.data.particles["Hair_01_Preset"].kink_amplitude = random.uniform(0.015, 0.03)
        bpy.context.object.particle_systems["Hair"].cloth.settings.bending_stiffness = random.uniform(0.1, 0.25)
    if randomstyle == 5:
        bpy.data.particles["Hair_01_Preset"].kink = 'NO'
#        bpy.data.particles["Hair_01_Preset"].kink_amplitude = random.uniform(0.001, 0.01)
#        bpy.context.object.particle_systems["Hair"].cloth.settings.bending_stiffness = random.uniform(0.25, 0.4)
    #Set The random Factors!!!!
    #Set styler vortex
    bpy.data.objects['styler_vortex_01'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_01']
    bpy.context.object.field.strength = random.uniform(2, 4)
    
    bpy.ops.object.select_all(action='DESELECT')
    bpy.data.objects['styler_vortex_backwards_01'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_backwards_01']
    bpy.context.object.field.strength = random.uniform(1, 2.5)
    
    bpy.ops.object.select_all(action='DESELECT')
    
    side = random.randint(1, 2)
    if side == 1:
        #Set styler vortex R
        bpy.data.objects['styler_vortex_r_01'].select = True
        bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_r_01']
        bpy.context.object.field.strength = random.uniform(2, 4)
        bpy.ops.object.select_all(action='DESELECT')
        bpy.data.objects['styler_vortex_l_01'].select = True
        bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_r_01']
        bpy.context.object.field.strength = random.uniform(0.1, 0.5)
        bpy.ops.object.select_all(action='DESELECT')
        #Set front R wind
        wind_R = bpy.data.objects['wind_r_01']
        wind_R.select = True
        bpy.context.scene.objects.active = wind_R
        wind_R.field.strength = random.uniform(0, 0.5)
        #!!!set frames to decay!!!!
        wind_R.keyframe_insert(data_path = 'field.strength', frame = 0)
        wind_R.field.strength = 0
        wind_R.keyframe_insert(data_path = 'field.strength', frame = 30)


        bpy.ops.object.select_all(action='DESELECT')
        #Set front L wind
        wind_L = bpy.data.objects['wind_l_01']
        wind_Lselect = True
        bpy.context.scene.objects.active = wind_L
        bpy.context.object.field.strength = random.uniform(0, 4)
        #!!!set frames to decay!!!!
        wind_L.keyframe_insert(data_path = 'field.strength', frame = 0)
        wind_L.field.strength = 0
        wind_L.keyframe_insert(data_path = 'field.strength', frame = 30)
        
        bpy.ops.object.select_all(action='DESELECT')
    if side == 2:
        #Set styler vortex L
        bpy.data.objects['styler_vortex_l_01'].select = True
        bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_l_01']
        bpy.context.object.field.strength = random.uniform(2, 4)
        bpy.ops.object.select_all(action='DESELECT')
        bpy.data.objects['styler_vortex_r_01'].select = True
        bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_l_01']
        bpy.context.object.field.strength = random.uniform(0.1, 0.5)
        bpy.ops.object.select_all(action='DESELECT')
        #Set front R wind
        wind_R = bpy.data.objects['wind_r_01']
        wind_R.select = True
        bpy.context.scene.objects.active = wind_R
        bpwind_Rstrength = random.uniform(0, 4)
        #!!!set frames to decay!!!!
        wind_R.keyframe_insert(data_path = 'field.strength', frame = 0)
        wind_R.field.strength = 0
        wind_R.keyframe_insert(data_path = 'field.strength', frame = 30)
                
        bpy.ops.object.select_all(action='DESELECT')
        #Set front L wind
        wind_L = bpy.data.objects['wind_l_01']
        wind_L.select = True
        bpy.context.scene.objects.active = wind_L
        wind_L.field.strength = random.uniform(0, 0.5)
        #!!!set frames to decay!!!!
        wind_L.keyframe_insert(data_path = 'field.strength', frame = 0)
        wind_L.field.strength = 0
        wind_L.keyframe_insert(data_path = 'field.strength', frame = 30)
        bpy.ops.object.select_all(action='DESELECT')
        
    #Set backwind
    wind_F = bpy.data.objects['wind_f_01']
    wind_F.select = True
    bpy.context.scene.objects.active = wind_F
    wind_F.field.strength = random.uniform(2, -4)
    #!!!set frames to decay!!!!
    wind_F.keyframe_insert(data_path = 'field.strength', frame = 0)
    wind_F.field.strength = 0
    wind_F.keyframe_insert(data_path = 'field.strength', frame = 30)
    bpy.ops.object.select_all(action='DESELECT')
    #Set hair ramp
    bpy.data.objects['hair_ramp'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['hair_ramp']
    bpy.context.object.field.strength = random.uniform(0, -4)
    bpy.ops.object.select_all(action='DESELECT')
    #Set Turbulence
    turb = bpy.data.objects['turbulence_01']
    turb.select = True
    bpy.context.scene.objects.active = turb
    turb.field.strength = random.uniform(1.5, 4)
    #!!!set frames to decay!!!!
    turb.keyframe_insert(data_path = 'field.strength', frame = 0)
    turb.field.strength = 0
    turb.keyframe_insert(data_path = 'field.strength', frame = 30)
    bpy.ops.object.select_all(action='DESELECT')    
    #Set Curve 01
    bpy.data.objects['hair_curve_01'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['hair_curve_01']
    bpy.context.object.field.strength = random.uniform(0, -5)
    bpy.ops.object.select_all(action='DESELECT')
    #Set Curve 01
    bpy.data.objects['hair_curve_02'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['hair_curve_02']
    bpy.context.object.field.strength = random.uniform(0, -5)
    bpy.ops.object.select_all(action='DESELECT')
    
    
def mid_hair_setup(gender, decay_frame):
    
    bpy.ops.object.select_all(action='DESELECT')
    
    if gender == 0:
        human = bpy.data.objects["f_ca01"]
        bpy.context.scene.objects.active  = human
        human.select = True
    if gender == 1:
        human = bpy.data.objects["m_ca01"]
        bpy.context.scene.objects.active  = human
        human.select = True     
        
    obj = bpy.context.active_object
    bpy.data.particles["Hair_01_Preset"].hair_length = random.uniform(0.05, 0.1)
    bpy.data.particles["Hair_01_Preset"].kink = 'NO'
    randomstyle = random.randint(1, 5)

    if randomstyle == 1:
        bpy.data.particles["Hair_01_Preset"].kink = 'CURL'
        bpy.data.particles["Hair_01_Preset"].kink_amplitude = random.uniform(0.005, 0.01)
        bpy.context.object.particle_systems["Hair"].cloth.settings.bending_stiffness = random.uniform(0.25, 0.4)
    if randomstyle == 2:
        bpy.data.particles["Hair_01_Preset"].kink = 'RADIAL'
        bpy.data.particles["Hair_01_Preset"].kink_amplitude = random.uniform(0.01, 1)
        bpy.context.object.particle_systems["Hair"].cloth.settings.bending_stiffness = random.uniform(0.25, 0.4)
    if randomstyle == 3:
        bpy.data.particles["Hair_01_Preset"].kink = 'WAVE'
        bpy.data.particles["Hair_01_Preset"].kink_amplitude = random.uniform(0.001, 0.01)
        bpy.context.object.particle_systems["Hair"].cloth.settings.bending_stiffness = random.uniform(0.25, 0.4)
    if randomstyle == 4:
        bpy.data.particles["Hair_01_Preset"].kink = 'BRAID'
        bpy.data.particles["Hair_01_Preset"].kink_amplitude = random.uniform(0.001, 0.01)
        bpy.context.object.particle_systems["Hair"].cloth.settings.bending_stiffness = random.uniform(0.25, 0.4)
    if randomstyle == 5:
        bpy.data.particles["Hair_01_Preset"].kink = 'NO'
#        bpy.data.particles["Hair_01_Preset"].kink_amplitude = random.uniform(0.001, 0.01)
#        bpy.context.object.particle_systems["Hair"].cloth.settings.bending_stiffness = random.uniform(0.25, 0.4)
    #Set The random Factors!!!!
    #Set styler vortex
    vortex = bpy.data.objects['styler_vortex_01']
    bpy.data.objects['styler_vortex_01'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_01']
    bpy.context.object.field.strength = random.uniform(5, 8)
    decay(vortex, decay_frame)
    bpy.ops.object.select_all(action='DESELECT')
    vortex_back = bpy.data.objects['styler_vortex_backwards_01']
    bpy.data.objects['styler_vortex_backwards_01'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_backwards_01']
    bpy.context.object.field.strength = random.uniform(1, 2.5)
    decay(vortex_back, decay_frame)
    bpy.ops.object.select_all(action='DESELECT')
    side = random.randint(1, 2)
    if side == 1:
        #Set styler vortex R
        vortex_R = bpy.data.objects['styler_vortex_r_01']
        bpy.data.objects['styler_vortex_r_01'].select = True
        bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_r_01']
        bpy.context.object.field.strength = random.uniform(5, 10)
        decay(vortex_R, decay_frame)
        bpy.ops.object.select_all(action='DESELECT')
        vortex_L = bpy.data.objects['styler_vortex_l_01']
        bpy.data.objects['styler_vortex_l_01'].select = True
        bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_r_01']
        bpy.context.object.field.strength = random.uniform(0.1, 1)
        decay(vortex_L, decay_frame)
        bpy.ops.object.select_all(action='DESELECT')
        #Set front R wind
        wind_R = bpy.data.objects['wind_r_01']
        wind_R.select = True
        bpy.context.scene.objects.active = wind_R
        wind_R.field.strength = random.uniform(0, 1)
        decay(wind_R, decay_frame)
        bpy.ops.object.select_all(action='DESELECT')

        bpy.ops.object.select_all(action='DESELECT')
        #Set front L wind
        wind_L = bpy.data.objects['wind_l_01']
        wind_L.select = True
        bpy.context.scene.objects.active = wind_L
        wind_L.field.strength = random.uniform(0, 8)
        decay(wind_L, decay_frame)
        bpy.ops.object.select_all(action='DESELECT')

    if side == 2:
        #Set styler vortex L
        vortex_L = bpy.data.objects['styler_vortex_l_01']
        bpy.data.objects['styler_vortex_l_01'].select = True
        bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_l_01']
        bpy.context.object.field.strength = random.uniform(5, 10)
        decay(vortex_L, decay_frame)
        bpy.ops.object.select_all(action='DESELECT')
        vortex_R = bpy.data.objects['styler_vortex_r_01']
        bpy.data.objects['styler_vortex_r_01'].select = True
        bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_l_01']
        bpy.context.object.field.strength = random.uniform(0.1, 1)
        decay(vortex_L, decay_frame)
        bpy.ops.object.select_all(action='DESELECT')
        #Set front R wind
        wind_R = bpy.data.objects['wind_r_01']
        wind_R.select = True
        bpy.context.scene.objects.active = wind_R
        wind_R.field.strength = random.uniform(0, 8)
        #!!!set frames to decay!!!!
        decay(wind_R, decay_frame)
        bpy.ops.object.select_all(action='DESELECT')
        #Set front L wind
        wind_L = bpy.data.objects['wind_l_01']
        wind_L.select = True
        bpy.context.scene.objects.active = wind_L
        wind_L.field.strength = random.uniform(0, 1)
        #!!!set frames to decay!!!!
        decay(wind_L, decay_frame)
        bpy.ops.object.select_all(action='DESELECT')
    #Set backwind
    wind_F = bpy.data.objects['wind_f_01']
    wind_F.select = True
    bpy.context.scene.objects.active = wind_F
    wind_F.field.strength = random.uniform(4, -8)
    decay(wind_F, decay_frame)
    bpy.ops.object.select_all(action='DESELECT')

    #Set hair ramp
    ramp = bpy.data.objects['hair_ramp']
    bpy.data.objects['hair_ramp'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['hair_ramp']
    bpy.context.object.field.strength = random.uniform(0, -5)
    decay(ramp, decay_frame)
    bpy.ops.object.select_all(action='DESELECT')
    #Set Turbulence
    turb = bpy.data.objects['turbulence_01']
    turb.select = True
    bpy.context.scene.objects.active = turb
    turb.field.strength = random.uniform(3, 8)
    #!!!set frames to decay!!!!
    decay(turb, decay_frame)
    bpy.ops.object.select_all(action='DESELECT')
    
    #Set Curve 01
    bpy.data.objects['hair_curve_01'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['hair_curve_01']
    bpy.context.object.field.strength = random.uniform(0, -10)
    bpy.ops.object.select_all(action='DESELECT')
    #Set Curve 01
    bpy.data.objects['hair_curve_02'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['hair_curve_02']
    bpy.context.object.field.strength = random.uniform(0, -10)
    bpy.ops.object.select_all(action='DESELECT')


def long_hair_setup(gender):

    bpy.ops.object.select_all(action='DESELECT')

    if gender == 0:
        human = bpy.data.objects["f_ca01"]
        bpy.context.scene.objects.active  = human
        human.select = True
    if gender == 1:
        human = bpy.data.objects["m_ca01"]
        bpy.context.scene.objects.active  = human
        human.select = True  


    obj = bpy.context.active_object
    bpy.data.particles["Hair_01_Preset"].hair_length = random.uniform(0.3, 0.4)
    bpy.data.particles["Hair_01_Preset"].kink = 'NO'
    randomstyle = random.randint(1, 5)
    if randomstyle == 1:
        bpy.data.particles["Hair_01_Preset"].kink = 'CURL'
        bpy.data.particles["Hair_01_Preset"].kink_amplitude = random.uniform(0.005, 0.015)
        bpy.context.object.particle_systems["Hair"].cloth.settings.bending_stiffness = random.uniform(0.25, 0.3)
    if randomstyle == 2:
        bpy.data.particles["Hair_01_Preset"].kink = 'RADIAL'
        bpy.data.particles["Hair_01_Preset"].kink_amplitude = random.uniform(0.1, 1)
        bpy.context.object.particle_systems["Hair"].cloth.settings.bending_stiffness = random.uniform(0.25, 0.3)
    if randomstyle == 3:
        bpy.data.particles["Hair_01_Preset"].kink = 'WAVE'
        bpy.data.particles["Hair_01_Preset"].kink_amplitude = random.uniform(0.001, 0.02)
        bpy.context.object.particle_systems["Hair"].cloth.settings.bending_stiffness = random.uniform(0.25, 0.3)
    if randomstyle == 4:
        bpy.data.particles["Hair_01_Preset"].kink = 'BRAID'
        bpy.data.particles["Hair_01_Preset"].kink_amplitude = random.uniform(0.001, 0.015)
        bpy.context.object.particle_systems["Hair"].cloth.settings.bending_stiffness = random.uniform(0.25, 0.3)
    if randomstyle == 5:
        bpy.data.particles["Hair_01_Preset"].kink = 'NO'
#        bpy.data.particles["Hair_01_Preset"].kink_amplitude = random.uniform(0.001, 0.01)
#        bpy.context.object.particle_systems["Hair"].cloth.settings.bending_stiffness = random.uniform(0.25, 0.4)
    #Set The random Factors!!!!
    #Set styler vortex
    bpy.data.objects['styler_vortex_01'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_01']
    bpy.context.object.field.strength = random.uniform(6.5, 8)
    bpy.ops.object.select_all(action='DESELECT')
    bpy.data.objects['styler_vortex_backwards_01'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_backwards_01']
    bpy.context.object.field.strength = random.uniform(1, 2.5)
    bpy.ops.object.select_all(action='DESELECT')
    side = random.randint(1, 2)
    if side == 1:
        #Set styler vortex R
        bpy.data.objects['styler_vortex_r_01'].select = True
        bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_r_01']
        bpy.context.object.field.strength = random.uniform(5, 8)
        bpy.ops.object.select_all(action='DESELECT')
        bpy.data.objects['styler_vortex_l_01'].select = True
        bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_r_01']
        bpy.context.object.field.strength = random.uniform(0.1, 1)
        bpy.ops.object.select_all(action='DESELECT')
        #Set front R wind
        bpy.data.objects['wind_r_01'].select = True
        bpy.context.scene.objects.active = bpy.data.objects['wind_r_01']
        bpy.context.object.field.strength = random.uniform(0, 1)
        bpy.ops.object.select_all(action='DESELECT')
        #Set front L wind
        bpy.data.objects['wind_l_01'].select = True
        bpy.context.scene.objects.active = bpy.data.objects['wind_l_01']
        bpy.context.object.field.strength = random.uniform(0, 8)
        bpy.ops.object.select_all(action='DESELECT')
    if side == 2:
        #Set styler vortex L
        bpy.data.objects['styler_vortex_l_01'].select = True
        bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_l_01']
        bpy.context.object.field.strength = random.uniform(5, 10)
        bpy.ops.object.select_all(action='DESELECT')
        bpy.data.objects['styler_vortex_r_01'].select = True
        bpy.context.scene.objects.active = bpy.data.objects['styler_vortex_l_01']
        bpy.context.object.field.strength = random.uniform(0.1, 1)
        bpy.ops.object.select_all(action='DESELECT')
        #Set front R wind
        bpy.data.objects['wind_r_01'].select = True
        bpy.context.scene.objects.active = bpy.data.objects['wind_r_01']
        bpy.context.object.field.strength = random.uniform(0, 8)
        bpy.ops.object.select_all(action='DESELECT')
        #Set front L wind
        bpy.data.objects['wind_l_01'].select = True
        bpy.context.scene.objects.active = bpy.data.objects['wind_l_01']
        bpy.context.object.field.strength = random.uniform(0, 1)
        bpy.ops.object.select_all(action='DESELECT')
    #Set backwind
    bpy.data.objects['wind_f_01'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['wind_f_01']
    bpy.context.object.field.strength = random.uniform(4, -1)
    bpy.ops.object.select_all(action='DESELECT')
    #Set hair ramp
    bpy.data.objects['hair_ramp'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['hair_ramp']
    bpy.context.object.field.strength = random.uniform(0, -4)
    bpy.ops.object.select_all(action='DESELECT')
    #Set Turbulence
    bpy.data.objects['turbulence_01'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['turbulence_01']
    bpy.context.object.field.strength = random.uniform(3, 8)
    bpy.ops.object.select_all(action='DESELECT')
    #Set Curve 01
    bpy.data.objects['hair_curve_01'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['hair_curve_01']
    bpy.context.object.field.strength = random.uniform(0, -10)
    bpy.ops.object.select_all(action='DESELECT')
    #Set Curve 01
    bpy.data.objects['hair_curve_02'].select = True
    bpy.context.scene.objects.active = bpy.data.objects['hair_curve_02']
    bpy.context.object.field.strength = random.uniform(0, -10)
    bpy.ops.object.select_all(action='DESELECT')


def set_up_hair_cache(gender, hair, beard, mustache):
    #SetUpCahce
    bpy.ops.object.select_all(action='DESELECT')

    if gender == 0:
        human = bpy.data.objects["f_ca01"]
        bpy.context.scene.objects.active  = human
        human.select = True
    if gender == 1:
        human = bpy.data.objects["m_ca01"]
        bpy.context.scene.objects.active  = human
        human.select = True

    obj = human

    if hair == 1:
        obj.particle_systems["Hair"].point_cache.frame_start = scene.frame_start
        obj.particle_systems["Hair"].point_cache.frame_end = scene.frame_end
        human.particle_systems["Hair"].point_cache.use_disk_cache = True
    if mustache ==1:

        obj.particle_systems['Mustache'].point_cache.frame_start = scene.frame_start
        obj.particle_systems['Mustache'].point_cache.frame_end = scene.frame_end
        human.particle_systems["Mustache"].point_cache.use_disk_cache = True
    if beard ==1:

        obj.particle_systems['Beard'].point_cache.frame_start = scene.frame_start
        obj.particle_systems['Beard'].point_cache.frame_end = scene.frame_end
        human.particle_systems["Beard"].point_cache.use_disk_cache = True

    
def cahce_hair(gender, hair, beard, mustache):
    scene = bpy.data.scenes["Scene"]
    bpy.ops.object.select_all(action='DESELECT')
    
    if gender == 0:
        objName = "f_ca01"
        human = bpy.data.objects['f_ca01']
        
    if gender == 1:
        objName = "m_ca01"
        human = bpy.data.objects['m_ca01']
        
    result = None
    
    if hair == 1:
        pname1 = 'Hair'
        human.particle_systems[pname1].point_cache.frame_start = scene.frame_start
        human.particle_systems[pname1].point_cache.frame_end = scene.frame_end
        human.particle_systems[pname1].point_cache.use_disk_cache = True
    elif hair == 0:
        pname1 = 'Null'
    if beard == 1:
        pname2 = 'Beard'
        human.particle_systems[pname2].point_cache.frame_start = scene.frame_start
        human.particle_systems[pname2].point_cache.frame_end = scene.frame_end
        human.particle_systems[pname2].point_cache.use_disk_cache = True
    elif beard == 0:
        pname2 = 'Null'
    if mustache == 1:
        pname3 = "Mustache"
        human.particle_systems[pname3].point_cache.frame_start = scene.frame_start
        human.particle_systems[pname3].point_cache.frame_end = scene.frame_end
        human.particle_systems[pname3].point_cache.use_disk_cache = True
    elif mustache == 0:
        pname3 = 'Null'
        
    for scene in bpy.data.scenes:
        for object in scene.objects:
            for particle in object.particle_systems:
                if object.name == objName:
                    if particle.name == pname1:
                        override = {"blend_data": bpy.data,"scene": scene, "active_object": object.name,"point_cache": particle.point_cache}
                        print ("Start baking")
                        bpy.ops.ptcache.bake(override, bake=True)
                        print ("Baking is finished")
                        #bpy.ops.ptcache.free_bake(override)
                        #print ("Freed Bake")
                        bpy.context.scene.frame_set(0)
                        
                    if particle.name == pname2:
                        override = {"blend_data": bpy.data,"scene": scene, "active_object": object.name,"point_cache": particle.point_cache}
                        print ("Start baking")
                        bpy.ops.ptcache.bake(override, bake=True)
                        print ("Baking is finished")
                        #bpy.ops.ptcache.free_bake(override)
                        #print ("Freed Bake")
                        bpy.context.scene.frame_set(0)
                        
                    if particle.name == pname3:
                        override = {"blend_data": bpy.data,"scene": scene, "active_object": object.name,"point_cache": particle.point_cache}
                        print ("Start baking")
                        bpy.ops.ptcache.bake(override, bake=True)
                        print ("Baking is finished")
                        #bpy.ops.ptcache.free_bake(override)
                        #print ("Freed Bake")
                        bpy.context.scene.frame_set(0)
                        
                        
def random_hair_color(gender):
    bpy.ops.object.select_all(action='DESELECT')
    
    if gender == 0:
        human = bpy.data.objects["f_ca01"]
        bpy.context.scene.objects.active  = human
        human.select = True
    if gender == 1:
        human = bpy.data.objects["m_ca01"]
        bpy.context.scene.objects.active  = human
        human.select = True
        
    mat = bpy.data.materials.get("hair_mat")
    ob = bpy.context.active_object
    ob.data.materials.append(mat)
    bpy.data.particles["Hair_01_Preset"].material_slot = 'hair_mat'
    bpy.data.particles["beard_preset"].material_slot = 'hair_mat'
    bpy.data.particles["mustache_preset"].material_slot = 'hair_mat'
    nodes = mat.node_tree.nodes
    #rgb
    #main color
    R = random.uniform(0, 75/255)
    G = R / random.uniform(1.2, 2)
    B = G / random.uniform(1.2, 2)
    R1 = (R / 100) * random.uniform(1, 25)
    G1 = (G / 100) * random.uniform(1, 25)
    B1 = (B / 100) * random.uniform(1, 25)
    R2 = (R / 100) * random.uniform(200, 250)
    G2 = (G / 100) * random.uniform(200, 250)
    B2 = (B / 100) * random.uniform(200, 250)
    R3 = (R2 / 100) * random.uniform(150, 250)
    G3 = (G2 / 100) * random.uniform(150, 250)
    B3 = (B2 / 100) * random.uniform(150, 250)
    #Main Hair color node
    hair_main_color = nodes.get("ColorRamp")
    #hair middle ramp
    hair_main_color.color_ramp.elements[1].color = (R, G, B, 1)
    #hair start ramp
    hair_main_color.color_ramp.elements[0].color = (R1, G1, B1, 1)
    #hair end ramp
    hair_main_color.color_ramp.elements[2].color = (R2, G2, B2, 1)
    #get HSV offset
    hair_variance = nodes.get("Hue Saturation Value")
    hair_variance.inputs[0].default_value = random.uniform(0.45, 0.55)
    hair_variance.inputs[1].default_value = random.uniform(0.1, 1.5)
    hair_variance.inputs[2].default_value = random.uniform(0.6, 0.8)
    bpy.ops.object.select_all(action='DESELECT')
        
    
#allign_colliders(1)
#styler_to_head(1)
#reset_colliders_syler()
#create_hair(1)
#short_hair_setup(1)
#set_up_hair_cache(1)
#cahce_hair(1)
#random_hair_color(1)
#damp_decay(1)

#FACIAL HAIRS!

def create_facial_hairs(gender, has_beard, has_mustache):
    bpy.ops.object.select_all(action='DESELECT')
    if gender == 0:
        human = bpy.data.objects["f_ca01"]
        bpy.context.scene.objects.active  = human
    if gender == 1:
        human = bpy.data.objects["m_ca01"]
        bpy.context.scene.objects.active  = human
        
    obj = bpy.context.active_object
    particle_systems_count = len(bpy.context.object.particle_systems)
    
#    has_beard = 1 #random.randint(0, 1)
#    has_mustache = 1 #random.randint(0, 1)
    
    if has_mustache == 1:
        particle_systems_count = len(bpy.context.object.particle_systems)
        obj.modifiers.new("Mustache", type='PARTICLE_SYSTEM')
        bpy.context.object.particle_systems[particle_systems_count].name = "Mustache"
        par_set = bpy.data.particles['mustache_preset'] #get particle setting you want to assign
        human.particle_systems['Mustache'].settings = par_set #assign particle settings to object's particle system
        bpy.context.object.particle_systems["Mustache"].vertex_group_density = "mustache"
        bpy.context.object.particle_systems["Mustache"].use_hair_dynamics = True
        bpy.data.particles["mustache_preset"].texture_slots[0].texture.image = bpy.data.images['mustache_01.png']
        bpy.data.particles["mustache_preset"].use_modifier_stack = True
        
        
    
    if has_beard == 1:
        particle_systems_count = len(bpy.context.object.particle_systems)
        obj.modifiers.new("Beard", type='PARTICLE_SYSTEM')
        bpy.context.object.particle_systems[particle_systems_count].name = "Beard"
        par_set = bpy.data.particles['beard_preset'] #get particle setting you want to assign
        human.particle_systems['Beard'].settings = par_set #assign particle settings to object's particle system
        bpy.context.object.particle_systems["Beard"].vertex_group_density = "face_hair"
        bpy.context.object.particle_systems["Beard"].use_hair_dynamics = True
        bpy.data.particles["beard_preset"].texture_slots[0].texture.image = bpy.data.images['beard_01.png']
        bpy.data.particles["beard_preset"].use_modifier_stack = True


    #set_up face hairs
        bpy.ops.object.select_all(action='DESELECT')   

    obj = bpy.context.active_object
    facial_hair_length = random.uniform(0.001, 0.003)
    bpy.data.particles["beard_preset"].hair_length = facial_hair_length
    bpy.data.particles["beard_preset"].kink = 'NO'
    bpy.data.particles["mustache_preset"].hair_length = facial_hair_length
    bpy.data.particles["mustache_preset"].kink = 'NO'
    
    #damp facial hairs 
    if gender == 0:
        human = bpy.data.objects["f_ca01"]
    if gender == 1:
        human = bpy.data.objects["m_ca01"]
    
    if has_beard == 1:
        print('Setting up beard')
        human.modifiers["Beard"].particle_system.cloth.settings.bending_damping = 3
        human.keyframe_insert(data_path = 'modifiers["Beard"].particle_system.cloth.settings.bending_damping', frame = 0)
        human.keyframe_insert(data_path = 'modifiers["Beard"].particle_system.cloth.settings.bending_damping', frame = 25)
        
        human.modifiers["Beard"].particle_system.cloth.settings.bending_damping = 15
        human.keyframe_insert(data_path = 'modifiers["Beard"].particle_system.cloth.settings.bending_damping', frame = 30)
        
    if has_mustache == 1:
        print('Setting up mustache')
        human.modifiers["Mustache"].particle_system.cloth.settings.bending_damping = 3
        human.keyframe_insert(data_path = 'modifiers["Mustache"].particle_system.cloth.settings.bending_damping', frame = 0)
        human.keyframe_insert(data_path = 'modifiers["Mustache"].particle_system.cloth.settings.bending_damping', frame = 25)
        
        human.modifiers["Mustache"].particle_system.cloth.settings.bending_damping = 15
        human.keyframe_insert(data_path = 'modifiers["Mustache"].particle_system.cloth.settings.bending_damping', frame = 30)
    
    
    
    #cahce
    obj = human
    
    if has_beard == 1:
        obj.particle_systems["Beard"].point_cache.frame_start = scene.frame_start
        obj.particle_systems["Beard"].point_cache.frame_start = scene.frame_start
    if has_mustache == 1:
        obj.particle_systems["Mustache"].point_cache.frame_end = scene.frame_end  
        obj.particle_systems["Mustache"].point_cache.frame_end = scene.frame_end  


def randomise_facial_hairs(gender, has_beard, has_mustache):
    randomstyle = random.randint(0, 3)
    
    if gender == 0:
        human = bpy.data.objects["f_ca01"]
    if gender == 1:
        human = bpy.data.objects["m_ca01"]
    
    if randomstyle == 0:
        
        facial_hair_length = random.uniform(0.02, 0.0015)
        if facial_hair_length >= 0.007:
            mustache_hair_length = facial_hair_length/2
        else:
            mustache_hair_length = facial_hair_length/1.1
            
        print(mustache_hair_length)
        print(facial_hair_length)
        
        human.select = True
        bpy.context.scene.objects.active = human
        
        facial_kink_amplitude = random.uniform(0.01, 0.1)
        facial_kink_frequency = random.uniform(0.005, 0.007)
        roughness_1 = random.uniform(0, 0.015)
        roughness_2 = random.uniform(0, 0.015)
        roughness_2_threshold = random.uniform(0, 1)
        facial_hair_bending_stiffness = random.uniform(0.3, 0.6)
        kink_type = 'CURL'
        
        if has_beard == 1:
            bpy.data.particles["beard_preset"].hair_length = facial_hair_length    
            bpy.data.particles["beard_preset"].kink = kink_type
            
            bpy.data.particles["beard_preset"].kink_amplitude = facial_kink_amplitude
            bpy.data.particles["beard_preset"].kink_frequency = facial_kink_frequency
            bpy.data.particles["beard_preset"].roughness_1 = roughness_1
            bpy.data.particles["beard_preset"].roughness_2 = roughness_2
            bpy.data.particles["beard_preset"].roughness_2_threshold = roughness_2_threshold
            bpy.context.object.particle_systems["Beard"].cloth.settings.bending_stiffness = facial_hair_bending_stiffness
            
        if has_mustache ==1:
#            mustache_hair_length = facial_hair_length
            bpy.data.particles["mustache_preset"].hair_length = mustache_hair_length
            bpy.data.particles["mustache_preset"].kink = kink_type
            
            bpy.data.particles["mustache_preset"].kink_amplitude = facial_kink_amplitude
            bpy.data.particles["mustache_preset"].kink_frequency = facial_kink_frequency
            bpy.data.particles["mustache_preset"].roughness_1 = roughness_1
            bpy.data.particles["mustache_preset"].roughness_2 = roughness_2
            bpy.data.particles["mustache_preset"].roughness_2_threshold = roughness_2_threshold
            bpy.context.object.particle_systems["Mustache"].cloth.settings.bending_stiffness = facial_hair_bending_stiffness
            
        bpy.ops.object.select_all(action='DESELECT')
        
    if randomstyle == 1:
        
        facial_hair_length = random.uniform(0.02, 0.0015)
        if facial_hair_length >= 0.007:
            mustache_hair_length = facial_hair_length/2
        else:
            mustache_hair_length = facial_hair_length/1.1
            
        print(mustache_hair_length)
        print(facial_hair_length)
        
        human.select = True
        bpy.context.scene.objects.active = human
        
        facial_kink_amplitude = random.uniform(0.005, 0.01)
        facial_kink_frequency = random.uniform(0.05, 0.15)
        roughness_1 = random.uniform(0, 0.015)
        roughness_2 = random.uniform(0, 0.015)
        roughness_2_threshold = random.uniform(0, 1)
        facial_hair_bending_stiffness = random.uniform(0.3, 0.6)
        kink_type = 'RADIAL'
        
        if has_beard == 1:
            bpy.data.particles["beard_preset"].hair_length = facial_hair_length    
            bpy.data.particles["beard_preset"].kink = kink_type
            
            bpy.data.particles["beard_preset"].kink_amplitude = facial_kink_amplitude
            bpy.data.particles["beard_preset"].kink_frequency = facial_kink_frequency
            bpy.data.particles["beard_preset"].roughness_1 = roughness_1
            bpy.data.particles["beard_preset"].roughness_2 = roughness_2
            bpy.data.particles["beard_preset"].roughness_2_threshold = roughness_2_threshold
            bpy.context.object.particle_systems["Beard"].cloth.settings.bending_stiffness = facial_hair_bending_stiffness
            
        if has_mustache ==1:
#            mustache_hair_length = facial_hair_length
            bpy.data.particles["mustache_preset"].hair_length = mustache_hair_length
            bpy.data.particles["mustache_preset"].kink = kink_type
            
            bpy.data.particles["mustache_preset"].kink_amplitude = facial_kink_amplitude
            bpy.data.particles["mustache_preset"].kink_frequency = facial_kink_frequency
            bpy.data.particles["mustache_preset"].roughness_1 = roughness_1
            bpy.data.particles["mustache_preset"].roughness_2 = roughness_2
            bpy.data.particles["mustache_preset"].roughness_2_threshold = roughness_2_threshold
            bpy.context.object.particle_systems["Mustache"].cloth.settings.bending_stiffness = facial_hair_bending_stiffness
            
        bpy.ops.object.select_all(action='DESELECT')


    if randomstyle == 2:

        facial_hair_length = random.uniform(0.02, 0.0015)
        if facial_hair_length >= 0.007:
            mustache_hair_length = facial_hair_length/2
        else:
            mustache_hair_length = facial_hair_length/1.1
            
        print(mustache_hair_length)
        print(facial_hair_length)
        
        human.select = True
        bpy.context.scene.objects.active = human
        
        facial_kink_amplitude = random.uniform(0.05, 0.2)
        facial_kink_frequency = random.uniform(0.01, 0.1)
        roughness_1 = random.uniform(0, 0.015)
        roughness_2 = random.uniform(0, 0.015)
        roughness_2_threshold = random.uniform(0, 1)
        facial_hair_bending_stiffness = random.uniform(0.3, 0.6)
        kink_type = 'WAVE'
        
        if has_beard == 1:
            bpy.data.particles["beard_preset"].hair_length = facial_hair_length    
            bpy.data.particles["beard_preset"].kink = kink_type
            
            bpy.data.particles["beard_preset"].kink_amplitude = facial_kink_amplitude
            bpy.data.particles["beard_preset"].kink_frequency = facial_kink_frequency
            bpy.data.particles["beard_preset"].roughness_1 = roughness_1
            bpy.data.particles["beard_preset"].roughness_2 = roughness_2
            bpy.data.particles["beard_preset"].roughness_2_threshold = roughness_2_threshold
            bpy.context.object.particle_systems["Beard"].cloth.settings.bending_stiffness = facial_hair_bending_stiffness
            
        if has_mustache ==1:
#            mustache_hair_length = facial_hair_length
            bpy.data.particles["mustache_preset"].hair_length = mustache_hair_length
            bpy.data.particles["mustache_preset"].kink = kink_type
            
            bpy.data.particles["mustache_preset"].kink_amplitude = facial_kink_amplitude
            bpy.data.particles["mustache_preset"].kink_frequency = facial_kink_frequency
            bpy.data.particles["mustache_preset"].roughness_1 = roughness_1
            bpy.data.particles["mustache_preset"].roughness_2 = roughness_2
            bpy.data.particles["mustache_preset"].roughness_2_threshold = roughness_2_threshold
            bpy.context.object.particle_systems["Mustache"].cloth.settings.bending_stiffness = facial_hair_bending_stiffness

        bpy.ops.object.select_all(action='DESELECT')

    if randomstyle == 3:
        facial_hair_length = random.uniform(0.02, 0.0015)

        if facial_hair_length >= 0.007:
            mustache_hair_length = facial_hair_length/2
        else:
            mustache_hair_length = facial_hair_length/1.1

        print(mustache_hair_length)
        print(facial_hair_length)

        if has_beard == 1:
            bpy.data.particles["beard_preset"].hair_length = facial_hair_length
            bpy.data.particles["beard_preset"].kink = 'NO'

        if has_mustache == 1:
            bpy.data.particles["mustache_preset"].hair_length = facial_hair_length
            bpy.data.particles["mustache_preset"].kink = 'NO'

        bpy.ops.object.select_all(action='DESELECT')