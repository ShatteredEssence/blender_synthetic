import sys
import bpy
import json
import random
import bmesh
import logging
import root_logger
import numpy as np
import mathutils
from pathlib import Path
from config import cfg
from utils import blender_utils, landmark_utils, materials, enviroment, path_utils
from bpy_extras.object_utils import world_to_camera_view
from characters.blender_character import Human
from characters.utils import export
from characters.utils import animation as blender_character_anumation
from cameras.blender_camera import BlenderCamera
from environment.blender_environment import BlenderEnvironment

MODULE_NAME = 'hand_generator'
module_logger = logging.getLogger(MODULE_NAME)
data_path = Path(cfg['Paths']['data_path'])

ik_conroller = bpy.data.objects["ik_controller"]
elbow = ik_conroller.pose.bones['elbow']
hand_ik = ik_conroller.pose.bones['hand_ik']
camera_rotator = bpy.data.objects['camera_rotator']

start_frame = 0
end_frame = start_frame + 10

def randomise_ring_material():
    material = bpy.data.materials['ring_material']
    nodes = material.node_tree.nodes
    principled = nodes.get('Principled BSDF')

    gold = (0.710098, 0.546053, 0.191534, 1)
    silver = (0.710098, 0.681323, 0.681323, 1)
    metals = (gold, silver)
    metal = random.choice(metals)

    principled.inputs[0].default_value = metal
    principled.inputs[0].default_value = random.uniform(0, 0.5)

def add_ring(armature, name, gender):
    prefix = 'male' if gender == 1 else 'female'
    name = prefix + '_' + name
    helper = bpy.data.objects['finger_helper']
    ring = bpy.data.objects[name]
    ring.hide_render = False
    ring.hide_viewport = False

    helper.constraints['Copy Location'].target = armature
    helper.constraints['Copy Location'].subtarget = 'ring01_R'
    helper.constraints['Copy Location'].head_tail = 0.5

    helper.constraints['Copy Rotation'].target = armature
    helper.constraints['Copy Rotation'].subtarget = 'ring01_R'

    return ring

def hide_ring(name, gender):
    prefix = 'male' if gender == 1 else 'female'
    name = prefix + '_' + name
    ring = bpy.data.objects[name]
    ring.hide_render = True
    ring.hide_viewport = True
    return ring

def load_hand_indices(gender):
    prefix = 'man' if gender == 1 else 'woman'
    filename = 'json/' + prefix + '_hand_indices.json'
    indices = []
    with open(data_path / filename, 'r') as f:
        indices = json.loads(f.read())
    return indices

def dump_submesh(object, indices, filepath):
    bpy.ops.object.select_all(action='DESELECT')
    #object.select_set(action='SELECT')
    blender_utils.select(object)
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.duplicate()
    copy = bpy.context.active_object
    
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_mode(type='VERT')
    me = copy.data
    bm = bmesh.from_edit_mesh(me)

    vertices = [e for e in bm.verts]

    for vert in vertices:
        if vert.index in indices:
            vert.select = True
        else:
            vert.select = False

    bmesh.update_edit_mesh(me, True)
    bpy.ops.mesh.select_all(action='INVERT')
    bpy.ops.mesh.delete(type='VERT')
    
    bpy.ops.object.mode_set(mode='OBJECT')
    
    bpy.ops.export_scene.obj(filepath=filepath, use_selection=True, use_materials=False, keep_vertex_order=True)
    
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.delete(use_global=True)
    bpy.ops.object.select_all(action='DESELECT')

def remove_inverted_verts(object, indices):
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    #object.select_set(action='SELECT')
    blender_utils.select(object)
    blender_utils.set_active(object)
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_mode(type='VERT')

    obj = object
    me = obj.data
    bm = bmesh.from_edit_mesh(me)

    vertices = [e for e in bm.verts]
    oa = bpy.context.active_object

    for vert in vertices:
        if vert.index in indices:
            vert.select = True
        else:
            vert.select = False

    bmesh.update_edit_mesh(me, True)

    #inverse, delete, exit
    bpy.ops.mesh.select_all(action='INVERT')
    bpy.ops.mesh.delete(type='VERT')
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')

def init_camera(armature):
    camera_holder = bpy.data.objects['camera_holder']
    camera_holder.constraints["Copy Location"].target = armature
    camera_holder.constraints["Copy Location"].subtarget = "lowerarm_R"
    camera_holder.constraints["Copy Location"].head_tail = random.uniform(0.5, 1)
    camera_target = bpy.data.objects['cam_target']
    camera_target.constraints["Copy Location"].target = armature
    camera_target.constraints["Copy Location"].subtarget = "middle01_R"
    camera_target.constraints["Copy Location"].head_tail = 0

def update_animation():
    bpy.context.scene.frame_set(start_frame + 1)
    bpy.context.scene.frame_set(start_frame)

def run(race, gender, path):
    blender_utils.makedirs(path)
    # blender_utils.makedirs(path / 'mesh')
    blender_utils.gc()

    human_config = {
        'gender': gender,
        'race':  race,
        'has_hair': False,
        'has_wrinkles': False,
        'randomize_lips': False,
        'randomize_brows': False,
        'has_eyebags': False,
    }
    human = Human(human_config)
    human.set_uvset('hands', 'hands_uv', 'UVMap.003')
    human.set_texture(str(human.hand_texture[0]), 'hands', 'hands_diff')
    human.set_texture(str(human.hand_texture[1]), 'hands', 'hands_spec')
    human.set_texture(str(human.hand_texture[2]), 'hands', 'hands_bump')
    human.set_character_material('hands')
    human.randomise_initial_character()

    #clear modifiers
    human.remove_modifier('mbastlab_corrective_modifier')
    human.remove_modifier('mbastlab_subdvision')
    human.remove_modifier('mbastlab_displacement')

    upperarm = human.armature.pose.bones['upperarm_R']
    lowerarm = human.armature.pose.bones['lowerarm_R']
    hand = human.armature.pose.bones['hand_R']

    #creating hand copy location constrains
    # right = human.armature.pose.bones['IK_control_a_R']
    # left = human.armature.pose.bones['IK_control_a_L']
    #
    # right.constraints.new('COPY_LOCATION')
    # right.constraints["Copy Location"].target = bpy.data.objects["right_hand_helper"]
    #
    # left.constraints.new('COPY_LOCATION')
    # left.constraints["Copy Location"].target = bpy.data.objects["left_hand_helper"]

    environment = BlenderEnvironment()
    white_sphere = bpy.data.objects['white_sphere']
    black_sphere = bpy.data.objects['black_sphere']

    ring_name = 'wedding_ring'
    # has_ring = random.randint(0, 1)
    has_ring = 0

    if has_ring == 1:
        ring = add_ring(human.armature, ring_name, gender)

    camera = BlenderCamera(existing_camera=bpy.data.objects["Camera"])

    bpy.context.scene.render.resolution_x = 1024
    bpy.context.scene.render.resolution_y = 1024
    bpy.context.scene.render.resolution_percentage = 100
    bpy.context.scene.render.image_settings.color_mode = 'RGB'

    #positoin controll rig bones!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    elbow.location = [upperarm.tail[0], (upperarm.tail[1] + 0.2), upperarm.tail[2]]
    hand_ik.location = hand.head

    #create constraints
    lowerarm.constraints.new('IK')
    lowerarm.constraints['IK'].target = ik_conroller
    lowerarm.constraints['IK'].subtarget = 'hand_ik'
    lowerarm.constraints['IK'].pole_target = ik_conroller
    lowerarm.constraints['IK'].pole_subtarget = 'elbow'
    lowerarm.constraints['IK'].chain_count = 2
    lowerarm.constraints['IK'].pole_angle = -1.5708

    #create dummy animation
    hand_safe_area_l_t = bpy.data.objects['hand_safe_area_l_t']
    hand_safe_area_r_b = bpy.data.objects['hand_safe_area_r_b']

    hand_limit_01 = hand_safe_area_l_t.matrix_world.decompose()
    hand_limit_02 = hand_safe_area_r_b.matrix_world.decompose()

    elbow.location[2] = random.uniform(1.5, 1.6)
    hand_ik.location = random.uniform(hand_limit_01[0][0], hand_limit_02[0][0]), \
                       random.uniform(hand_limit_01[0][1], hand_limit_02[0][1]), \
                       random.uniform(hand_limit_01[0][2], hand_limit_02[0][2])

    #hotkeys
    hand_ik.keyframe_insert(data_path='location', frame=start_frame)
    elbow.keyframe_insert(data_path='location', frame=start_frame)
    #pose
    elbow.location[2] = random.uniform(1.5, 1.6)
    hand_ik.location = random.uniform(hand_limit_01[0][0], hand_limit_02[0][0]),\
                       random.uniform(hand_limit_01[0][1], hand_limit_02[0][1]), \
                       random.uniform(hand_limit_01[0][2], hand_limit_02[0][2])

    #hotkeys
    hand_ik.keyframe_insert(data_path='location', frame=end_frame)
    elbow.keyframe_insert(data_path='location', frame=end_frame)

    blender_utils.select(human.armature)
    blender_utils.set_active(human.armature)
    blender_utils.set_pose_mode()

    # pmode = random.choice([0, 1])
    pmode = 0
    if pmode:
        ### load hand mocap BVH animation
        mocap_folder = data_path / 'animations/mocap_data'
        mocap_file = random.choice((list(mocap_folder.glob('*.bvh'))))
        blender_character_anumation.load_bvh_animation(str(mocap_file))
    else:
        ### load fingers pose from JSON file
        pose_folder = data_path / 'animations/poses/right'
        mocap_file = random.choice((list(pose_folder.glob('*.json'))))
        module_logger.info('loading json pose {}'.format(mocap_file))
        with mocap_file.open() as f:
            pose_data = json.load(f)
            blender_character_anumation.set_finger_pose(pose_data)

    ### keying hand animation at START FRAME
    for bone_name in human.arm_bones + human.finger_bones:
        bone = bpy.context.object.pose.bones.get(bone_name)
        bone.keyframe_insert(data_path='rotation_quaternion', frame=start_frame)
        bone.keyframe_insert(data_path='rotation_euler', frame=start_frame)

    ### rotating and then keying hand animation at END FRAME
    hand_r_bone = bpy.context.object.pose.bones.get('hand_R')
    current_euler = hand_r_bone.rotation_euler
    new_euler = current_euler + np.array([0, random.uniform(0.5, 1), 0])
    new_euler = mathutils.Euler((new_euler[0], new_euler[1], new_euler[2]), 'XYZ')
    hand_r_bone.rotation_quaternion = new_euler.to_quaternion()
    hand_r_bone.keyframe_insert(data_path='rotation_quaternion', frame=end_frame)
    hand_r_bone.rotation_euler = new_euler
    hand_r_bone.keyframe_insert(data_path='rotation_euler', frame=end_frame)

    blender_utils.set_object_mode()

    #human.armature.select_set(action='SELECT')
    blender_utils.select(human.armature)
    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='TOGGLE')
    bpy.ops.pose.select_all(action='SELECT')
    bpy.ops.nla.bake(
        frame_start=start_frame, frame_end=end_frame, step=10, only_selected=True, visual_keying=True,
        clear_constraints=True, clear_parents=True, use_current_action=False, bake_types={'POSE'}
    )

    remove_inverted_verts(human.character, load_hand_indices(gender))

    update_animation()

    blender_utils.render_sequence(path, 'frame', 'RGB', start_frame, end_frame, -start_frame)

    human.set_character_material('black')
    blender_utils.unhide(white_sphere)
    blender_utils.render_sequence(path, 'background_mask', 'BW', start_frame, end_frame, -start_frame)
    blender_utils.hide(white_sphere)
    blender_utils.unhide(black_sphere)

    human.set_character_material('hand_mask_male') if gender else human.set_character_material('hand_mask_female')
    blender_utils.render_sequence(path, 'hand_mask', 'BW', start_frame, end_frame, -start_frame)

    human.set_character_material('arm_mask_male') if gender else human.set_character_material('arm_mask_female')
    blender_utils.render_sequence(path, 'arm_mask', 'BW', start_frame, end_frame, -start_frame)

    # horiz_angle = 10
    # vert_angle = 10
    # RMin = 0.13
    # RMax = 0.23
    # origin = bpy.data.objects['Camera']
    #
    # cases = ['create_hands_runner_animation_00', 'create_hands_runner_animation_01', 'create_hands_runner_animation_02']
    # current_case = random.choice(cases)
    #
    # if current_case == 'create_hands_runner_animation_00':
    #     blender_character_anumation.create_hands_runner_animation_00(
    #         start_frame, end_frame, human.armature, horiz_angle, vert_angle, RMin, RMax, origin)
    # if current_case == 'create_hands_runner_animation_01':
    #     blender_character_anumation.create_hands_runner_animation_01(
    #         start_frame, end_frame, human.armature, horiz_angle, vert_angle, RMin, RMax, origin)
    # if current_case == 'create_hands_runner_animation_02':
    #     blender_character_anumation.create_hands_runner_animation_02(
    #         start_frame, end_frame, human.armature, horiz_angle, vert_angle, RMin, RMax, origin)

    for frame in range(start_frame, end_frame):
        bpy.context.scene.frame_set(frame)
        human.dump_properties(path, frame-start_frame, mocap_file=str(mocap_file))
        camera.dump_properties(path, frame-start_frame)
        environment.dump_properties(path, frame-start_frame)

        # export.dump_2d_and_3d_coords(path, human, camera, frame-start_frame)

    """
    # blender_utils.select(human.character)
    # blender_utils.set_active(human.character)
    # for frame in range(start_frame, end_frame):
    #     bpy.context.scene.frame_set(frame)
    #     blender_utils.export_obj_active(str(path / 'character_{}.obj'.format(frame)))
    # blender_utils.deselect(human.character)

    human.set_character_material('black')
    if has_ring == 1:
        materials.set_character_material(ring, 'White')
    blender_utils.render_sequence(path, 'accessories_mask', 'BW', start_frame, end_frame)
    wtf? )
    """

    blender_utils.set_object_mode()
    blender_utils.deselect_all()

    for frame in range(start_frame, end_frame):
        bpy.context.scene.frame_set(frame)
        # blender_utils.update_morphs()
        export.dump_2d_and_3d_coords(path, human, camera, frame)

    blender_utils.delete_object(human.armature)
    blender_utils.delete_object(human.character)
    blender_utils.hide(black_sphere)
    hide_ring(ring_name, gender)

    blender_utils.gc()


def job(sequence_path=None):
    sequence_path = sequence_path or path_utils.build_seq_path(MODULE_NAME)

    race = random.choice(['f_ca01', 'm_ca01'])
    gender = 0 if race.startswith('f') else 1

    run(race, gender, Path(sequence_path))

def run_from_blender(iterations):
    for i in range(iterations):
        job()


if __name__ == "__main__":
    if '--sequence_path' in sys.argv:
        index = sys.argv.index('--sequence_path') + 1
        sequence_path = sys.argv[index]
    else:
        sequence_path = None

    try:
        job(sequence_path)
        sys.exit(0)
    except Exception:
        sys.exit(1)
