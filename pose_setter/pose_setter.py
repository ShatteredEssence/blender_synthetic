import bpy
import sys
import time
import random
import json
import logging
import root_logger
from math import pi
from config import cfg
from pathlib import Path
from utils import landmark_utils as landmarks_helper
from utils import blender_utils as utils
from utils import poly_hair_utils as poly_hair
from utils import materials, enviroment, path_utils
from characters import BLENDER_CHARACTER_FULL_CONFIG, HUMAN_INIT_CONFIG
from characters.blender_character import Human
from characters.utils import randomizer, facebox
from characters.utils import animation as blender_character_anumation
from accessories.glasses import Glasses
from accessories.shirts import Shirt
from environment import ENVIRONMENT_CONFIG
from environment.blender_environment import BlenderEnvironment
from cameras.blender_camera import BlenderCamera

MODULE_NAME = 'pose_setter'

module_logger = logging.getLogger(MODULE_NAME)
module_logger.setLevel(logging.DEBUG)

data_path = Path(cfg['Paths']['data_path'])

resolution_x = 1024
resolution_y = 1024
prewarm_frame_count = 30
render_frame_count = 2

frame_start = prewarm_frame_count
frame_stop = prewarm_frame_count + render_frame_count

OCCLUSION = True

# def render_mask_sequence(character, path, mask_name, material_name, material_slot):
#     start_time = time.time()
#     module_logger.info('START %s. Measuring time', path)
#     #outer_eye_material = bpy.data.materials.get('outer_eyes')
#     #transparent_material = bpy.data.materials.get('transparent')
#     #materials.change_character_material(character, material_name)
#     character.material_slots[material_slot].material = bpy.data.materials.get(material_name)
#     #materials.set_vertex_group_material(character, 'outer_eyes', outer_eye_material)
#     #materials.set_vertex_group_material(character, 'eyelashes', transparent_material)
#     #materials.add_hair_material(character)
#     utils.render_sequence(path, mask_name, 'BW', frame_start, frame_stop, -prewarm_frame_count)
#
#     stop_time = time.time()
#     time_spent = stop_time - start_time
#     module_logger.info('STOP. Time spent %s', time_spent)
#
# def dump_additional_meta(path, frames_meta):
#     utils.makedirs(path / 'additional_meta')
#     for frame in range(0, render_frame_count):
#         with open((path / 'additional_meta' / 'frame_{:07}.json'.format(frame)), 'w') as meta_file:
#             json.dump(frames_meta[frame], meta_file, indent=4)

def render_sequences_data(camera, environment, human, path, landmarks, landmark_classes, facebox_indices, frame_start, frame_stop, frames_meta):
    for frame in range(frame_start, frame_stop):
        bpy.context.scene.frame_set(frame)
        utils.update_morphs()
        frame_meta = frames_meta[frame-prewarm_frame_count]
        frame_meta['tongue'] = 1 if human.is_tongue_out() else 0
        landmarks_helper.write_landmarks(camera, frame_meta, path, human.character, landmarks, landmark_classes,
                                         frame - prewarm_frame_count, human.vertex_groups_indices['outer_eyes'])
        landmarks_helper.write_barimetric_landmarks(camera, path, human, frame - prewarm_frame_count)
        facebox.write_facebox_indices(camera, path, human.character, facebox_indices,
                                             frame - prewarm_frame_count, resolution_x, resolution_y)
        human.dump_properties(path, frame - prewarm_frame_count)
        human.dump_pose_data(path, frame - prewarm_frame_count)
        environment.dump_properties(path, frame-prewarm_frame_count)
        camera.dump_properties(path, frame-prewarm_frame_count)

def init_frames_meta():
    frames_meta = []
    for frame in range(0, render_frame_count):
        frame_meta = dict()
        frame_meta['tongue'] = dict()
        if OCCLUSION:
            frame_meta['occlusion'] = dict()
        frames_meta.append(frame_meta)
    return frames_meta

def run(gender, race, character_config, path, landmarks, landmark_classes, facebox_indices, pose_file):
    bpy.context.scene.render.resolution_x = resolution_x
    bpy.context.scene.render.resolution_y = resolution_y
    bpy.context.scene.frame_end = prewarm_frame_count + render_frame_count

    utils.makedirs(path)
    utils.gc()

    human_config = {
        'gender': gender,
        'race': race,
        'has_hair': True,
        'has_wrinkles': not bool(gender),
    }
    human = Human(human_config=human_config, blender_character_config=character_config)
    human.remove_modifier('mbastlab_corrective_modifier')
    human.remove_modifier('mbastlab_subdvision')
    human.remove_modifier('mbastlab_displacement')

    environment = BlenderEnvironment()
    camera = BlenderCamera(existing_camera=bpy.data.objects['camera_free'])
    camera.location = (0, -1.75, 1.5)

    utils.select(human.armature)
    utils.set_active(human.armature)
    utils.set_pose_mode()

    human.remove_bone_constrains(human.all_bones)

    with open(pose_file) as f:
        data = json.load(f)
    human.set_pose(data)

    for bone_name in human.all_bones:
        bone = bpy.context.object.pose.bones.get(bone_name)
        bone.keyframe_insert(data_path='rotation_quaternion', frame=frame_start)
        bone.keyframe_insert(data_path='rotation_euler', frame=frame_start)

    utils.render_sequence(path, 'frame', 'RGB', frame_start, frame_stop, -prewarm_frame_count)
    frame_meta = init_frames_meta()
    render_sequences_data(camera, environment, human, path, landmarks, landmark_classes,
                          facebox_indices, frame_start, frame_stop, frame_meta)

    utils.deselect_pose_all()
    utils.set_object_mode()
    utils.deselect_all()


def job(race=None, sequence_path=None, character_config=None, pose_file=None):
    """
    Actual quantum of work. When calling from Blender
    UI run_from_blender will iterate over this function.
    """
    landmarks_list = [landmarks_helper.get_landmarks_86('woman'), landmarks_helper.get_landmarks_86('man')]
    landmark_classes_list = [landmarks_helper.get_landmark_classes('woman'), landmarks_helper.get_landmark_classes('man')]
    facebox_list = [facebox.get_facebox_indices('woman'), facebox.get_facebox_indices('man')]
    sequence_path = sequence_path or path_utils.build_seq_path(MODULE_NAME)
    race = race or random.choice(('f_ca01', 'm_ca01', 'm_af01', 'm_as01'))
    gender = 0 if race.startswith('f') else 1

    start_time = time.time()
    module_logger.info('[START] Iteration %s. Measuring time', sequence_path)
    run(gender, race, character_config, Path(sequence_path), landmarks_list[gender],
        landmark_classes_list[gender], facebox_list[gender], pose_file)
    stop_time = time.time()
    time_spent = stop_time - start_time
    module_logger.info('[STOP] Iteration %s. Time spent %s', sequence_path, time_spent)

def run_from_blender(iterations):
    """
    Function for calling this module from Blender UI
    and iterating over it till the end of time.
    """
    for i in range(iterations):
        job()


if __name__ == "__main__":
    if '--sequence_path' in sys.argv:
        index = sys.argv.index('--sequence_path') + 1
        sequence_path = sys.argv[index]
    else:
        sequence_path = None

    if '--blender_char_props' in sys.argv:
        index = sys.argv.index('--blender_char_props') + 1
        with open(sys.argv[index]) as json_data:
            blender_char_props = json.load(json_data)
    else:
        blender_char_props = None

    if '--race' in sys.argv:
        index = sys.argv.index('--race') + 1
        race = sys.argv[index]
    else:
        race = None

    if '--pose_file' in sys.argv:
        index = sys.argv.index('--pose_file') + 1
        pose_file = sys.argv[index]
    else:
        pose_file = None

    try:
        job(race, sequence_path, blender_char_props, pose_file)
        sys.exit(0)
    except Exception:
        sys.exit(1)
